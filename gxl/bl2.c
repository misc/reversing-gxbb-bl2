typedef unsigned char   undefined;

typedef unsigned char    bool;
typedef unsigned char    byte;
typedef unsigned char    uchar;
typedef unsigned int    uint;
typedef unsigned long    ulong;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined8;
typedef unsigned short    ushort;
typedef struct ddr_timing ddr_timing, *Pddr_timing;

typedef struct ddr_timing ddr_timing_t;

struct ddr_timing {
    uchar identifier;
    uchar cfg_ddr_rtp;
    uchar cfg_ddr_wtr;
    uchar cfg_ddr_rp;
    uchar cfg_ddr_rcd;
    uchar cfg_ddr_ras;
    uchar cfg_ddr_rrd;
    uchar cfg_ddr_rc;
    uchar cfg_ddr_mrd;
    uchar cfg_ddr_mod;
    uchar cfg_ddr_faw;
    uchar cfg_ddr_wlmrd;
    uchar cfg_ddr_wlo;
    uchar cfg_ddr_xp;
    ushort cfg_ddr_rfc;
    ushort cfg_ddr_xs;
    ushort cfg_ddr_dllk;
    uchar cfg_ddr_cke;
    uchar cfg_ddr_rtodt;
    uchar cfg_ddr_rtw;
    uchar cfg_ddr_refi;
    uchar cfg_ddr_refi_mddr3;
    uchar cfg_ddr_cl;
    uchar cfg_ddr_wr;
    uchar cfg_ddr_cwl;
    uchar cfg_ddr_al;
    uchar cfg_ddr_dqs;
    uchar cfg_ddr_cksre;
    uchar cfg_ddr_cksrx;
    uchar cfg_ddr_zqcs;
    uchar cfg_ddr_xpdll;
    ushort cfg_ddr_exsr;
    ushort cfg_ddr_zqcl;
    ushort cfg_ddr_zqcsi;
    uchar cfg_ddr_tccdl;
    uchar cfg_ddr_tdqsck;
    uchar cfg_ddr_tdqsckmax;
    uchar rsv_char;
    uint rsv_int;
};

typedef struct ddr_set ddr_set, *Pddr_set;

typedef struct ddr_set ddr_set_t;

struct ddr_set {
    uchar ddr_channel_set;
    uchar ddr_type;
    uchar ddr_2t_mode;
    uchar ddr_full_test;
    uchar ddr_size_detect;
    uchar ddr_drv;
    uchar ddr_odt;
    uchar ddr_timing_ind;
    ushort ddr_size;
    ushort ddr_clk;
    uint ddr_base_addr;
    uint ddr_start_offset;
    uint ddr_pll_ctrl;
    uint ddr_dmc_ctrl;
    uint ddr0_addrmap[5];
    uint ddr1_addrmap[5];
    uint t_pub_ptr[5];
    ushort t_pub_mr[8];
    uint t_pub_odtcr;
    uint t_pub_dtpr[8];
    uint t_pub_pgcr0;
    uint t_pub_pgcr1;
    uint t_pub_pgcr2;
    uint t_pub_pgcr3;
    uint t_pub_dxccr;
    uint t_pub_dtcr0;
    uint t_pub_dtcr1;
    uint t_pub_aciocr[5];
    uint t_pub_dx0gcr[3];
    uint t_pub_dx1gcr[3];
    uint t_pub_dx2gcr[3];
    uint t_pub_dx3gcr[3];
    uint t_pub_dcr;
    uint t_pub_dtar;
    uint t_pub_dsgcr;
    uint t_pub_zq0pr;
    uint t_pub_zq1pr;
    uint t_pub_zq2pr;
    uint t_pub_zq3pr;
    uint t_pub_vtcr1;
    ushort t_pctl0_1us_pck;
    ushort t_pctl0_100ns_pck;
    ushort t_pctl0_init_us;
    ushort t_pctl0_rsth_us;
    uint t_pctl0_mcfg;
    uint t_pctl0_mcfg1;
    ushort t_pctl0_scfg;
    ushort t_pctl0_sctl;
    uint t_pctl0_ppcfg;
    ushort t_pctl0_dfistcfg0;
    ushort t_pctl0_dfistcfg1;
    ushort t_pctl0_dfitctrldelay;
    ushort t_pctl0_dfitphywrdata;
    ushort t_pctl0_dfitphywrlta;
    ushort t_pctl0_dfitrddataen;
    ushort t_pctl0_dfitphyrdlat;
    ushort t_pctl0_dfitdramclkdis;
    ushort t_pctl0_dfitdramclken;
    ushort t_pctl0_dfitphyupdtype0;
    ushort t_pctl0_dfitphyupdtype1;
    ushort t_pctl0_dfitctrlupdmin;
    ushort t_pctl0_dfitctrlupdmax;
    ushort t_pctl0_dfiupdcfg;
    ushort t_pctl0_cmdtstaten;
    ushort t_ddr_align;
    uint t_pctl0_dfiodtcfg;
    uint t_pctl0_dfiodtcfg1;
    uint t_pctl0_dfilpcfg0;
    uint t_pub_acbdlr0;
    uint t_pub_aclcdlr;
    uint ddr_func;
    uchar wr_adj_per[6];
    uchar rd_adj_per[6];
    ushort ddr4_clk;
    uchar ddr4_drv;
    uchar ddr4_odt;
    uint t_pub_acbdlr3;
    ushort t_pub_soc_vref_dram_vref;
    ushort t_pub_soc_vref_dram_vref_rank1;
    uchar wr_adj_per_rank1[6];
    uchar rd_adj_per_rank1[6];
};

typedef struct pll_set pll_set, *Ppll_set;

struct pll_set {
    ushort cpu_clk;
    ushort pxp;
    uint spi_ctrl;
    ushort vddee;
    ushort vcck;
    uchar szPad[4];
    ulong lCustomerID;
    ushort debug_mode;
    ushort ddr_clk_debug;
    ushort cpu_clk_debug;
    ushort rsv_s1;
    uint ddr_pll_ssc;
    uint rsv_i1;
    ulong rsv_l3;
    ulong rsv_l4;
    ulong rsv_l5;
};

typedef struct pll_set pll_set_t;

typedef struct ep_info ep_info, *Pep_info;

typedef struct param_header param_header, *Pparam_header;

typedef ulong uintptr_t;

typedef uint uint32_t;

typedef struct aapcs64_params aapcs64_params, *Paapcs64_params;

typedef uchar uint8_t;

typedef ushort uint16_t;

typedef ulong uint64_t;

struct aapcs64_params {
    uint64_t arg0;
    uint64_t arg1;
    uint64_t arg2;
    uint64_t arg3;
    uint64_t arg4;
    uint64_t arg5;
    uint64_t arg6;
    uint64_t arg7;
};

struct param_header {
    uint8_t type;
    uint8_t version;
    uint16_t size;
    uint32_t attr;
};

struct ep_info {
    struct param_header h;
    uintptr_t pc;
    uint32_t spsr;
    struct aapcs64_params args;
};

typedef struct image_info image_info, *Pimage_info;

struct image_info {
    struct param_header h;
    uintptr_t img_base;
    uint32_t img_size;
    uint32_t img_max_size;
};

typedef struct astruct astruct, *Pastruct;

struct astruct {
    undefined field0_0x0;
    undefined field1_0x1;
    undefined field2_0x2;
    undefined field3_0x3;
    undefined field4_0x4;
    undefined field5_0x5;
    undefined field6_0x6;
    undefined field7_0x7;
    undefined field8_0x8;
    undefined field9_0x9;
    undefined field10_0xa;
    undefined field11_0xb;
    undefined field12_0xc;
    undefined field13_0xd;
    undefined field14_0xe;
    undefined field15_0xf;
    undefined1 src; /* Created by retype action */
    undefined field17_0x11;
    undefined field18_0x12;
    undefined field19_0x13;
    undefined field20_0x14;
    undefined field21_0x15;
    undefined field22_0x16;
    undefined field23_0x17;
    undefined1 size; /* Created by retype action */
    undefined field25_0x19;
    undefined field26_0x1a;
    undefined field27_0x1b;
    undefined field28_0x1c;
    undefined field29_0x1d;
    undefined field30_0x1e;
    undefined field31_0x1f;
    undefined field32_0x20;
    undefined field33_0x21;
    undefined field34_0x22;
    undefined field35_0x23;
    undefined field36_0x24;
    undefined field37_0x25;
    undefined field38_0x26;
    undefined field39_0x27;
};

typedef struct sd_emmc_global_regs sd_emmc_global_regs, *Psd_emmc_global_regs;

struct sd_emmc_global_regs {
    undefined field0_0x0;
    undefined field1_0x1;
    undefined field2_0x2;
    undefined field3_0x3;
    undefined field4_0x4;
    undefined field5_0x5;
    undefined field6_0x6;
    undefined field7_0x7;
    undefined field8_0x8;
    undefined field9_0x9;
    undefined field10_0xa;
    undefined field11_0xb;
    undefined field12_0xc;
    undefined field13_0xd;
    undefined field14_0xe;
    undefined field15_0xf;
    undefined field16_0x10;
    undefined field17_0x11;
    undefined field18_0x12;
    undefined field19_0x13;
    undefined field20_0x14;
    undefined field21_0x15;
    undefined field22_0x16;
    undefined field23_0x17;
    undefined field24_0x18;
    undefined field25_0x19;
    undefined field26_0x1a;
    undefined field27_0x1b;
    undefined field28_0x1c;
    undefined field29_0x1d;
    undefined field30_0x1e;
    undefined field31_0x1f;
    undefined field32_0x20;
    undefined field33_0x21;
    undefined field34_0x22;
    undefined field35_0x23;
    undefined field36_0x24;
    undefined field37_0x25;
    undefined field38_0x26;
    undefined field39_0x27;
    undefined field40_0x28;
    undefined field41_0x29;
    undefined field42_0x2a;
    undefined field43_0x2b;
    undefined field44_0x2c;
    undefined field45_0x2d;
    undefined field46_0x2e;
    undefined field47_0x2f;
    undefined field48_0x30;
    undefined field49_0x31;
    undefined field50_0x32;
    undefined field51_0x33;
    undefined field52_0x34;
    undefined field53_0x35;
    undefined field54_0x36;
    undefined field55_0x37;
    undefined field56_0x38;
    undefined field57_0x39;
    undefined field58_0x3a;
    undefined field59_0x3b;
    undefined field60_0x3c;
    undefined field61_0x3d;
    undefined field62_0x3e;
    undefined field63_0x3f;
    undefined field64_0x40;
    undefined field65_0x41;
    undefined field66_0x42;
    undefined field67_0x43;
    undefined field68_0x44;
    undefined field69_0x45;
    undefined field70_0x46;
    undefined field71_0x47;
    uint gstatus;
    undefined field73_0x4c;
    undefined field74_0x4d;
    undefined field75_0x4e;
    undefined field76_0x4f;
    uint gcmd_cfg;
    undefined4 gcmd_arg;
    undefined field79_0x58;
    undefined field80_0x59;
    undefined field81_0x5a;
    undefined field82_0x5b;
    undefined4 gcmd_rsp0;
};

typedef struct astruct_1 astruct_1, *Pastruct_1;

struct astruct_1 {
    undefined field0_0x0;
    undefined field1_0x1;
    undefined field2_0x2;
    undefined field3_0x3;
    uint field4_0x4;
    undefined8 field5_0x8;
    undefined4 field6_0x10;
    undefined field7_0x14;
    undefined field8_0x15;
    undefined field9_0x16;
    undefined field10_0x17;
    undefined1 * field11_0x18;
};

typedef struct acs_setting acs_setting, *Pacs_setting;

struct acs_setting {
    char acs_magic[5];
    uchar chip_type;
    ushort version;
    ulong acs_set_length;
    char ddr_magic[5];
    uchar ddr_set_version;
    ushort ddr_set_length;
    ulong ddr_set_addr;
    char ddrt_magic[5];
    uchar ddrt_set_version;
    ushort ddrt_set_length;
    ulong ddrt_set_addr;
    char pll_magic[5];
    uchar pll_set_version;
    ushort pll_set_length;
    ulong pll_set_addr;
};




undefined1 * FUN_d9001068(void)

{
  FUN_d9008f04((long)&DAT_d900a5c0,0,0x1c0);
  DAT_d900a5d0 = &DAT_d900a6d0;
  DAT_d900a5c2 = 0x40;
  DAT_d900a6d2 = 0x58;
  DAT_d900a67a = 0x58;
  DAT_d900a5c0 = 3;
  DAT_d900a5c8 = &DAT_d900a630;
  DAT_d900a5d8 = &DAT_d900a648;
  DAT_d900a5e8 = &DAT_d900a660;
  DAT_d900a5c1 = 1;
  DAT_d900a630 = 2;
  DAT_d900a631 = 1;
  DAT_d900a632 = 0x18;
  DAT_d900a6d0 = 1;
  DAT_d900a6d1 = 1;
  DAT_d900a648 = 2;
  DAT_d900a649 = 1;
  DAT_d900a64a = 0x18;
  DAT_d900a5e0 = &DAT_d900a678;
  DAT_d900a678 = 1;
  DAT_d900a679 = 1;
  DAT_d900a660 = 2;
  DAT_d900a661 = 1;
  DAT_d900a662 = 0x18;
  DAT_d900a5f0 = &DAT_d900a600;
  DAT_d900a600 = 2;
  DAT_d900a601 = 1;
  DAT_d900a5c4 = 0;
  DAT_d900a634 = 0;
  DAT_d900a6d4 = 0;
  DAT_d900a64c = 0;
  DAT_d900a67c = 0;
  DAT_d900a690 = 0;
  DAT_d900a664 = 0;
  DAT_d900a602 = 0x18;
  DAT_d900a619 = 1;
  DAT_d900a604 = 0;
  DAT_d900a5f8 = &DAT_d900a618;
  DAT_d900a618 = 2;
  DAT_d900a61a = 0x18;
  DAT_d900a61c = 0;
  return &DAT_d900a5c0;
}



undefined * FUN_d9001170(void)

{
  DAT_d900a748 = 0xf1e2d3c4b5a6978;
  return &DAT_d900a728;
}



void FUN_d9001194(void)

{
  DAT_d900a580 = &DAT_d9000000;
  DAT_d900a588 = 0x14000;
  DAT_d900a590 = &DAT_d900c000;
  DAT_d900a598 = 0x8000;
  DAT_d900a5a0 = 0xe939e2e8cf8effd;
  DAT_d900a5a8 = 0;
  return;
}



undefined8 storage_init(void)

{
  uint uVar1;
  
  uVar1 = get_boot_device();
  if (uVar1 == 1) {
    sdio_eMMC_prep(1);
  }
  else if (uVar1 == 2) {
    serial_puts(s_NAND_init_d9009299);
    nfio_init();
  }
  return 0;
}



void FUN_d90011e4(void)

{
  FUN_d9009158((ulong)&DAT_d900a5c0,0x1c0);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90011f4(void)

{
  uint uVar1;
  
  _DAT_d9013dfc = _DAT_d904050c;
  if (_DAT_da8345a8 == 0x1111111) {
    if ((_DAT_d904050c == 0x52800022) || (uVar1 = 0xb0000, _DAT_d904050c == -0x6bfffb0f)) {
      uVar1 = 0xa0000;
    }
  }
  else {
    uVar1 = 0xc0000;
    if (_DAT_da8345a8 != 0x1111311) {
      uVar1 = 0xd0000;
    }
  }
  _DAT_d9013df8 = _DAT_c1107d4c << 0x18 | (_DAT_d9013c0c >> 0x18) << 8 | uVar1;
  return;
}



void FUN_d90012a8(undefined8 param_1,long param_2)

{
  *(uint *)(param_2 + 4) = *(uint *)(param_2 + 4) & 0xfffffffe;
  *(undefined4 *)(param_2 + 0x10) = 0x3cd;
  return;
}



void FUN_d90012c0(undefined8 param_1,long param_2)

{
  *(undefined4 *)(param_2 + 0x10) = 0;
  *(uint *)(param_2 + 4) = *(uint *)(param_2 + 4) & 0xfffffffe;
  return;
}



void FUN_d90012d4(undefined8 param_1,long param_2)

{
  int iVar1;
  ulong uVar2;
  
  uVar2 = FUN_d90091f0();
  iVar1 = 2;
  if ((uVar2 >> 8 & 0xf) == 0) {
    iVar1 = 1;
  }
  *(uint *)(param_2 + 0x10) = iVar1 << 2 | 0x3c1;
  *(uint *)(param_2 + 4) = *(uint *)(param_2 + 4) | 1;
  return;
}



void dump_ddr_data(void)

{
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 spi_read(ulong param_1,long param_2,long param_3)

{
  _DAT_c8834528 = 0xffff87ff;
  _DAT_c88344f0 = 0xffff8700;
  _DAT_c88344cc = _DAT_c88344cc & 0xefffffe2 | 0x3c00;
  _DAT_c1108c88 = 0xb000;
  if (plls.pxp == 0) {
    _DAT_c1108c88 = 0xa949;
  }
  _DAT_c1108c88 = _DAT_c1108c88 | 0x2a0000;
  memcpy(param_2,param_1 | 0xcc000000,param_3);
  return 0;
}



uint sdio_mmc_send_cmd(sd_emmc_global_regs *sd_emmc_base_addr,undefined4 *cmd)

{
  uint status;
  
  sd_emmc_base_addr->gcmd_cfg = *cmd;
  sd_emmc_base_addr->gcmd_arg = cmd[1];
  status = 0;
  if ((sd_emmc_base_addr->gcmd_cfg >> 0x1e & 1) != 0) {
    status = sd_emmc_base_addr->gstatus & 0x1fff;
  }
  cmd[3] = sd_emmc_base_addr->gcmd_rsp0;
  return status;
}



uint sdio_read_blocks(long param_1,ulong param_2,uint param_3,undefined8 param_4,long param_5)

{
  uint uVar1;
  uint uVar2;
  undefined4 local_b0;
  undefined4 local_ac;
  uint local_a8;
  undefined4 local_a4;
  uint local_a0;
  undefined4 local_9c;
  undefined4 local_98;
  undefined auStack_30 [16];
  uint local_20;
  undefined8 local_10;
  long local_8;
  
  local_20 = 0;
  local_10 = param_4;
  local_8 = param_5;
  FUN_d9008f04((long)&local_b0,0,0x80);
  uVar1 = local_b0 & 0xc0ffffff;
  local_ac = (int)(param_2 >> 9);
  if (local_8 == 0) {
    local_ac = (int)param_2;
  }
  local_b0._1_1_ = (byte)(uVar1 >> 8);
  if (local_8 == 0) {
    local_b0._1_1_ = local_b0._1_1_ & 0xfd;
  }
  else {
    local_b0._1_1_ = local_b0._1_1_ | 2;
  }
  local_b0._2_1_ = (undefined)(uVar1 >> 0x10);
  local_a4 = SUB84(auStack_30,0);
  local_b0 = CONCAT13((char)(uVar1 >> 0x18),
                      CONCAT12(local_b0._2_1_,
                               (local_b0._1_1_ & 0xfe) << 8 | (ushort)local_10 & 0x1ff)) &
             0xff360fff | 0x92047800;
  local_20 = (uint)((byte)local_20 & 0xfe | 2);
  *(undefined4 *)(param_1 + 0x48) = 0x3fff;
  local_a8 = param_3 & 0xfffffffe;
  *(uint *)(param_1 + 0x50) = local_b0;
  *(uint *)(param_1 + 0x58) = local_a8;
  *(undefined4 *)(param_1 + 0x54) = local_ac;
  do {
  } while ((*(uint *)(param_1 + 0x48) >> 0xd & 1) == 0);
  uVar1 = local_a0 >> 8;
  local_a0 = local_a0 & 0xc0faffff;
  local_a0 = CONCAT22((short)(local_a0 >> 0x10),CONCAT11((char)uVar1,(undefined)local_a0)) |
             0x8c000c00;
  local_20 = local_20 & 3 | (int)((ulong)&local_a0 >> 2) << 2;
  *(undefined4 *)(param_1 + 0x48) = 0x3fff;
  *(uint *)(param_1 + 0x50) = local_a0;
  *(undefined4 *)(param_1 + 0x58) = local_98;
  *(undefined4 *)(param_1 + 0x54) = local_9c;
  do {
    uVar1 = *(uint *)(param_1 + 0x48);
  } while ((uVar1 >> 0xd & 1) == 0);
  uVar2 = (uint)((uVar1 & 0xff) != 0) | (uVar1 >> 8 & 1) << 1 | (uVar1 >> 9 & 1) << 2 |
          (uVar1 >> 10 & 1) << 3 | (uVar1 >> 0xb & 1) << 4;
  if ((uVar1 >> 0xc & 1) == 0) {
    if (uVar2 == 0) {
      return 0;
    }
  }
  else {
    uVar2 = uVar2 | 0x20;
  }
  serial_puts(s_sd_emmc_read_data_error__ret__d9009258);
  serial_put_dec((ulong)uVar2);
  serial_puts(s__d9009869);
  return uVar2;
}



uint sdio_read_data(long sd_or_emmc,ulong src,int dst,long len)

{
  byte bVar1;
  byte bVar2;
  int iVar3;
  uint uVar4;
  uint uVar5;
  ulong uVar6;
  ulong regbase;
  
                    /* Exactly the same function as on GXBB */
  if (sd_or_emmc == 1) {
    regbase = 0x4000;
  }
  else {
    if (sd_or_emmc != 4) {
      serial_puts(s_sd_emmc_boot_device_error_d9009276);
      regbase = 0;
      goto LAB_d900162c;
    }
    regbase = 0x2000;
  }
  regbase = regbase | 0xd0070000;
LAB_d900162c:
                    /* What are these????? */
  bVar1 = DAT_da100244 >> 7;
  bVar2 = DAT_da100245 >> 1;
  uVar6 = len + 0x1ffU >> 9;
  iVar3 = (int)src;
  while( true ) {
    uVar5 = (uint)uVar6;
    uVar4 = uVar5;
    if (0x80 < uVar5) {
      uVar4 = 0x80;
    }
    uVar4 = sdio_read_blocks(regbase,src,(dst - iVar3) + (int)src,(ulong)uVar4,
                             (ulong)((uint)bVar1 | bVar2 & 1));
    if ((uVar4 != 0) || (uVar5 < 0x81)) break;
    src = src + 0x10000;
    uVar6 = (ulong)(uVar5 - 0x80);
  }
  return uVar4;
}



bool hdr_check__notsure(long boot_device,long param_2)

{
  undefined uVar1;
  int iVar2;
  
  sdio_read_data(boot_device,0x200,0x1800200,0x200);
  iVar2 = memcmp__notsure(param_2,0x1800200,0x200);
  uVar1 = (undefined)iVar2;
  if (iVar2 == 0) {
    serial_puts(s__s_d9009295);
  }
  else {
    serial_puts(s__f_d9009291);
    uVar1 = 1;
  }
  return (bool)uVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */
/* Doesn't actually seem to init the eMMC...as it looks like it reads the header? */

void sdio_eMMC_prep(long param_1)

{
  uint uVar1;
  bool bVar2;
  undefined4 local_20;
  uint local_1c;
  undefined4 local_10;
  
  if (param_1 == 1) {
    sdio_read_data(1,0x200,0x1800000,0x200);
    local_1c = 0x3b70200;
    local_20 = 0x6007400;
    sdio_mmc_send_cmd((sd_emmc_global_regs *)&DAT_d0074000,&local_20);
    uVar1 = _DAT_d0074044 & 3;
    local_10 = _DAT_d0074044 & 0xfffffffc | 2;
                    /* sd_emmc_global_regs.gcfg? */
    _DAT_d0074044 = local_10;
    bVar2 = hdr_check__notsure(1,0x1800000);
    if (bVar2) {
      local_1c = uVar1 << 8 | 0x3b70000;
      local_20 = 0x6007400;
      sdio_mmc_send_cmd((sd_emmc_global_regs *)&DAT_d0074000,&local_20);
      local_10 = CONCAT31(local_10._1_3_,(byte)local_10 & 0xfc | (byte)uVar1);
      _DAT_d0074044 = local_10;
    }
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

uint get_boot_device(void)

{
  uint uVar1;
  
  uVar1 = 6;
  if ((_DAT_da10001c >> 0xc & 0xf) != 2) {
    uVar1 = _DAT_da100240 & 0xf;
  }
  return uVar1;
}



undefined8 storage_init(void)

{
  uint boot_device;
  
  boot_device = get_boot_device();
                    /* if (boot_device == BOOT_DEVICE_EMMC) */
  if (boot_device == 1) {
    sdio_eMMC_prep(1);
  }
  else {
                    /* else if (boot_device == BOOT_DEVICE_NAND) */
    if (boot_device == 2) {
      serial_puts(s_NAND_init_d9009299);
      nfio_init();
    }
  }
  return 0;
}



undefined8 storage_load(ulong src,ulong dst,ulong size,char *name)

{
  uint boot_device;
  long sd_or_emmc;
  char *s;
  ulong uVar1;
  
  boot_device = get_boot_device();
  switch((ulong)boot_device) {
  case 0:
    s = s_Rsv_d90092a4;
    break;
  case 1:
    src = src + 0x200;
    s = s_eMMC_d90092a8;
    break;
  case 2:
    s = s_NAND_d90092ad;
    break;
  case 3:
    s = s_SPI_d90092b2;
    break;
  case 4:
    src = src + 0x200;
    s = s_SD_d90092b6;
    break;
  case 5:
  case 6:
    s = s_USB_d90092b9;
    break;
  default:
    s = s_UNKNOWN_d90092bd;
  }
  serial_puts(s_Load_d90092c5);
  serial_puts(name);
  serial_puts(s__from_d90092cb);
  serial_puts(s);
  serial_puts(s___src__0x_d90092d2);
  serial_put_hex(src,0x20);
  serial_puts(s___des__0x_d90092dc);
  serial_put_hex(dst,0x20);
  serial_puts(s___size__0x_d90092e6);
  serial_put_hex(size,0x20);
  serial_puts(s__d9009869);
  uVar1 = (ulong)boot_device - 1;
  if (5 < uVar1) {
    return 0;
  }
  if (5 < (uint)uVar1) {
    return 0;
  }
  switch(uVar1 & 0xffffffff) {
  case 0:
    sd_or_emmc = 1;
    goto LAB_d9001a1c;
  case 1:
    nf_read(2,(uint)src,(uint)dst,(uint)size);
    break;
  case 2:
    spi_read(src,dst,size);
    break;
  case 3:
    sd_or_emmc = 4;
LAB_d9001a1c:
    sdio_read_data(sd_or_emmc,src,(uint)dst,size);
    break;
  default:
    usb_read__notsure(src,dst,size);
  }
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

uint FUN_d9001a58(void)

{
  return _DAT_da100240 >> 0x10;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9001a6c(void)

{
  _DAT_c11098d0 = _DAT_c11098d0 & 0xfdfbffff;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void reset_system(void)

{
  int iVar1;
  
  udelay(10000);
  do {
    iVar1 = 100;
    do {
      iVar1 = iVar1 + -1;
    } while (iVar1 != 0);
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9001af8(void)

{
  undefined local_30 [48];
  
  _DAT_c883e018 = 0xf;
  _DAT_c883e008 = (uint)local_30 | 2;
  return;
}



void FUN_d9001bc0(long param_1,long param_2,long param_3)

{
  memcpy(param_3,param_1 + 0xd9013c00,param_2);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void cpu_pll_switch_to(int param_1,int param_2,int param_3,int param_4)

{
  do {
  } while ((_DAT_c883c19c >> 0x1c & 1) != 0);
  if (param_1 == 1) {
    _DAT_c883c19c = _DAT_c883c19c | 0x4000800;
  }
  else {
    if ((_DAT_c883c19c >> 10 & 1) == 0) {
      _DAT_c883c19c = _DAT_c883c19c & 0xfc08fbff | 0x4000400 | (uint)(param_1 == 1000) << 0x10;
    }
    else {
      _DAT_c883c19c = _DAT_c883c19c & 0xfffff808 | 0x4000000 | (uint)(param_1 == 1000);
    }
    _DAT_c883c19c = _DAT_c883c19c & 0xfffff7ff;
  }
  if (param_2 - 2U < 7) {
    _DAT_c883c15c = _DAT_c883c15c & 0xffffffc7 | (param_2 - 2U) * 8;
  }
  if (param_3 - 2U < 7) {
    _DAT_c883c15c = _DAT_c883c15c & 0xfffff1ff | (param_3 - 2U) * 0x200;
  }
  if (param_4 - 2U < 7) {
    _DAT_c883c15c = _DAT_c883c15c & 0xfffffe3f | (param_4 - 2U) * 0x40;
  }
  return;
}



uint pll_lock_check(int *param_1,char *param_2)

{
  int iVar1;
  
  iVar1 = *param_1;
  if (-(iVar1 >> 0x1f) == 0) {
    DAT_d900a7e8 = DAT_d900a7e8 + 1;
    serial_puts(param_2);
    serial_puts(s__lock_check_d90092f1);
    serial_put_dec((ulong)DAT_d900a7e8);
    serial_puts(s__d9009869);
    if (9 < DAT_d900a7e8) {
      serial_puts(param_2);
      serial_puts(s__lock_failed__reset____d90092fe);
      reset_system();
      do {
                    /* WARNING: Do nothing block with infinite loop */
      } while( true );
    }
  }
  else {
    DAT_d900a7e8 = 0;
  }
  return -(iVar1 >> 0x1f) ^ 1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 pll_init(void)

{
  int iVar1;
  uint pll_lockchk;
  uint tmp1;
  uint tmp2;
  uint tmp3;
  
  iVar1 = check_if_remap_addr(0);
  tmp1 = _DAT_c883c26c;
  if ((0x21 < iVar1) && (tmp1 = _DAT_c883c26c & 0xfffffbff, (_DAT_c883c26c & 0x400) == 0)) {
    tmp1 = _DAT_c883c26c;
  }
  _DAT_c883c26c = tmp1;
  _DAT_c883c174 = _DAT_c883c174 & 0xfffffeff;
  cpu_pll_switch_to(0,0,0,0);
  _DAT_c883c294 = _DAT_c883c294 | 0x4000000;
  udelay(100);
  tmp1 = (uint)plls.cpu_clk;
                    /* *** */
  do {
    _DAT_c883c2fc = 0xc4258100;
    _DAT_c883c304 = 0xb7400000;
    _DAT_c883c308 = 0xa59a288;
    _DAT_c883c30c = 0x40002d;
    _DAT_c883c310 = 0x7c700007;
    _DAT_c883c300 = tmp1 / 0x18 | 0x40000200;
    udelay(0x14);
    pll_lockchk = pll_lock_check((int *)&DAT_c883c300,s_SYS_PLL_d9009316);
  } while (pll_lockchk != 0);
  cpu_pll_switch_to(1,0,0,0);
  tmp1 = 0;
  if ((_DAT_c883c300 >> 9 & 0x1f) != 0) {
    tmp1 = 0x18 / (_DAT_c883c300 >> 9 & 0x1f);
  }
  tmp2 = _DAT_c883c300 & 0x1ff;
  tmp3 = _DAT_c883c300 >> 0x10;
  serial_puts(s_CPU_clk__d900931e);
  serial_put_dec((ulong)(tmp1 * tmp2 >> ((ulong)tmp3 & 3)));
  serial_puts(s_MHz_d9009328);
  _DAT_c883c28c = 0x10006;
  _DAT_c883c280 = _DAT_c883c280 | 0x20000000;
  udelay(200);
  _DAT_c883c284 = 0x59c80000;
  _DAT_c883c288 = 0xca753822;
  _DAT_c883c290 = 0x95520e1a;
  _DAT_c883c294 = 0xfc454545;
                    /* All these 3 with pll_lockchk: zero'd out...
                       0x298 -> HHI_MPLL_CNTL7
                       0x29c -> HHI_MPLL_CNTL8
                       0x2a0 -> HHI_MPLL_CNTL9
                       I think that's unnecessary. */
  _DAT_c883c280 = 0x400006fa;
  _DAT_c883c298 = pll_lockchk;
  _DAT_c883c29c = pll_lockchk;
  _DAT_c883c2a0 = pll_lockchk;
  udelay(800);
  _DAT_c883c28c = _DAT_c883c28c | 0x4000;
  do {
    if ((int)_DAT_c883c280 < 0) break;
    _DAT_c883c280 = _DAT_c883c280 | 0x20000000;
    udelay(1000);
    _DAT_c883c280 = _DAT_c883c280 & 0xdfffffff;
    udelay(1000);
    tmp1 = pll_lock_check((int *)&DAT_c883c280,s_FIX_PLL_d900932d);
  } while (tmp1 != 0);
                    /* More registers!
                       0x2a4 -> HHI_MPLL_CNTL10
                       0x298 -> HHI_MPLL_CNTL7
                       0x174 -> HHI_MPEG_CLK_CNTL
                       0x29c -> HHI_MPLL_CNTL8 */
  _DAT_c883c2a4 = 0x1003;
  _DAT_c883c298 = 0x5edb7;
  _DAT_c883c174 = _DAT_c883c174 & 0xffff8f00 | 0x5182;
  _DAT_c883c29c = 0x4e15a;
  iVar1 = check_if_remap_addr(0);
  if (0x21 < iVar1) {
    if ((_DAT_c883c26c >> 10 & 1) == 0) {
      _DAT_c883c26c = _DAT_c883c26c & 0xfc00ffff | 0x10400;
    }
    else {
      _DAT_c883c26c = _DAT_c883c26c & 0xfffff800 | 1;
    }
    InstructionSynchronizationBarrier();
    DataMemoryBarrier(3,3);
  }
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

ulong saradc_ch1_get(void)

{
  uint uVar1;
  ulong board_id;
  
  _DAT_c1108684 = 1;
  _DAT_c1108688 = 0x3000;
  _DAT_c110868c = 0xc8a8500a;
  _DAT_c1108690 = 0x10a000a;
  _DAT_c110869c = 0x3eb1a0c;
  _DAT_c11086a0 = 0x8c000c;
  _DAT_c11086a4 = 0x30e030c;
  _DAT_c11086a8 = 0xc00c400;
  _DAT_c883c3d8 = 0x114;
  _DAT_c11086ac = 0x2060;
  _DAT_c1108680 = 0x84064040;
  udelay(0x14);
  _DAT_c1108680 = 0x84064041;
  udelay(0x14);
  _DAT_c1108680 = 0x84064045;
  udelay(0x14);
  uVar1 = 0;
  do {
    if ((_DAT_c1108680 & 0x70000000) == 0) {
      if (uVar1 < 100) goto LAB_d9002300;
      break;
    }
    uVar1 = uVar1 + 1;
  } while (uVar1 != 101);
  serial_puts(s__Get_saradc_sample_Error__Cnt__d9009335);
  serial_put_dec((ulong)uVar1);
  serial_puts(s__d9009869);
LAB_d9002300:
  board_id = 0;
  do {
    if ((_DAT_c1108698 >> 2 & 0x3ff) <= (uint)(&DAT_d9009360)[board_id]) break;
    board_id = board_id + 1;
  } while (board_id != 13);
  board_id = board_id & 0xffffffff;
                    /*   */
  _DAT_c8100240 = _DAT_c8100240 & 0xffff00ff | (int)board_id << 8;
  serial_puts(s_Board_ID___d9009354);
  serial_put_dec(board_id);
  serial_puts(s__d9009869);
  if (((int)board_id == 0) && (plls.debug_mode != 0)) {
    plls.cpu_clk = plls.cpu_clk_debug;
    ddrs.ddr_clk = plls.ddr_clk_debug;
  }
  return board_id;
}



/* WARNING: Removing unreachable block (ram,0xd90023dc) */

void udelay(undefined8 usec)

{
                    /* WARNING: Do nothing block with infinite loop */
  do {
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_detect_size(char *param_1)

{
  uint uVar1;
  ushort uVar2;
  uint uVar3;
  ulong uVar4;
  ulong uVar5;
  int iVar6;
  
  uVar2 = *(ushort *)(param_1 + 8);
  uVar3 = (uint)uVar2;
  if (uVar3 != 0) {
    iVar6 = -1;
    do {
      uVar1 = uVar3 & 1;
      uVar3 = uVar3 >> 1;
      iVar6 = iVar6 + uVar1;
    } while (uVar3 != 0);
    uVar4 = (ulong)(uVar2 >> 6);
    uVar5 = uVar4;
    if (((byte)(*param_1 - 3U) < 2) &&
       (uVar2 = uVar2 >> 7, uVar4 = (ulong)uVar2, uVar5 = uVar4, iVar6 != 0)) {
      uVar5 = (ulong)uVar2 << 1;
      uVar4 = (ulong)uVar2 << 2;
    }
    uVar3 = 0;
    while (uVar4 = uVar4 >> 1, (uVar4 & 1) == 0) {
      uVar3 = uVar3 + 1;
    }
    iVar6 = 0;
    while (uVar5 = uVar5 >> 1, (uVar5 & 1) == 0) {
      iVar6 = iVar6 + 1;
    }
    _DAT_da838768 = uVar3 | *(uint *)(param_1 + 0x18) & 0xffffffc0 | iVar6 << 3;
    *(uint *)(param_1 + 0x18) = _DAT_da838768;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900247c(char *param_1)

{
  char cVar1;
  uint uVar2;
  uint uVar3;
  int *piVar4;
  int iVar5;
  uint uVar6;
  
  _DAT_da838768 = *(uint *)(param_1 + 0x18) & 0xffffffc0 | 0x2d;
  cVar1 = *param_1;
  *(undefined2 *)(param_1 + 8) = 0;
  piVar4 = (int *)&DAT_01000000;
  for (iVar5 = 0; iVar5 <= (int)(uint)(cVar1 == '\x03'); iVar5 = iVar5 + 1) {
    udelay(10);
    _DAT_da838768 = 5 << (ulong)(iVar5 * 3 & 0x1f) | *(uint *)(param_1 + 0x18) & 0xffffffc0;
    udelay(10);
    uVar6 = 0x19;
    do {
      *piVar4 = 0;
      udelay(10);
      uVar2 = 4 << (ulong)(uVar6 & 0x1f);
      *(undefined4 *)((ulong)uVar2 + (long)piVar4) = 0xaabbccdd;
      udelay(10);
      uVar3 = _DAT_da838674 & 2;
      if ((_DAT_da838674 >> 1 & 1) == 0) {
        if (*piVar4 != 0) {
          uVar3 = (uint)(*piVar4 == -0x55443323);
        }
      }
      else {
        uVar3 = 0;
        if (*piVar4 != 0) {
          uVar3 = (uint)(*piVar4 != -0x55443323);
        }
      }
    } while (((*(int *)((ulong)uVar2 + (long)piVar4) != 0) && (uVar3 == 0)) &&
            (uVar6 = uVar6 + 1, uVar6 != 0x21));
    *(ushort *)(param_1 + 8) = (ushort)(uVar2 >> 0x14) + *(short *)(param_1 + 8);
    piVar4 = piVar4 + 0x2000000;
  }
  ddr_detect_size(param_1);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_setup_ssc(void)

{
  uint uVar1;
  char *s;
  
  if (plls.ddr_pll_ssc != 0) {
    serial_puts(s_Set_ddr_ssc__ppm_d90093f0);
    serial_put_dec((ulong)((plls.ddr_pll_ssc >> 0x10 & 0xf) * 500));
    uVar1 = plls.ddr_pll_ssc >> 0xe & 3;
    if (uVar1 == 1) {
      s = &DAT_d9009401;
    }
    else if (uVar1 == 2) {
      s = &DAT_d9009404;
    }
    else {
      s = s__d9009869;
    }
    serial_puts(s);
    udelay(1000);
    _DAT_c8837004 = _DAT_c8837004 | 0x40000;
    udelay(1000);
    _DAT_c883700c = _DAT_c883700c | 0x20;
    udelay(1000);
    _DAT_c8837010 = _DAT_c8837010 & 0xffff73ff | 0x8000;
    udelay(1000);
    _DAT_c8837010 = plls.ddr_pll_ssc & 0x1f0000 | _DAT_c8837010 & 0xffe0ffff;
    udelay(1000);
    return;
  }
  return;
}



void print_ddr_params(void)

{
  char *message;
  
  if ((ddrs.ddr_func >> 5 & 1) != 0) {
    serial_puts(s_DDR_pll_bypass_enabled_d9009407);
  }
  if ((ddrs.ddr_func >> 1 & 1) != 0) {
    serial_puts(s_DDR_low_power_enabled_d900941f);
  }
  if ((ddrs.ddr_func >> 6 & 1) != 0) {
    serial_puts(s_DDR_enable_rdbi_d9009436);
  }
  if ((ddrs.ddr_func >> 3 & 1) != 0) {
                    /* Enabled on lepotato before, later disabled. */
    serial_puts(s_DDR_use_ext_vref_d9009447);
  }
  if ((ddrs.ddr_func >> 2 & 1) != 0) {
    serial_puts(s_DDR_ZQ_power_down_d9009459);
  }
  if ((ddrs.wr_adj_per[0] != '\0') && (ddrs.rd_adj_per[0] != '\0')) {
                    /* Enabled on lepotato and lafrite */
    serial_puts(s_DQS_corr_enabled_d900946c);
  }
  if (plls.pxp == 0) {
                    /* Unless you're one of the rare people who have that emulator, it is always
                       enabled on real boards. */
    message = s_DDR_scramble_enabled_d900947e;
  }
  else {
    message = s_DDR_scramble_disabled_d9009494;
  }
  serial_puts(message);
  return;
}



void set_pll_ctrl(ddr_set_t *ddrs)

{
  uint ddr_clk;
  uint uVar1;
  
  ddr_clk = (uint)ddrs->ddr_clk;
  if (ddrs->ddr_clk < 200) {
    ddr_clk = ddr_clk / 3;
    uVar1 = 0xe;
  }
  else if ((ddr_clk - 200 & 0xffff) < 200) {
    uVar1 = 6;
    ddr_clk = ddr_clk / 6;
  }
  else if ((ddr_clk - 400 & 0xffff) < 400) {
    ddr_clk = ddr_clk / 12;
    uVar1 = 5;
  }
  else {
    if (699 < (ddr_clk - 800 & 0xffff)) {
      return;
    }
    ddr_clk = ddr_clk / 24;
    uVar1 = 4;
  }
  ddrs->ddr_pll_ctrl = uVar1 | 0x10000 | ddr_clk << 4;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 ddr_init_pll(void)

{
  uint uVar1;
  bool bVar2;
  uint uVar3;
  uint uVar4;
  uint uVar5;
  
  _DAT_c88345bc = _DAT_c88345bc | 1;
  _DAT_c883c290 = _DAT_c883c290 | 1;
  _DAT_c8837010 = _DAT_c8837010 & 0xffffefff | 0x1000;
  udelay(10);
  if ((ddrs.ddr_func & 1) != 0) {
                    /* dummy */
    serial_print(s_STICKY_REG0__0x_d90094ab,(ulong)_DAT_c88345c0,0,s__d9009869);
    serial_print(s_STICKY_REG1__0x_d90094bb,(ulong)_DAT_c88345c4,0,s__d9009869);
    serial_print(s_STICKY_REG9__0x_d90094cb,(ulong)_DAT_c88345e4,0,s__d9009869);
    if (_DAT_c88345c0 >> 0x14 == 0xf13) {
      DAT_d900a803 = 0;
      uVar1 = _DAT_c88345c0 & 0xfffff;
      if ((_DAT_c88345e4 & 0xffff) != 0) {
        if ((_DAT_c88345e4 & 0xff) != 0) {
          ddrs.t_pub_soc_vref_dram_vref =
               ddrs.t_pub_soc_vref_dram_vref & 0xff00 | (ushort)_DAT_c88345e4 & 0xff;
          ddrs.t_pub_soc_vref_dram_vref_rank1 = ddrs.t_pub_soc_vref_dram_vref;
        }
        if ((_DAT_c88345e4 & 0xff00) != 0) {
          ddrs.t_pub_soc_vref_dram_vref =
               (ushort)_DAT_c88345e4 & 0xff00 | ddrs.t_pub_soc_vref_dram_vref & 0xff;
          ddrs.t_pub_soc_vref_dram_vref_rank1 = ddrs.t_pub_soc_vref_dram_vref;
        }
      }
      if (_DAT_c88345e4 >> 0x18 != 0) {
        ddrs.t_pub_mr[7] = (ushort)(byte)(_DAT_c88345e4 >> 0x18);
      }
      uVar5 = uVar1;
      uVar3 = uVar1;
      uVar4 = uVar1;
      if (uVar1 == 0) {
        uVar1 = ddrs.t_pub_zq0pr;
        uVar5 = ddrs.t_pub_zq1pr;
        uVar3 = ddrs.t_pub_zq2pr;
        uVar4 = ddrs.t_pub_zq3pr;
      }
      ddrs.t_pub_zq3pr = uVar4;
      ddrs.t_pub_zq2pr = uVar3;
      ddrs.t_pub_zq1pr = uVar5;
      ddrs.t_pub_zq0pr = uVar1;
      uVar1 = ddrs.t_pub_zq0pr;
      serial_print(s_ZQCR__0x_d90094db,(ulong)ddrs.t_pub_zq0pr,0,s__d9009869 + 1);
      serial_print(s_____0x_d90094e4,(ulong)uVar1,0,s__d9009869);
      set_pll_ctrl(&ddrs);
      bVar2 = -1 < (int)_DAT_c88345c4;
      if (bVar2) {
        serial_print(s_CLK___d90094f4,(ulong)ddrs.ddr_clk,1,s__d9009869 + 1);
        serial_print(&DAT_d90094fb,(ulong)(ushort)_DAT_c88345c4,1,s__d9009869);
        ddrs.ddr_clk = (ushort)_DAT_c88345c4;
        ddrs.ddr4_clk = ddrs.ddr_clk;
      }
      else {
        serial_print(s_PLL___0x_d90094eb,(ulong)ddrs.ddr_pll_ctrl,0,s__d9009869 + 1);
        serial_print(s_____0x_d90094e4,(ulong)_DAT_c88345c4,0,s__d9009869);
        ddrs.ddr_pll_ctrl = _DAT_c88345c4;
      }
      if ((_DAT_c88345c4 >> 0x15 & 1) != 0) {
        DAT_d900a805 = 1;
      }
      _DAT_c88345c0 = 0;
      _DAT_c88345c4 = 0;
      _DAT_c88345e4 = 0;
      if (!bVar2) goto LAB_d9002be0;
    }
  }
  set_pll_ctrl(&ddrs);
LAB_d9002be0:
  uVar1 = 0;
  if ((ddrs.ddr_pll_ctrl >> 16 & 0x1f) != 0) {
    uVar1 = ((ddrs.ddr_pll_ctrl >> 4 & 511) * 24) / (ddrs.ddr_pll_ctrl >> 16 & 31);
  }
  do {
    _DAT_c8837004 = 0xaa203;
    _DAT_c8837008 = 0x2919a288;
    _DAT_c883700c = 0x3e3b744;
    if (uVar1 < 0x438) {
      _DAT_c8837010 = 0xc0103;
    }
    else {
      _DAT_c8837010 = 0xc0101;
    }
    _DAT_c8837014 = 0xe600001e;
    _DAT_c8837000 = ddrs.ddr_pll_ctrl | 0xb0000000;
    udelay(100);
    uVar5 = pll_lock_check((int *)0xc8837018,s_DDR_PLL_d9009500);
  } while (uVar5 != 0);
  _DAT_c883701c = 0xb000a005;
  uVar1 = 0;
  if ((_DAT_c8837000 >> 0x10 & 0x1f) != 0) {
    uVar1 = ((_DAT_c8837000 >> 4 & 0x1ff) * 0x18) / (_DAT_c8837000 >> 0x10 & 0x1f);
  }
  uVar5 = (_DAT_c8837000 >> 2 & 3) + 1;
  uVar3 = 0;
  if (uVar5 != 0) {
    uVar3 = uVar1 / uVar5;
  }
  ddrs.ddr_clk = (ushort)((uVar3 << 1) >> (ulong)(_DAT_c8837000 & 3));
  if ((ddrs.ddr_func >> 5 & 1) != 0) {
    _DAT_c8836080 = _DAT_c8836080 | 0xa0000000;
    _DAT_c8836700 = _DAT_c8836700 | 0xa0000;
    _DAT_c8836800 = _DAT_c8836800 | 0xa0000;
    _DAT_c8836900 = _DAT_c8836900 | 0xa0000;
    _DAT_c8836a00 = _DAT_c8836a00 | 0xa0000;
  }
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_print_setting(void)

{
  uint uVar1;
  uint uVar2;
  byte bVar3;
  char *2t_mode;
  uint ddr_n;
  
  if (ddrs.ddr_size_detect == '\0') {
    ddr_detect_size((char *)&ddrs);
  }
  else {
    FUN_d900247c((char *)&ddrs);
  }
  uVar2 = _DAT_da838768;
  bVar3 = ddrchannelset - 3;
  uVar1 = _DAT_c8839104 >> 3;
  for (ddr_n = 0; (int)ddr_n <= (int)(uint)(bVar3 < 2); ddr_n = ddr_n + 1) {
    serial_print(s_Rank_d9009512,(ulong)ddr_n,1,s__d9009869 + 1);
                    /*      ↓↓↓ ": " */
    serial_print(s_aml_log___SIG_CHK___d90097f9 + 0x12,
                 (ulong)(uint)(1 << (ulong)((uVar2 >> (ulong)(ddr_n * 3 & 0x1f) & 7) + 7)),1,
                 s_MB_d9009517);
    if (ddrs.ddr_size_detect != '\0') {
      serial_puts(s__auto__d900951a);
    }
    if ((uVar1 & 1) == 0) {
      2t_mode = s__1T__d900950d;
    }
    else {
      2t_mode = s__2T__d9009508;
    }
    serial_print(2t_mode,(ulong)ddrs.ddr_timing_ind,1,s__d9009869);
  }
  _DAT_da100240 = _DAT_da100240 & 0xffff | (uint)ddrs.ddr_size << 0x10;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 ddr_init_dmc(void)

{
  if ((ddrs.ddr_func >> 2 & 1) == 0) {
                    /* DDR_FUNC_ZQ_PD (disabled on lepotato) */
    _DAT_c8836680 = _DAT_c8836680 & 0xfffffffb;
  }
  else {
    _DAT_c8836680 = _DAT_c8836680 | 4;
  }
  if ((ddrs.ddr_func >> 3 & 1) != 0) {
    _DAT_c8836520 = 0;
  }
  _DAT_c8838090 = (uint)ddrt_p->cfg_ddr_refi << 8 | 0x20100000 | ddrs.ddr_clk / 20 - 1;
  _DAT_c883808c = 0xf08f;
                    /* ADDRMAP */
  if ((ddrchannelset == 4) || (ddrchannelset == 1)) {
                    /* CONFIG_DDR0_16BIT(_2) */
    if (ddrtype == 0) {
                    /* DDR3 */
      _DAT_da838740 = 0x128398a0;
      _DAT_da838744 = 0x1ee683aa;
      _DAT_da838748 = 0x2b49ca30;
      _DAT_da83874c = 0x39ace2f6;
      _DAT_da838750 = 0x3c0db17d;
      _DAT_da838754 = 0x128398a0;
      _DAT_da838758 = 0x1ee683aa;
      _DAT_da83875c = 0x2b49ca30;
      _DAT_da838760 = 0x39ace2f6;
      _DAT_da838764 = 0x3c0db17d;
    }
    else if (ddrtype == 1) {
                    /* DDR4 */
      _DAT_da838740 = 0x14941cc0;
      _DAT_da838744 = 0x20f7000b;
      _DAT_da838748 = 0x2d5a4e51;
      _DAT_da83874c = 0x39bd6717;
      _DAT_da838750 = 0x3c02b59d;
      _DAT_da838754 = 0x14941cc0;
      _DAT_da838758 = 0x20f7000b;
      _DAT_da83875c = 0x2d5a4e51;
      _DAT_da838760 = 0x39bd6717;
      _DAT_da838764 = 0x3c02b59d;
    }
    else if (ddrtype == '\x02') {
      _DAT_da838740 = 0x128398a0;
      _DAT_da838744 = 0x1ee6800a;
      _DAT_da838748 = 0x2b49ca30;
      _DAT_da83874c = 0x39ace2f6;
      _DAT_da838750 = 0x3a0db17e;
      _DAT_da838754 = 0x128398a0;
      _DAT_da838758 = 0x1ee6800a;
      _DAT_da83875c = 0x2b49ca30;
      _DAT_da838760 = 0x39ace2f6;
      _DAT_da838764 = 0x3a0db17e;
    }
  }
  else if ((byte)(ddrchannelset - 2U) < 2) {
                    /* CONFIG_DDR0_RANK0
                       CONFIG_DDR0_RANK01 */
    if (ddrtype == '\x01') {
      _DAT_da838740 = 0x16a4a0e6;
      _DAT_da838744 = 0x2307800c;
      _DAT_da838748 = 0x2f6ad272;
      _DAT_da83874c = 0x3bcdeb38;
      _DAT_da838750 = 0x3e02b9be;
      _DAT_da838754 = 0x16a4a0e6;
      _DAT_da838758 = 0x2307800c;
      _DAT_da83875c = 0x2f6ad272;
      _DAT_da838760 = 0x3bcdeb38;
      _DAT_da838764 = 0x3e02b9be;
    }
    else {
      _DAT_da838740 = 0x14941cc5;
      if (ddrtype == '\x02') {
        _DAT_da838744 = 0x20f703ab;
        _DAT_da838748 = 0x2d5a4e51;
        _DAT_da83874c = 0x3dbd6717;
        _DAT_da838750 = 0xe359f;
        _DAT_da838754 = 0x14941cc5;
        _DAT_da838758 = 0x20f703ab;
        _DAT_da83875c = 0x2d5a4e51;
        _DAT_da838760 = 0x3dbd6717;
        _DAT_da838764 = 0xe359f;
      }
      else {
                    /* CONFIG_DDR_TYPE_DDR3
                        * DDR0_ADDRMAP_1
                        * DDR0_ADDRMAP_2
                        * DDR0_ADDRMAP_3
                        * DDR0_ADDRMAP_4 */
        _DAT_da838744 = 0x230783cb;
        _DAT_da838748 = 0x2f6ad272;
        _DAT_da83874c = 0x3bcdeb38;
        _DAT_da838750 = 0x3e07359e;
                    /*  * DDR1_ADDRMAP_0
                        * DDR1_ADDRMAP_1
                        * DDR1_ADDRMAP_2
                        * DDR1_ADDRMAP_3
                        * DDR1_ADDRMAP_4 */
        _DAT_da838754 = 0x14941cc5;
        _DAT_da838758 = 0x230783cb;
        _DAT_da83875c = 0x2f6ad272;
        _DAT_da838760 = 0x3bcdeb38;
        _DAT_da838764 = 0x3e07359e;
      }
    }
  }
  if (plls.pxp != 0) {
                    /* PXP emulator code, please ignore :) */
    _DAT_da838740 = 0x14941cc5;
    _DAT_da838744 = 0x20d6000b;
    _DAT_da838748 = 0x2d5a4e51;
    _DAT_da83874c = 0x39bd6717;
    _DAT_da838750 = 0xebdc0;
    _DAT_da838754 = 0x14941cc5;
    _DAT_da838758 = 0x20d6000b;
    _DAT_da83875c = 0x2d5a4e51;
    _DAT_da838760 = 0x39bd6717;
    _DAT_da838764 = 0xebdc0;
  }
  if (ddrs.ddr0_addrmap[0] != 0) {
    _DAT_da838740 = ddrs.ddr0_addrmap[0];
    _DAT_da838744 = ddrs.ddr0_addrmap[1];
    _DAT_da838748 = ddrs.ddr0_addrmap[2];
    _DAT_da83874c = ddrs.ddr0_addrmap[3];
    _DAT_da838750 = ddrs.ddr0_addrmap[4];
  }
  if (ddrs.ddr1_addrmap[0] != 0) {
    _DAT_da838754 = ddrs.ddr1_addrmap[0];
    _DAT_da838758 = ddrs.ddr1_addrmap[1];
    _DAT_da83875c = ddrs.ddr1_addrmap[2];
    _DAT_da838760 = ddrs.ddr1_addrmap[3];
    _DAT_da838764 = ddrs.ddr1_addrmap[4];
  }
  if ((ddrs.ddr_func >> 1 & 1) != 0) {
                    /* DDR_FUNC_LP */
    _DAT_c8837028 = 0x10000000;
    _DAT_c883702c = 0x7ff;
    _DAT_c883601c = _DAT_c883601c | 0xee00;
    _DAT_c8838118 = 0;
    _DAT_c883701c = _DAT_c883701c & 0xfffffff0 | 5;
  }
  _DAT_c8837024 = 0x2000;
  _DAT_da83841c = 0;
  _DAT_da838400 = 0x80000000;
  _DAT_da838438 = 0x55555555;
  _DAT_da8384d8 = 0x55555555;
  _DAT_da8384dc = 0x55555555;
  _DAT_da8384d0 = 0x15;
  _DAT_da8384d4 = 5;
  _DAT_da838494 = 0xffffffff;
  _DAT_da8384cc = 0x55555555;
  _DAT_da8384c8 = 0x55555555;
  _DAT_da838448 = 0xffffffff;
  _DAT_da838444 = 0x55555555;
  _DAT_da838440 = 0x55555555;
  _DAT_da838464 = 0xffffffff;
  _DAT_da838460 = 0x55555555;
  _DAT_da83845c = 0x55555555;
  _DAT_da838480 = 0xffffffff;
  _DAT_da83847c = 0x55555555;
  _DAT_da838478 = 0x55555555;
                    /* DMC_DES_KEY(0,1)_(H,L) ? */
  _DAT_da838640 = 0;
  _DAT_da838644 = 0;
  _DAT_da838648 = _DAT_c8834500;
  _DAT_da83864c = _DAT_c8834500;
  if (plls.pxp == 0) {
                    /* DMC_DES_CTRL */
    _DAT_da838674 = 2;
  }
  else {
    _DAT_da838674 = 0;
  }
  _DAT_c8838000 = 0xffff;
  _DAT_c1107d40 = 0xbaadf00d;
  InstructionSynchronizationBarrier();
  DataMemoryBarrier(3,3);
  return 0;
}



/* WARNING: Type propagation algorithm not settling */
/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */
/* WARNING: Restarted to delay deadcode elimination for space: ram */

undefined8 ddr_init_pctl(int err_report)

{
  bool bVar1;
  undefined4 *pub_pir_tmpvar__notsure;
  uint *dxXlcdlr;
  undefined4 uVar2;
  ddr_set_t *ddrs______;
  ddr_timing_t *ddrt_p_;
  char ddrtype_____;
  ushort is_pxp_emu;
  byte read_adj_per__2;
  uint tmp1;
  uint tmp2;
  
  Unk_DAT_d900a804 = 0;
  do {
    Unk_DAT_d900a808 = 0;
    _DAT_c8838004 = 0;
    _DAT_c8838008 = 0;
    udelay(10);
                    /* DMC_SOFT_RST and DMC_SOFT_RST1. Same ↑ */
    _DAT_c8838004 = 0xffffffff;
    _DAT_c8838008 = 0xffffffff;
    udelay(10);
                    /* DMC_PCTL_LP_CTRL */
    _DAT_c8838118 = 0;
                    /* DDR0_SOFT_RESET */
    _DAT_c8837020 = 0;
    udelay(10);
    _DAT_c8837020 = 0xf;
    udelay(10);
    ddrt_p_ = ddrt_p;
    is_pxp_emu = plls.pxp;
                    /* PTR3 and PTR4 */
    _DAT_c883604c = 0x190c3500;
    _DAT_c8836050 = 0x12c493e0;
                    /* IOVCR0 and IOVCR1 */
    _DAT_c8836520 = 0x1f090909;
    _DAT_c8836524 = 0x109;
                    /* lepotato pub_mr[7] isn't defined. */
    if (pub_mr__7 != 0) {
      _DAT_c8836524 = pub_mr__7 | 0x100;
    }
    _DAT_c8836710 = 0xe09093c;
    _DAT_c8836810 = 0xe09093c;
    _DAT_c8836910 = 0xe09093c;
    _DAT_c8836a10 = 0xe09093c;
    _DAT_c8836098 = ddrs.t_pub_odtcr;
    _DAT_c8836180 = (uint)ddrs.t_pub_mr[0];
    _DAT_c8836184 = (uint)ddrs.t_pub_mr[1];
    _DAT_c8836188 = (uint)ddrs.t_pub_mr[2];
    _DAT_c883618c = (uint)ddrs.t_pub_mr[3];
    _DAT_c8836190 = (uint)ddrs.t_pub_mr[4];
    _DAT_c8836194 = (uint)ddrs.t_pub_mr[5];
    _DAT_c8836198 = (uint)ddrs.t_pub_mr[6];
                    /* ↓ this is odt
                       PUB_MR11 */
    _DAT_c88361ac = _odt_operatorOR_1shift2;
    _DAT_c8836110 = ddrs.t_pub_dtpr[0];
    _DAT_c8836114 = ddrs.t_pub_dtpr[1];
    _DAT_c8836118 = ddrs.t_pub_dtpr[2];
    _DAT_c883611c = ddrs.t_pub_dtpr[3];
    _DAT_c8836120 = ddrs.t_pub_dtpr[4];
    _DAT_c8836124 = ddrs.t_pub_dtpr[5];
    _DAT_c8836018 = ddrs.t_pub_pgcr2;
    _DAT_c883601c = ddrs.t_pub_pgcr3;
    if (DAT_d900a803 != '\0') {
                    /* Unclear if it's ran or not... */
      _DAT_c883601c = ddrs.t_pub_pgcr3 & 0xff00ffff | 0x10000;
    }
    _DAT_c8836088 = ddrs.t_pub_dxccr;
    _DAT_c8836200 = ddrs.t_pub_dtcr0;
    _DAT_c8836204 = ddrs.t_pub_dtcr1;
    _DAT_c8836014 = ddrs.t_pub_pgcr1;
                    /* ACIOCR: */
    _DAT_c8836504 = 0;
    _DAT_c8836508 = 0;
    _DAT_c883650c = 0;
    _DAT_c8836510 = 0;
    _DAT_c8836514 = 0;
                    /* DX: */
    _DAT_c8836704 = 0;
    _DAT_c8836708 = 0;
    _DAT_c8836804 = 0;
    _DAT_c8836808 = 0;
    _DAT_c8836904 = 0;
    _DAT_c8836908 = 0;
    _DAT_c8836a04 = 0;
    _DAT_c8836a08 = 0;
                    /* CONFIG_DDR0_16BIT */
    if (ddrchannelset == 1) {
                    /* (in order) these registers are 
                       * DX2GCR0
                       * DX3GCR0 */
      _DAT_c8836900 = 0;
      _DAT_c8836a00 = 0;
    }
                    /* PUB_PIR ?? */
    _DAT_c8836004 = 0x73;
    do {
                    /* Waits for PGSR0 */
    } while ((_DAT_c8836034 & 1) == 0);
                    /* PUB_DCR */
    _DAT_c8836100 = ddrs.t_pub_dcr | (uint)ddrs.ddr_2t_mode << 28;
                    /* PUB_VTCR1 */
    _DAT_c883652c = ddrs.t_pub_vtcr1;
                    /* PUB_DSGCR */
    _DAT_c8836090 = ddrs.t_pub_dsgcr & 0xff7ffffb;
                    /* DDR0_PCTL_MCFG ?? */
    _DAT_c8839104 = 0;
    if (ddrs.ddr_2t_mode != 0) {
      _DAT_c8839104 = 8;
    }
    _DAT_c8839104 = _DAT_c8839104 | ddrs.t_pctl0_mcfg;
    do {
    } while ((_DAT_c8839118 & 1) == 0);
    _DAT_c8839000 = (uint)ddrs.t_pctl0_scfg;
    _DAT_c8839004 = (uint)ddrs.t_pctl0_sctl;
    do {
    } while ((_DAT_c8839120 & 1) == 0);
    if (plls.pxp != 0) {
                    /* ******************************************* initialise_dram_pctl after
                       do{}while PGSR0 loop */
      _DAT_c8836004 = 0x581;
    }
    do {
    } while ((_DAT_c8836034 & 1) == 0);
                    /* 
                       Timings
                               */
    _DAT_c883901c = (uint)ddrt_p->cfg_ddr_rfc;
    _DAT_c8839044 = (uint)ddrt_p->cfg_ddr_faw;
    _DAT_c8839090 = (uint)ddrt_p->cfg_ddr_refi_mddr3;
    _DAT_c8839018 = (uint)ddrt_p->cfg_ddr_mrd;
    if (6 < ddrt_p->cfg_ddr_mrd) {
      _DAT_c8839018 = 7;
    }
    _DAT_c8839020 = (uint)ddrt_p->cfg_ddr_rp | (uint)ddrt_p->cfg_ddr_rp << 0x10;
    _DAT_c8839140 = (uint)ddrt_p->cfg_ddr_al;
    _DAT_c8839088 = ddrt_p->cfg_ddr_cke + 1;
    if (ddrtype == 2) {
                    /* not ran */
      _DAT_c8839020 = _DAT_d900a81c | _DAT_d900a810 << 0x10;
      _DAT_c8839094 = _DAT_d900a820;
      _DAT_c8839088 = _DAT_d900a814;
      _DAT_c8836700 = _DAT_c8836700 | 0x600;
      _DAT_c8836800 = _DAT_c8836800 | 0x600;
      _DAT_c8836900 = _DAT_c8836900 | 0x600;
      _DAT_c8836a00 = _DAT_c8836a00 | 0x600;
    }
    _DAT_c8839030 = (uint)ddrt_p->cfg_ddr_cwl;
    _DAT_c883902c = (uint)ddrt_p->cfg_ddr_cl;
    _DAT_c8839034 = (uint)ddrt_p->cfg_ddr_ras;
    _DAT_c8839038 = (uint)ddrt_p->cfg_ddr_rc;
    _DAT_c883903c = (uint)ddrt_p->cfg_ddr_rcd;
    _DAT_c8839040 = (uint)ddrt_p->cfg_ddr_rrd | (ddrt_p->cfg_ddr_rrd + 2) * 0x10000;
                    /* tccdl is new on gxl, used for DDR4 it seems... */
    _DAT_c8839148 = (uint)ddrt_p->cfg_ddr_tccdl << 0x10 | 4;
    _DAT_c8839048 = (uint)ddrt_p->cfg_ddr_rtp;
    _DAT_c883904c = (uint)ddrt_p->cfg_ddr_wr;
    _DAT_c8839050 = (uint)ddrt_p->cfg_ddr_wtr;
    _DAT_c8839054 = (uint)ddrt_p->cfg_ddr_exsr;
    if (0x3ff < ddrt_p->cfg_ddr_exsr) {
      _DAT_c8839054 = 0x3ff;
    }
    ddrt_p->cfg_ddr_exsr = (ushort)_DAT_c8839054;
    _DAT_c8839058 = (uint)ddrt_p_->cfg_ddr_xp;
    _DAT_c8839078 = (uint)ddrt_p_->cfg_ddr_dqs;
    _DAT_c8839024 = (uint)ddrt_p_->cfg_ddr_rtw;
    _DAT_c8839068 = (uint)ddrt_p_->cfg_ddr_cksre;
    _DAT_c883906c = (uint)ddrt_p_->cfg_ddr_cksrx;
    _DAT_c8839074 = (uint)ddrt_p_->cfg_ddr_mod;
    _DAT_c8839070 = (uint)ddrt_p_->cfg_ddr_cke;
    _DAT_c8839060 = (uint)ddrt_p_->cfg_ddr_zqcs;
    if ((char)ddrt_p_->cfg_ddr_zqcs < '\0') {
      _DAT_c8839060 = 0x7f;
    }
    ddrt_p_->cfg_ddr_zqcs = (uchar)_DAT_c8839060;
    _DAT_c8839080 = (uint)ddrt_p_->cfg_ddr_zqcl;
    _DAT_c883905c = (uint)ddrt_p_->cfg_ddr_xpdll;
    _DAT_c8839064 = (uint)ddrt_p_->cfg_ddr_zqcsi;
    _DAT_c8839110 = 0xab0a560a;
    _DAT_c1107d40 = 0xdeadbeef;
    _DAT_c883c010 = 0x88776655;
    _DAT_c883910c = ddrs.t_pctl0_ppcfg;
    _DAT_c88390f0 = (uint)ddrs.t_pctl0_dfistcfg0;
    _DAT_c88390f4 = (uint)ddrs.t_pctl0_dfistcfg1;
    _DAT_c8839098 = (uint)ddrs.t_pctl0_dfitctrldelay;
    _DAT_c88390a8 = (uint)ddrs.t_pctl0_dfitphywrdata;
    _DAT_c88390ac = (uint)ddrs.t_pctl0_dfitphywrlta;
    _DAT_c88390b0 = (uint)ddrs.t_pctl0_dfitrddataen;
    _DAT_c88390b4 = (uint)ddrs.t_pctl0_dfitphyrdlat;
    _DAT_c88390fc = (uint)ddrs.t_pctl0_dfitdramclkdis;
    _DAT_c88390f8 = (uint)ddrs.t_pctl0_dfitdramclken;
                    /* ******************************************** */
    _DAT_c88390c8 = (uint)ddrs.t_pctl0_dfitctrlupdmin;
    _DAT_c88390cc = (uint)ddrs.t_pctl0_dfitctrlupdmax;
    _DAT_c88390d4 = (uint)ddrs.t_pctl0_dfiupdcfg;
    _DAT_c8839100 = ddrs.t_pctl0_dfilpcfg0;
    _DAT_c88390b8 = (uint)ddrs.t_pctl0_dfitphyupdtype0;
    _DAT_c88390bc = (uint)ddrs.t_pctl0_dfitphyupdtype1;
    _DAT_c883909c = ddrs.t_pctl0_dfiodtcfg;
    _DAT_c88390a0 = ddrs.t_pctl0_dfiodtcfg1;
    if (is_pxp_emu == 0) {
      _DAT_c8836684 = ddrs.t_pub_zq0pr;
      _DAT_c8836694 = ddrs.t_pub_zq1pr;
      _DAT_c88366a4 = ddrs.t_pub_zq2pr;
      _DAT_c8836004 = 3;
      do {
      } while ((_DAT_c8836034 & 1) == 0);
                    /* ZQCR hack(?) */
      _DAT_c8836680 = _DAT_c8836680 | 0x8000004;
      udelay(10);
      tmp1 = _DAT_c8836680 & 0xf7fffffb;
      if (ddrtype == '\x01') {
        tmp1 = _DAT_c8836680 & 0xf0fffffb | 0x3000000;
      }
      _DAT_c8836680 = tmp1;
      udelay(30);
                    /* ************************ */
      if (ddrchannelset == 1) {
        _DAT_c8836900 = _DAT_c8836900 & 0xfff4fffe | 0xb0000;
        _DAT_c8836a00 = _DAT_c8836a00 & 0xfff4fffe | 0xb0000;
      }
      _DAT_c8836540 = ddrs.t_pub_acbdlr0;
      _DAT_c883654c = ddrs.t_pub_acbdlr3;
      if ((ddrs.ddr_2t_mode != '\0') && (ddrtype == '\0')) {
        _DAT_c8836544 = 0x10101010;
        _DAT_c883655c = 0x10101010;
        _DAT_c8836560 = 0x20202020;
        _DAT_c8836564 = 0x30303030;
                    /* acbdlr6 */
        _DAT_c8836548 = 0x3f003f;
        _DAT_c8836558 = 0;
      }
      if (ddrtype == '\0') {
                    /* DXXCR */
        _DAT_c8836088 = _DAT_c8836088 & 0xffffe01f | 0x1200;
        pub_pir_tmpvar__notsure = (undefined4 *)&DAT_c8836004;
                    /* Not ran (lepotato) ↓↓↓↓↓↓↓↓ ghidrabug */
        uVar2 = 0xfff3;
      }
      else {
        if (ddrtype != '\x01') {
          if (ddrtype == '\x02') {
            tmp1 = (ddrs.t_pub_zq1pr & 0xf) + 1;
            tmp2 = 0;
            if (tmp1 != 0) {
              tmp2 = 0x1e0 / tmp1;
            }
            pub_zq1pr = (undefined2)tmp2;
            DAT_d900a7fe = (short)(_odt_operatorOR_1shift2 & 3) * 0x3c;
            if ((_odt_operatorOR_1shift2 & 3) == 3) {
              DAT_d900a7fe = 0xf0;
            }
            if (DAT_d900a7fe == 0) {
              DAT_d900a7fe = 10000;
            }
            tmp1 = 0;
            if (tmp2 + DAT_d900a7fe != 0) {
              tmp1 = (tmp2 * 0x32) / (tmp2 + DAT_d900a7fe);
            }
            DAT_d900a7ec = ((tmp1 + 0x32) * 1000 - 0xac26) / 0x2ba;
            _DAT_c8836520 = DAT_d900a7ec | 0x1f000000 | DAT_d900a7ec << 8 | DAT_d900a7ec << 0x10;
            _DAT_c8836524 = 0x109;
            _DAT_c8836088 = _DAT_c8836088 & 0xffffe01f | 0x1ee0;
            _DAT_c8836004 = 0x171;
            do {
              udelay(2);
            } while ((_DAT_c8836034 & 1) == 0);
            _DAT_c8836088 = _DAT_c8836088 & 0xffffe01f | 0x1ee0;
            _DAT_c8836090 = _DAT_c8836090 | 0xc0;
            _DAT_c8836004 = 0x201;
            do {
              udelay(2);
            } while ((_DAT_c8836034 & 1) == 0);
            udelay(2);
            _DAT_c88367c0 = 0x20000;
            _DAT_c88368c0 = 0x20000;
            _DAT_c88369c0 = 0x20000;
            _DAT_c8836ac0 = 0x20000;
            _DAT_c88364dc = 0;
            _DAT_c8836004 = 0xf001;
            do {
              udelay(2);
            } while ((_DAT_c8836034 & 1) == 0);
            udelay(2);
          }
          goto LAB_d9004b20;
        }
        ddrs.t_pub_odtcr = 0x10000;
        _DAT_c8836098 = 0x10000;
        if (ddrs.ddr4_odt == '\0') {
          _DAT_c8836184 = (uint)(ddrs.t_pub_mr[1] | 0x400);
          _DAT_c8836188 = (uint)ddrs.t_pub_mr[2];
        }
        _DAT_c8836528 = DAT_d900a7ec | 0xf0032000;
        _DAT_c883652c = 0xfc011f2;
        _DAT_c8836088 = _DAT_c8836088 & 0xffffe01f;
        _DAT_c8836004 = 0xf3;
        do {
          udelay(10);
        } while ((_DAT_c8836034 & 1) == 0);
        _DAT_c8836580 = ddrs.t_pub_aclcdlr;
        _DAT_c8836548 = 0x3f003f;
        _DAT_c8836550 = 0x3f3f;
        _DAT_c8836554 = 0x3f3f;
        if (ddrs.t_pub_aclcdlr < 63) {
          _DAT_c8836548 = ddrs.t_pub_aclcdlr | ddrs.t_pub_aclcdlr << 0x10;
          _DAT_c8836550 = ddrs.t_pub_aclcdlr | ddrs.t_pub_aclcdlr << 8;
          _DAT_c8836554 = ddrs.t_pub_aclcdlr | ddrs.t_pub_aclcdlr << 8;
        }
        if ((ddrs.ddr_func >> 6 & 1) != 0) {
          _DAT_c8836200 = ddrs.t_pub_dtcr0 | 0xc000;
          ddrs.t_pub_dtcr0 = _DAT_c8836200;
        }
        _DAT_c8836004 = 0xf01;
        do {
          udelay(10);
        } while ((_DAT_c8836034 & 1) == 0);
        if ((_DAT_c8836034 >> 0x16 & 1) != 0) {
          return 0xffffffff;
        }
        _DAT_c8836088 = _DAT_c8836088 & 0xffffe01f;
        _DAT_c8836004 = 0x1001;
        udelay(1000);
        if ((((_DAT_c8836758 == 0) || (_DAT_c8836858 == 0)) || (_DAT_c8836958 == 0)) ||
           (_DAT_c8836a58 == 0)) {
          udelay(1000);
        }
        _DAT_c8836200 = ddrs.t_pub_dtcr0 & 0xffff3fff;
        _DAT_c8836004 = 0xe001;
        ddrs.t_pub_dtcr0 = _DAT_c8836200;
        do {
          udelay(10);
        } while ((_DAT_c8836034 & 1) == 0);
        tmp1 = (uint)pub_soc_vref_dram_vref;
        if (pub_soc_vref_dram_vref != 0) {
          _DAT_c8836528 =
               ((pub_soc_vref_dram_vref + 1) * 0x1000 | (tmp1 - 1) * 0x40 | tmp1 | 0xf0000000) &
               0xefffffff;
        }
        _DAT_c8836a14 = DAT_d900a7f0 | 0x9090900;
        if (pub_soc_vref_dram_vref_right8 == 0) {
LAB_d90046f4:
          _DAT_c8836004 = 0x20001;
          _DAT_c8836714 = _DAT_c8836a14;
          _DAT_c8836814 = _DAT_c8836a14;
          _DAT_c8836914 = _DAT_c8836a14;
          do {
            udelay(10);
          } while ((_DAT_c8836034 & 1) == 0);
        }
        else {
          _DAT_c883652c = _DAT_c883652c & 0xfffffffd;
          _DAT_c8836714 = pub_soc_vref_dram_vref_right8 | 0x9090900;
          _DAT_c8836814 = _DAT_c8836714;
          _DAT_c8836914 = _DAT_c8836714;
          _DAT_c8836a14 = _DAT_c8836714;
          if (tmp1 == 0) goto LAB_d90046f4;
        }
        _DAT_c8836200 = _DAT_c8836200 | 0x80000000;
        if (pub_soc_vref_dram_vref != 0) {
          _DAT_c8836184 = ddrs.t_pub_mr[1] & 0xffffff7f;
          _DAT_c8836198 = (uint)(ddrs.t_pub_mr[6] | 0x80);
          _DAT_c8836004 = 0x101;
          do {
            udelay(1);
          } while ((_DAT_c8836034 & 1) == 0);
          udelay(1);
          _DAT_c8836198 = (uint)(ddrs.t_pub_mr[6] | 0x80);
          _DAT_c8836004 = 0x101;
          do {
            udelay(1);
          } while ((_DAT_c8836034 & 1) == 0);
          udelay(1);
          _DAT_c8836198 = ddrs.t_pub_mr[6] & 0xffffff7f;
          if (ddrs.ddr4_odt == '\0') {
            _DAT_c8836184 = ddrs.t_pub_mr[1] & 0xfffffb7f | 0x400;
          }
          _DAT_c8836004 = 0x101;
          do {
            udelay(1);
          } while ((_DAT_c8836034 & 1) == 0);
        }
        uVar2 = 0x808;
        ddrs.t_pctl0_dfiodtcfg = 0x808;
        pub_pir_tmpvar__notsure = (undefined4 *)&DAT_c883909c;
      }
      *pub_pir_tmpvar__notsure = uVar2;
    }
LAB_d9004b20:
    while( true ) {
                    /* Nothing to see here... */
      udelay(1000);
      ddrtype_____ = ddrtype;
      Unk_DAT_d900a808 = Unk_DAT_d900a808 + 1;
      Unk_DAT_d900a804 = Unk_DAT_d900a804 + 1;
      if (10 < Unk_DAT_d900a808) break;
      if (100 < Unk_DAT_d900a804) {
        if (err_report == 0) {
          return 0xffffffff;
        }
        serial_puts(s_DDR_init_fail__reset____d90095d9);
        reset_system();
        do {
                    /* WARNING: Do nothing block with infinite loop */
        } while( true );
      }
      if (plls.pxp == 0) {
        if (ddrtype == '\x02') {
          tmp1 = ddrs.ddr_func & 0x80;
          if ((ddrs.ddr_func >> 7 & 1) == 0) {
            tmp2 = 0xfbf;
          }
          else {
            tmp2 = 0x1fbf;
          }
          if (_DAT_c8836034 != (tmp2 | 0xc0000000)) {
            tmp2 = 0x1fbf;
            if (tmp1 == 0) {
              tmp2 = 0xfbf;
            }
            if (_DAT_c8836034 != (tmp2 | 0xe0000000)) {
              tmp2 = 0x1fbf;
              if (tmp1 == 0) {
                tmp2 = 0xfbf;
              }
              if (_DAT_c8836034 != (tmp2 | 0xa0000000)) {
                tmp2 = 0x1fbf;
                if (tmp1 == 0) {
                  tmp2 = 0xfbf;
                }
                if (_DAT_c8836034 != (tmp2 | 0x80000000)) {
                  tmp2 = 7999;
                  if (tmp1 == 0) {
                    tmp2 = 0xf3f;
                  }
                  if (_DAT_c8836034 != (tmp2 | 0xc0000000)) {
                    tmp2 = 7999;
                    if (tmp1 == 0) {
                      tmp2 = 0xf3f;
                    }
                    if (_DAT_c8836034 != (tmp2 | 0xe0000000)) {
                      tmp2 = 7999;
                      if (tmp1 == 0) {
                        tmp2 = 0xf3f;
                      }
                      if (_DAT_c8836034 != (tmp2 | 0xa0000000)) {
                        if (tmp1 == 0) {
                          tmp1 = 0xf3f;
                        }
                        else {
                          tmp1 = 7999;
                        }
                        bVar1 = _DAT_c8836034 != (tmp1 | 0x80000000);
                        goto joined_r0xd9004e80;
                      }
                    }
                  }
                }
              }
            }
          }
          bVar1 = false;
        }
        else {
          bVar1 = false;
          if (((_DAT_c8836034 != 0x80000fff) && (_DAT_c8836034 != 0x80004fff)) &&
             (_DAT_c8836034 != 0x4fff)) {
            bVar1 = _DAT_c8836034 != 0xfff;
          }
        }
joined_r0xd9004e80:
        if (!bVar1) goto LAB_d9004f00;
      }
      else if ((_DAT_c8836034 & 0x7ff80000) == 0) {
LAB_d9004f00:
                    /* *******************
                       PGSR0 == 0x80000fff
                       ******************* */
        if ((ddrs.ddr_2t_mode != 0) && (ddrtype != '\x02')) {
                    /* 2T mode (ran on lepotato)                ↓ ACMDLR0 */
          ddrs.t_pub_aclcdlr = (ddrs.t_pub_aclcdlr - 0x18) + (_DAT_c88365a0 & 0x1ff);
          _DAT_c8836548 = 0x3f003f;
          if (ddrs.t_pub_aclcdlr < 0x3f) {
            _DAT_c8836548 = ddrs.t_pub_aclcdlr | ddrs.t_pub_aclcdlr * 65536;
          }
        }
        if (ddrtype != '\x02') {
          _DAT_c8836580 = ddrs.t_pub_aclcdlr;
        }
        ddrs______ = &ddrs;
        if ((ddrs.wr_adj_per[0] != '\0') && (ddrs.rd_adj_per[0] != '\0')) {
                    /* s_DQS-corr_enabled
                       
                       ACLCDLR */
          _DAT_c8836580 = _DAT_c8836580 & 0x1ff;
          if (_DAT_c8836580 == 0) {
            _DAT_c8836580 = 1;
          }
                    /*                           ↓ == 100 (lepotato) */
          _DAT_c8836580 = (_DAT_c8836580 * ddrs.wr_adj_per[0]) / 100 & 0x1ff;
                    /* ACBDLR0 */
          _DAT_c8836540 = _DAT_c8836540 & 0x1ff;
          if (_DAT_c8836540 == 0) {
            _DAT_c8836540 = 1;
          }
                    /*                           ↓ == 100 (lepotato) */
          _DAT_c8836540 = (_DAT_c8836540 * ddrs.wr_adj_per[1]) / 100 & 0x3f;
                    /* DDR0_PUB_DXxLCDLR1 */
          dxXlcdlr = (uint *)&DAT_c8836784;
          do {
                    /* >>dxXlcdlr1[2]
                       >I guess this would be bit 2 in readl(DDR0_PUB_DXxCDLR1)?
                       >
                       >TODO: figure this junk out.
                       Seems like it's actuallx dxXlcdlr[X] e.g. dx1lcdlr[1] -> DX1LCDLR1 ?? */
            tmp1 = *dxXlcdlr & 0x1ff;
            if (tmp1 == 0) {
              tmp1 = 1;
            }
            tmp2 = dxXlcdlr[2] & 0x1ff;
            read_adj_per__2 = ddrs______->rd_adj_per[2];
            if (tmp2 == 0) {
              tmp2 = 1;
            }
            *dxXlcdlr = (tmp1 * ddrs______->wr_adj_per[2]) / 100;
            tmp1 = (tmp2 * read_adj_per__2) / 100;
            dxXlcdlr[2] = tmp1;
            dxXlcdlr[3] = tmp1;
            dxXlcdlr = dxXlcdlr + 0x40;
                    /* loop until we reach DX3LCDLRx address */
                    /*   */
            ddrs______ = (ddr_set_t *)&ddrs______->ddr_type;
          } while (dxXlcdlr != (uint *)0xc8836b84);
        }
        ddrs______ = &ddrs;
        if ((ddrs.wr_adj_per_rank1[0] != '\0') && (ddrs.rd_adj_per_rank1[0] != '\0')) {
                    /* Not ran on lepotato */
          _DAT_c8836580 = _DAT_c8836580 & 0x1ff;
          if (_DAT_c8836580 == 0) {
            _DAT_c8836580 = 1;
          }
          _DAT_c8836580 = (_DAT_c8836580 * ddrs.wr_adj_per_rank1[0]) / 100 & 0x1ff;
          _DAT_c8836540 = _DAT_c8836540 & 0x1ff;
          if (_DAT_c8836540 == 0) {
            _DAT_c8836540 = 1;
          }
          _DAT_c8836540 = (_DAT_c8836540 * ddrs.wr_adj_per_rank1[1]) / 100 & 0x3f;
          dxXlcdlr = (uint *)&DAT_c8836784;
          do {
            tmp1 = *dxXlcdlr & 0x1ff;
            if (tmp1 == 0) {
              tmp1 = 1;
            }
            tmp2 = dxXlcdlr[2] & 0x1ff;
            read_adj_per__2 = ddrs______->rd_adj_per_rank1[2];
            if (tmp2 == 0) {
              tmp2 = 1;
            }
            *dxXlcdlr = (tmp1 * ddrs______->wr_adj_per_rank1[2]) / 100;
            tmp1 = (tmp2 * read_adj_per__2) / 100;
            dxXlcdlr[2] = tmp1;
            dxXlcdlr[3] = tmp1;
            dxXlcdlr = dxXlcdlr + 0x40;
            ddrs______ = (ddr_set_t *)&ddrs______->ddr_type;
          } while (dxXlcdlr != (uint *)0xc8836b84);
        }
                    /* PUB_PGCR6 */
        _DAT_c8836028 = 0x3f0f8;
        if (ddrtype_____ == 2) {
          _DAT_c8836028 = 0x5f0f8;
          _DAT_c8836680 = _DAT_c8836680 & 0xf301fffa | 1;
        }
        do {
                    /* Waits for PCTL_STAT and then writes GO to PCTL_SCTL?? */
        } while ((_DAT_c8839120 & 7) != 3);
        _DAT_c8839004 = 2;
        _DAT_c8836a0c = 0xfffc0000;
        _DAT_c883690c = 0xfffc0000;
        _DAT_c883680c = 0xfffc0000;
        _DAT_c883670c = 0xfffc0000;
        _DAT_c88364dc = 0;
        _DAT_c8836090 = ddrs.t_pub_dsgcr | 0x800004;
                    /* Some notes about what's at the top:
                       * PUB_DXxGCR3 => 0xfffc0000
                       * c8836090 is PUB_DSGCR (wow!)
                       * c88364dc is PUB_RANKIDR (new on gxl?) */
        return 0;
      }
    }
    if (err_report != 0) {
      serial_print(s_Training_Err_PGSR0__0x_d9009521,(ulong)_DAT_c8836034,0,s__Retry____d9009538);
      serial_print(s_DX0GSR0__0x_d9009543,(ulong)_DAT_c88367e0,0,s______d900954f);
      serial_print(s_DX1GSR0__0x_d9009555,(ulong)_DAT_c88368e0,0,s______d900954f);
      serial_print(s_DX2GSR0__0x_d9009561,(ulong)_DAT_c88369e0,0,s______d900954f);
      serial_print(s_DX3GSR0__0x_d900956d,(ulong)_DAT_c8836ae0,0,s______d900954f);
      serial_print(s_DX0GSR2__0x_d9009579,(ulong)_DAT_c88367e8,0,s______d900954f);
      serial_print(s_DX1GSR2__0x_d9009585,(ulong)_DAT_c88368e8,0,s______d900954f);
      serial_print(s_DX2GSR2__0x_d9009591,(ulong)_DAT_c88369e8,0,s______d900954f);
      serial_print(s_DX3GSR2__0x_d900959d,(ulong)_DAT_c8836ae8,0,s______d900954f);
      serial_print(s_DX0GSR3__0x_d90095a9,(ulong)_DAT_c88367ec,0,s______d900954f);
      serial_print(s_DX1GSR3__0x_d90095b5,(ulong)_DAT_c88368ec,0,s______d900954f);
      serial_print(s_DX2GSR3__0x_d90095c1,(ulong)_DAT_c88369ec,0,s______d900954f);
      serial_print(s_DX3GSR3__0x_d90095cd,(ulong)_DAT_c8836aec,0,s______d900954f);
    }
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_pre_init(int is_lpddr2)

{
  byte bVar1;
  uchar uVar2;
  ulong i;
  int iVar3;
  ushort uVar4;
  ddr_timing_t *ddrt_p_;
  byte ddrtype_;
  uint tmp1;
  uint tmp2;
  uint tmp3;
  byte tmp3_;
  ushort tmp4_ddrclk_clal;
  short tmp_val;
  
  ddrs.t_pctl0_mcfg = ddrs.t_pctl0_mcfg & 0xffffff8f | (uint)ddrtype << 4;
                    /* Irrelevant for U-Boot SPL hacking */
  if ((ushort)(ddrs.ddr_clk - 20) < 513) {
    ddrs.ddr_timing_ind = 7;
  }
  else if ((ushort)(ddrs.ddr_clk - 533) < 134) {
    ddrs.ddr_timing_ind = 9;
  }
  else if ((ushort)(ddrs.ddr_clk - 667) < 133) {
    ddrs.ddr_timing_ind = 11;
  }
  else if ((ushort)(ddrs.ddr_clk - 800) < 700) {
    ddrs.ddr_timing_ind = 13;
  }
  else {
    serial_puts(s_DDR_clk_err____d90095f2);
    reset_system();
  }
                    /* CONFIG_DDR_TYPE_DDR4 */
  if (ddrtype == 1) {
                    /* CONFIG_DDR_TIMMING_DDR4_2400 */
    ddrs.ddr_timing_ind = 18;
    if ((ddrs.ddr_func >> 4 & 1) != 0) {
                    /* DDR_FUNC_DDR4_TIMING_TEST */
      ddrs.ddr_timing_ind = 15;
    }
  }
  else if (ddrtype == 2) {
    ddrs.ddr_timing_ind = '\x03';
  }
  i = 0;
  do {
    if (ddrt[i].identifier == ddrs.ddr_timing_ind) {
      ddrt_p = ddrt + (i & 0xffffffff);
      break;
    }
    i = i + 1;
  } while (i != 7);
  if (ddrt_p == NULL) {
    serial_puts(s_DDR_Timing_err____d9009602);
    reset_system();
  }
                    /* This is where the fun actually starts! */
  ddrtype_ = ddrtype;
  ddrt_p_ = ddrt_p;
  tmp4_ddrclk_clal = ddrs.ddr_clk;
                    /* CONFIG_DDR_TYPE_DDR4 */
  if (ddrtype == 1) {
    ddrt_p->cfg_ddr_rtw = 8;
    if (ddrs.ddr_clk < 801) {
      ddrt_p_->cfg_ddr_tccdl = 4;
    }
  }
  else {
                    /* CONFIG_DDR_TYPE_LPDDR3 */
    if (ddrtype == 2) {
      ddrs.t_pub_dsgcr = 0x2064df;
      ddrs.t_pub_dxccr = 0x20581ee4;
      ddrs.t_pub_dcr = 0x89;
      ddrs.t_pctl0_mcfg = 0x222f29;
      tmp1 = (uint)ddrs.ddr_clk;
      _DAT_d900a818 = tmp1 / 400;
      _DAT_d900a824 = (tmp1 * 0x37) / 20000 + 1;
      ddrt_p->cfg_ddr_tdqsck = (uchar)_DAT_d900a818;
      if (tmp4_ddrclk_clal < 0x191) {
        ddrt_p_->cfg_ddr_cl = '\x06';
        uVar2 = '\x03';
LAB_d9005560:
        ddrt_p_->cfg_ddr_cwl = uVar2;
      }
      else {
        if ((ushort)(tmp4_ddrclk_clal - 0x191) < 0x85) {
          ddrt_p_->cfg_ddr_cl = '\b';
          uVar2 = '\x04';
          goto LAB_d9005560;
        }
        if ((ushort)(tmp4_ddrclk_clal - 0x216) < 0x43) {
          ddrt_p_->cfg_ddr_cl = '\t';
          uVar2 = '\x05';
          goto LAB_d9005560;
        }
        if ((ushort)(tmp4_ddrclk_clal - 0x259) < 0x43) {
          uVar2 = '\n';
LAB_d9005538:
          ddrt_p_->cfg_ddr_cl = uVar2;
          uVar2 = '\x06';
          goto LAB_d9005560;
        }
        if ((ushort)(tmp4_ddrclk_clal - 0x29c) < 0x42) {
          uVar2 = '\v';
          goto LAB_d9005538;
        }
        if (((ushort)(tmp4_ddrclk_clal - 0x2de) < 0x43) ||
           ((ushort)(tmp4_ddrclk_clal - 0x321) < 0x85)) {
          uVar2 = '\f';
          goto LAB_d9005538;
        }
        if ((tmp1 - 0x3a6 & 0xffff) < 0x29b) {
          ddrt_p_->cfg_ddr_cl = '\x10';
          uVar2 = '\b';
          goto LAB_d9005560;
        }
      }
      tmp3_ = ddrt_p_->cfg_ddr_wr;
      if (8 < tmp3_) {
        tmp3_ = 8;
      }
      ddrt_p_->cfg_ddr_wr = tmp3_;
      ddrt_p_->cfg_ddr_rp = ddrt_p_->cfg_ddr_cl;
      ddrt_p_->cfg_ddr_rcd = ddrt_p_->cfg_ddr_cl;
      if (is_lpddr2 != 0) {
        ddrt_p_->cfg_ddr_ras = (char)((ddrt_p_->cfg_ddr_ras * tmp1) / 800) + '\x01';
        ddrt_p_->cfg_ddr_faw = (char)((ddrt_p_->cfg_ddr_faw * tmp1) / 800) + '\x01';
        ddrt_p_->cfg_ddr_rfc = (short)((int)(ddrt_p_->cfg_ddr_rfc * tmp1) / 800) + 1;
      }
      if (0x1ff < ddrt_p_->cfg_ddr_rfc) {
        ddrt_p_->cfg_ddr_rfc = 0x1ff;
      }
    }
  }
  tmp3_ = ddrchannelset;
  ddrs.t_pctl0_ppcfg = 0x1e0;
  ddrs.t_pctl0_dfiodtcfg = 8;
                    /* ddrs.dmc_ctrl = 0x6d;
                                     = (5 << 0) | <- size (set to max, later set to DRAM size;
                                                           see datasheet)
                                       (5 << 3) | <- same here
                                       (1 << 6)   <- channel 0 only on GXBB, undocumented on GXL */
  ddrs.ddr_dmc_ctrl = 0x6d;
                    /* CONFIG_DDR0_RANK0 */
  tmp1 = (uint)ddrchannelset;
  if (ddrchannelset == 2) {
    ddrs.t_pub_dtcr1 = ddrs.t_pub_dtcr1 & 0xfffdffff;
    ddrs.t_pctl0_dfiodtcfg1 = 0x6080000;
  }
  else if (ddrchannelset == 3) {
                    /* CONFIG_DDR0_RANK01 */
    ddrs.t_pctl0_dfiodtcfg = 0xc0c;
    ddrs.t_pctl0_dfiodtcfg1 = 0x6080000;
    ddrs.ddr_dmc_ctrl = 0x20006d;
    ddrs.t_pub_dtcr1 = ddrs.t_pub_dtcr1 | 0x20000;
    bVar1 = ddrt_p->cfg_ddr_tccdl;
LAB_d900568c:
    ddrt_p->cfg_ddr_tccdl = bVar1 + 1;
  }
  else if (ddrchannelset == 1) {
                    /* CONFIG_DDR0_16BIT
                       
                       tmp1 == 1 so: dmc_ctrl = 1 << 16 | 5 << 0 | 5 << 3 | 1 << 6
                       and: 1 << 16 <- 16-bit rank (coincidence isn't it?) */
    ddrs.ddr_dmc_ctrl = tmp1 << 16 | 0x6d;
    ddrs.t_pctl0_dfiodtcfg1 = 0x80000;
    ddrs.t_pctl0_ppcfg = 0x1fd;
    bVar1 = ddrt_p->cfg_ddr_tccdl;
    if ((bVar1 & 1) != 0) goto LAB_d900568c;
  }
  else if (ddrchannelset == 4) {
    ddrs.t_pctl0_ppcfg = 0x1fd;
    ddrs.t_pctl0_dfiodtcfg1 = 0x80000;
    ddrs.t_pub_dtcr1 = ddrs.t_pub_dtcr1 | 0x20000;
    ddrs.ddr_dmc_ctrl = 0x21006d;
  }
  ddrt_p_ = ddrt_p;
  tmp4_ddrclk_clal = ddrs.ddr_clk;
  ddrs.t_pub_dsgcr = ddrs.t_pub_dsgcr | 0x800004;
  if (ddrtype_ == 0) {
                    /* DDR3 */
    if (932 < ddrs.ddr_clk) {
      ddrt_p->cfg_ddr_cl = 14;
      ddrt_p_->cfg_ddr_rp = 14;
      ddrt_p_->cfg_ddr_rcd = 14;
      ddrt_p_->cfg_ddr_cwl = 10;
    }
    goto LAB_d90058c4;
  }
  if (ddrtype_ != 1) goto LAB_d90058c4;
                    /* ************************** DDR4 ************************** */
  ddrs.t_pub_dtcr0 = ddrs.t_pub_dtcr0 | 0x40;
  ddrs.t_pub_dcr = 0x1800040c;
  ddrs.ddr_dmc_ctrl = ddrs.ddr_dmc_ctrl | 0x500000;
  if (is_lpddr2 != 0) {
    tmp2 = (uint)ddrs.ddr_clk;
                    /* LPDDR2 */
    ddrt_p->cfg_ddr_ras = (char)((ddrt_p->cfg_ddr_ras * tmp2) / 0x4b0) + '\x01';
    ddrt_p_->cfg_ddr_faw = (char)((ddrt_p_->cfg_ddr_faw * tmp2) / 0x4b0) + '\x01';
    tmp2 = (uint)tmp4_ddrclk_clal;
    ddrt_p_->cfg_ddr_rfc = (short)((int)(ddrt_p_->cfg_ddr_rfc * tmp2) / 0x4b0) + 10;
    ddrt_p_->cfg_ddr_wlo = (uchar)((ddrt_p_->cfg_ddr_wlo * tmp2) / 0x4b0);
    ddrt_p_->cfg_ddr_wr = (uchar)((ddrt_p_->cfg_ddr_wr * tmp2) / 0x4b0);
  }
  ddrt_p_ = ddrt_p;
  if (0x1ff < ddrt_p->cfg_ddr_rfc) {
    ddrt_p->cfg_ddr_rfc = 0x1ff;
  }
  if (ddrs.ddr_clk < 667) {
    uVar2 = 10;
LAB_d90057f0:
    ddrt_p_->cfg_ddr_cl = uVar2;
    ddrt_p_->cfg_ddr_tccdl = '\x04';
    uVar2 = 9;
LAB_d9005860:
    ddrt_p_->cfg_ddr_cwl = uVar2;
  }
  else {
    if (ddrs.ddr_clk < 800) {
      uVar2 = '\f';
      goto LAB_d90057f0;
    }
    if (ddrs.ddr_clk < 933) {
      ddrt_p_->cfg_ddr_cl = 14;
      ddrt_p_->cfg_ddr_tccdl = 5;
      uVar2 = '\n';
      goto LAB_d9005860;
    }
    if (ddrs.ddr_clk < 1200) {
      ddrt_p_->cfg_ddr_cl = 0x10;
      ddrt_p_->cfg_ddr_tccdl = 0x6;
      uVar2 = 12;
      goto LAB_d9005860;
    }
    if (ddrs.ddr_clk < 1600) {
      ddrt_p_->cfg_ddr_cl = '\x12';
      ddrt_p_->cfg_ddr_tccdl = '\a';
      uVar2 = '\x0e';
      goto LAB_d9005860;
    }
  }
  if ((tmp1 == 4) || (tmp3_ == 1)) {
                    /* o.O */
    ddrt_p_->cfg_ddr_tccdl = 4;
  }
  uVar2 = ddrt_p_->cfg_ddr_cl;
  tmp3_ = ddrt_p_->cfg_ddr_wr + 1 & 0xfe;
  ddrt_p_->cfg_ddr_rp = uVar2;
  ddrt_p_->cfg_ddr_rcd = uVar2;
  if (0x15 < tmp3_) {
    tmp3_ = 0x18;
  }
  ddrt_p_->cfg_ddr_wr = tmp3_;
  ddrt_p_->cfg_ddr_rtp = ddrt_p_->cfg_ddr_wr >> 1;
  ddrt_p_->cfg_ddr_rrd = ddrt_p_->cfg_ddr_tccdl;
  ddrt_p_->cfg_ddr_rc = uVar2 + ddrt_p_->cfg_ddr_ras;
LAB_d90058c4:
                    /* ************************************************************************** */
  ddrt_p_ = ddrt_p;
  ddrs.t_pctl0_dfitctrldelay = 2;
  ddrs.t_pctl0_dfitphywrdata = 2;
                    /* Is ddrs.t_pctl0_dfitphywrlta/ddrs.t_pctl0_dfitphyrdlat redefined later? */
  ddrs.t_pctl0_dfitphywrlta = ((ushort)ddrt_p->cfg_ddr_cwl + (ushort)ddrt_p->cfg_ddr_al) - 2;
  tmp_val = (ushort)ddrt_p->cfg_ddr_cl + (ushort)ddrt_p->cfg_ddr_al;
  ddrs.t_pctl0_dfitphyrdlat = 0x16;
  if (ddrs.ddr_type == 2) {
    ddrs.ddr_2t_mode = 1;
    ddrs.t_pctl0_rsth_us = 200;
    ddrs.t_pctl0_dfitphywrlta = ((ushort)ddrt_p->cfg_ddr_cwl + (ushort)ddrt_p->cfg_ddr_al) - 1;
    tmp_val = (ushort)ddrt_p->cfg_ddr_cl + (ushort)ddrt_p->cfg_ddr_tdqsck +
              (ushort)ddrt_p->cfg_ddr_al;
  }
  ddrs.t_pctl0_dfitrddataen = tmp_val - 4;
  if (ddrtype_ == 0) {
                    /* DDR3 */
    tmp3_ = ddrt_p->cfg_ddr_wr;
    tmp1 = ddrt_p->cfg_ddr_cl - 4;
    if (tmp3_ < 9) {
      tmp2 = tmp3_ - 4;
    }
    else {
      tmp2 = (uint)(tmp3_ >> 1);
    }
    ddrs.t_pub_mr[0] =
         (ushort)((tmp2 & 7) << 9) |
         (ushort)((int)(tmp1 & 8) >> 1) | (ushort)((tmp1 & 7) << 4) | 0x1c00;
    tmp4_ddrclk_clal = 0;
    if (ddrt_p->cfg_ddr_al != 0) {
      tmp4_ddrclk_clal = (ushort)(((uint)ddrt_p->cfg_ddr_cl - (uint)ddrt_p->cfg_ddr_al & 3) << 3);
    }
    ddrs.t_pub_mr[1] =
         (ushort)ddrs.ddr_drv << 1 | 0x80 | (ushort)((ddrs.ddr_odt & 1) << 2) |
         (ushort)((ddrs.ddr_odt >> 1 & 1) << 6) | (ushort)((ddrs.ddr_odt >> 2 & 1) << 9) |
         tmp4_ddrclk_clal;
    ddrs.t_pub_mr[3] = 0;
    ddrs.t_pub_mr[2] = (ushort)((ddrt_p->cfg_ddr_cwl - 5 & 7) << 3) | 0x40;
    ddrs.t_pub_mr[5] = 0x420;
    ddrs.t_pub_mr[4] = 0;
    ddrs.t_pub_mr[6] = 0x800;
  }
  else if (ddrtype_ == 1) {
                    /* DDR4! */
    ddrs.t_pub_pgcr3 = ddrs.t_pub_pgcr3 | 0x4000000;
    ddrs.t_pub_mr[5] = 0x400;
    if ((ddrs.ddr_func >> 6 & 1) != 0) {
                    /* Enabled in lafrite, but let's not bother with that just yet. */
      ddrs.t_pub_mr[5] = 0x1400;
      ddrt_p->cfg_ddr_cl = ddrt_p->cfg_ddr_cl + '\x02';
      ddrs.t_pctl0_dfitrddataen = ddrs.t_pctl0_dfitrddataen + 2;
      if (0xe < ddrs.t_pctl0_dfitrddataen) {
        ddrs.t_pctl0_dfitrddataen = 0xe;
      }
    }
    tmp3_ = ddrt_p_->cfg_ddr_cl;
    tmp1 = (uint)tmp3_;
    if (tmp3_ < 0x11) {
                    /* tmp1 = t.cl - 9 >> 1 */
      tmp4_ddrclk_clal = 0;
      if ((tmp3_ & 1) == 0) {
        tmp4_ddrclk_clal = 4;
      }
      tmp1 = tmp1 - 9 >> 1;
    }
    else {
      tmp4_ddrclk_clal = 0;
      if (0x14 < tmp1) {
        tmp4_ddrclk_clal = 4;
      }
      tmp1 = tmp1 - 2 >> 2;
    }
                    /* lf: MR[0] = 4 | (((t.cl - 9 >> 1) & 7) << 4) | (((t.wr - 10 >> 1 & 7) << 9))
                        */
    uVar4 = 0xc00;
    if (ddrt_p_->cfg_ddr_wr < 0x16) {
      uVar4 = (ushort)((ddrt_p_->cfg_ddr_wr - 10 >> 1 & 7) << 9);
    }
    ddrs.t_pub_mr[0] = tmp4_ddrclk_clal | (ushort)((tmp1 & 7) << 4) | uVar4;
                    /* *********************************** */
    uVar4 = (ushort)ddrs.ddr4_odt << 8 | (ushort)ddrs.ddr4_drv << 1;
    tmp4_ddrclk_clal = 0;
    if (ddrt_p_->cfg_ddr_al != 0) {
      tmp4_ddrclk_clal = (ushort)(((uint)ddrt_p_->cfg_ddr_cl - (uint)ddrt_p_->cfg_ddr_al & 3) << 3);
    }
    ddrs.t_pub_mr[1] = uVar4 | 0x81 | tmp4_ddrclk_clal;
    tmp3_ = ddrt_p_->cfg_ddr_cwl;
    if (tmp3_ < 0xd) {
      tmp1 = tmp3_ - 9;
    }
    else {
      tmp1 = tmp3_ - 6 >> 1;
    }
    ddrs.t_pub_mr[2] = (ushort)((tmp1 & 7) << 3) | 0xc0;
    ddrs.t_pub_mr[4] = 8;
    pub_soc_vref_dram_vref_right8 = (undefined)(ddrs.t_pub_soc_vref_dram_vref >> 8);
    pub_soc_vref_dram_vref = (char)ddrs.t_pub_soc_vref_dram_vref;
    ddrs.t_pub_mr[3] = 0;
    tmp1 = (ddrs.t_pub_zq1pr >> 0xc & 0xf) + 1;
    tmp2 = 0;
    if (tmp1 != 0) {
      tmp2 = 0x1e0 / tmp1;
    }
    pub_zq1pr = (undefined2)tmp2;
    tmp1 = (ddrs.t_pub_zq1pr >> 0x10 & 0xf) + 1;
    tmp3 = 0;
    if (tmp1 != 0) {
      tmp3 = 0x1e0 / tmp1;
    }
    DAT_d900a7fc = (undefined2)tmp3;
    tmp1 = (uint)(uVar4 >> 8);
    iVar3 = 0x22;
    if (((ushort)ddrs.ddr4_drv << 1 & 2) != 0) {
      iVar3 = 0x30;
    }
    DAT_d900a7f8 = (undefined2)iVar3;
    tmp1 = (tmp1 & 2) + ((tmp1 & 7) >> 2) + (tmp1 & 1) * 4;
    DAT_d900a7fe = 0xf0;
    if ((tmp1 != 0) && (DAT_d900a7fe = 0, tmp1 != 0)) {
      DAT_d900a7fe = (ushort)(0xf0 / tmp1);
    }
    if ((uVar4 & 0x700) == 0) {
      DAT_d900a7fe = 10000;
    }
    tmp1 = 0;
    if (tmp2 + DAT_d900a7fe != 0) {
      tmp1 = (tmp2 * 0x32) / (tmp2 + DAT_d900a7fe);
    }
    tmp2 = 0;
    if (iVar3 + tmp3 != 0) {
      tmp2 = (uint)(iVar3 * 0x32) / (iVar3 + tmp3);
    }
    DAT_d900a7f0 = ((tmp2 + 0x32) * 1000 - 0xac26) / 0x2ba;
    iVar3 = (tmp1 + 0x32) * 1000;
    if (tmp1 + 0x32 < 0x3c) {
      DAT_d900a7ec = (iVar3 - 45000U) / 0x28a | 0x40;
    }
    else {
      DAT_d900a7ec = (iVar3 - 60000U) / 0x28a;
    }
    if (pub_soc_vref_dram_vref == '\0') {
      ddrs.t_pub_mr[6] = (ushort)DAT_d900a7ec | 0x80 | (ddrt_p_->cfg_ddr_tccdl - 4) * 0x400;
    }
    else {
      ddrs.t_pub_mr[6] = ddrs.t_pub_soc_vref_dram_vref & 0xff | (ddrt_p_->cfg_ddr_tccdl - 4) * 0x400
      ;
    }
    pub_mr__7 = (undefined)ddrs.t_pub_mr[7];
  }
  else if (ddrtype_ == 2) {
    if (0x1ff < ddrt_p->cfg_ddr_rfc) {
      ddrt_p->cfg_ddr_rfc = 0x1ff;
    }
    tmp1 = (uint)ddrt_p_->cfg_ddr_wr;
    if (ddrt_p_->cfg_ddr_wr < 10) {
      tmp2 = tmp1 - 2;
    }
    else {
      tmp2 = tmp1 - 10;
    }
    ddrs.t_pub_mr[1] = (ushort)((tmp2 & 7) << 5) | 3;
    tmp4_ddrclk_clal = 0;
    if (9 < tmp1) {
      tmp4_ddrclk_clal = 0x10;
    }
    ddrs.t_pub_mr[2] = ddrt_p_->cfg_ddr_cl - 2 | tmp4_ddrclk_clal;
    ddrs.t_pub_mr[3] = (ushort)ddrs.ddr_drv;
    _odt_operatorOR_1shift2 = (uint)(ddrs.ddr_odt | 4);
    _DAT_d900a81c = 0xf;
    _DAT_d900a810 = 0x11;
    _DAT_d900a820 = 500;
    _DAT_d900a814 = 0xc;
    ddrs.t_pub_dtcr0 = 0x80003187;
    ddrs.t_pctl0_dfiodtcfg = 0xc0c;
    ddrs.t_pctl0_dfiodtcfg1 = ddrt_p_->cfg_ddr_cwl - 3 | 0x80a0000;
  }
  ddrs.t_pub_dtpr[0] =
       (uint)ddrt_p_->cfg_ddr_ras << 16 | (uint)ddrt_p_->cfg_ddr_rp << 8 |
       (uint)ddrt_p_->cfg_ddr_rtp | (uint)ddrt_p_->cfg_ddr_rrd << 24;
  ddrs.t_pub_dtpr[1] =
       (uint)ddrt_p_->cfg_ddr_wlmrd << 24 | (uint)ddrt_p_->cfg_ddr_faw << 16 |
       (uint)ddrt_p_->cfg_ddr_mrd | (ddrt_p_->cfg_ddr_mod - 12) * 0x100;
  ddrs.t_pub_dtpr[2] = (uint)CONCAT12(ddrt_p_->cfg_ddr_cke,ddrt_p_->cfg_ddr_xs);
  ddrs.t_pub_dtpr[3] = (uint)ddrt_p_->cfg_ddr_dllk << 0x10 | 0x40000000;
  ddrs.t_pub_dtpr[4] = ddrt_p_->cfg_ddr_xp | 0x800 | (uint)ddrt_p_->cfg_ddr_rfc << 0x10;
  ddrs.t_pub_dtpr[5] =
       (uint)ddrt_p_->cfg_ddr_rc << 0x10 | (uint)ddrt_p_->cfg_ddr_rcd << 8 |
       (uint)ddrt_p_->cfg_ddr_wtr;
  if (ddrtype_ == 1) {
    ddrs.t_pub_dtpr[1] =
         (uint)ddrt_p_->cfg_ddr_wlmrd << 0x18 | (uint)ddrt_p_->cfg_ddr_faw << 0x10 |
         (uint)ddrt_p_->cfg_ddr_mrd | (ddrt_p_->cfg_ddr_mod - 0x18) * 0x100;
  }
  else if (ddrtype_ == 2) {
    ddrs.t_pub_dtpr[3] = _DAT_d900a818 | 0x42000000 | _DAT_d900a824 << 8 | ddrs.t_pub_dtpr[3];
  }
  ddrs.t_pctl0_mcfg1 = ddrs.t_pctl0_mcfg1 & 0xfffffcff;
  return;
}



void ddr_init(void)

{
  bool bVar1;
  undefined8 ret_;
  uint ddr_channel_mode_is_lpddr2;
  uchar ddrs_channelset;
  
  ddrs_channelset = ddrs.ddr_channel_set;
  bVar1 = ddrs.ddr_type == '\x0f';
  if (bVar1) {
    ddrtype = 0;
  }
  else {
    ddrtype = ddrs.ddr_type;
  }
  ddr_channel_mode_is_lpddr2 = 1;
  do {
    if (ddrtype == 1) {
      ddrs.ddr_clk = ddrs.ddr4_clk;
    }
    ddr_init_pll();
    ddrchannelset = ddrs_channelset;
    if ((ddrs_channelset == 15) && (plls.pxp == 0)) {
      ddrchannelset = 3;
      bVar1 = true;
    }
    do {
      ddrs.ddr_channel_set = ddrchannelset;
      serial_puts(s_DDR3_d9009398 + (ulong)ddrtype * 7);
      serial_puts(s__chl__d9009615);
      serial_puts(s_Rank0_16bit_d90093b8 + (long)(int)(ddrchannelset - 1) * 0xd);
      if (bVar1) {
        ddr_channel_mode_is_lpddr2 = (uint)(ddrchannelset == 3);
      }
      ddr_pre_init(ddr_channel_mode_is_lpddr2);
      serial_print(s____d900961c,(ulong)ddrs.ddr_clk,1,s_MHz_d9009620);
      if (!bVar1) {
        serial_puts(s__d9009869);
        ddr_init_pctl(1);
        return;
      }
      ret_ = ddr_init_pctl(0);
      if ((int)ret_ == 0) {
        serial_puts(s____PASS_d9009624);
        return;
      }
      serial_puts(s____FAIL_d900962d);
    } while ((ddrs_channelset == '\x0f') && (ddrchannelset = ddrchannelset - 1, ddrchannelset != 0))
    ;
    if ((ddrs.ddr_type != '\x0f') || (ddrtype = ddrtype + 1, ddrtype != 1)) {
      serial_puts(s_DDR_init_failed____Reset____d9009636);
      reset_system();
      do {
                    /* WARNING: Do nothing block with infinite loop */
      } while( true );
    }
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_test(void)

{
  int iVar1;
  undefined8 uVar2;
  ulong uVar3;
  
  iVar1 = check_if_remap_addr(0);
  if (iVar1 == 0x21) {
    _DAT_c1300000 = 1;
    InstructionSynchronizationBarrier();
    DataMemoryBarrier(3,3);
  }
  uVar2 = memTestDataBus((uint *)(ulong)ddrs.ddr_base_addr);
  if ((int)uVar2 == 0) {
    serial_puts(s_DataBus_test_pass__d900966b);
  }
  else {
    serial_puts(s_DataBus_test_failed____d9009653);
    reset_system();
  }
  uVar3 = memTestAddrBus((uint *)(ulong)ddrs.ddr_base_addr,(ulong)((uint)ddrs.ddr_size << 0x14));
  if ((int)uVar3 == 0) {
    serial_puts(s_AddrBus_test_pass__d9009697);
  }
  else {
    serial_puts(s_AddrBus_test_failed____d900967f);
    reset_system();
  }
  if ((DAT_d900a805 | ddrs.ddr_full_test) != 0) {
    FUN_d9001a6c();
    uVar3 = FUN_d9008d6c((uint *)(ulong)ddrs.ddr_base_addr,(ulong)((uint)ddrs.ddr_size << 0x14));
    if ((int)uVar3 == 0) {
      serial_puts(s_Device_test_pass__d90096c2);
    }
    else {
      serial_puts(s_Device_test_failed____d90096ab);
      reset_system();
    }
  }
  iVar1 = check_if_remap_addr(0);
  if (iVar1 == 0x21) {
    _DAT_c1300000 = 0;
    InstructionSynchronizationBarrier();
    DataMemoryBarrier(3,3);
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 ddr_init_all(int param_1)

{
  print_ddr_params();
  ddr_init();
  ddr_init_dmc();
  ddr_print_setting();
  if (plls.pxp == 0) {
    if (param_1 != 0) {
      ddr_test();
    }
  }
  else {
    _DAT_c810025c = _DAT_c810025c | 0x40000000;
  }
  ddr_setup_ssc();
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void pinmux_init(void)

{
  uint tmp;
  uint boot_device;
  char *s;
  
                    /* 0xc88344c8 -> PinMux REG6 */
  tmp = _DAT_c88344c8;
  _DAT_c883444c = _DAT_c883444c | 0x3f00000;
  _DAT_c8834448 = _DAT_c8834448 | 0x3f00000;
  _DAT_c88344c8 = _DAT_c88344c8 & 0xffffffc0;
  if ((_DAT_c8834450 >> 0x18 & 1) == 0) {
    _DAT_c8100014 = _DAT_c8100014 & 0xffffe7ff;
    tmp = get_boot_device();
    if (tmp == 4) {
      _DAT_c88344c8 = _DAT_c88344c8 | 0x31c;
    }
    else {
      _DAT_c88344c8 = _DAT_c88344c8 | 0x300;
    }
    s = s__sdio_debug_board_detected_d90096d5;
  }
  else {
    boot_device = get_boot_device();
    if (boot_device != 4) {
                    /* Not booted from an SD card */
      return;
    }
    s = s__no_sdio_debug_board_detected_d90096f1;
    _DAT_c88344c8 = tmp;
  }
  serial_puts(s);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900639c(void)

{
  ulong uVar1;
  
                    /* SEC_AO_RTI_STATUS_REG2 */
  if (_DAT_da100008 != 0) {
    _DAT_da100008 = 0;
  }
                    /* SEC_AO_RTI_STATUS_REG3 */
  if ((_DAT_da10001c >> 0xc & 0xf) == 8) {
    _DAT_da100008 = 0x4f5244b1;
    ddr_init_all(0);
    _DAT_da10001c = _DAT_da10001c & 0xffff0fff;
    serial_puts(s_Enter_Crash_Dump__d9009710);
    FUN_d9001a6c();
    if (_DAT_d904050c == -0x6bfffb02) {
      uVar1 = 0x3c58;
    }
    else if (_DAT_d904050c == -0x6bffe734) {
      uVar1 = 0x452c;
    }
    else if (_DAT_d904050c == 0x52800022) {
      uVar1 = 0x3d4c;
    }
    else {
      uVar1 = 0x3c24;
    }
    check_handler(uVar1 | 0xd9040000);
    return;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900648c(void)

{
  ddr_timing_t *pdVar1;
  
  _DAT_da838438 = 0x10d510d5;
  _DAT_da8384d8 = 0x5ff55fff;
  _DAT_da8384dc = 0x5ff55fff;
  _DAT_da8384d4 = 5;
  _DAT_da8384d0 = 0x157f15;
  _DAT_da838494 = 0xffffffff;
  _DAT_da8384cc = 0x55555555;
  _DAT_da8384c8 = 0x55555555;
  _DAT_da838448 = 0xffffffff;
  _DAT_da838444 = 0x55555555;
  _DAT_da838440 = 0x55555555;
  _DAT_da83844c = 0xffffffff;
  _DAT_da838450 = 0xffffffff;
  _DAT_da838454 = 0x55555555;
  _DAT_da838458 = 0x55555555;
  if ((PTR_d900a788 != NULL) || (pdVar1 = PTR_d900a790, PTR_d900a790 != NULL)) {
    _DAT_da838404 =
         ((int)PTR_d900a788 + (int)PTR_d900a790) - 1U & 0xffff0000 |
         (uint)((ulong)PTR_d900a788 >> 0x10) & 0xffff;
    _DAT_da83841c = _DAT_da83841c | 1;
    pdVar1 = (ddr_timing_t *)0x1;
  }
  if (((PTR_d900a798 == NULL) && (PTR_d900a7a0 == NULL)) && ((int)pdVar1 == 0)) {
    return;
  }
  do {
  } while( true );
}



void FUN_d9006654(undefined8 *param_1,ulong param_2)

{
  ddr_timing_t *pdVar1;
  
  pdVar1 = (ddr_timing_t *)param_1[1];
  (&PTR_d900a788)[(param_2 & 0xffffffff) * 2] = (ddr_timing_t *)*param_1;
  (&PTR_d900a790)[(param_2 & 0xffffffff) * 2] = pdVar1;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9006678_nandrelated(void)

{
  _DAT_c8834528 = 0xffff87ff;
  _DAT_c88344f0 = 0xffff8700;
  _DAT_c88344cc = 0x800000ff;
  return;
}



/* WARNING: Removing unreachable block (ram,0xd9006790) */
/* WARNING: Removing unreachable block (ram,0xd9006794) */
/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 FUN_d90066ac(void)

{
  _DAT_d0074800 = 0x33800;
  return 0;
}



void FUN_d90067ac(uint param_1,undefined4 *param_2)

{
  uint uVar1;
  int iVar2;
  undefined4 uVar3;
  ulong uVar4;
  uint uVar5;
  ulong uVar6;
  long lVar7;
  byte local_20 [4];
  undefined local_1c;
  undefined local_1b;
  undefined local_1a;
  undefined local_19;
  undefined local_18;
  undefined local_17;
  undefined local_16;
  undefined local_15;
  undefined local_14;
  undefined local_13;
  undefined local_12;
  undefined local_11;
  undefined local_10;
  undefined local_f;
  undefined local_e;
  undefined local_d;
  undefined local_c;
  undefined local_b;
  undefined local_a;
  undefined local_9;
  undefined local_8;
  undefined local_7;
  undefined local_6;
  undefined local_5;
  undefined local_4;
  undefined local_3;
  undefined local_2;
  undefined local_1;
  
  local_1a = 0x78;
  local_19 = 0x78;
  local_17 = 0x76;
  local_e = 0x76;
  local_b = 0x76;
  local_20[0] = 4;
  local_20[1] = 4;
  local_16 = 0x74;
  local_14 = 8;
  local_13 = 8;
  local_d = 0x74;
  local_9 = 0x70;
  local_20[2] = 0x7c;
  local_20[3] = 0x7e;
  local_1c = 0;
  local_1b = 0x7c;
  local_18 = 0x7c;
  local_15 = 0x72;
  local_12 = 0;
  local_11 = 0;
  local_10 = 0xb;
  local_f = 0x7e;
  local_c = 0x10;
  local_a = 0x72;
  local_8 = 2;
  local_7 = 0;
  local_6 = 0x7e;
  local_5 = 0x7c;
  local_4 = 0;
  local_3 = 0;
  local_2 = 0;
  local_1 = 0;
  uVar1 = param_1;
  if (param_1 == 7) {
    param_1 = 0xff;
LAB_d90068c0:
    uVar4 = 0;
  }
  else {
    if (param_1 == 0xff) {
      uVar1 = 7;
      goto LAB_d90068c0;
    }
    uVar5 = 0;
    if (7 < param_1) goto LAB_d9006a34;
    if (param_1 == 0) {
      *param_2 = 0x1785c;
      param_2[1] = 0x178c5;
      param_2[2] = 0x33800;
      uVar4 = 3;
    }
    else {
      uVar4 = 0;
    }
  }
  lVar7 = 0;
  uVar6 = uVar4;
  do {
    param_2[uVar6] = 0x17855;
    iVar2 = (int)uVar6;
    param_2[iVar2 + 1] = 0x33802;
    param_2[iVar2 + 2] = (int)lVar7 + 4U | 0x1b800;
    param_2[iVar2 + 3] = 0x33802;
    param_2[iVar2 + 4] = local_20[uVar1 * 4 + (int)lVar7] | 0x13800;
    lVar7 = lVar7 + 1;
    param_2[iVar2 + 5] = 0x33800;
    uVar6 = (ulong)(iVar2 + 6);
  } while (lVar7 != 4);
  iVar2 = (int)uVar4;
  param_2[iVar2 + 0x18] = 0x17855;
  param_2[iVar2 + 0x19] = 0x33802;
  param_2[iVar2 + 0x1a] = 0x1b80d;
  param_2[iVar2 + 0x1b] = 0x33802;
  param_2[iVar2 + 0x1c] = 0x13800;
  uVar1 = iVar2 + 0x1e;
  param_2[iVar2 + 0x1d] = 0x33800;
  if (param_1 == 6) {
    param_2[uVar1] = 0x178b3;
    uVar1 = iVar2 + 0x20;
    param_2[iVar2 + 0x1f] = 0x33800;
LAB_d90069f8:
    param_2[uVar1] = 0x17826;
    param_2[uVar1 + 1] = 0x1785d;
    uVar5 = uVar1 + 3;
    uVar3 = 0x33800;
    uVar4 = (ulong)(uVar1 + 2);
  }
  else {
    if (param_1 != 0xff) goto LAB_d90069f8;
    param_2[uVar1] = 0x178ff;
    param_2[iVar2 + 0x1f] = 0x33802;
    uVar5 = iVar2 + 0x21;
    uVar4 = (ulong)(iVar2 + 0x20);
    if (DAT_d900a82a < '\0') {
      param_2[uVar4] = 0x17870;
      param_2[uVar5] = 0x33802;
      uVar5 = iVar2 + 0x23;
      param_2[iVar2 + 0x22] = 0x142c0d;
      goto LAB_d9006a34;
    }
    uVar3 = 0x10380d;
  }
  param_2[uVar4] = uVar3;
LAB_d9006a34:
  param_2[uVar5] = 0;
  return;
}



void FUN_d9006aa0(uint param_1,undefined4 *param_2)

{
  undefined4 *puVar1;
  long lVar2;
  uint uVar3;
  byte abStack_40 [64];
  
  memcpy((long)abStack_40,(long)&DAT_d9009728,0x3f);
  if (param_1 == 0x15) {
LAB_d9006c0c:
    *param_2 = 0x1783b;
    param_2[1] = 0x178b9;
    param_2[2] = 0x17854;
    param_2[3] = 0x1b804;
    param_2[4] = 0x13800;
    param_2[5] = 0x17854;
    param_2[6] = 0x1b805;
    param_2[7] = 0x13800;
    param_2[8] = 0x17854;
    param_2[9] = 0x1b807;
    param_2[10] = 0x13800;
    uVar3 = 0x78d6;
  }
  else {
    if (param_1 == 0) {
      *param_2 = 0x1783b;
      param_2[1] = 0x178b9;
      uVar3 = 4;
      puVar1 = param_2 + 2;
      do {
        *puVar1 = 0x17854;
        puVar1[1] = uVar3 | 0x1b800;
        uVar3 = uVar3 + 1;
        puVar1[2] = 0x13800;
        puVar1 = puVar1 + 3;
      } while (uVar3 != 0xd);
      param_2[0x1d] = 0x178b6;
      lVar2 = 0x1e;
      goto LAB_d9006ca0;
    }
    if (0x14 < param_1) {
      lVar2 = 0;
      if (param_1 != 0xff) goto LAB_d9006ca0;
      goto LAB_d9006c0c;
    }
    *param_2 = 0x1783b;
    param_2[1] = 0x178b9;
    param_2[2] = 0x17854;
    uVar3 = param_1 * 3;
    param_2[3] = 0x1b804;
    param_2[4] = abStack_40[uVar3] | 0x13800;
    param_2[5] = 0x17854;
    param_2[6] = 0x1b805;
    param_2[7] = abStack_40[uVar3 + 1] | 0x13800;
    param_2[8] = 0x17854;
    param_2[9] = 0x1b807;
    param_2[10] = abStack_40[uVar3 + 2] | 0x13800;
    uVar3 = 0x78b6;
  }
  param_2[0xb] = uVar3 | 0x10000;
  lVar2 = 0xc;
LAB_d9006ca0:
  param_2[lVar2] = 0;
  return;
}



void FUN_d9006cb0(uint param_1,undefined4 *param_2)

{
  long lVar1;
  
  if (param_1 == 0xff) {
    param_1 = 0;
  }
  else {
    lVar1 = 0;
    if (7 < param_1) goto LAB_d9006d78;
  }
  *param_2 = 0x178ef;
  param_2[1] = 0x1b889;
  param_2[2] = 0x33803;
  param_2[3] = param_1 | 0x13800;
  param_2[4] = 0x13800;
  param_2[5] = 0x13800;
  param_2[6] = 0x13800;
  param_2[7] = 0x33802;
  if (DAT_d900a82a < '\0') {
    param_2[8] = 0x17870;
    param_2[9] = 0x33802;
    param_2[10] = 0x142c0d;
    lVar1 = 0xb;
  }
  else {
    param_2[8] = 0x10380d;
    lVar1 = 9;
  }
LAB_d9006d78:
  param_2[lVar1] = 0;
  return;
}



void FUN_d9006d80(uint param_1,undefined4 *param_2)

{
  long lVar1;
  undefined4 *puVar2;
  byte local_50 [64];
  byte local_10 [16];
  
  local_10[0] = 0xa7;
  local_10[1] = 0xa4;
  local_10[2] = 0xa5;
  local_10[3] = 0xa6;
  memcpy((long)local_50,(long)&DAT_d9009768,0x3c);
  if (param_1 == 0xff) {
    param_1 = 0;
  }
  else {
    lVar1 = 0;
    if (0xe < param_1) goto LAB_d9006e6c;
  }
  lVar1 = 0;
  puVar2 = param_2;
  do {
    *puVar2 = 0x178a1;
    puVar2[1] = 0x1b800;
    puVar2[2] = local_10[lVar1] | 0x1b800;
    puVar2[3] = 0x33802;
    puVar2[4] = local_50[param_1 * 4 + (int)lVar1] | 0x13800;
    lVar1 = lVar1 + 1;
    puVar2[5] = 0x33808;
    puVar2 = puVar2 + 6;
  } while (lVar1 != 4);
  lVar1 = 0x18;
LAB_d9006e6c:
  param_2[lVar1] = 0;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9006e7c(void)

{
  uint *puVar1;
  uint uVar2;
  long lVar3;
  uint local_200 [128];
  
  if (_DAT_d900a82e != 0) {
    if (_DAT_d900a82c == 0x45) {
      FUN_d9006aa0(DAT_d900a7f4,local_200);
    }
    else if (_DAT_d900a82c < 0x46) {
      if (_DAT_d900a82c == 0x2c) {
        FUN_d9006cb0(DAT_d900a7f4,local_200);
      }
    }
    else if (_DAT_d900a82c == 0x98) {
      FUN_d90067ac(DAT_d900a7f4,local_200);
    }
    else if (_DAT_d900a82c == 0xec) {
      FUN_d9006d80(DAT_d900a7f4,local_200);
    }
    DAT_d900a7f4 = DAT_d900a7f4 + 1;
    if (_DAT_d900a82e <= DAT_d900a7f4) {
      DAT_d900a7f4 = 0;
    }
    lVar3 = 0;
    uVar2 = _DAT_d0074800;
    do {
      _DAT_d0074800 = uVar2;
      if ((_DAT_d0074800 >> 0x16 & 0x1f) == 0x1f) {
        return;
      }
      puVar1 = (uint *)((long)local_200 + lVar3);
      lVar3 = lVar3 + 4;
      uVar2 = *puVar1;
    } while (*puVar1 != 0);
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 FUN_d9006f88(void)

{
  uint uVar1;
  uint uVar2;
  long lVar3;
  uint uVar4;
  
  uVar1 = _DAT_d900a828 & 0x3f;
  FUN_d9008f04(0x1800200,0,(ulong)uVar1 << 3);
  lVar3 = (ulong)uVar1 << 3;
  FUN_d9009158(0x1800200,lVar3);
  do {
  } while ((_DAT_d0074800 >> 0x16 & 0x1f) != 0);
  _DAT_d0074800 = 0x33800;
  do {
    FUN_d900918c(0x1800200,lVar3);
  } while (*(long *)((ulong)(uVar1 - 1) * 8 + 0x1800200) == 0);
  uVar4 = 0;
  while( true ) {
    if (uVar1 <= uVar4) {
      return 0;
    }
    uVar2 = *(uint *)(ulong)((uVar4 + 0x300040) * 8);
    if ((uVar2 >> 0x18 & 0x3f) == 0x3f) break;
    if ((uVar4 == 0) && ((uVar2 & 0xc000ffff) != 0xc000aa55)) {
      return 0x83;
    }
    uVar4 = uVar4 + 1;
  }
  if ((_DAT_d900a828 & 0x1000000) != 0) {
    FUN_d90066ac();
  }
  if (9 < (uVar2 >> 0x10 & 0x3f)) {
    return 0x82;
  }
  return 0x85;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int FUN_d9007334(uint param_1,uint param_2,uint param_3)

{
  uint uVar1;
  byte bVar2;
  uint uVar3;
  ulong uVar4;
  int iVar5;
  int iVar6;
  uint uVar7;
  uint uVar8;
  uint uVar9;
  
  uVar4 = (ulong)_DAT_d900a828 & 0x3fffff;
  if (((uint)uVar4 >> 0xd & 1) == 0) {
    iVar5 = 0x400;
    if (((uint)(uVar4 >> 0xe) & 7) < 2) {
      iVar5 = 0x200;
    }
  }
  else {
    iVar5 = ((uint)(uVar4 >> 6) & 0x7f) << 3;
  }
  uVar3 = iVar5 * ((uint)uVar4 & 0x3f);
  iVar5 = 0x200;
  if (0xfff < uVar3) {
    iVar5 = 0x100;
  }
  FUN_d9009158((ulong)param_2,(ulong)param_3);
  uVar8 = 0;
  while( true ) {
    if (param_3 <= uVar8) {
      return 0;
    }
    uVar1 = param_1 + 1;
    if (PTR_d900a7a8._4_4_ != 0) {
      uVar9 = _DAT_d900a7b0 >> 1;
      uVar7 = 0;
      if (uVar9 != 0) {
        uVar7 = param_1 / uVar9;
      }
      if (PTR_d900a7a8._4_4_ < 10) {
        uVar9 = param_1 - uVar7 * uVar9;
        if (PTR_d900a7a8._4_4_ == 6) {
          bVar2 = (&DAT_d900a350)[uVar9];
        }
        else {
          bVar2 = (&DAT_d900a3d0)[uVar9];
        }
        param_1 = (uint)bVar2 + uVar7 * _DAT_d900a7b0;
      }
      else if (PTR_d900a7a8._4_4_ == 0x28) {
        param_1 = uVar7 * _DAT_d900a7b0 + (param_1 - uVar7 * uVar9) * 2;
      }
    }
    uVar9 = 0;
    uVar7 = param_1;
    while (iVar6 = FUN_d9006f88(), iVar6 == 0x82) {
      uVar7 = uVar7 + iVar5;
      if (0x3ff < uVar7 - param_1) {
        if (_DAT_d900a82e <= uVar9) {
          return 0x82;
        }
        uVar9 = uVar9 + 1;
        FUN_d9006e7c();
        uVar7 = param_1;
      }
    }
    if (iVar6 != 0) break;
    uVar8 = uVar8 + uVar3;
    param_1 = uVar1;
  }
  return iVar6;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int nfio_init(void)

{
  int iVar1;
  uint uVar2;
  undefined auStack_10 [6];
  byte local_a;
  
  FUN_d9001bc0(0,8,(long)auStack_10);
  _DAT_d900a828 = 0xebec01;
  DAT_d900a82b = 0x80;
  FUN_d9006678_nandrelated();
  _DAT_d0074000 = 0x90200201;
  uVar2 = (DAT_d900a82b >> 3 & 3) << 10;
  _DAT_d0074804 = uVar2 | 0x80000064;
  if ((local_a >> 5 & 1) != 0) {
    _DAT_d0074804 = uVar2 | 0x80020064;
  }
  iVar1 = FUN_d90066ac();
  if (iVar1 != 0) {
    return iVar1;
  }
  _DAT_d0074800 = 0x33800;
  _DAT_d900a82c = (ushort)(byte)_DAT_d0074810;
  if ((_DAT_d0074810 & 0xff) == 0x45) {
    _DAT_d900a82e = 0x16;
  }
  else if ((_DAT_d0074810 & 0xff) < 0x46) {
    uVar2 = _DAT_d0074810 & 0xff;
    if (((uVar2 == 7) || (uVar2 < 8)) || ((uVar2 == 0x20 || (uVar2 != 0x2c)))) {
LAB_d90076a0:
      _DAT_d900a82e = 0;
    }
    else {
LAB_d900768c:
      _DAT_d900a82e = 8;
    }
  }
  else {
    uVar2 = _DAT_d0074810 & 0xff;
    if (uVar2 == 0x98) goto LAB_d900768c;
    if (((uVar2 < 0x99) || (uVar2 == 0xad)) || (uVar2 != 0xec)) goto LAB_d90076a0;
    _DAT_d900a82e = 0xf;
  }
  iVar1 = FUN_d9007334(0,0x1800000,0x180);
  if (iVar1 == 0x82) {
    if (_DAT_d900a82c == 0x45) {
      _DAT_d900a82e = 0;
      _DAT_d900a828 = CONCAT13(DAT_d900a82b,_DAT_d900a828) | 0x1000000;
    }
    else {
      if (((DAT_d900a82b >> 3 & 3) != 0) || ((_DAT_d900a82c != 0xec && (_DAT_d900a82c != 0x98))))
      goto LAB_d900775c;
      _DAT_d900a828 = CONCAT13(DAT_d900a82b & 0xe0 | DAT_d900a82b & 7 | 0x10,_DAT_d900a828);
      _DAT_d0074804 = _DAT_d0074804 | 0x800;
      _DAT_c88344f0 = _DAT_c88344f0 & 0xffff7fff;
    }
    iVar1 = FUN_d9007334(0,0x1800000,0x180);
  }
LAB_d900775c:
  DAT_d900a82b = (byte)((uint)_DAT_01800000 >> 0x18);
  _DAT_d900a828 = (undefined3)_DAT_01800000;
  _DAT_d900a82e = _DAT_01800006;
  _DAT_d900a82c = _DAT_01800004;
  _DAT_d900a7b0 = _DAT_01800060;
  PTR_d900a7a8._4_4_ = _DAT_0180005c;
  DAT_d900a82b = DAT_d900a82b & 0x9f;
  return iVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int nf_read(undefined8 param_1,uint param_2,uint param_3,uint param_4)

{
  uint uVar1;
  uint uVar2;
  int iVar3;
  
  iVar3 = 0x400;
  if (((uint)(((ulong)_DAT_d900a828 & 0x3fffff) >> 0xe) & 7) < 2) {
    iVar3 = 0x200;
  }
  if ((param_2 & 0x3fff) == 0) {
    uVar1 = iVar3 * ((uint)((ulong)_DAT_d900a828 & 0x3fffff) & 0x3f);
    uVar2 = 0;
    if (uVar1 != 0) {
      uVar2 = param_2 / uVar1;
    }
    iVar3 = FUN_d9007334(uVar2 + 1,param_3,param_4);
    return iVar3;
  }
  return 0x86;
}



void FUN_d9007800(long param_1,long param_2)

{
  long lVar1;
  uint uVar2;
  long lVar3;
  
  lVar1 = 0;
  lVar3 = 0;
  for (uVar2 = 0; uVar2 < *(uint *)(param_1 + 0x408); uVar2 = uVar2 + 1) {
    lVar3 = ((ulong)*(uint *)(param_2 + lVar1) + lVar3) - (ulong)*(uint *)(param_1 + lVar1);
    *(int *)(param_2 + lVar1) = (int)lVar3;
    lVar3 = lVar3 >> 0x20;
    lVar1 = lVar1 + 4;
  }
  return;
}



void FUN_d9007840(uint *param_1,uint *param_2,long param_3,uint *param_4)

{
  long lVar1;
  uint uVar2;
  uint uVar3;
  uint uVar4;
  long lVar5;
  ulong uVar6;
  uint uVar7;
  ulong uVar8;
  
  for (uVar4 = 0; uVar4 < param_1[0x102]; uVar4 = uVar4 + 1) {
    param_2[uVar4] = 0;
  }
  for (uVar4 = 0; uVar4 < param_1[0x102]; uVar4 = uVar4 + 1) {
    uVar2 = *(uint *)(param_3 + (ulong)uVar4 * 4);
    uVar6 = (ulong)*param_2 + (ulong)uVar2 * (ulong)*param_4;
    uVar3 = param_1[0x101] * (int)uVar6;
    uVar8 = (uVar6 & 0xffffffff) + (ulong)uVar3 * (ulong)*param_1;
    uVar7 = 1;
    lVar5 = 0;
    while( true ) {
      lVar1 = lVar5 + 4;
      if (param_1[0x102] <= uVar7) break;
      uVar7 = uVar7 + 1;
      uVar6 = (ulong)*(uint *)((long)param_2 + lVar1) +
              (ulong)uVar2 * (ulong)*(uint *)((long)param_4 + lVar1) + (uVar6 >> 0x20);
      uVar8 = (uVar8 >> 0x20) + (ulong)uVar3 * (ulong)*(uint *)((long)param_1 + lVar1) +
              (uVar6 & 0xffffffff);
      *(int *)((long)param_2 + lVar5) = (int)uVar8;
      lVar5 = lVar1;
    }
    uVar8 = (uVar8 >> 0x20) + (uVar6 >> 0x20);
    param_2[uVar7 - 1] = (uint)uVar8;
    if (uVar8 >> 0x20 != 0) {
      FUN_d9007800((long)param_1,(long)param_2);
    }
  }
  return;
}



void FUN_d900793c(uint *param_1,undefined *param_2,uint *param_3)

{
  uint *puVar1;
  uint *puVar2;
  uint uVar3;
  undefined4 uVar4;
  uint uVar5;
  long lVar6;
  ulong uVar7;
  long lVar8;
  uint *puVar9;
  uint *puVar10;
  uint *puVar11;
  int iVar12;
  
  uVar3 = param_1[0x102];
  puVar1 = param_3 + (ulong)uVar3 * 2;
  lVar6 = 0;
  while( true ) {
    if (param_1[0x102] <= (uint)lVar6) break;
    uVar5 = ((param_1[0x102] + 0x3fffffff) - (uint)lVar6) * 4;
    param_3[lVar6] =
         (uint)(byte)param_2[uVar5 + 3] | (uint)(byte)param_2[uVar5] << 0x18 |
         (uint)(byte)param_2[uVar5 + 1] << 0x10 | (uint)(byte)param_2[uVar5 + 2] << 8;
    lVar6 = lVar6 + 1;
  }
  iVar12 = 0;
  for (uVar5 = param_1[0x80]; 0 < (int)uVar5; uVar5 = (int)uVar5 >> 1) {
    iVar12 = iVar12 + 1;
  }
  FUN_d9007840(param_1,puVar1,(long)param_3,param_1 + 0x81);
  puVar9 = puVar1;
  puVar11 = param_3 + uVar3;
  for (uVar5 = iVar12 - 2; -1 < (int)uVar5; uVar5 = uVar5 - 1) {
    FUN_d9007840(param_1,puVar11,(long)puVar9,puVar9);
    puVar2 = puVar9;
    if (puVar9 == puVar1) {
      puVar2 = param_3;
    }
    puVar10 = puVar2;
    puVar9 = puVar11;
    if ((1 << (ulong)(uVar5 & 0x1f) & param_1[0x80]) != 0) {
      FUN_d9007840(param_1,puVar2,(long)puVar11,puVar1);
      puVar10 = puVar11;
      puVar9 = puVar2;
    }
    puVar11 = puVar10;
  }
  FUN_d9008f04((long)puVar1,0,((ulong)param_1[0x102] & 0x3fffffff) << 2);
  *puVar1 = 1;
  FUN_d9007840(param_1,puVar11,(long)puVar9,puVar1);
  do {
    uVar7 = (ulong)param_1[0x102];
    do {
      if ((int)uVar7 == 0) break;
      uVar7 = (ulong)((int)uVar7 - 1);
      if (puVar11[uVar7] < param_1[uVar7]) {
        iVar12 = param_1[0x102] - 1;
        lVar6 = (long)iVar12;
        lVar8 = 0;
        for (; -1 < iVar12; iVar12 = iVar12 + -1) {
          uVar4 = *(undefined4 *)((long)puVar11 + lVar8 + lVar6 * 4);
          *param_2 = (char)((uint)uVar4 >> 0x18);
          param_2[1] = (char)((uint)uVar4 >> 0x10);
          param_2[2] = (char)((uint)uVar4 >> 8);
          param_2[3] = (char)uVar4;
          lVar8 = lVar8 + -4;
          param_2 = param_2 + 4;
        }
        return;
      }
    } while (puVar11[uVar7] <= param_1[uVar7]);
    FUN_d9007800((long)param_1,(long)puVar11);
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007b28(undefined8 param_1,uint param_2,undefined8 param_3,char param_4)

{
  undefined local_10 [16];
  
  if ((param_4 == '\0') && ((param_2 & 0x3f) != 0)) {
    serial_puts(s_Err_sha5_d90097a8);
  }
  else {
    _DAT_c883e018 = 0xf;
    _DAT_c883e008 = (uint)local_10 | 2;
    *(uint *)(DAT_d900a7d0 + 0x20) = *(int *)(DAT_d900a7d0 + 0x20) + param_2;
    do {
    } while (_DAT_c883e018 == 0);
  }
  return;
}



void FUN_d9007bfc(long param_1,int param_2)

{
  undefined4 uVar1;
  
  if (DAT_d900a7d0 != 0) {
    serial_puts(s_Err_sha4_d90097b2);
    return;
  }
  uVar1 = 0x100;
  if (param_2 == 0xe0) {
    uVar1 = 0xe0;
  }
  DAT_d900a7d0 = param_1;
  *(undefined4 *)(param_1 + 0x28) = uVar1;
  *(undefined4 *)(param_1 + 0x20) = 0;
  *(undefined4 *)(param_1 + 0x24) = 0;
  return;
}



void FUN_d9007c3c(long param_1,long param_2,uint param_3)

{
  uint uVar1;
  long lVar2;
  ulong uVar3;
  uint uVar4;
  
  lVar2 = DAT_d900a7d0;
  if (DAT_d900a7d0 != param_1) {
    serial_puts(s_Err_sha3_d90097bc);
    return;
  }
  uVar1 = *(uint *)(DAT_d900a7d0 + 0x24);
  uVar4 = 0;
  if (uVar1 != 0) {
    uVar4 = 0x40 - uVar1;
    memcpy(DAT_d900a7d0 + (ulong)uVar1 + 0x2c,param_2,(ulong)uVar4);
    param_3 = param_3 - uVar4;
    *(uint *)(lVar2 + 0x24) = *(int *)(lVar2 + 0x24) + uVar4;
  }
  uVar3 = (ulong)param_3;
  if (*(int *)(lVar2 + 0x24) == 0x40) {
    if (param_3 == 0) {
      return;
    }
    FUN_d9007b28(lVar2 + 0x2c,0x40,lVar2 + 0xac,'\0');
    *(undefined4 *)(lVar2 + 0x24) = 0;
    if (param_3 < 0x41) goto LAB_d9007d28;
  }
  else if (param_3 < 0x41) {
    if (param_3 == 0) {
      return;
    }
    goto LAB_d9007d28;
  }
  uVar1 = param_3 & 0x3f;
  if ((param_3 & 0x3f) == 0) {
    uVar1 = 0x40;
  }
  uVar3 = (ulong)uVar4;
  uVar4 = uVar4 + (param_3 - uVar1);
  FUN_d9007b28(param_2 + uVar3,param_3 - uVar1,lVar2 + 0xac,'\0');
  uVar3 = (ulong)uVar1;
LAB_d9007d28:
  memcpy(lVar2 + 0x2c,param_2 + (ulong)uVar4,uVar3);
  *(int *)(lVar2 + 0x24) = (int)uVar3;
  return;
}



long FUN_d9007d50(long param_1)

{
  long lVar1;
  
  if (DAT_d900a7d0 == param_1) {
    lVar1 = DAT_d900a7d0 + 0xac;
    if (*(uint *)(DAT_d900a7d0 + 0x24) - 1 < 0x40) {
      FUN_d9007b28(DAT_d900a7d0 + 0x2c,*(uint *)(DAT_d900a7d0 + 0x24),lVar1,'\x01');
      DAT_d900a7d0 = 0;
    }
    else {
      serial_puts(s_Err_sha2_d90097d0);
    }
  }
  else {
    serial_puts(s_Err_sha1_d90097c6);
    lVar1 = param_1 + 0xac;
  }
  return lVar1;
}



void FUN_d9007dd0(long param_1,uint param_2,long param_3,int param_4)

{
  int iVar1;
  uint uVar2;
  uint uVar3;
  undefined auStack_d0 [172];
  undefined auStack_24 [36];
  
  uVar2 = param_2;
  if (0x1ffbf < param_2) {
    uVar2 = 0x1ffc0;
  }
  iVar1 = 0x100;
  if (param_4 != 0) {
    iVar1 = 0xe0;
  }
  FUN_d9007bfc((long)auStack_d0,iVar1);
  uVar3 = 0;
  while (uVar3 < param_2) {
    FUN_d9007c3c((long)auStack_d0,param_1 + (int)uVar3,uVar2);
    uVar3 = uVar3 + uVar2;
    if (param_2 - uVar3 < uVar2) {
      uVar2 = param_2 - uVar3;
    }
  }
  FUN_d9007d50((long)auStack_d0);
  memcpy(param_3,(long)auStack_24,0x20);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007e74(long param_1)

{
  int iVar1;
  ulong uVar2;
  int *piVar3;
  undefined4 *puVar4;
  int *piVar5;
  ulong uVar6;
  undefined8 uVar7;
  undefined8 local_10;
  undefined8 local_8;
  
  piVar3 = (int *)(param_1 + 0x430);
  piVar5 = (int *)(param_1 + 0x10);
  if (*(int *)(param_1 + 0x400) == -0x789abcdf) {
    serial_puts(s_New_fip_structure__d90097da);
  }
  uVar6 = 0;
  local_10 = 0;
  local_8 = 0;
  uVar7 = 0;
  do {
    if (*(long *)(piVar5 + 6) == 0) {
      return;
    }
    iVar1 = *piVar5;
    for (puVar4 = &DAT_d900a450; puVar4[1] != 0; puVar4 = puVar4 + 10) {
      if (iVar1 == puVar4[1]) {
        *puVar4 = 1;
        *(long *)(puVar4 + 2) = *(long *)(piVar5 + 4) + 0xc000;
        *(undefined8 *)(puVar4 + 4) = *(undefined8 *)(piVar5 + 6);
        if (*piVar3 == 0x12348765) {
          *(undefined8 *)(puVar4 + 6) = *(undefined8 *)(piVar3 + 2);
          local_10 = *(undefined8 *)(piVar3 + 8);
          uVar7 = *(undefined8 *)(piVar3 + 4);
          local_8 = *(undefined8 *)(piVar3 + 10);
          uVar6 = *(ulong *)(piVar3 + 6);
LAB_d9007f90:
          if (iVar1 != 0x6d08d447) {
            if (iVar1 == -0x761e2ffb) goto LAB_d9007ff0;
            goto LAB_d900802c;
          }
LAB_d9007fb4:
          _DAT_da100254 = (undefined4)uVar7;
          _DAT_da10024c = _DAT_da10024c & 0xffff | (int)(uVar6 >> 10) << 0x10;
          uVar2 = 0;
        }
        else {
          if (iVar1 == 0x6d08d447) {
            local_10 = 0x5100000;
            local_8 = 0x200000;
            uVar7 = 0x5000000;
            uVar6 = 0x300000;
            goto LAB_d9007fb4;
          }
          if (iVar1 != -0x761e2ffb) goto LAB_d9007f90;
          uVar7 = 0x5300000;
          local_10 = 0x5300000;
          local_8 = 0x1400000;
          uVar6 = 0x2000000;
LAB_d9007ff0:
          _DAT_da100250 = (undefined4)uVar7;
          _DAT_da10024c = (uint)(uVar6 >> 10) & 0xffff | _DAT_da10024c & 0xffff0000;
          uVar2 = 1;
        }
        FUN_d9006654(&local_10,uVar2);
      }
LAB_d900802c:
    }
    piVar5 = piVar5 + 10;
    piVar3 = piVar3 + 0x14;
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void check_handler(undefined8 param_1)

{
  uint uVar1;
  undefined1 *puVar2;
  undefined *ep_info;
  ulong uVar3;
  
  puVar2 = FUN_d9001068();
  ep_info = FUN_d9001170();
  *(undefined1 **)(ep_info + 0x18) = puVar2;
  *(undefined8 *)(ep_info + 8) = param_1;
  *(uint *)(ep_info + 4) = *(uint *)(ep_info + 4) & 0xfffffffe;
  *(undefined4 *)(ep_info + 0x10) = 0x2cd;
  FUN_d9001a6c();
  FUN_d9009150();
  *(undefined8 *)(ep_info + 0x18) = 1;
  smc((ulong *)0xc0000000,(ulong)ep_info,0,0,0,0,0,0);
  uVar1 = get_boot_device();
  if (uVar1 == 5) {
    serial_puts(s_USB_mode__d90097ee);
    if (_DAT_d904050c == -0x6bfffb02) {
      uVar3 = 0x454c;
    }
    else if (_DAT_d904050c == -0x6bffe734) {
      uVar3 = 20000;
    }
    else if (_DAT_d904050c == 0x52800022) {
      uVar3 = 0x4640;
    }
    else {
      uVar3 = 0x4518;
    }
    check_handler(uVar3 | 0xd9040000);
  }
  serial_puts(s__lock_failed__reset____d90092fe + 0xe);
  reset_system();
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90080cc(void)

{
  uint uVar1;
  ulong uVar2;
  
  uVar1 = get_boot_device();
  if (uVar1 == 5) {
    serial_puts(s_USB_mode__d90097ee);
    if (_DAT_d904050c == -0x6bfffb02) {
      uVar2 = 0x454c;
    }
    else if (_DAT_d904050c == -0x6bffe734) {
      uVar2 = 20000;
    }
    else if (_DAT_d904050c == 0x52800022) {
      uVar2 = 0x4640;
    }
    else {
      uVar2 = 0x4518;
    }
    check_handler(uVar2 | 0xd9040000);
  }
  serial_puts(s__lock_failed__reset____d90092fe + 0xe);
  reset_system();
  return;
}



int FUN_d9008160(ulong param_1,ulong param_2,int param_3)

{
  int iVar1;
  
  FUN_d9009150();
  iVar1 = aml_data_check(param_1,param_2,param_3);
  if (iVar1 != 0) {
    serial_puts(s_aml_log___SIG_CHK___d90097f9);
    serial_put_dec((long)iVar1);
    serial_puts(s__for_address_0x_d900980e);
    serial_put_hex(param_1,0x20);
    serial_puts(s__d9009869);
    FUN_d90080cc();
  }
  FUN_d9009154();
  return iVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int bl2_load_images(void)

{
  uint uVar1;
  int iVar2;
  int iVar3;
  undefined1 *puVar4;
  long lVar5;
  undefined4 uVar6;
  uintptr_t uVar7;
  long lVar8;
  undefined8 uVar9;
  astruct *name;
  uint *puVar10;
  uint uVar11;
  ep_info *ep_info;
  ulong dst;
  undefined auStack_2a0 [2];
  ushort uStack_29e;
  short sStack_29c;
  ushort uStack_29a;
  int iStack_294;
  uint uStack_290;
  int iStack_28c;
  uint uStack_288;
  uint uStack_284;
  undefined auStack_280 [32];
  undefined auStack_260 [186];
  short sStack_1a6;
  int iStack_1a4;
  undefined auStack_a0 [48];
  undefined auStack_70 [32];
  
  dump_ddr_data();
  storage_load(0xc000,0x1400000,0x4000,s_fip_header_d900981e);
  memcpy(0x1700000,0x1400000,0x4000);
  uVar11 = _DAT_c8100228;
  FUN_d9008160(0x1700000,0x1400000,-1);
  FUN_d9007e74(0x1400000);
  FUN_d900648c();
  puVar4 = FUN_d9001068();
  ep_info = NULL;
  iVar3 = 0;
  for (name = (astruct *)&LAB_d900a470; *(int *)&name[-1].field_0xc != 0; name = name + 1) {
    if (*(int *)&name[-1].field_0x8 != 0) {
      dst = 0x1700000;
      if ((uVar11 & 0x10) == 0) {
        dst = *(ulong *)&name[-1].field_0x20;
      }
      storage_load(*(ulong *)&name[-1].src,dst,*(ulong *)&name[-1].size,(char *)name);
      FUN_d9008160(dst,*(ulong *)&name[-1].field_0x20,iVar3);
      uVar1 = *(uint *)&name[-1].field_0xc;
      iVar3 = iVar3 + 1;
      if (uVar1 == 0x89e1d005) {
        lVar5 = *(long *)(puVar4 + 0x18);
        uVar9 = *(undefined8 *)&name[-1].field_0x20;
        *(undefined8 *)(lVar5 + 8) = uVar9;
        *(int *)(lVar5 + 0x10) = (int)*(undefined8 *)&name[-1].size;
        lVar8 = *(long *)(puVar4 + 0x10);
        if (lVar8 != 0) {
          *(undefined8 *)(lVar8 + 8) = uVar9;
        }
        FUN_d90012c0(lVar5,lVar8);
      }
      else if (uVar1 < 0x89e1d006) {
        if (uVar1 == 0x3dfd6697) {
          lVar5 = *(long *)(puVar4 + 0x30);
          lVar8 = *(long *)&name[-1].field_0x20;
          *(undefined4 *)(lVar5 + 0x10) = 0xa000;
          *(long *)(lVar5 + 8) = lVar8;
          lVar5 = *(long *)(puVar4 + 0x38);
          *(long *)(lVar5 + 8) = lVar8 + 0xa000;
          uVar6 = 0x3400;
LAB_d9008384:
          *(undefined4 *)(lVar5 + 0x10) = uVar6;
        }
        else if (uVar1 == 0x6d08d447) {
          ep_info = (ep_info *)FUN_d9001170();
          *(undefined1 **)((long)&(ep_info->args).arg0 + 4) = puVar4;
          uVar7 = *(uintptr_t *)&name[-1].field_0x20;
          lVar5 = *(long *)(puVar4 + 8);
          *(uintptr_t *)(lVar5 + 8) = uVar7;
          *(int *)(lVar5 + 0x10) = (int)*(undefined8 *)&name[-1].size;
          ep_info->pc = uVar7;
          FUN_d90012a8(lVar5,(long)ep_info);
        }
      }
      else if (uVar1 == 0xa7eed0d6) {
        lVar5 = *(long *)(puVar4 + 0x28);
        uVar9 = *(undefined8 *)&name[-1].field_0x20;
        *(undefined8 *)(lVar5 + 8) = uVar9;
        *(int *)(lVar5 + 0x10) = (int)*(undefined8 *)&name[-1].size;
        lVar8 = *(long *)(puVar4 + 0x20);
        if (lVar8 != 0) {
          *(undefined8 *)(lVar8 + 8) = uVar9;
        }
        FUN_d90012d4(lVar5,lVar8);
      }
      else if (uVar1 == 0xaabbccdd) {
        lVar5 = *(long *)(puVar4 + 0x38);
        *(undefined8 *)(lVar5 + 8) = *(undefined8 *)&name[-1].field_0x20;
        uVar6 = (undefined4)*(undefined8 *)&name[-1].size;
        goto LAB_d9008384;
      }
    }
  }
  FUN_d90011e4();
  FUN_d9009150();
  FUN_d9001a6c();
  iVar3 = 0;
  lVar5 = 0xc0000000;
                    /* Run BL31 via an SMC call to BL1. */
  smc((ulong *)0xc0000000,(ulong)ep_info,0,0,0,0,0,0);
  uVar11 = _DAT_c8100228 & 0x10;
  uVar1 = _DAT_c8100228 >> 4 & 1;
  if ((iVar3 < 0) || (uVar1 == 0)) {
    if (iVar3 < 0) {
      _DAT_d9000000 = 0x47584c4650484452;
      _DAT_d9000008 = 0x1400000;
    }
  }
  else if (iVar3 < 10) {
    puVar10 = (uint *)0x1400dc0;
    do {
      iVar2 = memcmp__notsure(((long)iVar3 + 100) * 0x20 + 0x1400000,(long)(puVar10 + 0x106),0x20);
      if (iVar2 == 0) goto LAB_d90084d4;
      puVar10 = puVar10 + 0x10e;
    } while (puVar10 != (uint *)0x14037f0);
    puVar10 = NULL;
    goto LAB_d90084d4;
  }
  puVar10 = (uint *)0xd9000b70;
LAB_d90084d4:
  memcpy((long)auStack_2a0,lVar5,0x200);
  if ((((iStack_294 == 0x434c4d41) && (iStack_1a4 == 0x434c4d41)) && (uStack_29a < 2)) &&
     (((uStack_29e == 0x200 && (sStack_1a6 == 0x200)) && (iStack_28c == 0x200)))) {
    if (uVar1 != 0) {
      return 0xdc;
    }
  }
  else {
    if (uVar11 == 0) {
      return 0xbf;
    }
    for (uVar11 = 0; uVar11 < 0x200; uVar11 = uVar11 + puVar10[0x102] * 4) {
      FUN_d900793c(puVar10,(undefined *)(lVar5 + (int)uVar11),(uint *)0x12000000);
    }
    memcpy((long)auStack_2a0,lVar5,0x200);
    iVar3 = 0x55;
    if (((iStack_294 == 0x434c4d41) && (iStack_1a4 == 0x434c4d41)) &&
       ((uStack_29a < 2 &&
        (((iVar3 = 0x5f, uStack_29e == 0x200 && (sStack_1a6 == 0x200)) &&
         (iVar3 = 0, iStack_28c != 0x200)))))) {
      iVar3 = 0x6a;
    }
    serial_puts(s_aml_log___R_d9009829);
    serial_put_dec(((ulong)puVar10[0x102] & 0x7ffffff) << 5);
    if (iVar3 != 0) {
      serial_puts(s__check_fail_with_ERR___d9009843);
      serial_put_dec((long)iVar3);
      serial_puts(s__d9009869);
      return iVar3;
    }
    serial_puts(s__check_pass__d9009835);
  }
  memcpy(lVar5,lVar5 + (ulong)uStack_290,(ulong)uStack_29e);
  FUN_d9007dd0(lVar5,uStack_288,(long)auStack_70,0);
  iVar3 = memcmp__notsure((long)auStack_70,(long)auStack_280,0x20);
  iVar2 = 0xe7;
  if (iVar3 == 0) {
    if (sStack_29c != 0) {
      memcpy((long)auStack_a0,(long)auStack_260,0x30);
      FUN_d9001af8();
    }
    FUN_d9009158((ulong)ep_info,(ulong)uStack_284);
    iVar2 = 0;
  }
  return iVar2;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int aml_data_check(long param_1,ulong param_2,int param_3)

{
  uint uVar1;
  int iVar2;
  uint *puVar3;
  uint uVar4;
  int iVar5;
  undefined auStack_250 [2];
  ushort local_24e;
  short local_24c;
  ushort local_24a;
  int local_244;
  uint local_240;
  int local_23c;
  uint local_238;
  uint local_234;
  undefined auStack_230 [32];
  undefined auStack_210 [186];
  short local_156;
  int local_154;
  undefined auStack_50 [48];
  undefined auStack_20 [32];
  
  uVar4 = _DAT_c8100228 & 0x10;
  uVar1 = _DAT_c8100228 >> 4 & 1;
  if ((param_3 < 0) || (uVar1 == 0)) {
    if (param_3 < 0) {
      _DAT_d9000000 = 0x47584c4650484452;
      _DAT_d9000008 = 0x1400000;
    }
  }
  else if (param_3 < 10) {
    puVar3 = (uint *)0x1400dc0;
    do {
      iVar2 = memcmp__notsure(((long)param_3 + 100) * 0x20 + 0x1400000,(long)(puVar3 + 0x106),0x20);
      if (iVar2 == 0) goto LAB_d90084d4;
      puVar3 = puVar3 + 0x10e;
    } while (puVar3 != (uint *)0x14037f0);
    puVar3 = NULL;
    goto LAB_d90084d4;
  }
  puVar3 = (uint *)0xd9000b70;
LAB_d90084d4:
  memcpy((long)auStack_250,param_1,0x200);
  if ((((local_244 == 0x434c4d41) && (local_154 == 0x434c4d41)) && (local_24a < 2)) &&
     (((local_24e == 0x200 && (local_156 == 0x200)) && (local_23c == 0x200)))) {
    if (uVar1 != 0) {
      return 0xdc;
    }
  }
  else {
    if (uVar4 == 0) {
      return 0xbf;
    }
    for (uVar4 = 0; uVar4 < 0x200; uVar4 = uVar4 + puVar3[0x102] * 4) {
      FUN_d900793c(puVar3,(undefined *)(param_1 + (int)uVar4),(uint *)0x12000000);
    }
    memcpy((long)auStack_250,param_1,0x200);
    iVar2 = 0x55;
    if (((local_244 == 0x434c4d41) && (local_154 == 0x434c4d41)) &&
       ((local_24a < 2 &&
        (((iVar2 = 0x5f, local_24e == 0x200 && (local_156 == 0x200)) &&
         (iVar2 = 0, local_23c != 0x200)))))) {
      iVar2 = 0x6a;
    }
    serial_puts(s_aml_log___R_d9009829);
    serial_put_dec(((ulong)puVar3[0x102] & 0x7ffffff) << 5);
    if (iVar2 != 0) {
      serial_puts(s__check_fail_with_ERR___d9009843);
      serial_put_dec((long)iVar2);
      serial_puts(s__d9009869);
      return iVar2;
    }
    serial_puts(s__check_pass__d9009835);
  }
  memcpy(param_1,param_1 + (ulong)local_240,(ulong)local_24e);
  FUN_d9007dd0(param_1,local_238,(long)auStack_20,0);
  iVar2 = memcmp__notsure((long)auStack_20,(long)auStack_230,0x20);
  iVar5 = 0xe7;
  if (iVar2 == 0) {
    if (local_24c != 0) {
      memcpy((long)auStack_50,(long)auStack_210,0x30);
      FUN_d9001af8();
    }
    FUN_d9009158(param_2,(ulong)local_234);
    iVar5 = 0;
  }
  return iVar5;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int aml_data_check(long param_1,ulong param_2,int param_3)

{
  uint uVar1;
  int iVar2;
  uint *puVar3;
  uint uVar4;
  int iVar5;
  undefined auStack_250 [2];
  ushort uStack_24e;
  short sStack_24c;
  ushort uStack_24a;
  int iStack_244;
  uint uStack_240;
  int iStack_23c;
  uint uStack_238;
  uint uStack_234;
  undefined auStack_230 [32];
  undefined auStack_210 [186];
  short sStack_156;
  int iStack_154;
  undefined auStack_50 [48];
  undefined auStack_20 [32];
  
  uVar4 = _DAT_c8100228 & 0x10;
  uVar1 = _DAT_c8100228 >> 4 & 1;
  if ((param_3 < 0) || (uVar1 == 0)) {
    if (param_3 < 0) {
      _DAT_d9000000 = 0x47584c4650484452;
      _DAT_d9000008 = 0x1400000;
    }
  }
  else if (param_3 < 10) {
    puVar3 = (uint *)0x1400dc0;
    do {
      iVar2 = memcmp__notsure(((long)param_3 + 100) * 0x20 + 0x1400000,(long)(puVar3 + 0x106),0x20);
      if (iVar2 == 0) goto LAB_d90084d4;
      puVar3 = puVar3 + 0x10e;
    } while (puVar3 != (uint *)0x14037f0);
    puVar3 = NULL;
    goto LAB_d90084d4;
  }
  puVar3 = (uint *)0xd9000b70;
LAB_d90084d4:
  memcpy((long)auStack_250,param_1,0x200);
  if ((((iStack_244 == 0x434c4d41) && (iStack_154 == 0x434c4d41)) && (uStack_24a < 2)) &&
     (((uStack_24e == 0x200 && (sStack_156 == 0x200)) && (iStack_23c == 0x200)))) {
    if (uVar1 != 0) {
      return 0xdc;
    }
  }
  else {
    if (uVar4 == 0) {
      return 0xbf;
    }
    for (uVar4 = 0; uVar4 < 0x200; uVar4 = uVar4 + puVar3[0x102] * 4) {
      FUN_d900793c(puVar3,(undefined *)(param_1 + (int)uVar4),(uint *)0x12000000);
    }
    memcpy((long)auStack_250,param_1,0x200);
    iVar2 = 0x55;
    if (((iStack_244 == 0x434c4d41) && (iStack_154 == 0x434c4d41)) &&
       ((uStack_24a < 2 &&
        (((iVar2 = 0x5f, uStack_24e == 0x200 && (sStack_156 == 0x200)) &&
         (iVar2 = 0, iStack_23c != 0x200)))))) {
      iVar2 = 0x6a;
    }
    serial_puts(s_aml_log___R_d9009829);
    serial_put_dec(((ulong)puVar3[0x102] & 0x7ffffff) << 5);
    if (iVar2 != 0) {
      serial_puts(s__check_fail_with_ERR___d9009843);
      serial_put_dec((long)iVar2);
      serial_puts(s__d9009869);
      return iVar2;
    }
    serial_puts(s__check_pass__d9009835);
  }
  memcpy(param_1,param_1 + (ulong)uStack_240,(ulong)uStack_24e);
  FUN_d9007dd0(param_1,uStack_238,(long)auStack_20,0);
  iVar2 = memcmp__notsure((long)auStack_20,(long)auStack_230,0x20);
  iVar5 = 0xe7;
  if (iVar2 == 0) {
    if (sStack_24c != 0) {
      memcpy((long)auStack_50,(long)auStack_210,0x30);
      FUN_d9001af8();
    }
    FUN_d9009158(param_2,(ulong)uStack_234);
    iVar5 = 0;
  }
  return iVar5;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void print_version(void)

{
  serial_puts(s__TE__d900985b);
  serial_put_dec((ulong)_DAT_c1109988);
  serial_puts(s__BL2_d9009861);
  serial_puts(s_Built___11_58_42__May_27_2017__g_d9009200);
  serial_puts(&DAT_d9009868);
  return;
}



void bl2_run_bl21(void)

{
  BL21_FUN_d900b400();
  return;
}



void bl2_main(void)

{
                    /* This BL2 is based on Arm Trusted Firmware-A v0.4. */
  pinmux_init();
                    /* seems useless? */
  FUN_d9008eb0();
                    /* Print version, run BL21 code, setup arch, check for USB, 
                       initialise DRAM and storage, and finally load images. */
  print_version();
  bl2_run_bl21();
  bl2_arch_setup();
  bl2_usb_handler();
  FUN_d900639c();
  ddr_init_all(1);
  storage_init();
  bl2_load_images();
  serial_puts(s_NEVER_BE_HERE_d900986b);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}



undefined8 usb_read__notsure(long param_1,long param_2,long param_3)

{
  memcpy(param_2,param_1 + (ulong)(DAT_d900a540 - 0xc000),param_3);
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 bl2_usb_handler(void)

{
  int iVar1;
  uint uVar2;
  undefined4 uVar3;
  uint uVar4;
  ulong uVar5;
  uint uVar6;
  long lVar7;
  
  uVar2 = _DAT_da10001c;
  uVar4 = get_boot_device();
  uVar6 = uVar2 >> 0xc & 0xf;
  if ((uVar4 != 5) && (uVar6 != 2)) {
    if (uVar6 != 1) {
      return 0;
    }
    serial_puts(s_Skip_usb__d900988a);
    _DAT_da10001c = uVar2 & 0xffff0fff | 0x2000;
    if (_DAT_d904050c == -0x6bfffb02) {
      uVar5 = 0x3c58;
    }
    else if (_DAT_d904050c == -0x6bffe734) {
      uVar5 = 0x452c;
    }
    else if (_DAT_d904050c == 0x52800022) {
      uVar5 = 0x3d4c;
    }
    else {
      uVar5 = 0x3c24;
    }
    goto LAB_d9008ac8;
  }
  serial_puts(s_BL2_USB_d9009880);
  iVar1 = DAT_d900c000;
  DAT_d900c000 = 0x7856efab;
  if ((DAT_d900c004 == 0x200) && (iVar1 == 0x3412cdab)) {
    uVar3 = 0xe3;
    switch(DAT_d900c008) {
    case 0xc0de:
      uVar3 = 0xe4;
      break;
    case 0xc0df:
      ddr_init_all(0);
      DAT_d900c018 = FUN_d9001a58();
      uVar3 = 0xe5;
      if (DAT_d900c018 != 0) {
        uVar3 = 0;
      }
      break;
    case 0xc0e0:
      if (DAT_d900c018 == 1) {
        uVar5 = (ulong)DAT_d900c01c;
        DAT_d900c028 = 0;
        uVar6 = DAT_d900c020 & 3;
        for (lVar7 = 0; (int)DAT_d900c020 >> 2 != (uint)lVar7; lVar7 = lVar7 + 1) {
          DAT_d900c028 = DAT_d900c028 + *(int *)(uVar5 + lVar7 * 4);
        }
        lVar7 = (ulong)(uint)((int)DAT_d900c020 >> 2) * 4;
        if (uVar6 == 1) {
          uVar6 = (uint)*(byte *)(uVar5 + lVar7);
LAB_d90089bc:
          DAT_d900c028 = DAT_d900c028 + uVar6;
        }
        else {
          if (uVar6 == 2) {
            uVar6 = (uint)*(ushort *)(uVar5 + lVar7);
            goto LAB_d90089bc;
          }
          if (uVar6 == 3) {
            uVar6 = *(uint *)(uVar5 + lVar7) & 0xffffff;
            goto LAB_d90089bc;
          }
        }
        uVar3 = 0xe6;
        if (DAT_d900c028 == DAT_d900c024) {
          uVar3 = 0;
        }
      }
      else {
        uVar3 = 0xe8;
      }
      break;
    case 0xc0e1:
      _DAT_da10025c = _DAT_da10025c | 0x80000000;
      if (DAT_d900c018 == 1) {
        DAT_d900a540 = DAT_d900c01c;
        storage_init();
        bl2_load_images();
      }
      else {
        if (DAT_d900c018 != 2) {
          uVar3 = 0xe9;
          break;
        }
        (*(code *)(ulong)DAT_d900c01c)();
      }
      uVar3 = 0;
    }
  }
  else {
    DAT_d900c00c = 0xe2;
    uVar3 = DAT_d900c00c;
  }
  DAT_d900c00c = uVar3;
  if (_DAT_d904050c == -0x6bfffb02) {
    uVar5 = 0x454c;
  }
  else if (_DAT_d904050c == -0x6bffe734) {
    uVar5 = 20000;
  }
  else if (_DAT_d904050c == 0x52800022) {
    uVar5 = 0x4640;
  }
  else {
    uVar5 = 0x4518;
  }
LAB_d9008ac8:
  check_handler(uVar5 | 0xd9040000);
  return 0;
}



void bl2_arch_setup(void)

{
  set_cpacr_el1(0x300000);
  saradc_ch1_get();
  pll_init();
  return;
}



void FUN_d9008afc(uint param_1)

{
  if ((param_1 & 0x3ffff) == 0) {
    serial_print(s__0x_d9009895,(ulong)(param_1 << 2),param_1 & 0x3ffff,s__d9009869 + 1);
    return;
  }
  return;
}



undefined8 FUN_d9008b24(char *param_1,ulong param_2,uint param_3,uint param_4)

{
                    /* <param_1>-W[0x<param_2>n]:0x<param_3>n,R:0x<param_4> */
  serial_puts(s__d9009899);
  serial_puts(param_1);
  serial_print(s__W_0x_d900989c,param_2,0,s__d9009869 + 1);
  serial_print(s___0x_d90098a2,(ulong)param_3,0,s__d9009869 + 1);
  serial_print(s__R_0x_d90098a7,(ulong)param_4,0,s__d9009869);
  return 1;
}



undefined8 memTestDataBus(uint *param_1)

{
  undefined8 uVar1;
  uint uVar2;
  int iVar3;
  
  iVar3 = 0x20;
  uVar1 = 0;
  uVar2 = 1;
  do {
    *param_1 = uVar2;
    if (*param_1 != uVar2) {
      uVar1 = FUN_d9008b24(s_DATA_d90098ad,(ulong)param_1 & 0xffffffff,uVar2,*param_1);
    }
    iVar3 = iVar3 + -1;
    uVar2 = uVar2 << 1;
  } while (iVar3 != 0);
  return uVar1;
}



ulong memTestAddrBus(uint *param_1,ulong param_2)

{
  uint uVar1;
  ulong uVar2;
  ulong uVar3;
  uint uVar4;
  uint uVar5;
  ulong uVar6;
  
  uVar1 = ((uint)(param_2 >> 2) & 0x3fffffff) - 1;
  uVar3 = 1;
  while( true ) {
    uVar4 = (uint)uVar3 & uVar1;
    uVar2 = (ulong)uVar4;
    if (uVar4 == 0) break;
    param_1[uVar3] = 0xaaaaaaaa;
    uVar3 = (ulong)((uint)uVar3 << 1);
  }
  *param_1 = 0x55555555;
  uVar3 = 1;
  while (uVar4 = (uint)uVar3, (uVar4 & uVar1) != 0) {
    if (param_1[uVar3] != 0xaaaaaaaa) {
      uVar2 = FUN_d9008b24(s_ADDR_d90098b2,(ulong)(uVar4 << 2),0xaaaaaaaa,param_1[uVar3]);
    }
    uVar3 = (ulong)(uVar4 << 1);
  }
  *param_1 = 0xaaaaaaaa;
  uVar3 = 1;
  while (uVar4 = (uint)uVar3, (uVar4 & uVar1) != 0) {
    param_1[uVar3] = 0x55555555;
    if (*param_1 != 0xaaaaaaaa) {
      uVar2 = FUN_d9008b24(s_ADDR2_d90098b7,(ulong)(uVar4 << 2),0x55555555,*param_1);
    }
    uVar6 = 1;
    while (uVar5 = (uint)uVar6, (uVar5 & uVar1) != 0) {
      if ((param_1[uVar6] != 0xaaaaaaaa) && (uVar5 != uVar4)) {
        uVar2 = FUN_d9008b24(s_ADDR3_d90098bd,(ulong)(uVar4 << 2),0x55555555,param_1[uVar6]);
      }
      uVar6 = (ulong)(uVar5 << 1);
    }
    param_1[uVar3] = 0xaaaaaaaa;
    uVar3 = (ulong)(uVar4 << 1);
  }
  return uVar2;
}



ulong FUN_d9008d6c(uint *param_1,ulong param_2)

{
  uint uVar1;
  uint uVar2;
  long lVar3;
  ulong uVar4;
  uint uVar5;
  uint *puVar6;
  
  uVar5 = (uint)(param_2 >> 2) & 0x3fffffff;
  serial_print(s__Total_Size_0x_d90098c3,param_2,0,s__d9009869);
  for (lVar3 = 0; uVar2 = (uint)lVar3, uVar2 < uVar5; lVar3 = lVar3 + 1) {
    param_1[lVar3] = uVar2 + 1;
    FUN_d9008afc(uVar2);
  }
  uVar4 = 0;
  serial_puts(s__d9009869);
  puVar6 = param_1;
  uVar2 = 0;
  while (uVar1 = uVar2 + 1, uVar2 != uVar5) {
    if (*puVar6 != uVar1) {
      uVar4 = FUN_d9008b24(s_FULL_d90098d2,(ulong)(uVar2 << 2),uVar1,*puVar6);
      uVar4 = uVar4 & 0xffffffff;
    }
    *puVar6 = -uVar2 - 2;
    FUN_d9008afc(uVar2);
    puVar6 = puVar6 + 1;
    uVar2 = uVar1;
  }
  serial_puts(s__d9009869);
  for (uVar2 = 0; uVar2 != uVar5; uVar2 = uVar2 + 1) {
    if (*param_1 != -uVar2 - 2) {
      uVar4 = FUN_d9008b24(s_FULL2_d90098d7,(ulong)(uVar2 << 2),-uVar2 - 2,*param_1);
      uVar4 = uVar4 & 0xffffffff;
    }
    FUN_d9008afc(uVar2);
    param_1 = param_1 + 1;
  }
  serial_puts(s__d9009869);
  return uVar4;
}



void FUN_d9008eb0(void)

{
  uint uVar1;
  
  uVar1 = readl((undefined4 *)&DAT_d9013df8);
  DAT_d900a7d8 = uVar1 >> 0x18;
  DAT_d900a7dc = uVar1 >> 0x10 & 0xff;
  return;
}



undefined4 check_if_remap_addr(uint param_1)

{
  undefined4 uVar1;
  
  uVar1 = 0;
  if (param_1 < 4) {
    uVar1 = (&DAT_d900a7d8)[(int)param_1];
  }
  return uVar1;
}



void FUN_d9008f04(long param_1,undefined param_2,long param_3)

{
  long lVar1;
  
  for (lVar1 = 0; lVar1 != param_3; lVar1 = lVar1 + 1) {
    *(undefined *)(param_1 + lVar1) = param_2;
  }
  return;
}



int memcmp__notsure(long d1,long d2,long len)

{
  long count;
  uint s2;
  byte *s1;
  
  count = 0;
  do {
    if (count == len) {
      return 0;
    }
    s1 = (byte *)(d1 + count);
    count = count + 1;
    s2 = (uint)*(byte *)(d2 + count + -1);
  } while (*s1 == s2);
  return *s1 - s2;
}



void memcpy(long dst,long src,long len)

{
  long count;
  
  for (count = 0; count != len; count = count + 1) {
    *(undefined *)(dst + count) = *(undefined *)(src + count);
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void serial_putc(char c)

{
  if ((byte)c == L'\n') {
    do {
    } while ((_DAT_c81004cc >> 0x15 & 1) != 0);
  }
  do {
  } while ((_DAT_c81004cc >> 0x15 & 1) != 0);
  _DAT_c81004c0 = (uint)(byte)c;
  return;
}



void serial_puts(char *s)

{
  for (; *s != 0; s = s + 1) {
    serial_putc(*s);
  }
  return;
}



void serial_put_hex(ulong hex,int len)

{
  uint uVar1;
  uint uVar2;
  char cVar3;
  ulong uVar4;
  
  for (uVar2 = len - 4; -1 < (int)uVar2; uVar2 = uVar2 - 4) {
    uVar4 = hex >> ((ulong)uVar2 & 0x3f);
    cVar3 = '0';
    if (uVar4 != 0) {
      uVar1 = (uint)uVar4 & 0xf;
      cVar3 = (char)uVar1;
      if (uVar1 < 10) {
        cVar3 = cVar3 + '0';
      }
      else {
        cVar3 = cVar3 + 'W';
      }
    }
    serial_putc(cVar3);
  }
  return;
}



void serial_put_dec(ulong param_1)

{
  uint uVar1;
  ulong uVar2;
  int iVar3;
  ulong uVar4;
  ulong uVar5;
  char local_20 [32];
  
  local_20[0] = '0';
  uVar4 = 0;
  do {
    uVar2 = param_1 / 10;
    local_20[uVar4] = (char)param_1 + (char)uVar2 * -10 + '0';
    iVar3 = (int)uVar4;
    uVar5 = uVar4 & 0xffffffff;
    uVar4 = uVar4 + 1;
    param_1 = uVar2;
  } while (iVar3 + 1U < 0x20 && uVar2 != 0);
  do {
    iVar3 = (int)uVar5;
    uVar1 = iVar3 - 1;
    uVar5 = (ulong)uVar1;
    serial_putc(local_20[iVar3]);
  } while (uVar1 != 0xffffffff);
  return;
}



void serial_print(char *prefix,ulong number,int hex_or_dec,char *suffix)

{
  if (prefix != NULL) {
    serial_puts(prefix);
  }
  if (hex_or_dec == 0) {
    serial_put_hex(number & 0xffffffff,0x20);
  }
  else {
    serial_put_dec(number & 0xffffffff);
  }
  if (suffix != NULL) {
    serial_puts(suffix);
    return;
  }
  return;
}



undefined4 readl(undefined4 *param_1)

{
  return *param_1;
}



undefined8 FUN_d9009128(void)

{
  return DAT_d9009138;
}



bool platform_is_primary_cpu(short param_1)

{
  return param_1 == 0;
}



void FUN_d9009150(void)

{
  return;
}



void FUN_d9009154(void)

{
  return;
}



void FUN_d9009158(ulong param_1,long param_2)

{
  ulong uVar1;
  long lVar2;
  
  uVar1 = ctr_el0;
  lVar2 = 4L << (uVar1 >> 0x10 & 0xf);
  uVar1 = param_1 & (lVar2 - 1U ^ 0xffffffffffffffff);
  do {
    DC_CIVAC(uVar1);
    uVar1 = uVar1 + lVar2;
  } while (uVar1 < param_1 + param_2);
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  return;
}



void FUN_d900918c(ulong param_1,long param_2)

{
  ulong uVar1;
  long lVar2;
  
  uVar1 = ctr_el0;
  lVar2 = 4L << (uVar1 >> 0x10 & 0xf);
  uVar1 = param_1 & (lVar2 - 1U ^ 0xffffffffffffffff);
  do {
    DC_IVAC(uVar1);
    uVar1 = uVar1 + lVar2;
  } while (uVar1 < param_1 + param_2);
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  return;
}



void smc(ulong *cmd,ulong ep_info,ulong unk1,ulong unk2,ulong unk3,ulong unk4,ulong unk5,ulong unk6)

{
  ulong *puVar1;
  
  CallSecureMonitor(0);
  puVar1 = (ulong *)((long)cmd + ep_info);
  for (; 0xf < (long)puVar1 - (long)cmd; cmd = cmd + 2) {
    *cmd = 0;
    cmd[1] = 0;
  }
  for (; cmd != puVar1; cmd = (ulong *)((long)cmd + 1)) {
    *(undefined *)cmd = 0;
  }
  return;
}



void FUN_d90091c4(undefined8 *param_1,long param_2)

{
  undefined8 *puVar1;
  
  puVar1 = (undefined8 *)((long)param_1 + param_2);
  for (; 0xf < (long)puVar1 - (long)param_1; param_1 = param_1 + 2) {
    *param_1 = 0;
    param_1[1] = 0;
  }
  for (; param_1 != puVar1; param_1 = (undefined8 *)((long)param_1 + 1)) {
    *(undefined *)param_1 = 0;
  }
  return;
}



undefined8 FUN_d90091f0(void)

{
  undefined8 uVar1;
  
  uVar1 = id_aa64pfr0_el1;
  return uVar1;
}



void set_cpacr_el1(undefined8 param_1)

{
  cpacr_el1 = param_1;
  return;
}



void BL21_FUN_d900b400(void)

{
  undefined8 *puVar1;
  undefined8 *puVar2;
  
  puVar1 = (undefined8 *)(PTR_DAT_d900b438 + DAT_d900b440);
  for (puVar2 = (undefined8 *)PTR_DAT_d900b438; 0xf < (long)puVar1 - (long)puVar2;
      puVar2 = puVar2 + 2) {
    *puVar2 = 0;
    puVar2[1] = 0;
  }
  for (; puVar2 != puVar1; puVar2 = (undefined8 *)((long)puVar2 + 1)) {
    *(undefined *)puVar2 = 0;
  }
  BL21_FUN_d900b664();
  return;
}



/* WARNING: Removing unreachable block (ram,0xd900b46c) */

void FUN_d900b448(void)

{
                    /* WARNING: Do nothing block with infinite loop */
  do {
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void BL21_FUN_d900b470(int param_1)

{
  if (param_1 == 1) {
    _DAT_c1108558 = _DAT_c1108558 & 0xff80ffff | 0x800002;
    _DAT_c1108554 = 0x1c;
    _DAT_da8344b4 = _DAT_da8344b4 & 0xfffffbff;
    _DAT_da8344b8 = _DAT_da8344b8 & 0xffffffdf | 0x800;
  }
  else if (param_1 == 3) {
    _DAT_c1108648 = _DAT_c1108648 & 0xff80ffff | 0x800002;
    _DAT_c1108644 = 0x1c;
    _DAT_da8344b4 = _DAT_da8344b4 & 0xfffff5ff;
    _DAT_da8344b8 = _DAT_da8344b8 | 0x1000;
  }
  FUN_d900b448();
  return;
}



void BL21_set_v(int param_1,uint param_2)

{
  long lVar1;
  undefined4 uVar2;
  ulong uVar3;
  ulong uVar4;
  
  uVar3 = 0;
  do {
    uVar4 = uVar3 & 0xffffffff;
    if (uVar3 == 0x1d) {
      uVar4 = 0x1c;
      break;
    }
    lVar1 = uVar3 * 2;
    uVar3 = uVar3 + 1;
  } while ((uint)(&DAT_d900b754)[lVar1] < param_2);
  if (param_1 == 1) {
    uVar2 = *(undefined4 *)
             ((long)&DAT_d900b750 + (-(uVar4 >> 0x1f) & 0xfffffff800000000 | uVar4 << 3));
    uVar3 = 0x8554;
  }
  else {
    if (param_1 != 3) goto LAB_d900b5e0;
    uVar2 = *(undefined4 *)
             ((long)&DAT_d900b750 + (-(uVar4 >> 0x1f) & 0xfffffff800000000 | uVar4 << 3));
    uVar3 = 0x8644;
  }
  *(undefined4 *)(uVar3 | 0xc1100000) = uVar2;
LAB_d900b5e0:
  FUN_d900b448();
  return;
}



void BL21_FUN_d900b5e8(void)

{
  BL21_FUN_d900b470(1);
  BL21_FUN_d900b470(3);
  BL21_serial_puts((byte *)s_set_vcck_to_d900b838);
  BL21_serial_put_dec(1120);
  BL21_serial_puts((byte *)s__mv_d900b845);
  BL21_set_v(3,0x460);
  BL21_serial_puts((byte *)s_set_vddee_to_d900b84a);
  BL21_serial_put_dec(1000);
  BL21_serial_puts((byte *)s__mv_d900b845);
  BL21_set_v(1,1000);
  return;
}



void BL21_FUN_d900b664(void)

{
  BL21_FUN_d900b5e8();
  return;
}



void BL21_FUN_d900b664(void)

{
  BL21_FUN_d900b5e8();
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void BL21_serial_putc(int param_1)

{
  if (param_1 == 10) {
    do {
    } while ((_DAT_c81004cc >> 0x15 & 1) != 0);
  }
  do {
  } while ((_DAT_c81004cc >> 0x15 & 1) != 0);
  _DAT_c81004c0 = param_1;
  return;
}



void BL21_serial_puts(byte *param_1)

{
  for (; *param_1 != 0; param_1 = param_1 + 1) {
    BL21_serial_putc((uint)*param_1);
  }
  return;
}



void BL21_serial_put_dec(ulong param_1)

{
  uint uVar1;
  ulong uVar2;
  ulong uVar3;
  int iVar4;
  ulong uVar5;
  byte local_10 [16];
  
  local_10[0] = 0x30;
  uVar3 = 0;
  do {
    uVar2 = param_1 / 10;
    local_10[uVar3] = (char)param_1 + (char)uVar2 * -10 + 0x30;
    uVar5 = uVar3 & 0xffffffff;
    uVar3 = uVar3 + 1;
    param_1 = uVar2;
  } while (uVar2 != 0);
  do {
    iVar4 = (int)uVar5;
    uVar1 = iVar4 - 1;
    uVar5 = (ulong)uVar1;
    BL21_serial_putc((uint)local_10[iVar4]);
  } while (uVar1 != 0xffffffff);
  return;
}


