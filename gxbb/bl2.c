typedef unsigned char   undefined;

typedef unsigned char    bool;
typedef unsigned char    byte;
typedef unsigned char    uchar;
typedef unsigned int    uint;
typedef unsigned long    ulong;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined8;
typedef unsigned short    ushort;
typedef struct struct image_info struct image_info, *Pstruct image_info;

struct struct image_info {
    ulong src;
    ulong size;
    char * name;
};

typedef struct meminfo_struct meminfo_struct, *Pmeminfo_struct;

struct meminfo_struct {
    undefined8 total_base;
    undefined8 total_size;
    undefined8 free_base;
    undefined8 free_size;
    undefined8 attr;
    undefined8 next;
};

#define USB_BL2_RETURN_ROM_ADDR 3640935684

#define BL2_SEC_BOOT_SP_BASE 25165824

#define USB_BL2_RUN_CMD_PARA_ADDR 3640705024

#define _UUID_NODE_LEN 6

#define BL2_NAND_BUF_SIZE 1048576

#define FM_FIP_HEADER_LOAD_ADDR 20971520

#define TOC_HEADER_SERIAL_NUMBER 305419896

#define FM_USB_MODE_LOAD_ADDR 33603584

#define FM_BIN_BL30_SIZE 40

#define FM_BL31_LOAD_ADDR 269484032

#define FM_BIN_BL33_SIZE 160

#define BL2_MMAP_BASE 23068672

#define FM_BIN_BL31_SIZE 80

#define FIP_HEADER_SIZE_OFFSET 32

#define BL2_MMU_TABLE_BASE 22020096

#define FM_BIN_BL32_OFFSET 112

#define BL2_SEC_BOOT_BUF_SIZE 1048576

#define FM_BIN_BL33_OFFSET 152

#define FM_BIN_BL31_OFFSET 72

#define FM_BIN_BL30_OFFSET 32

#define BL2_SEC_BOOT_BUF_BASE 24117248

#define FM_BL32_LOAD_ADDR 85983232

#define FM_BIN_BL32_SIZE 120

#define TOC_HEADER_NAME 2858680321

#define FM_FIP_BL3X_TEMP_LOAD_ADDR 24117248

#define BL2_NAND_BUF_BASE 25165824

#define BL2_RETURN_ROM_USB_ADDR 3640933872

#define PL011_BAUDRATE 115200

#define PL011_CLK_IN_HZ 24000000

#define PL011_UARTFR_RI 256

#define UARTIBRD 36

#define UARTRSR 4

#define UARTFBRD 40

#define PL011_UARTLCR_H_WLEN_8 96

#define PL011_UARTCR_RTSEN 16384

#define UARTIMSC 56

#define PL011_UARTLCR_H_WLEN_7 64

#define PL011_UARTLCR_H_WLEN_6 32

#define PL011_UARTCR_UARTEN 1

#define PL011_UARTLCR_H_WLEN_5 0

#define UARTDR 0

#define PL011_UARTFR_RXFE 16

#define PL011_UARTFR_RXFF 64

#define UARTMIS 64

#define PL011_UARTCR_LBE 128

#define PL011_UARTLCR_H_FEN 16

#define PL011_LINE_CONTROL 112

#define UART_STATUS_ERROR_MASK 15

#define UARTCR 48

#define PL011_UARTFR_CTS 1

#define PL011_UARTFR_BUSY 8

#define PL011_UARTLCR_H_SPS 128

#define PL011_UARTCR_CTSEN 32768

#define PL011_UARTLCR_H_EPS 4

#define PL011_UARTLCR_H_BRK 1

#define PL011_UARTFR_DCD 4

#define PL011_UARTLCR_H_PEN 2

#define PL011_UARTFR_DSR 2

#define UART_DATA_ERROR_MASK 3840

#define PL011_UARTFR_TXFF 32

#define UARTDMACR 72

#define PL011_UARTFR_TXFE 128

#define UARTILPR 32

#define UARTIFLS 52

#define UARTRIS 60

#define UARTFR 24

#define PL011_UARTLCR_H_STP2 8

#define UARTICR 68

#define PL011_UARTCR_RTS 2048

#define UARTECR 4

#define UARTLCR_H 44

#define PL011_UARTCR_DTR 1024

#define PL011_UARTCR_TXE 256

#define PL011_UARTCR_RXE 512

#define INT_CLEAR_CLEAR_SHIFT 0

#define ACTION_RV_HIGHERR 3

#define CID1_OFF 4084

#define GATE_KEEPER_OS_MASK 15

#define REGION_ATTRIBUTES_F_EN_MASK 15

#define INT_STATUS_STATUS_SHIFT 0

#define FAIL_CONTROL_PRIV_SHIFT 1048576

#define BUILD_CONFIG_NF_MASK 3

#define BUILD_CONFIG_OFF 0

#define FAIL_ID_VNET_SHIFT 24

#define ACTION_OFF 4

#define PID6_OFF 4056

#define REGION_ID_ACCESS_NSAID_ID_MASK 15

#define REGION_BASE_LOW_OFF 256

#define REGION_TOP_HIGH_OFF 268

#define TZC400_COMPONENT_ID 2969956365

#define INT_CLEAR 20

#define FAIL_CONTROL_PRIV_UNPRIV 1

#define BUILD_CONFIG_NR_MASK 31

#define ACTION_RV_HIGHOK 2

#define GATE_KEEPER_OR_MASK 15

#define FAIL_ID_VNET_MASK 15

#define PID4_OFF 4048

#define REGION_ID_ACCESS_NSAID_WR_EN_SHIFT 16

#define PID3_OFF 4076

#define GATE_KEEPER_OFF 8

#define FAIL_ADDRESS_LOW_OFF 32

#define ACTION_RV_MASK 3

#define FAIL_CONTROL_NS_SHIFT 2097152

#define FAIL_ID 44

#define REGION_ID_ACCESS_NSAID_RD_EN_SHIFT 0

#define PID1_OFF 4068

#define INT_STATUS_OVERRUN_SHIFT 8

#define REGION_ATTRIBUTES_OFF 272

#define INT_STATUS 16

#define FAIL_CONTROL_DIR_WRITE 1

#define FAIL_ID_ID_SHIFT 0

#define SPECULATION_CTRL_WRITE_DISABLE 2

#define INT_STATUS_OVERLAP_MASK 15

#define REGION_TOP_LOW_OFF 264

#define FAIL_CONTROL_DIR_SHIFT 16777216

#define FAIL_CONTROL_PRIV_PRIV 0

#define CID2_OFF 4088

#define FAIL_CONTROL_OFF 40

#define FAIL_CONTROL_NS_NONSECURE 1

#define REGION_BASE_HIGH_OFF 260

#define PID5_OFF 4052

#define INT_CLEAR_CLEAR_MASK 15

#define BUILD_CONFIG_NR_SHIFT 0

#define CID0_OFF 4080

#define FAIL_CONTROL_DIR_READ 0

#define SPECULATION_CTRL_READ_DISABLE 1

#define INT_STATUS_STATUS_MASK 15

#define INT_STATUS_OVERRUN_MASK 15

#define ACTION_RV_LOWOK 0

#define BUILD_CONFIG_AW_SHIFT 8

#define GATE_KEEPER_OR_SHIFT 0

#define FAIL_ADDRESS_HIGH_OFF 36

#define REGION_ATTRIBUTES_SEC_SHIFT 30

#define FAIL_CONTROL_NS_SECURE 0

#define REGION_ID_ACCESS_OFF 276

#define PID2_OFF 4072

#define ACTION_RV_SHIFT 0

#define BUILD_CONFIG_AW_MASK 63

#define PID7_OFF 4060

#define CID3_OFF 4092

#define BUILD_CONFIG_NF_SHIFT 24

#define INT_STATUS_OVERLAP_SHIFT 16

#define REGION_ATTRIBUTES_F_EN_SHIFT 0

#define PID0_OFF 4064

#define ACTION_RV_LOWERR 1

#define GATE_KEEPER_OS_SHIFT 16

#define SPECULATION_CTRL_OFF 12

#define UART_CNTL_MASK_PRTY_EVEN 0

#define UART_CNTL_MASK_CHAR_8BIT 0

#define UART_CNTL_MASK_RINT_EN 134217728

#define UART_CNTL_MASK_PRTY_EN 524288

#define UART_CNTL_MASK_INV_RTS 2147483648

#define UART_STAT_MASK_FRAM_ERR 131072

#define UART_CNTL_MASK_2WIRE 32768

#define UART_CNTL_MASK_CHAR_LEN 3145728

#define UART_STAT_MASK_RFIFO_EMPTY 1048576

#define UART_CNTL_MASK_STP_BITS 196608

#define UART_STAT_MASK_RFIFO_FULL 524288

#define UART_CNTL_MASK_CHAR_7BIT 1048576

#define UART_STAT_MASK_TFIFO_CNT 16128

#define UART_CNTL_MASK_RST_RX 8388608

#define UART_CNTL_MASK_PRTY_ODD 262144

#define UART_STAT_MASK_TFIFO_FULL 2097152

#define UART_STAT_MASK_RFIFO_CNT 63

#define UART_CNTL_MASK_INV_RX 33554432

#define UART_STATUS 12

#define UART_CNTL_MASK_TX_EN 4096

#define UART_CNTL_MASK_INV_CTS 536870912

#define UART_CNTL_MASK_CHAR_6BIT 2097152

#define UART_CNTL_MASK_CLR_ERR 16777216

#define UART_CNTL_MASK_STP_1BIT 0

#define UART_STAT_MASK_WFULL_ERR 262144

#define UART_RFIFO 4

#define UART_CNTL_MASK_TINT_EN 268435456

#define UART_STAT_MASK_TFIFO_EMPTY 4194304

#define UART_CNTL_MASK_RX_EN 8192

#define UART_CNTL_MASK_BAUD_RATE 4095

#define UART_STAT_MASK_PRTY_ERR 65536

#define UART_CNTL_MASK_MASK_ERR 1073741824

#define UART_CNTL_MASK_RST_TX 4194304

#define UART_CNTL_MASK_CHAR_5BIT 3145728

#define UART_MISC 16

#define UART_CONTROL 8

#define UART_WFIFO 0

#define UART_CNTL_MASK_STP_2BIT 65536

#define UART_CNTL_MASK_INV_TX 67108864

#define UART_CNTL_MASK_PRTY_TYPE 262144

#define NULL 0

#define WAKER_CA 4

#define GICV3_AFF2_SHIFT 16

#define GICR_TYPER_LAST 16

#define GICR_TYPER_AFF_MASK 4294967295

#define ICC_SRE_SRE 1

#define ICC_SRE_EN 8

#define GICV3_AFF0_SHIFT 0

#define GICV3_AFFINITY_MASK 4294967295

#define GICV3_AFFLVL_MASK 255

#define GICR_PCPUBASE_SHIFT 17

#define GICR_TYPER_AFF_SHIFT 32

#define GICR_WAKER 20

#define GICV3_AFF3_SHIFT 24

#define GICR_TYPER 8

#define WAKER_PS 2

#define GICV3_AFF1_SHIFT 8

#define __SHRT_MIN -32768

#define __WORD_BIT 32

#define __OFF_MAX 9223372036854775807

#define __ULONG_MAX -1

#define __INT_MAX 2147483647

#define __QUAD_MAX 9223372036854775807

#define __USHRT_MAX 65535

#define __LONG_MIN -9223372036854775808

#define __CHAR_BIT 8

#define __SCHAR_MIN -128

#define __LONG_MAX 9223372036854775807

#define __INT_MIN -2147483648

#define __QUAD_MIN -9223372036854775808

#define __OFF_MIN -9223372036854775808

#define __SIZE_T_MAX -1

#define __UINT_MAX 4294967295

#define __LONG_BIT 64

#define __UQUAD_MAX -1

#define __LLONG_MIN -9223372036854775808

#define __MINSIGSTKSZ 4096

#define __LLONG_MAX 9223372036854775807

#define __ULLONG_MAX -1

#define __UCHAR_MAX 255

#define __SCHAR_MAX 127

#define __SHRT_MAX 32767

#define __SSIZE_MAX 9223372036854775807

#define CTX_CPTR_EL3 48

#define CTX_SYSREGS_END 240

#define CTX_FPREGS_END 528

#define CTX_SCTLR_EL3 40

#define CTX_SCTLR_EL1 48

#define CTX_EL3STATE_OFFSET 256

#define CTX_DAIF_EL3 96

#define CTX_FP_FPEXC32_EL2 224

#define CTX_FP_Q4 64

#define CTX_FP_Q5 80

#define CTX_FP_Q6 96

#define CTX_FP_Q7 112

#define CTX_FP_Q0 0

#define CTX_FP_Q1 16

#define CTX_FP_Q2 32

#define DWORD_SHIFT 3

#define CTX_FP_Q3 48

#define CTX_FP_Q11 176

#define CTX_FP_Q12 192

#define CTX_FP_Q13 208

#define CTX_FP_Q14 224

#define CTX_FP_Q15 240

#define CTX_FP_Q8 128

#define CTX_FP_Q16 256

#define CTX_FP_Q9 144

#define CTX_FP_Q17 272

#define CTX_FP_Q18 288

#define CTX_FP_Q10 160

#define CTX_PAR_EL1 176

#define CTX_TPIDRRO_EL0 152

#define CTX_ELR_EL1 8

#define CTX_ELR_EL3 24

#define CTX_FP_Q22 352

#define CTX_FP_Q23 368

#define CTX_FP_Q24 384

#define CTX_SP_EL1 80

#define CTX_FP_Q25 400

#define CTX_FP_Q26 416

#define CTX_FP_Q27 432

#define CTX_FP_Q28 448

#define CTX_FP_Q29 464

#define CTX_FP_Q20 320

#define CTX_FP_Q21 336

#define CTX_TCR_EL1 128

#define CTX_FPREG_ALL 66

#define CTX_TCR_EL3 80

#define CTX_FP_Q19 304

#define CTX_SPSR_FIQ 40

#define CTX_FP_Q30 480

#define CTX_FP_Q31 496

#define CTX_CNTFRQ_EL0 64

#define CTX_AFSR0_EL1 192

#define CTX_GPREG_LR 240

#define CTX_TTBR0_EL1 96

#define CTX_TTBR0_EL3 88

#define CTX_ACTLR_EL1 56

#define CTX_FPREGS_OFFSET 608

#define CTX_CONTEXTIDR_EL1 208

#define CTX_GPREGS_END 256

#define CTX_RUNTIME_SP 8

#define CTX_SPSR_EL1 0

#define CTX_SPSR_EL3 16

#define CTX_ESR_EL1 88

#define CTX_SPSR_ABT 16

#define CTX_SPSR_IRQ 32

#define CTX_GPREG_X10 80

#define CTX_AFSR1_EL1 200

#define CTX_GPREG_X18 144

#define CTX_SCR_EL3 32

#define CTX_GPREG_X17 136

#define CTX_GPREG_X16 128

#define CTX_GPREG_X15 120

#define CTX_GPREG_X14 112

#define CTX_GPREG_X13 104

#define CTX_GPREG_X12 96

#define CTX_GPREG_X11 88

#define CTX_SYSREGS_OFFSET 368

#define CTX_GPREG_X19 152

#define CTX_AMAIR_EL1 120

#define CTX_TPIDR_EL1 136

#define CTX_TPIDR_EL0 144

#define CTX_TTBR1_EL1 104

#define CTX_GPREGS_OFFSET 0

#define PTR_CACHE_CRASH_STACK_OFFSET 0

#define CTX_MAIR_EL1 112

#define CTX_MAIR_EL3 72

#define CTX_GPREG_X8 64

#define CTX_GPREG_X9 72

#define CTX_VBAR_EL3 0

#define CTX_VBAR_EL1 216

#define CTX_GPREG_SP_EL0 248

#define CTX_GPREG_X4 32

#define CTX_GPREG_X5 40

#define CTX_GPREG_X6 48

#define CTX_GPREG_X7 56

#define CTX_GPREG_X0 0

#define CTX_GPREG_X1 8

#define CTX_GPREG_X2 16

#define CTX_IFSR32_EL2 168

#define CTX_GPREG_X3 24

#define CTX_GPREG_X21 168

#define CTX_GPREG_X20 160

#define CTX_FP_FPSR 512

#define CTX_GPREG_X29 232

#define CTX_GPREG_X28 224

#define CTX_GPREG_X27 216

#define CTX_GPREG_X26 208

#define CTX_GPREG_X25 200

#define CTX_GPREG_X24 192

#define CTX_CPACR_EL1 64

#define CTX_GPREG_X23 184

#define CTX_GPREG_X22 176

#define CTX_CSSELR_EL1 72

#define CTX_EL3STATE_END 112

#define CTX_DACR32_EL2 160

#define CTX_FP_FPCR 520

#define CTX_SPSR_UND 24

#define CTX_FAR_EL1 184

#define TABLE_DESC 3

#define ID_AA64PFR0_EL3_SHIFT 12

#define MIDR_PN_SHIFT 4

#define TCR_RGN_OUTER_WBNA 3072

#define SPSR_DAIF_SHIFT 6

#define MPIDR_CLUSTER_MASK 65280

#define L3_XLAT_ADDRESS_SHIFT 12

#define DAIF_IRQ_BIT 2

#define SCTLR_A_BIT 2

#define EC_PC_ALIGN 34

#define SPSR_T_ARM 0

#define EC_AARCH64_FP 44

#define CONT_HINT 1

#define XN 4

#define XLAT_TABLE_ENTRIES 512

#define SCR_FIQ_BIT 4

#define XLAT_TABLE_ENTRIES_SHIFT 9

#define NUM_2MB_IN_GB 512

#define EC_IABORT_LOWER_EL 32

#define MPIDR_AFF1_SHIFT 8

#define XLAT_ENTRY_SIZE 8

#define MIDR_PN_AEM 3343

#define CNTP_CTL_IMASK_SHIFT 1

#define ATTR_SO 0

#define TCR_SH_NON_SHAREABLE 0

#define NUM_GB_IN_4GB 4

#define FOUR_KB_SHIFT 12

#define SPSR_T_THUMB 1

#define DCISW 0

#define EC_FP_SIMD 7

#define ISH 192

#define MPIDR_AFF3_SHIFT 32

#define LOC_SHIFT 24

#define AP_RO 32

#define MODE_EL_SHIFT 2

#define INVALID_DESC 0

#define AP_RW 0

#define SPSR_E_MASK 1

#define SCTLR_I_BIT 4096

#define MPIDR_AFFINITY_BITS 8

#define PAGE_SIZE_MASK 4095

#define MPIDR_AFFLVL3 3

#define MPIDR_AFFLVL2 2

#define MPIDR_AFFLVL1 1

#define MPIDR_AFFLVL0 0

#define EC_AARCH32_CP15_MRC_MCR 3

#define EL1PCTEN_BIT 1

#define MODE32_und 11

#define HCR_IMO_BIT 16

#define HCR_RW_BIT 2147483648

#define THIRD_LEVEL_DESC_N 12

#define SCR_NS_BIT 1

#define CNTCR_EN 1

#define DCCSW 2

#define MODE32_fiq 1

#define SCR_SIF_BIT 512

#define CNTP_CTL_ENABLE_SHIFT 0

#define MIDR_REV_SHIFT 0

#define DAIF_DBG_BIT 8

#define EC_AARCH32_FP 40

#define TWO_MB_SHIFT 21

#define SCTLR_C_BIT 4

#define CNTACR_RPCT_SHIFT 0

#define SPSR_E_SHIFT 9

#define EC_AARCH64_SMC 23

#define OSH 128

#define EL0VCTEN_BIT 2

#define PAGE_SIZE 4096

#define ID_AA64PFR0_EL0_SHIFT 0

#define EC_WFE_WFI 1

#define DISABLE_ALL_EXCEPTIONS 15

#define CNTACR_RWPT_SHIFT 5

#define CNTACR_RVOFF_SHIFT 3

#define DCCISW 1

#define SCR_TWE_BIT 8192

#define TCR_RGN_OUTER_NC 0

#define FIRST_MPIDR 0

#define CPUACTLR_NO_ALLOC_WBWA 562949953421312

#define MODE_EL2 2

#define MODE_EL1 1

#define MODE_EL0 0

#define MODE_EL3 3

#define EC_AARCH64_HVC 22

#define ACCESS_FLAG 256

#define SCR_RW_BIT 1024

#define MODE32_abt 7

#define EC_SERROR 47

#define CPACR_EL1_FP_TRAP_ALL 2

#define ID_AA64PFR0_EL1_SHIFT 4

#define CPUECTLR_SMP_BIT 64

#define MODE32_irq 2

#define NS 8

#define ID_PFR1_VIRTEXT_MASK 15

#define PXN 2

#define LEVEL2 2

#define LEVEL3 3

#define LEVEL1 1

#define ONE_GB_SHIFT 30

#define CNTP_CTL_ISTATUS_MASK 1

#define XLAT_TABLE_ENTRIES_MASK 511

#define EC_AARCH32_SVC 17

#define EC_AARCH32_CP15_MRRC_MCRR 4

#define DAIF_FIQ_BIT 1

#define SPSR_DAIF_MASK 15

#define EC_AARCH32_CP10_MRC 8

#define CNTACR_RVCT_SHIFT 1

#define SCTLR_B_BIT 128

#define MPIDR_MAX_AFFLVL 2

#define SCR_ST_BIT 2048

#define EC_DABORT_LOWER_EL 36

#define EL0PCTEN_BIT 1

#define CNTP_CTL_ENABLE_MASK 1

#define ATTR_SO_INDEX 2

#define CPUACTLR_DCC_AS_DCCI 17592186044416

#define TCR_SH_INNER_SHAREABLE 12288

#define PAGE_SIZE_SHIFT 12

#define LEVEL_SHIFT 1

#define NSH 0

#define MPIDR_AFFLVL_MASK 255

#define SCR_HCE_BIT 256

#define EC_IABORT_CUR_EL 33

#define SPSR_E_BIG 1

#define SCTLR_WXN_BIT 524288

#define XLAT_TABLE_SIZE 4096

#define SCR_EA_BIT 8

#define MIDR_VAR_MASK 15

#define ID_PFR1_VIRTEXT_SHIFT 12

#define MODE32_mon 6

#define MODE_RW_64 0

#define SCTLR_EE_BIT 33554432

#define EC_UNKNOWN 0

#define TCR_RGN_INNER_WBA 256

#define ATTR_IWBWA_OWBWA_NTR_INDEX 0

#define SCR_VALID_BIT_MASK 12175

#define EC_AARCH64_SVC 21

#define SCR_SMD_BIT 128

#define TTA_BIT 1048576

#define MODE_EL_MASK 3

#define SPSR_T_SHIFT 5

#define TCR_SH_OUTER_SHAREABLE 8192

#define CNTP_CTL_ISTATUS_SHIFT 2

#define ID_AA64PFR0_EL2_SHIFT 8

#define MIDR_PN_MASK 4095

#define MODE_RW_32 1

#define MIDR_PN_A57 3335

#define EC_AARCH32_CP14_MRRC_MCRR 12

#define MIDR_PN_A53 3331

#define TCR_T0SZ_4GB 32

#define MPIDR_AFF2_SHIFT 16

#define SCTLR_M_BIT 1

#define CNTFID_OFF 32

#define SCR_TWI_BIT 4096

#define EC_AARCH32_SMC 19

#define TCR_RGN_INNER_NC 0

#define MPIDR_CPU_MASK 255

#define CPUACTLR_DIS_DMB_NULL 288230376151711744

#define EC_DABORT_CUR_EL 37

#define MODE_RW_SHIFT 4

#define L2_XLAT_ADDRESS_SHIFT 21

#define NUM_4K_IN_2MB 512

#define ATTR_IWBWA_OWBWA_NTR 255

#define MPIDR_AFFINITY_MASK 1095233437695

#define L1_XLAT_ADDRESS_SHIFT 30

#define ESR_EC_SHIFT 26

#define MODE32_sys 15

#define MIDR_REV_MASK 15

#define ATTR_DEVICE 4

#define SPSR_E_LITTLE 0

#define CNTNSAR 4

#define MODE_SP_MASK 1

#define FIRST_LEVEL_DESC_N 30

#define TCR_RGN_INNER_WBNA 768

#define SPSR_T_MASK 1

#define SCTLR_SA_BIT 8

#define CNTCR_OFF 0

#define EC_AARCH32_CP14_MRC_MCR 5

#define MODE_RW_MASK 1

#define XLAT_TABLE_SIZE_SHIFT 12

#define MODE_SP_EL0 0

#define HCR_AMO_BIT 32

#define TCR_EL3_RES1 2155872256

#define EL0PTEN_BIT 512

#define SCR_RES1_BITS 48

#define MODE_SP_SHIFT 0

#define SPSR_AIF_MASK 7

#define SECOND_LEVEL_DESC_N 21

#define ESR_EC_MASK 63

#define CNTACR_RWVT_SHIFT 4

#define LOUIS_SHIFT 21

#define EC_AARCH32_HVC 18

#define MODE_SP_ELX 1

#define TCR_RGN_OUTER_WBA 1024

#define TFP_BIT 1024

#define ID_AA64PFR0_ELX_MASK 15

#define TCPAC_BIT 2147483648

#define CLIDR_FIELD_WIDTH 3

#define CNTP_CTL_IMASK_MASK 1

#define HCR_FMO_BIT 8

#define MODE32_svc 3

#define DAIF_ABT_BIT 4

#define CPACR_EL1_FP_TRAP_NONE 3

#define CPACR_EL1_FP_TRAP_EL0 1

#define TCR_RGN_OUTER_WT 2048

#define MODE32_usr 0

#define MODE32_hyp 10

#define EC_SP_ALIGN 38

#define SPSR_AIF_SHIFT 6

#define ATTR_DEVICE_INDEX 1

#define EL1PCEN_BIT 2

#define MIDR_VAR_SHIFT 20

#define EC_AARCH32_CP14_LDC_STC 6

#define CNTCR_HDBG 2

#define EC_AARCH64_SYS 24

#define CNTACR_RFRQ_SHIFT 2

#define MODE32_MASK 15

#define SCR_IRQ_BIT 2

#define ESR_EC_LENGTH 6

#define MPIDR_AFFLVL_SHIFT 3

#define MPIDR_AFF0_SHIFT 0

#define MODE32_SHIFT 0

#define SCTLR_Z_BIT 2048

#define BLOCK_DESC 1

#define EC_ILLEGAL 14

#define EL0VTEN_BIT 256

#define XLAT_ENTRY_SIZE_SHIFT 3

#define SCTLR_EXCEPTION_BITS 192

#define NON_GLOBAL 512

#define TCR_RGN_INNER_WT 512

typedef enum enum_3 {
    IO_SEEK_INVALID=0,
    IO_SEEK_SET=1,
    IO_SEEK_END=2,
    IO_SEEK_CUR=3,
    IO_SEEK_MAX=4
} enum_3;

typedef enum enum_3 io_seek_mode_t;

typedef struct io_dev_connector io_dev_connector, *Pio_dev_connector;

struct io_dev_connector {
};

typedef enum enum_2 {
    IO_TYPE_INVALID=0,
    IO_TYPE_SEMIHOSTING=1,
    IO_TYPE_MEMMAP=2,
    IO_TYPE_FIRMWARE_IMAGE_PACKAGE=3,
    IO_TYPE_MAX=4
} enum_2;

typedef struct io_file_spec io_file_spec, *Pio_file_spec;

typedef struct io_file_spec io_file_spec_t;

struct io_file_spec {
    char * path;
    uint mode;
};

typedef struct io_block_spec io_block_spec, *Pio_block_spec;

struct io_block_spec {
};

typedef enum enum_2 io_type_t;

#define IO_SUCCESS 0

#define IO_FAIL -1

#define IO_RESOURCES_EXHAUSTED -3

#define IO_MODE_RW 2

#define IO_MODE_INVALID 0

#define IO_MODE_RO 1

#define IO_NOT_SUPPORTED -2

typedef struct ddr_set ddr_set, *Pddr_set;

struct ddr_set {
    uchar ddr_channel_set;
    uchar ddr_type;
    uchar ddr_2t_mode;
    uchar ddr_full_test;
    uchar ddr_size_detect;
    uchar ddr_drv;
    uchar ddr_odt;
    uchar ddr_timing_ind;
    ushort ddr_size;
    ushort ddr_clk;
    uint ddr_base_addr;
    uint ddr_start_offset;
    uint ddr_pll_ctrl;
    uint ddr_dmc_ctrl;
    uint ddr0_addrmap[5];
    uint ddr1_addrmap[5];
    uint t_pub_ptr[5];
    ushort t_pub_mr[4];
    uint t_pub_odtcr;
    uint t_pub_dtpr[4];
    uint t_pub_pgcr0;
    uint t_pub_pgcr1;
    uint t_pub_pgcr2;
    uint t_pub_pgcr3;
    uint t_pub_dxccr;
    uint t_pub_dtcr;
    uint t_pub_aciocr[5];
    uint t_pub_dx0gcr[3];
    uint t_pub_dx1gcr[3];
    uint t_pub_dx2gcr[3];
    uint t_pub_dx3gcr[3];
    uint t_pub_dcr;
    uint t_pub_dtar;
    uint t_pub_dsgcr;
    uint t_pub_zq0pr;
    uint t_pub_zq1pr;
    uint t_pub_zq2pr;
    uint t_pub_zq3pr;
    ushort t_pctl0_1us_pck;
    ushort t_pctl0_100ns_pck;
    ushort t_pctl0_init_us;
    ushort t_pctl0_rsth_us;
    uint t_pctl0_mcfg;
    uint t_pctl0_mcfg1;
    ushort t_pctl0_scfg;
    ushort t_pctl0_sctl;
    uint t_pctl0_ppcfg;
    ushort t_pctl0_dfistcfg0;
    ushort t_pctl0_dfistcfg1;
    ushort t_pctl0_dfitctrldelay;
    ushort t_pctl0_dfitphywrdata;
    ushort t_pctl0_dfitphywrlta;
    ushort t_pctl0_dfitrddataen;
    ushort t_pctl0_dfitphyrdlat;
    ushort t_pctl0_dfitdramclkdis;
    ushort t_pctl0_dfitdramclken;
    ushort t_pctl0_dfitphyupdtype1;
    ushort t_pctl0_dfitctrlupdmin;
    ushort t_pctl0_cmdtstaten;
    uint t_pctl0_dfiodtcfg;
    uint t_pctl0_dfiodtcfg1;
    uint t_pctl0_dfilpcfg0;
    uint t_pub_acbdlr0;
    uint ddr_func;
    uchar wr_adj_per[6];
    uchar rd_adj_per[6];
    ushort t_pub_mr11;
    uchar t_lpddr3_ca0;
    uchar t_lpddr3_ca1;
    uchar t_lpddr3_remap;
    uchar t_lpddr3_wl;
    uchar rsv1;
    uchar rsv2;
};

typedef struct ddr_timing ddr_timing, *Pddr_timing;

typedef struct ddr_timing ddr_timing_t;

struct ddr_timing {
    uchar identifier;
    uchar cfg_ddr_rtp;
    uchar cfg_ddr_wtr;
    uchar cfg_ddr_rp;
    uchar cfg_ddr_rcd;
    uchar cfg_ddr_ras;
    uchar cfg_ddr_rrd;
    uchar cfg_ddr_rc;
    uchar cfg_ddr_mrd;
    uchar cfg_ddr_mod;
    uchar cfg_ddr_faw;
    uchar cfg_ddr_wlmrd;
    uchar cfg_ddr_wlo;
    uchar cfg_ddr_xp;
    ushort cfg_ddr_rfc;
    ushort cfg_ddr_xs;
    ushort cfg_ddr_dllk;
    uchar cfg_ddr_cke;
    uchar cfg_ddr_rtodt;
    uchar cfg_ddr_rtw;
    uchar cfg_ddr_refi;
    uchar cfg_ddr_refi_mddr3;
    uchar cfg_ddr_cl;
    uchar cfg_ddr_wr;
    uchar cfg_ddr_cwl;
    uchar cfg_ddr_al;
    uchar cfg_ddr_dqs;
    uchar cfg_ddr_cksre;
    uchar cfg_ddr_cksrx;
    uchar cfg_ddr_zqcs;
    uchar cfg_ddr_xpdll;
    ushort cfg_ddr_exsr;
    ushort cfg_ddr_zqcl;
    ushort cfg_ddr_zqcsi;
    ushort cfg_ddr_rpab;
    ushort cfg_ddr_rppb;
    ushort cfg_ddr_tdqsck;
    ushort cfg_ddr_tdqsckmax;
    ushort cfg_ddr_tckesr;
    ushort cfg_ddr_tdpd;
    ushort cfg_ddr_taond_aofd;
};

typedef struct pll_set pll_set, *Ppll_set;

struct pll_set {
    ushort cpu_clk;
    ushort pxp;
    uint spi_ctrl;
    ushort vddee;
    ushort vcck;
    uchar szPad[4];
    ulong lCustomerID;
};

typedef struct pll_set pll_set_t;

typedef struct ddr_set ddr_set_t;

#define ENOTBLK 15

#define EILSEQ 86

#define EFAULT 14

#define ENOBUFS 55

#define EISCONN 56

#define ENODEV 19

#define ENETRESET 52

#define ECAPMODE 94

#define ENXIO 6

#define ENOTCONN 57

#define ENETUNREACH 51

#define EINTR 4

#define ENOTTY 25

#define EPFNOSUPPORT 46

#define ELAST 94

#define EXDEV 18

#define EIDRM 82

#define EPROTONOSUPPORT 43

#define EAFNOSUPPORT 47

#define ENOLCK 77

#define ENOMEM 12

#define ERPCMISMATCH 73

#define ECANCELED 85

#define ENOSPC 28

#define EPROGUNAVAIL 74

#define EPROCUNAVAIL 76

#define ERANGE 34

#define ELOOP 62

#define EHOSTUNREACH 65

#define ENOEXEC 8

#define EPROGMISMATCH 75

#define EMSGSIZE 40

#define ENOLINK 91

#define EROFS 30

#define ENOATTR 87

#define EINPROGRESS 36

#define EBUSY 16

#define EMULTIHOP 90

#define ENOTCAPABLE 93

#define EAGAIN 35

#define ECONNREFUSED 61

#define ESPIPE 29

#define ESOCKTNOSUPPORT 44

#define EIO 5

#define EOVERFLOW 84

#define EDEADLK 11

#define EWOULDBLOCK 35

#define ENFILE 23

#define EDQUOT 69

#define EISDIR 21

#define ENETDOWN 50

#define EUSERS 68

#define EACCES 13

#define ECHILD 10

#define E2BIG 7

#define ENOENT 2

#define ENOTSUP 45

#define EADDRNOTAVAIL 49

#define EALREADY 37

#define EBADMSG 89

#define ENAMETOOLONG 63

#define ECONNRESET 54

#define ENOTSOCK 38

#define EDESTADDRREQ 39

#define ENOSYS 78

#define EDOM 33

#define EEXIST 17

#define EDOOFUS 88

#define EMFILE 24

#define ENOTDIR 20

#define ESHUTDOWN 58

#define EPIPE 32

#define EAUTH 80

#define EINVAL 22

#define EFBIG 27

#define EPROCLIM 67

#define EHOSTDOWN 64

#define EBADRPC 72

#define EPERM 1

#define EFTYPE 79

#define ETXTBSY 26

#define EREMOTE 71

#define ETIMEDOUT 60

#define ENOMSG 83

#define ENOTEMPTY 66

#define ENEEDAUTH 81

#define EBADF 9

#define ENOPROTOOPT 42

#define ECONNABORTED 53

#define EPROTO 92

#define EPROTOTYPE 41

#define ESRCH 3

#define ETOOMANYREFS 59

#define EOPNOTSUPP 45

#define ESTALE 70

#define EADDRINUSE 48

#define EMLINK 31

#define FUNCID_CC_MASK 1

#define FUNCID_OEN_WIDTH 6

#define SMC_32 0

#define RT_SVC_DESC_HANDLE 24

#define SERROR_AARCH32 15

#define OEN_STD_START 4

#define SYNC_EXCEPTION_AARCH32 12

#define FUNCID_TYPE_MASK 1

#define OEN_ARM_START 0

#define OEN_OEM_END 3

#define IRQ_SP_EL0 1

#define FIQ_SP_ELX 6

#define OEN_ARM_END 0

#define SMC_FROM_NON_SECURE 1

#define OEN_TOS_END 63

#define OEN_STD_END 4

#define RT_SVC_DESC_INIT 16

#define SYNC_EXCEPTION_AARCH64 8

#define FIQ_AARCH32 14

#define FUNCID_NUM_WIDTH 16

#define IRQ_AARCH64 9

#define FUNCID_CC_SHIFT 30

#define SIZEOF_RT_SVC_DESC 32

#define FUNCID_OEN_SHIFT 24

#define MAX_RT_SVCS 128

#define OEN_OEM_START 3

#define OEN_SIP_START 2

#define SYNC_EXCEPTION_SP_ELX 4

#define OEN_LIMIT 64

#define FUNCID_TYPE_SHIFT 31

#define RT_SVC_SIZE_LOG2 5

#define OEN_CPU_END 1

#define OEN_CPU_START 1

#define FUNCID_OEN_MASK 63

#define SERROR_AARCH64 11

#define FUNCID_TYPE_WIDTH 1

#define SMC_TYPE_STD 0

#define SMC_64 1

#define SMC_FROM_SECURE 0

#define OEN_TAP_END 49

#define OEN_SIP_END 2

#define SMC_TYPE_FAST 1

#define FUNCID_NUM_MASK 65535

#define OEN_TOS_START 50

#define IRQ_AARCH32 13

#define IRQ_SP_ELX 5

#define OEN_TAP_START 48

#define FUNCID_CC_WIDTH 1

#define SMC_UNK 4294967295

#define SERROR_SP_EL0 3

#define FUNCID_NUM_SHIFT 0

#define SERROR_SP_ELX 7

#define SYNC_EXCEPTION_SP_EL0 0

#define SMC_PREEMPTED 4294967294

#define FIQ_SP_EL0 2

#define FIQ_AARCH64 10

#define UINT_FAST64_MAX -1

#define UINT_LEAST8_MAX 255

#define INT64_MAX 9223372036854775807

#define PTRDIFF_MAX 9223372036854775807

#define SIZE_MAX -1

#define INT_LEAST8_MIN -128

#define PTRDIFF_MIN -9223372036854775808

#define UINT8_MAX 255

#define INT32_MIN -2147483648

#define UINT16_MAX 65535

#define INT_FAST8_MAX 2147483647

#define WINT_MAX 2147483647

#define INT_LEAST64_MIN -9223372036854775808

#define INTMAX_MIN -9223372036854775808

#define INT_FAST8_MIN -2147483648

#define INT64_MIN -9223372036854775808

#define WINT_MIN -2147483648

#define UINT_LEAST16_MAX 65535

#define SIG_ATOMIC_MAX 2147483647

#define INTMAX_MAX 9223372036854775807

#define INT32_MAX 2147483647

#define INT_LEAST64_MAX 9223372036854775807

#define UINTMAX_MAX -1

#define SIG_ATOMIC_MIN -2147483648

#define INT_LEAST16_MAX 32767

#define INT_FAST16_MIN -2147483648

#define INT8_MAX 127

#define INT_LEAST32_MIN -2147483648

#define INT_LEAST32_MAX 2147483647

#define UINT_FAST8_MAX 4294967295

#define INT_LEAST16_MIN -32768

#define WCHAR_MIN -2147483648

#define INT_FAST16_MAX 2147483647

#define INTPTR_MAX 9223372036854775807

#define INT8_MIN -128

#define INT_FAST32_MAX 2147483647

#define INTPTR_MIN -9223372036854775808

#define UINTPTR_MAX -1

#define WCHAR_MAX 2147483647

#define UINT_FAST32_MAX 4294967295

#define INT_FAST32_MIN -2147483648

#define UINT_FAST16_MAX 4294967295

#define INT16_MIN -32768

#define UINT32_MAX 4294967295

#define INT_FAST64_MIN -9223372036854775808

#define INT_LEAST8_MAX 127

#define INT16_MAX 32767

#define UINT_LEAST32_MAX 4294967295

#define INT_FAST64_MAX 9223372036854775807

#define UINT_LEAST64_MAX -1

#define UINT64_MAX -1

#define FOPEN_MODE_AB 9

#define SEMIHOSTING_SYS_WRITEC 3

#define FOPEN_MODE_RB 1

#define SEMIHOSTING_SYS_ERRNO 19

#define SEMIHOSTING_SYS_FLEN 12

#define FOPEN_MODE_APLUS 10

#define FOPEN_MODE_RPLUS 2

#define FOPEN_MODE_W 4

#define SEMIHOSTING_SYS_OPEN 1

#define SEMIHOSTING_SYS_WRITE0 4

#define FOPEN_MODE_R 0

#define SEMIHOSTING_SYS_SYSTEM 18

#define FOPEN_MODE_WB 5

#define SEMIHOSTING_SYS_REMOVE 14

#define FOPEN_MODE_A 8

#define FOPEN_MODE_RPLUSB 3

#define FOPEN_MODE_APLUSB 11

#define SEMIHOSTING_SYS_CLOSE 2

#define FOPEN_MODE_WPLUSB 7

#define SEMIHOSTING_SYS_SEEK 10

#define SEMIHOSTING_SYS_WRITE 5

#define FOPEN_MODE_WPLUS 6

#define SEMIHOSTING_SYS_READC 7

#define SEMIHOSTING_SYS_READ 6

#define PSCI_STATE_ON 0

#define PSCI_AFF_PRESENT 1

#define PSCI_E_NOT_PRESENT -7

#define PSCI_E_DISABLED -8

#define PSCI_E_ON_PENDING -5

#define PSCI_STATE_ON_PENDING 2

#define PSCI_CPU_OFF 2214592514

#define PSCI_MAJOR_VER 0

#define PSCI_CPU_ON_AARCH64 3288334339

#define PSCI_STATE_SHIFT 1

#define PSCI_AFFINITY_INFO_AARCH32 2214592516

#define PSCI_CPU_SUSPEND_AARCH64 3288334337

#define PSTATE_TYPE_MASK 1

#define PSTATE_TYPE_POWERDOWN 1

#define PSCI_NUM_CALLS 13

#define PSCI_E_SUCCESS 0

#define PSCI_MIG_INFO_UP_CPU_AARCH64 3288334343

#define PSCI_AFF_ABSENT 0

#define PSCI_NUM_AFFS 32

#define PSCI_INVALID_DATA -1

#define PSCI_SYSTEM_OFF 2214592520

#define PSTATE_AFF_LVL_MASK 3

#define PSCI_TOS_UP_MIG_CAP 0

#define PSCI_CPU_ON_AARCH32 2214592515

#define PSCI_MIG_AARCH32 2214592517

#define PSCI_TOS_NOT_UP_MIG_CAP 1

#define PSCI_STATE_OFF 1

#define PSTATE_TYPE_STANDBY 0

#define PSCI_MIG_INFO_TYPE 2214592518

#define PSCI_VERSION 2214592512

#define PSCI_E_INVALID_PARAMS -2

#define PSCI_SYSTEM_RESET 2214592521

#define PSCI_E_ALREADY_ON -4

#define PSTATE_VALID_MASK 4244504576

#define PSTATE_AFF_LVL_SHIFT 24

#define PSCI_E_DENIED -3

#define PSCI_MINOR_VER 2

#define PSCI_MIG_INFO_UP_CPU_AARCH32 2214592519

#define PSCI_STATE_SUSPEND 3

#define PSCI_AFFINITY_INFO_AARCH64 3288334340

#define PSTATE_TYPE_SHIFT 16

#define PSCI_E_INTERN_FAIL -6

#define PSCI_CPU_SUSPEND_AARCH32 2214592513

#define PSCI_TOS_NOT_PRESENT_MP 2

#define PSTATE_ID_SHIFT 0

#define PSCI_STATE_MASK 255

#define PSCI_E_NOT_SUPPORTED -1

#define PSTATE_ID_MASK 65535

#define PSCI_MIG_AARCH64 3288334341

#define __POSIX_VISIBLE 200809

#define __func__ 0

#define __BSD_VISIBLE 1

#define __ISO_C_VISIBLE 1999

#define __XSI_VISIBLE 700

typedef struct acs_setting acs_setting, *Pacs_setting;

struct acs_setting {
    char acs_magic[5];
    uchar chip_type;
    ushort version;
    ulong acs_set_length;
    char ddr_magic[5];
    uchar ddr_set_version;
    ushort ddr_set_length;
    ulong ddr_set_addr;
    char ddrt_magic[5];
    uchar ddrt_set_version;
    ushort ddrt_set_length;
    ulong ddrt_set_addr;
    char pll_magic[5];
    uchar pll_set_version;
    ushort pll_set_length;
    ulong pll_set_addr;
};

#define INTR_TYPE_FLAGS_MASK 4294967292

#define INTR_TYPE_INVAL 3

#define INTR_TYPE_S_EL1 0

#define INTR_RM_FROM_NS_SHIFT 1

#define INTR_RM_FLAGS_MASK 3

#define MAX_INTR_TYPES 3

#define INTR_TYPE_EL3 1

#define INTR_RM_FROM_FLAG_MASK 1

#define INTR_SRC_SS_FLAG_SHIFT 0

#define INTR_SEL1_VALID_RM1 3

#define INTR_SEL1_VALID_RM0 2

#define INTR_RM_FLAGS_SHIFT 0

#define INTR_ID_UNAVAILABLE 4294967295

#define INTR_TYPE_NS 2

#define INTR_NS_VALID_RM1 1

#define INTR_NS_VALID_RM0 0

#define INTR_RM_FROM_SEC_SHIFT 0

#define INTR_SRC_SS_FLAG_MASK 1

#define EOF -1

typedef struct bakery_lock bakery_lock, *Pbakery_lock;

typedef struct bakery_lock bakery_lock_t;

struct bakery_lock {
    int owner;
    char entering[0];
    uint number[0];
};

#define NO_OWNER -1

#define GICD_SPENDSGIR 3872

#define GIC_HIGHEST_SEC_PRIORITY 0

#define FIQ_BYP_DIS_GRP1 128

#define GICV_DEACTIVATE 4096

#define FIQ_BYP_DIS_GRP0 32

#define GICC_CTLR 0

#define GICV_INTACK 12

#define CPENDSGIR_SHIFT 2

#define GICD_IGROUPR 128

#define GICD_ICPENDR 640

#define GICH_ELRSR0 48

#define GICH_ELRSR1 52

#define GICV_RUNNINGPRI 20

#define GICD_ICFGR 3072

#define GICC_IIDR 252

#define GICC_IIDR_ARCH_SHIFT 16

#define GICC_IIDR_PID_MASK 4095

#define ENABLE_GRP1 2

#define ENABLE_GRP0 1

#define GICC_IIDR_REV_SHIFT 12

#define CBPR 16

#define GICV_BP 8

#define GICV_EOI 16

#define MAX_SGIS 16

#define ISACTIVER_SHIFT 5

#define GICC_DIR 4096

#define ISPENDR_SHIFT 5

#define GICD_ISENABLER 256

#define GICC_IIDR_REV_MASK 15

#define FIQ_EN 8

#define GICC_IIDR_PID_SHIFT 20

#define GICH_LR_BASE 256

#define GICD_CTLR 0

#define GICC_HPPIR 24

#define ICACTIVER_SHIFT 5

#define IRQ_BYP_DIS_GRP1 256

#define IRQ_BYP_DIS_GRP0 64

#define GICC_IAR 12

#define IT_LINES_NO_MASK 31

#define GRP1 1

#define GICC_IIDR_ARCH_MASK 15

#define IPRIORITYR_SHIFT 2

#define GICD_TYPER 4

#define GIC_LOWEST_SEC_PRIORITY 127

#define ITARGETSR_SHIFT 2

#define GICD_ISACTIVER 768

#define ICENABLER_SHIFT 5

#define ICPENDR_SHIFT 5

#define GICC_PRIODROP 16

#define GICH_VTR 4

#define GRP0 0

#define GICD_ICACTIVER 896

#define ISENABLER_SHIFT 5

#define SPENDSGIR_SHIFT 2

#define GICD_SGIR 3840

#define GICH_APR0 240

#define GICD_ICENABLER 384

#define GICC_IIDR_IMP_SHIFT 0

#define EOI_MODE_NS 1024

#define GIC_PRI_MASK 255

#define GICD_ISPENDR 512

#define GIC_HIGHEST_NS_PRIORITY 128

#define GIC_LOWEST_NS_PRIORITY 254

#define GICC_EOIR 16

#define GICD_IPRIORITYR 1024

#define ACK_CTL 4

#define IGROUPR_SHIFT 5

#define GICV_HIGHESTPEND 24

#define ICFGR_SHIFT 4

#define GICD_CPENDSGIR 3856

#define GICC_PMR 4

#define GIC_SPURIOUS_INTERRUPT 1023

#define GICV_CTL 0

#define GICV_PRIMASK 4

#define GICC_AHPPIR 40

#define EOI_MODE_S 512

#define GIC400_NUM_SPIS 480

#define MAX_PPIS 14

#define GICC_IIDR_IMP_MASK 4095

#define GICC_RPR 20

#define GICD_ITARGETSR 2048

#define GICH_CTL 0

#define GICC_BPR 8

#define GICD_BASE 738263040

#define IRQ_SEC_SGI_2 10

#define TRUSTED_MAILBOX_SHIFT 4

#define SOC_NIC400_TLX_MASTER 1

#define IRQ_SEC_SGI_1 9

#define IRQ_SEC_SGI_0 8

#define DEVICE1_BASE 1073741824

#define SPI_SIZE 4194304

#define TZC400_BASE 709492736

#define MHU_BASE 723451904

#define NSRAM_SIZE 32768

#define DEVICE0_BASE 536870912

#define SOC_NIC400_BOOTSEC_BRIDGE_UART1 4096

#define SYS_CNTREAD_BASE 713031680

#define IRQ_MHU 69

#define TZC400_NSAID_AP 9

#define PL011_UART0_BASE 470351872

#define MHU_PAYLOAD_CACHED 0

#define CSS_NIC400_SLAVE_BOOTSECURE 8

#define DEVICE1_SIZE 1073741824

#define GICC_BASE 738390016

#define IRQ_SEC_SGI_6 14

#define IRQ_SEC_SGI_5 13

#define IRQ_SEC_SGI_4 12

#define IRQ_SEC_SGI_3 11

#define PLAT_BL31_PLAT_PARAM_VAL 1089357896855742840

#define CSS_NIC400_BASE 704643072

#define EMMC_BASE 201326592

#define IOFPGA_BASE 469762048

#define IRQ_SEC_SGI_8 16

#define MAILBOX_START 3640733696

#define IRQ_SEC_SGI_7 15

#define EMMC_SIZE 67108864

#define NSRAM_BASE 771751936

#define PL011_UART1_BASE 470417408

#define TZC400_NSAID_DMA330 5

#define TZC400_NSAID_PCIE 1

#define V2M_SYS_LED 8

#define SPI_BASE 3422552064

#define DRAM_SIZE 2130706432

#define SYS_CNTCTL_BASE 709033984

#define GICV_BASE 738652160

#define SYS_TIMCTL_BASE 713097216

#define SYS_LED_EL_SHIFT 1

#define IOFPGA_SIZE 50331648

#define DEVICEE_BASE 3489660928

#define MHU_SECURE_BASE 67108864

#define IRQ_TZ_WDOG 86

#define PSRAM_BASE 335544320

#define TZC400_NSAID_SCP 11

#define DEVICED_SIZE 33554432

#define SOC_NIC400_APB4_BRIDGE 4

#define TZC400_NSAID_CORESIGHT 12

#define TRUSTED_MAILBOXES_BASE 67108864

#define PCIE_CONTROL_BASE 2146566144

#define TZC400_NSAID_USB 4

#define DEVICEC_BASE 3221225472

#define TZC400_NSAID_THINLINKS 6

#define TZC400_NSAID_HDLCD1 3

#define TZC400_NSAID_HDLCD0 2

#define PSRAM_SIZE 33554432

#define IRQ_GPU_SMMU_1 73

#define IRQ_GPU_SMMU_0 71

#define DEVICED_BASE 3658481664

#define SOC_NIC400_BOOTSEC_BRIDGE 5

#define PL011_UART3_BASE 2146893824

#define MHU_SECURE_SIZE 4096

#define DEVICEE_SIZE 16777216

#define SOC_NIC400_PL354_SMC 3

#define SOC_NIC400_USB_OHCI 2

#define MAILBOX_SIZE 4096

#define NSROM_BASE 520093696

#define SYS_LED_EC_SHIFT 3

#define PL011_UART2_BASE 2146959360

#define IRQ_ETR_SMMU 75

#define TZC400_NSAID_GPU 10

#define DRAM_BASE 16777216

#define SOC_NIC400_BASE 2144337920

#define DEVICE0_SIZE 234881024

#define GICH_BASE 738521088

#define TZC400_NSAID_CCI400 0

#define NSROM_SIZE 4096

#define DEVICEC_SIZE 201326592

#define IRQ_TZC400 80

#define SYS_LED_SS_SHIFT 0

#define VE_SYSREGS_BASE 469827584

#define SOC_NIC400_USB_EHCI 0

#define ARM_STD_SVC_CALL_COUNT 2214657792

#define STD_SVC_VERSION_MINOR 1

#define PSCI_FID_VALUE 0

#define ARM_STD_SVC_UID 2214657793

#define STD_SVC_VERSION_MAJOR 0

#define PSCI_FID_MASK 65504

#define ARM_STD_SVC_VERSION 2214657795

#define LOAD_MASK 1

#define PARAM_IMAGE_BINARY 2

#define TOP_LOAD 1

#define PARAM_BL31 3

#define ENTRY_POINT_INFO_PC_OFFSET 8

#define VERSION_1 1

#define ENTRY_POINT_INFO_ARGS_OFFSET 24

#define PARAM_EP 1

#define NON_SECURE 1

#define UP 1

#define RUN_IMAGE 3221225472

#define PARAM_EP_SECURITY_MASK 1

#define DOWN 0

#define SECURE 0

#define WRITE_CHNL_QOS_VAL_OVERRIDE_REG 260

#define IMPRECISE_ERR_REG 16

#define TARGET_LATENCY_REG 304

#define MAX_OT_REG 272

#define SLAVE_IFACE3_OFFSET 16384

#define QOS_RANGE_REG 312

#define SLAVE_IFACE4_OFFSET 20480

#define SLAVE_IFACE2_OFFSET 12288

#define CHANGE_PENDING_BIT 1

#define SLAVE_IFACE0_OFFSET 4096

#define SLAVE_IFACE1_OFFSET 8192

#define CTRL_OVERRIDE_REG 0

#define PERFMON_CTRL_REG 256

#define QOS_CTRL_REG 268

#define SPEC_CTRL_REG 4

#define SECURE_ACCESS_REG 8

#define LATENCY_REGULATION_REG 308

#define SNOOP_CTRL_REG 0

#define DVM_EN_BIT 2

#define READ_CHNL_QOS_VAL_OVERRIDE_REG 256

#define SH_OVERRIDE_REG 4

#define STATUS_REG 12

#define SNOOP_EN_BIT 1

#define USHRT_MAX 65535

#define SHRT_MAX 32767

#define SSIZE_MAX 9223372036854775807

#define LONG_MIN -9223372036854775808

#define ULONG_MAX -1

#define SIZE_T_MAX -1

#define QUAD_MAX 9223372036854775807

#define SHRT_MIN -32768

#define INT_MIN -2147483648

#define UQUAD_MAX -1

#define OFF_MIN -9223372036854775808

#define CHAR_BIT 8

#define INT_MAX 2147483647

#define UID_MAX 4294967295

#define MQ_PRIO_MAX 64

#define QUAD_MIN -9223372036854775808

#define LONG_BIT 64

#define UCHAR_MAX 255

#define SCHAR_MAX 127

#define UINT_MAX 4294967295

#define OFF_MAX 9223372036854775807

#define WORD_BIT 32

#define SCHAR_MIN -128

#define CHAR_MAX 127

#define CHAR_MIN -128

#define LONG_MAX 9223372036854775807

#define GID_MAX 4294967295

#define __WORDSIZE 32




/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined * FUN_d9001068(void)

{
  memset((ulong)&DAT_d900a580,0,400);
  _DAT_d900a582 = 0x40;
  _DAT_d900a590 = &DAT_d900a660;
  DAT_d900a580 = 3;
  _DAT_d900a588 = &DAT_d900a5c0;
  _DAT_d900a662 = 0x58;
  _DAT_d900a598 = &DAT_d900a5d8;
  _DAT_d900a60a = 0x58;
  DAT_d900a581 = 1;
  DAT_d900a5c1 = 1;
  DAT_d900a660 = 1;
  DAT_d900a661 = 1;
  DAT_d900a5d9 = 1;
  DAT_d900a608 = 1;
  DAT_d900a609 = 1;
  DAT_d900a5f1 = 1;
  _DAT_d900a584 = 0;
  DAT_d900a5c0 = 2;
  _DAT_d900a5c2 = 0x18;
  _DAT_d900a5c4 = 0;
  _DAT_d900a664 = 0;
  DAT_d900a5d8 = 2;
  _DAT_d900a5da = 0x18;
  _DAT_d900a5dc = 0;
  _DAT_d900a5a0 = &DAT_d900a608;
  _DAT_d900a60c = 0;
  _DAT_d900a620 = 0;
  _DAT_d900a5a8 = &DAT_d900a5f0;
  DAT_d900a5f0 = 2;
  _DAT_d900a5f2 = 0x18;
  _DAT_d900a5f4 = 0;
  return &DAT_d900a580;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 bl2_plat_get_bl31_ep_info(void)

{
                    /* PLAT_BL31_PLAT_PARAM_VAL */
  _DAT_d900a6d8 = 0xf1e2d3c4b5a6978;
  return 0xd900a6b8;
}



void bl2_early_platform_setup(void)

{
                    /* These are actually all members of bl2_tzram_layout, including (in order:
                       total_base, total_size, free_base, free_size, attr, next */
  bl2_tzram_layout.total_base = 0xd9000000;
  bl2_tzram_layout.total_size = 0x20000;
                    /* 0xD900C000 -> BL2 header? */
  bl2_tzram_layout.free_base = &DAT_d900c000;
  bl2_tzram_layout.free_size = 0x14000;
  bl2_tzram_layout.attr = 0xe939e2e8cf8effd;
  bl2_tzram_layout.next = 0;
  return;
}



void bl2_platform_setup(void)

{
  configure_mmu_el1(bl2_tzram_layout.total_base,bl2_tzram_layout.total_size,(ulong)&LAB_d9001000,
                    (long)&ddrs,(ulong)&DAT_d900c000,(long)&DAT_d900c000);
  storage_init();
  return;
}



void FUN_d90011f4(void)

{
  inv_dcache_range((ulong)&DAT_d900a580,400);
  return;
}



void bl2_plat_arch_setup(void)

{
  return;
}



void FUN_d9001208(undefined8 param_1,long param_2)

{
  *(uint *)(param_2 + 4) = *(uint *)(param_2 + 4) & 0xfffffffe;
  *(undefined4 *)(param_2 + 0x10) = 0x3cd;
  return;
}



void FUN_d9001220(undefined8 param_1,long param_2)

{
  *(undefined4 *)(param_2 + 0x10) = 0;
  *(uint *)(param_2 + 4) = *(uint *)(param_2 + 4) & 0xfffffffe;
  return;
}



void bl2_plat_set_bl33_ep_info(undefined8 param_1,long param_2)

{
  int iVar1;
  ulong uVar2;
  
  uVar2 = read_id_aa64pfr0_el1();
                    /* In Amlogic's old BL2 sources ID_AA64PFR0_EL2_SHIFT == 8. */
  iVar1 = 2;
  if ((uVar2 >> 8 & 0xf) == 0) {
    iVar1 = 1;
  }
  *(uint *)(param_2 + 0x10) = iVar1 << 2 | 0x3c1;
  *(uint *)(param_2 + 4) = *(uint *)(param_2 + 4) | 1;
  return;
}



void configure_mmu_el1(ulong param_1,ulong param_2,ulong param_3,long param_4,ulong param_5,
                      long param_6)

{
  mmap_add_region(param_1,param_2,3);
  mmap_add_region(param_3,param_4 - param_3,1);
  mmap_add_region(param_5,param_6 - param_5,2);
  mmap_add_region(0xd9013000,0x1000,2);
  mmap_add_region(0xd9040000,0x10000,1);
  mmap_add_region(0x1000000,0x7f000000,7);
  mmap_add_region(0xcc000000,0x400000,3);
  mmap_add_region(0xc0000000,0xc000000,2);
  mmap_add_region(0xda000000,0x2000000,2);
  mmap_add_region(0xd0000000,0x2000000,2);
  FUN_d900892c();
  FUN_d900899c();
  return;
}



void dump_ddr_data(void)

{
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 nf_read(ulong param_1,ulong param_2,long param_3)

{
  _DAT_c8834528 = 0xffff87ff;
  _DAT_c88344f0 = 0xffff8700;
  _DAT_c88344c0 = _DAT_c88344c0 & 0x7e2fffff;
  _DAT_c88344c4 = _DAT_c88344c4 | 0xf;
  _DAT_c1108c88 = 0x2aa949;
  memcpy(param_2,param_1 | 0xcc000000,param_3);
  return 0;
}



uint FUN_d90013e8(long param_1,undefined4 *param_2)

{
  uint uVar1;
  
  *(undefined4 *)(param_1 + 0x50) = *param_2;
  *(undefined4 *)(param_1 + 0x54) = param_2[1];
  uVar1 = 0;
  if ((*(uint *)(param_1 + 0x50) >> 0x1e & 1) != 0) {
    uVar1 = *(uint *)(param_1 + 0x48) & 0x1fff;
  }
  param_2[3] = *(undefined4 *)(param_1 + 0x5c);
  return uVar1;
}



uint sdio_read_blocks(long sd_emmc,ulong src,uint dst,undefined8 size,long mode)

{
  uint uVar1;
  uint uVar2;
  undefined4 local_b0;
  undefined4 local_ac;
  uint local_a8;
  undefined4 local_a4;
  uint local_a0;
  undefined4 local_9c;
  undefined4 local_98;
  undefined auStack_30 [16];
  uint local_20;
  undefined8 local_10;
  long local_8;
  
  local_20 = 0;
  local_10 = size;
  local_8 = mode;
                    /* Why are they filling this with zeros? */
  memset((ulong)&local_b0,0,0x80);
  uVar1 = local_b0 & 0xc0ffffff;
  local_ac = (int)(src >> 9);
  if (local_8 == 0) {
    local_ac = (int)src;
  }
  local_b0._1_1_ = (byte)(uVar1 >> 8);
  if (local_8 == 0) {
    local_b0._1_1_ = local_b0._1_1_ & 0xfd;
  }
  else {
    local_b0._1_1_ = local_b0._1_1_ | 2;
  }
  local_b0._2_1_ = (undefined)(uVar1 >> 0x10);
  local_a4 = SUB84(auStack_30,0);
  local_b0 = CONCAT13((char)(uVar1 >> 0x18),
                      CONCAT12(local_b0._2_1_,
                               (local_b0._1_1_ & 0xfe) << 8 | (ushort)local_10 & 0x1ff)) &
             0xff360fff | 0x92047800;
  local_20 = (uint)((byte)local_20 & 0xfe | 2);
  *(undefined4 *)(sd_emmc + 0x48) = 0x3fff;
  local_a8 = dst & 0xfffffffe;
  *(uint *)(sd_emmc + 0x50) = local_b0;
  *(uint *)(sd_emmc + 0x58) = local_a8;
  *(undefined4 *)(sd_emmc + 0x54) = local_ac;
  do {
  } while ((*(uint *)(sd_emmc + 0x48) >> 0xd & 1) == 0);
  uVar1 = local_a0 >> 8;
  local_a0 = local_a0 & 0xc0faffff;
  local_a0 = CONCAT22((short)(local_a0 >> 0x10),CONCAT11((char)uVar1,(undefined)local_a0)) |
             0x8c000c00;
  local_20 = local_20 & 3 | (int)((ulong)&local_a0 >> 2) << 2;
  *(undefined4 *)(sd_emmc + 0x48) = 0x3fff;
  *(uint *)(sd_emmc + 0x50) = local_a0;
  *(undefined4 *)(sd_emmc + 0x58) = local_98;
  *(undefined4 *)(sd_emmc + 0x54) = local_9c;
  do {
    uVar1 = *(uint *)(sd_emmc + 0x48);
  } while ((uVar1 >> 0xd & 1) == 0);
  uVar2 = (uint)((uVar1 & 0xff) != 0) | (uVar1 >> 8 & 1) << 1 | (uVar1 >> 9 & 1) << 2 |
          (uVar1 >> 10 & 1) << 3 | (uVar1 >> 0xb & 1) << 4;
  if ((uVar1 >> 0xc & 1) == 0) {
    if (uVar2 == 0) {
      return 0;
    }
  }
  else {
    uVar2 = uVar2 | 0x20;
  }
  serial_puts(s_sd_emmc_read_data_error__ret__d9009068);
  serial_put_dec((ulong)uVar2);
  serial_puts(&CHAR_NL);
  return uVar2;
}



uint sdio_read_data(long boot_device,ulong src,int dst,long size)

{
  byte bVar1;
  byte bVar2;
  int iVar3;
  uint uVar4;
  uint uVar5;
  ulong uVar6;
  ulong device_register;
  
  if (boot_device == 1) {
                    /* eMMC */
    device_register = 0x4000;
  }
  else {
                    /* SD */
    if (boot_device != 4) {
      serial_puts(s_sd_emmc_boot_device_error_d9009086);
      device_register = 0;
      goto LAB_d9001648;
    }
    device_register = 0x2000;
  }
  device_register = device_register | 0xd0070000;
LAB_d9001648:
  bVar1 = DAT_da100244 >> 7;
  bVar2 = DAT_da100245 >> 1;
  uVar6 = size + 0x1ffU >> 9;
  iVar3 = (int)src;
  while( true ) {
    uVar5 = (uint)uVar6;
    uVar4 = uVar5;
    if (0x80 < uVar5) {
      uVar4 = 0x80;
    }
    uVar4 = sdio_read_blocks(device_register,src,(dst - iVar3) + (int)src,(ulong)uVar4,
                             (ulong)((uint)bVar1 | bVar2 & 1));
    if ((uVar4 != 0) || (uVar5 < 0x81)) break;
    src = src + 0x10000;
    uVar6 = (ulong)(uVar5 - 0x80);
  }
  return uVar4;
}



bool FUN_d90016cc(long param_1,long param_2)

{
  undefined uVar1;
  int iVar2;
  
  sdio_read_data(param_1,0,0x1800200,0x200);
  iVar2 = FUN_d9008b00(param_2,0x1800200,0x200);
  uVar1 = (undefined)iVar2;
  if (iVar2 == 0) {
    serial_puts(&DAT_d90090a5);
  }
  else {
    serial_puts(&DAT_d90090a1);
    uVar1 = 1;
  }
  return (bool)uVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900173c(long param_1)

{
  uint uVar1;
  bool bVar2;
  undefined4 local_20;
  uint local_1c;
  undefined4 local_10;
  
  if (param_1 == 1) {
    sdio_read_data(1,0,0x1800000,0x200);
    local_1c = 0x3b70200;
    local_20 = 0x6007400;
    FUN_d90013e8((long)&DAT_d0074000,&local_20);
    uVar1 = _DAT_d0074044 & 3;
    local_10 = _DAT_d0074044 & 0xfffffffc | 2;
    _DAT_d0074044 = local_10;
    bVar2 = FUN_d90016cc(1,0x1800000);
    if (bVar2) {
      local_1c = uVar1 << 8 | 0x3b70000;
      local_20 = 0x6007400;
      FUN_d90013e8((long)&DAT_d0074000,&local_20);
      local_10 = CONCAT31(local_10._1_3_,(byte)local_10 & 0xfc | (byte)uVar1);
      _DAT_d0074044 = local_10;
    }
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

uint get_boot_device(void)

{
  uint boot_device;
  
                    /* BOOT_DEVICE_USB_FORCEMODE */
  boot_device = 6;
  if ((_DAT_da10001c >> 0xc & 0xf) != 2) {
                    /* Actually get the boot device */
    boot_device = _DAT_da100240 & 0xf;
  }
  return boot_device;
}



undefined8 storage_init(void)

{
  uint uVar1;
  
  uVar1 = get_boot_device();
  if (uVar1 == 1) {
    FUN_d900173c(1);
  }
  else if (uVar1 == 2) {
    serial_puts(s_NAND_init_d90090a9);
    nfio_init();
  }
  return 0;
}



undefined8 storage_load(ulong src,ulong dst,ulong size,byte *param_4)

{
  uint boot_device;
  long mmc_device_id;
  char *device_name;
  ulong uVar1;
  
  boot_device = get_boot_device();
  switch((ulong)boot_device) {
  case 0:
                    /* Reserved */
    device_name = s_Rsv_d90090b4;
    break;
  case 1:
    device_name = s_eMMC_d90090b8;
    break;
  case 2:
    device_name = s_NAND_d90090bd;
    break;
  case 3:
    device_name = s_SPI_d90090c2;
    break;
  case 4:
    device_name = s_SD_d90090c6;
    break;
  case 5:
  case 6:
    device_name = s_USB_d90090c9;
    break;
  default:
    device_name = s_UNKNOWN_d90090cd;
  }
  serial_puts(s_Load_d90090d5);
  serial_puts((char *)param_4);
  serial_puts(s__from_d90090db);
  serial_puts(device_name);
  serial_puts(s___src__0x_d90090e2);
  serial_put_hex(src,0x20);
  serial_puts(s___des__0x_d90090ec);
  serial_put_hex(dst,0x20);
  serial_puts(s___size__0x_d90090f6);
  serial_put_hex(size,0x20);
  serial_puts(&CHAR_NL);
  uVar1 = (ulong)boot_device - 1;
  if ((5 < uVar1) || (5 < (uint)uVar1)) goto LAB_d9001a58;
  switch(uVar1 & 0xffffffff) {
  case 0:
                    /* eMMC */
    mmc_device_id = 1;
    goto LAB_d9001a34;
  case 1:
    nand_read(2,(uint)src,(uint)dst,(uint)size);
    break;
  case 2:
    nf_read(src,dst,size);
    break;
  case 3:
    mmc_device_id = 4;
LAB_d9001a34:
    sdio_read_data(mmc_device_id,src,(uint)dst,size);
    break;
  default:
    FUN_d9007e70(src,dst,size);
  }
LAB_d9001a58:
  inv_dcache_range(dst,size);
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

uint FUN_d9001a7c(void)

{
  return _DAT_da100240 >> 0x10;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void hw_update(undefined4 *param_1,uint param_2,char param_3)

{
  uint uVar1;
  undefined4 *puVar2;
  uint uVar3;
  
  if ((param_3 == '\0') && ((param_2 & 0x3f) != 0)) {
    serial_puts(s_Err_sha_d9009101);
    return;
  }
  while (param_2 != 0) {
    puVar2 = param_1;
    uVar3 = param_2;
    uVar1 = 0;
    if (0x3f < param_2) {
      uVar3 = 0x40;
      uVar1 = param_2 - 0x40;
    }
    do {
      param_2 = uVar1;
      if (uVar3 < 4) {
        _DAT_da8320c8 = _DAT_da8320c8 & 0xffc3ffff | 0x300000 | (uVar3 - 1) * 0x40000;
        uVar3 = 0;
      }
      else {
        uVar3 = uVar3 - 4;
        if ((uVar3 == 0) &&
           ((uVar1 = _DAT_da8320c8 & 0xffcfffff, param_2 != 0 ||
            (_DAT_da8320c8 = uVar1 | 0x300000, param_3 == '\0')))) {
          _DAT_da8320c8 = uVar1 | 0x200000;
        }
      }
      param_1 = puVar2 + 1;
      _DAT_da8320cc = *puVar2;
      puVar2 = param_1;
      uVar1 = param_2;
    } while (uVar3 != 0);
    do {
    } while ((int)_DAT_da8320c8 < 0);
    _DAT_da8320c8 = _DAT_da8320c8 & 0xffcfffff | 0x400000;
  }
  return;
}



/* WARNING: Removing unreachable block (ram,0xd9001ce8) */
/* WARNING: Removing unreachable block (ram,0xd9001cf0) */
/* WARNING: Removing unreachable block (ram,0xd9001d14) */
/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9001b84(long param_1,int param_2)

{
  if (_DAT_d900a710 == 0) {
    do {
    } while( true );
  }
  serial_puts(s_Err_sha_d9009101);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void SHA2_HW_update(long param_1,long param_2,uint param_3)

{
  uint uVar1;
  long lVar2;
  ulong uVar3;
  uint uVar4;
  
  lVar2 = _DAT_d900a710;
  if (_DAT_d900a710 != param_1) {
    serial_puts(s_Err_sha_d9009101);
    return;
  }
  uVar1 = *(uint *)(_DAT_d900a710 + 0x24);
  uVar4 = 0;
  if (uVar1 != 0) {
    uVar4 = 0x40 - uVar1;
    memcpy(_DAT_d900a710 + (ulong)uVar1 + 0x2c,param_2,(ulong)uVar4);
    param_3 = param_3 - uVar4;
    *(uint *)(lVar2 + 0x24) = *(int *)(lVar2 + 0x24) + uVar4;
  }
  uVar3 = (ulong)param_3;
  if (*(int *)(lVar2 + 0x24) == 0x40) {
    if (param_3 == 0) {
      return;
    }
    hw_update((undefined4 *)(lVar2 + 0x2c),0x40,'\0');
    *(undefined4 *)(lVar2 + 0x24) = 0;
    if (param_3 < 0x41) goto LAB_d9001e04;
  }
  else if (param_3 < 0x41) {
    if (param_3 == 0) {
      return;
    }
    goto LAB_d9001e04;
  }
  uVar1 = param_3 & 0x3f;
  if ((param_3 & 0x3f) == 0) {
    uVar1 = 0x40;
  }
  uVar3 = (ulong)uVar4;
  uVar4 = uVar4 + (param_3 - uVar1);
  hw_update((undefined4 *)(param_2 + uVar3),param_3 - uVar1,'\0');
  uVar3 = (ulong)uVar1;
LAB_d9001e04:
  memcpy(lVar2 + 0x2c,param_2 + (ulong)uVar4,uVar3);
  *(int *)(lVar2 + 0x24) = (int)uVar3;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

long FUN_d9001e2c(long param_1)

{
  long lVar1;
  long lVar2;
  undefined4 *puVar3;
  
  lVar1 = _DAT_d900a710;
  if (_DAT_d900a710 == param_1) {
    lVar2 = _DAT_d900a710 + 0xac;
    if (*(uint *)(_DAT_d900a710 + 0x24) - 1 < 0x40) {
      hw_update((undefined4 *)(_DAT_d900a710 + 0x2c),*(uint *)(_DAT_d900a710 + 0x24),'\x01');
      _DAT_da832094 = _DAT_da832094 & 0xffffffbf;
      puVar3 = (undefined4 *)&DAT_da832098;
      do {
        *(undefined4 *)((long)puVar3 + lVar1 + -0xda831fec) = *puVar3;
        puVar3 = puVar3 + 1;
      } while (puVar3 != (undefined4 *)0xda8320b8);
      _DAT_d900a710 = 0;
    }
    else {
      serial_puts(s_Err_sha_d9009101);
    }
  }
  else {
    serial_puts(s_Err_sha_d9009101);
    lVar2 = param_1 + 0xac;
  }
  return lVar2;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9001f18(void)

{
  _DAT_d900a940 = 0;
  _DAT_da832090 = _DAT_da832090 & 0xffffc00f | 0x1a40;
  _DAT_da832094 = _DAT_da832094 & 0xffffffe8 | 0x14;
  _DAT_da8320c8 = _DAT_da8320c8 & 0xffffff8f | 0x48;
  _DAT_da8320d0 = 0x6a09e667;
  _DAT_da8320d4 = 0;
  memset(0x5510000,0,0x140);
  NDMA_set_table_position_secure(_DAT_d900a940,0x5510000,0x5510140);
  _DAT_d900a944 = 0;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90020d4(undefined8 param_1,uint param_2,uint param_3)

{
  if ((param_3 != 0) && (FUN_d9005ff8(_DAT_d900a940,1,2,0,param_3,param_2,0), _DAT_d900a944 == 0)) {
    FUN_d9005e6c(_DAT_d900a940);
    _DAT_d900a944 = 1;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9002138(long param_1,uint param_2,uint param_3)

{
  FUN_d9005ff8(_DAT_d900a940,1,2,0,param_3,param_2,1);
  if (_DAT_d900a944 == 0) {
    FUN_d9005e6c(_DAT_d900a940);
    _DAT_d900a944 = 1;
  }
  FUN_d9005eb4(_DAT_d900a940);
  FUN_d9005ee8(_DAT_d900a940);
  _DAT_d900a944 = 0;
  *(undefined4 *)(param_1 + 0xac) = _DAT_da832098;
  *(undefined4 *)(param_1 + 0xb0) = _DAT_da83209c;
  *(undefined4 *)(param_1 + 0xb4) = _DAT_da8320a0;
  *(undefined4 *)(param_1 + 0xb8) = _DAT_da8320a4;
  *(undefined4 *)(param_1 + 0xbc) = _DAT_da8320a8;
  *(undefined4 *)(param_1 + 0xc0) = _DAT_da8320ac;
  *(undefined4 *)(param_1 + 0xc4) = _DAT_da8320b0;
  *(undefined4 *)(param_1 + 200) = _DAT_da8320b4;
  return;
}



void sha2(ulong param_1,uint param_2,ulong param_3,int param_4)

{
  int iVar1;
  undefined auStack_d0 [172];
  undefined auStack_24 [36];
  
  if ((param_1 >> 0x18 & 0xff) == 0xd9) {
    iVar1 = 0x100;
    if (param_4 != 0) {
      iVar1 = 0xe0;
    }
    FUN_d9001b84((long)auStack_d0,iVar1);
    SHA2_HW_update((long)auStack_d0,param_1,param_2);
    FUN_d9001e2c((long)auStack_d0);
  }
  else {
    FUN_d9001f18();
    FUN_d9002138((long)auStack_d0,(uint)param_1,param_2);
  }
  memcpy(param_3,(long)auStack_24,0x20);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void send_bl30x(int addr,uint size,long *sha2,uint sha2_len,byte *image_str)

{
  int iVar1;
  ulong uVar2;
  uint loop1;
  uint tmp;
  
  _DAT_d9013800 = size;
  iVar1 = strcmp((long)s_bl301_d9009167,(long)image_str);
  if (iVar1 == 0) {
    serial_puts(s_Wait_bl30____d900916d);
    do {
    } while ((_DAT_c810023c >> 0x14 & 3) != 3);
    serial_puts(s_Done_d900917a);
  }
  serial_puts(s_Sending_d9009180);
  serial_puts((char *)image_str);
  _DAT_da83c428 = 0xc0dec0d0;
  do {
  } while (_DAT_da83c42c != 0);
  memcpy((ulong)&DAT_d9013800,(long)sha2,(ulong)sha2_len);
  _DAT_da83c428 = 0xc0de0001;
  do {
  } while (_DAT_da83c42c != 0);
  loop1 = 0;
  do {
    if (size <= loop1) {
      _DAT_da83c428 = 0xc0de0002;
      do {
      } while (_DAT_da83c42c != 0);
      serial_puts(s_OK__Run_d9009189);
      serial_puts((char *)image_str);
      serial_puts(&DAT_d9009337);
      _DAT_da83c428 = 0xe00de00d;
      return;
    }
                    /* serial_puts(".") */
    serial_puts(s_Wait_bl30____d900916d + 0xb);
    tmp = loop1 + 1024;
    if (size < tmp) {
      if (loop1 < size) {
        uVar2 = (ulong)(size - loop1);
        goto LAB_d9002428;
      }
    }
    else {
      uVar2 = 1024;
LAB_d9002428:
      memcpy((ulong)&DAT_d9013800,(ulong)(loop1 + addr),uVar2);
    }
    _DAT_da83c428 = 0xc0dec0de;
    do {
      loop1 = tmp;
    } while (_DAT_da83c42c != 0);
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void watchdog_disable(void)

{
                    /* Disable watchdog. */
  _DAT_c11098d0 = _DAT_c11098d0 & 0xfdfbffff;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void reset_system(void)

{
  int iVar1;
  
  udelay(10000);
  do {
    iVar1 = 100;
    do {
      iVar1 = iVar1 + -1;
    } while (iVar1 != 0);
  } while( true );
}



void efuse_read(long param_1,long param_2,ulong param_3)

{
  memcpy(param_3,param_1 * 4 + 0xd9013c00,param_2);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void clocks_set_sys_cpu_clk(int param_1,int param_2,int param_3,int param_4)

{
  do {
  } while ((_DAT_c883c19c >> 0x1c & 1) != 0);
  if (param_1 == 1) {
    _DAT_c883c19c = _DAT_c883c19c | 0x4000800;
  }
  else {
    if ((_DAT_c883c19c >> 10 & 1) == 0) {
      _DAT_c883c19c = _DAT_c883c19c & 0xfc08fbff | 0x4000400;
    }
    else {
      _DAT_c883c19c = _DAT_c883c19c & 0xfffff808 | 0x4000000;
    }
    _DAT_c883c19c = _DAT_c883c19c & 0xfffff7ff;
  }
  if (param_2 - 2U < 7) {
    _DAT_c883c15c = _DAT_c883c15c & 0xffffffc7 | (param_2 - 2U) * 8;
  }
  if (param_3 - 2U < 7) {
    _DAT_c883c15c = _DAT_c883c15c & 0xfffff1ff | (param_3 - 2U) * 0x200;
  }
  if (param_4 - 2U < 7) {
    _DAT_c883c15c = _DAT_c883c15c & 0xfffffe3f | (param_4 - 2U) * 0x40;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

uint pll_lock_check(int *param_1,byte *param_2)

{
  int iVar1;
  
  iVar1 = *param_1;
  if (-(iVar1 >> 0x1f) == 0) {
    _DAT_d900a948 = _DAT_d900a948 + 1;
    serial_puts((char *)param_2);
    serial_puts(s__lock_check_d9009193);
    serial_put_dec((ulong)_DAT_d900a948);
    serial_puts(&CHAR_NL);
    if (9 < _DAT_d900a948) {
      serial_puts((char *)param_2);
      serial_puts(s__lock_failed__reset____d90091a0);
      reset_system();
      do {
                    /* WARNING: Do nothing block with infinite loop */
      } while( true );
    }
  }
  else {
    _DAT_d900a948 = 0;
  }
  return -(iVar1 >> 0x1f) ^ 1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 pll_init(void)

{
  uint uVar1;
  uint uVar2;
  uint uVar3;
  
                    /* CPU pll init. */
  uVar3 = (uint)plls.pxp;
  if (plls.pxp == 0) {
    _DAT_c883c174 = _DAT_c883c174 & 0xfffffeff;
    clocks_set_sys_cpu_clk(0,0,0,0);
    _DAT_c883c294 = _DAT_c883c294 | 0x4000000;
    udelay(100);
    if ((ushort)(plls.cpu_clk - 600) < 0x259) {
      uVar3 = plls.cpu_clk / 0xc | 0x10200;
    }
    else if ((ushort)(plls.cpu_clk - 0x4b1) < 800) {
      uVar3 = plls.cpu_clk / 0x18 | 0x200;
    }
    do {
      _DAT_c883c304 = 0x5ac80000;
      _DAT_c883c308 = 0x8e452015;
      _DAT_c883c30c = 0x401d40c;
      _DAT_c883c310 = 0x870;
      _DAT_c883c300 = uVar3 | 0x40000000;
      udelay(0x14);
      uVar2 = pll_lock_check((int *)&DAT_c883c300,(byte *)s_SYS_PLL_d90091b8);
    } while (uVar2 != 0);
    clocks_set_sys_cpu_clk(1,0,0,0);
    uVar3 = 0;
    if ((_DAT_c883c300 >> 9 & 0x1f) != 0) {
      uVar3 = 0x18 / (_DAT_c883c300 >> 9 & 0x1f);
    }
    uVar2 = _DAT_c883c300 & 0x1ff;
    uVar1 = _DAT_c883c300 >> 0x10;
    serial_puts(s_CPU_clk__d90091c0);
    serial_put_dec((ulong)(uVar3 * uVar2 >> ((ulong)uVar1 & 3)));
    serial_puts(&DAT_d90091ca);
    _DAT_c883c28c = 0x10007;
    _DAT_c883c280 = _DAT_c883c280 | 0x20000000;
    udelay(200);
    _DAT_c883c284 = 0x59c80000;
    _DAT_c883c288 = 0xca45b822;
    _DAT_c883c290 = 0xb5500e1a;
    _DAT_c883c294 = 0xfc454545;
    _DAT_c883c280 = 0x400006fa;
    udelay(800);
    _DAT_c883c28c = _DAT_c883c28c | 0x4000;
    do {
      if ((int)_DAT_c883c280 < 0) break;
      _DAT_c883c280 = _DAT_c883c280 | 0x20000000;
      udelay(1000);
      _DAT_c883c280 = _DAT_c883c280 & 0xdfffffff;
      udelay(1000);
      uVar3 = pll_lock_check((int *)&DAT_c883c280,(byte *)s_FIX_PLL_d90091cf);
    } while (uVar3 != 0);
    _DAT_c883c2a4 = 0xfff0000;
    _DAT_c883c298 = 0x7d249;
    _DAT_c883c174 = _DAT_c883c174 & 0xffff8f00 | 0x5182;
    _DAT_c883c29c = 0x5f0ec;
  }
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

ulong saradc_ch1_get(void)

{
  ulong uVar1;
  uint uVar2;
  
  _DAT_c1108684 = 1;
  _DAT_c1108688 = 0x3000;
  _DAT_c110868c = 0xc3a8500a;
  _DAT_c1108690 = 0x10a000a;
  _DAT_c110869c = 0x3eb1a0c;
  _DAT_c11086a0 = 0x8c000c;
  _DAT_c11086a4 = 0x8e038c;
  _DAT_c11086a8 = 0xc00c400;
  _DAT_c883c3d8 = 0x114;
  _DAT_c11086ac = 0x2000;
  _DAT_c1108680 = 0x84064040;
  udelay(0x14);
  _DAT_c1108680 = 0x84064041;
  udelay(0x14);
  _DAT_c1108680 = 0x84064045;
  udelay(0x14);
  uVar2 = 0;
  do {
    if ((_DAT_c1108680 & 0x70000000) == 0) {
      if (uVar2 < 100) goto LAB_d9002bd4;
      break;
    }
    uVar2 = uVar2 + 1;
  } while (uVar2 != 0x65);
  serial_puts(s__Get_saradc_sample_Error__Cnt__d90091d7);
  serial_put_dec((ulong)uVar2);
  serial_puts(&CHAR_NL);
LAB_d9002bd4:
  uVar1 = 0;
  do {
    if ((_DAT_c1108698 & 0x3ff) <= (uint)(&DAT_d9009208)[uVar1]) break;
    uVar1 = uVar1 + 1;
  } while (uVar1 != 9);
  uVar1 = uVar1 & 0xffffffff;
  _DAT_c8100240 = _DAT_c8100240 & 0xffff00ff | (int)uVar1 << 8;
  serial_puts(s_Board_ID___d90091f6);
  serial_put_dec(uVar1);
  serial_puts(&CHAR_NL);
  return uVar1;
}



/* WARNING: Removing unreachable block (ram,0xd9002c84) */

void udelay(undefined8 us)

{
                    /* Name: Not sure */
                    /* WARNING: Do nothing block with infinite loop */
  do {
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int FUN_d9002c88(int *param_1,undefined4 *param_2)

{
  uint uVar1;
  bool bVar2;
  int iVar3;
  long lVar4;
  uint uVar5;
  ulong uVar6;
  undefined auStack_f0 [172];
  undefined auStack_44 [36];
  undefined auStack_20 [32];
  
  iVar3 = 0x170;
  if ((((*param_1 == 0x4c4d4140) && (*(char *)((long)param_1 + 10) == '\x01')) &&
      (*(char *)((long)param_1 + 0xb) == '\0')) &&
     ((*(char *)(param_1 + 2) == '@' && (param_1[5] == 0x40)))) {
    if (param_1[4] == 0) {
      uVar5 = _DAT_c8100228 & 0x10;
      if ((_DAT_c8100228 >> 4 & 1) != 0) {
        return 0x1a3;
      }
    }
    else {
      if (3 < (uint)param_1[4]) {
        return 0x1a3;
      }
      iVar3 = param_1[6];
      if (((iVar3 != 0x100) && (iVar3 != 0x200)) && (iVar3 != 0x80)) {
        return 0x1a3;
      }
      if (_DAT_d900a94c == 0) {
        _DAT_d900a94c = 1;
        sha2(0xd9000b70,_DAT_d9000f78 * 4 + 4,0xd900a718,0);
        iVar3 = _DAT_d9000f78 * 4;
        for (lVar4 = 0; (int)lVar4 < iVar3; lVar4 = lVar4 + 1) {
          *(undefined *)((0x11ffffff - lVar4) + (long)iVar3) = *(undefined *)(lVar4 + 0xd9000b70);
        }
        memcpy(0xd9000b70,(long)&DAT_12000000,((ulong)_DAT_d9000f78 & 0x3fffffff) << 2);
        if (param_1[6] == 0x200) {
          DAT_12000003 = DAT_d9000d70;
          DAT_12000002 = DAT_d9000d71;
          DAT_12000001 = DAT_d9000d72;
          DAT_12000000 = DAT_d9000d73;
          memcpy((ulong)&DAT_d9000d70,(long)&DAT_12000000,4);
        }
      }
      uVar5 = 1;
    }
    uVar1 = 0x40 - (param_1[10] & 0x3fU);
    memcpy(0x10800000,(long)param_1,(ulong)(uint)param_1[0xd]);
    inv_dcache_range(0x10800000,(ulong)_DAT_10800034);
    memcpy((ulong)param_1,(long)param_1 + (ulong)_DAT_10800038,(ulong)_DAT_10800034);
    inv_dcache_range((ulong)param_1,(ulong)_DAT_10800034);
    memcpy((ulong)_DAT_10800034 + 0x10800000,(long)param_1,(ulong)uVar1);
    inv_dcache_range((ulong)_DAT_10800034 + 0x10800000,(ulong)uVar1);
    iVar3 = _DAT_1080001c;
    uVar6 = (ulong)_DAT_10800014;
    FUN_d9001f18();
    FUN_d90020d4(auStack_f0,0x10800000,(uint)DAT_10800008);
    FUN_d90020d4(auStack_f0,iVar3 + 0x10800000,uVar1 + _DAT_10800028);
    FUN_d9002138((long)auStack_f0,uVar1 + (int)param_1,_DAT_10800038 - uVar1);
    memcpy((ulong)auStack_20,(long)auStack_44,0x20);
    inv_dcache_range((ulong)auStack_20,0x20);
    if (uVar5 == 0) {
      iVar3 = FUN_d9008b00((long)auStack_20,(ulong)_DAT_10800014 + 0x10800000,0x20);
      if (iVar3 != 0) {
        iVar3 = 0x223;
      }
    }
    else {
      lVar4 = 0;
      do {
        if ((int)(uint)DAT_d9000814 <= (int)lVar4) {
          return 0x22e;
        }
        iVar3 = FUN_d9008b00(0xd900a718,lVar4 * 0x30 + 0xd9000828,0x20);
        lVar4 = lVar4 + 1;
      } while (iVar3 != 0);
      bVar2 = FUN_d9007c78((uint *)0xd9000b70,uVar6 + 0x10800000,(long)auStack_20,
                           (uint *)&DAT_12000000);
      iVar3 = 0x1dd;
      if (bVar2) {
        if (param_2 != (undefined4 *)0x0) {
          *param_2 = 0;
        }
        serial_puts(s_aml_log___R__d9009230);
        serial_put_dec(((ulong)_DAT_d9000f78 & 0x7ffffff) << 5);
        serial_puts(s__lock_check_d9009193 + 5);
        serial_puts(s_Device_test_pass__d900940c + 0xc);
        iVar3 = 0;
      }
    }
  }
  return iVar3;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void aes_setkey(undefined4 *param_1)

{
  _DAT_d900a950 = 0;
  _DAT_da832094 = _DAT_da832094 | 5;
  _DAT_da832090 = _DAT_da832090 & 0xffffff00 | 0xf00;
  _DAT_da832000 = _DAT_da832000 & 0xfffffff7;
  _DAT_c8832270 = _DAT_c8832270 & 0xfffffcff;
  _DAT_c8832240 = *param_1;
  _DAT_c8832244 = param_1[1];
  _DAT_c8832248 = param_1[2];
  _DAT_c883224c = param_1[3];
  _DAT_c8832250 = param_1[4];
  _DAT_c8832254 = param_1[5];
  _DAT_c8832258 = param_1[6];
  _DAT_c883225c = param_1[7];
  _DAT_c8832260 = 0;
  _DAT_c8832264 = 0;
  _DAT_c8832268 = 0;
  _DAT_c883226c = 0;
  NDMA_set_table_position_secure(0,0x5500000,0x5500140);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90031bc(uint param_1,uint param_2,uint param_3)

{
  undefined4 in_stack_ffffffffffffffb4;
  undefined4 in_stack_ffffffffffffffbc;
  undefined4 in_stack_ffffffffffffffc4;
  undefined4 in_stack_ffffffffffffffcc;
  
  FUN_d9005f44(_DAT_d900a950,1,1,1,0,2,0,0,param_3,in_stack_ffffffffffffffb4,param_1,
               in_stack_ffffffffffffffbc,param_2,in_stack_ffffffffffffffc4,0xf,
               in_stack_ffffffffffffffcc,0);
  FUN_d9005e6c(_DAT_d900a950);
  FUN_d9005eb4(_DAT_d900a950);
  FUN_d9005ee8(_DAT_d900a950);
  return;
}



int aml_data_check(int *param_1,int *param_2,uint param_3,int param_4)

{
  int iVar1;
  undefined4 auStack_20 [8];
  
  if ((*param_1 == -0x559bffff) && (param_1[1] == 0x12345678)) {
LAB_d900327c:
    iVar1 = 0;
  }
  else {
    if (param_4 != 0) {
      sha2(0xd9000000,0x10,(ulong)auStack_20,0);
      aes_setkey(auStack_20);
      FUN_d90031bc((uint)param_1,(uint)param_2,param_3);
      inv_dcache_range((ulong)param_2,(ulong)param_3);
      param_1 = param_2;
      if ((*param_2 == -0x559bffff) && (param_2[1] == 0x12345678)) goto LAB_d900327c;
    }
    iVar1 = FUN_d9002c88(param_1,(undefined4 *)0x0);
  }
  return iVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_set_dmc_ctrl_new(ddr_set *ddrs)

{
  ulong ddr0_size;
  ulong ddr1_size;
  uint ddr0_size_reg;
  int ddr1_size_reg;
  ushort ddr_size;
  
  ddr_size = ddrs->ddr_size;
  ddr1_size_reg = -1;
  for (ddr0_size_reg = (uint)ddr_size; ddr0_size_reg != 0; ddr0_size_reg = ddr0_size_reg >> 1) {
    ddr1_size_reg = ddr1_size_reg + (ddr0_size_reg & 1);
  }
  ddr0_size = (ulong)(ddr_size >> 6);
  if ((((byte)(ddrs->ddr_channel_set - 3) < 2) ||
      (ddr1_size = ddr0_size, ddrs->ddr_channel_set == '\x06')) &&
     (ddr_size = ddr_size >> 7, ddr0_size = (ulong)ddr_size, ddr1_size = ddr0_size,
     ddr1_size_reg != 0)) {
    ddr1_size = (ulong)ddr_size << 1;
    ddr0_size = (ulong)ddr_size << 2;
  }
  ddr0_size_reg = 0;
  while (ddr0_size = ddr0_size >> 1, (ddr0_size & 1) == 0) {
    ddr0_size_reg = ddr0_size_reg + 1;
  }
  ddr1_size_reg = 0;
  while (ddr1_size = ddr1_size >> 1, (ddr1_size & 1) == 0) {
                    /* == 12 after the loop */
    ddr1_size_reg = ddr1_size_reg + 1;
  }
                    /* 0 | (0x200040 | 0x2D) & 0xFFFFFFC0 | 12 << 3
                         == 0x200060 */
  _DAT_da838768 = ddr0_size_reg | ddrs->ddr_dmc_ctrl & 0xffffffc0 | ddr1_size_reg << 3;
  ddrs->ddr_dmc_ctrl = _DAT_da838768;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_detect_size(ddr_set *ddrs)

{
  uint uVar1;
  uint uVar2;
  bool channel_set;
  bool bVar3;
  uint uVar4;
  int *piVar5;
  int iVar6;
  
  _DAT_da838768 = _DAT_da838768 & 0xffffffc0 | 0x2d;
  uVar1 = ddrs->ddr_dmc_ctrl;
  ddrs->ddr_size = 0;
  channel_set = ddrs->ddr_channel_set == '\x04';
  piVar5 = (int *)&DAT_01000000;
  iVar6 = 0;
  do {
    if ((int)(uint)channel_set < iVar6) {
      if (channel_set) {
        ddrs->ddr_size = ddrs->ddr_size >> 1;
      }
      if (ddrs->ddr_channel_set == '\x03') {
        ddrs->ddr_size = ddrs->ddr_size << 1;
      }
      ddr_set_dmc_ctrl_new(ddrs);
      return;
    }
    uVar4 = 0x14;
    do {
      *piVar5 = 0;
      udelay(10);
      uVar2 = 4 << (ulong)(uVar4 & 0x1f);
      *(undefined4 *)((ulong)uVar2 + (long)piVar5) = 0xaabbccdd;
      udelay(10);
      if (ddrs->ddr_channel_set == '\x03') {
        bVar3 = *piVar5 == 0;
LAB_d9003488:
        bVar3 = !bVar3;
      }
      else {
        bVar3 = false;
        if (*piVar5 != 0) {
          bVar3 = *piVar5 == -0x55443323;
          goto LAB_d9003488;
        }
      }
    } while ((!bVar3) && (uVar4 = uVar4 + 1, uVar4 != 0x1e));
    ddrs->ddr_size = (ushort)(uVar2 >> 0x14) + ddrs->ddr_size;
    iVar6 = iVar6 + 1;
    piVar5 = (int *)(ulong)(uint)((int)piVar5 + (1 << (ulong)((uVar1 >> 8 & 7) + 8)));
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_init_pll(void)

{
  uint uVar1;
  uint uVar2;
  
  _DAT_c88345bc = _DAT_c88345bc | 1;
  _DAT_c883c290 = _DAT_c883c290 | 1;
  _DAT_c8836810 = _DAT_c8836810 & 0xffffefff | 0x1000;
  udelay(10);
                    /* ddrs.ddr_clk == 0x390/912 MHz
                       (ddrs.ddr_clk - 200) == 0x2C8/712 MHz
                       0x2C8 > 0x226
                       Thus this code inside `if` is never ran when 
                       using the WeTek Play2 ACS */
  if ((ushort)(ddrs.ddr_clk - 200) < 0x226) {
    uVar1 = ddrs.ddr_clk / 6;
    uVar2 = 0x20200;
  }
  else {
    if (0x4e1 < (ushort)(ddrs.ddr_clk - 0x2ee)) goto LAB_d90035e4;
                    /* 0x390 / 0xC = 0x4C */
    uVar1 = ddrs.ddr_clk / 12;
    uVar2 = 0x10200;
  }
  ddrs.ddr_pll_ctrl = uVar2 | uVar1 >> 1;
LAB_d90035e4:
                    /* In order:
                       * `1 < DAT_D900A2F5` -> version check in ddrs_
                       * check in ddrs_
                       This thing never gets run in a WeTek Play2/Videostrong KII Pro!! */
  if ((1 < acs_setting_d900a2e0.ddr_set_version) && ((ddrs.ddr_func & 1) != 0)) {
    serial_print_new(s_STICKY_REG0__0x_d9009288,(ulong)_DAT_c88345f0,0,&CHAR_NL);
    serial_print_new(s_STICKY_REG1__0x_d9009298,(ulong)_DAT_c88345f4,0,&CHAR_NL);
    if (_DAT_c88345f0 >> 0x14 == 0xf13) {
      uVar2 = _DAT_c88345f0 & 0xfffff;
      if ((_DAT_c88345f0 & 0xfffff) == 0) {
        uVar2 = ddrs.t_pub_zq0pr;
      }
      serial_print_new(s_ZQCR__0x_d90092a8,(ulong)ddrs.t_pub_zq0pr,0,&CHAR_NUL);
      serial_print_new(s_____0x_d90092b1,(ulong)uVar2,0,&CHAR_NL);
      ddrs.t_pub_zq0pr = uVar2;
      ddrs.t_pub_zq1pr = uVar2;
      ddrs.t_pub_zq2pr = uVar2;
      ddrs.t_pub_zq3pr = uVar2;
      serial_print_new(s_PLL___0x_d90092b8,(ulong)ddrs.ddr_pll_ctrl,0,&CHAR_NUL);
      serial_print_new(s_____0x_d90092b1,(ulong)_DAT_c88345f4,0,&CHAR_NL);
      ddrs.ddr_pll_ctrl = _DAT_c88345f4;
      _DAT_c88345f0 = 0;
      _DAT_c88345f4 = 0;
    }
  }
  do {
    _DAT_c8836804 = 0x69c80000;
    _DAT_c8836808 = 0xca463823;
    _DAT_c883680c = 0xc00023;
    _DAT_c8836810 = 0x303500;
    _DAT_c8836800 = ddrs.ddr_pll_ctrl & 0xdfffffff;
    udelay(200);
    uVar2 = pll_lock_check((int *)&DAT_c8836800,(byte *)s_DDR_PLL_d90092c1);
  } while (uVar2 != 0);
  _DAT_c8836818 = 0xb0000000;
  uVar2 = _DAT_c8836800 & 0xdfffffff;
  uVar1 = 0;
  if ((uVar2 >> 9 & 0x1f) != 0) {
    uVar1 = ((_DAT_c8836800 & 0x1ff) * 0x18) / (uVar2 >> 9 & 0x1f);
  }
                    /* ddrs.ddr_clk = (ushort)(((uVar1 >> ((ulong)(uVar2 >> 0x10) & 3)) >>
                       ((ulong)(uVar2 >> 0xe) & 3)) << 1)
                                    = (uVar1 >> (uVar2 >> 0x10) & 3) >> (((uVar2 >> 0xE) & 3) << 1)
                        */
  ddrs.ddr_clk = (ushort)(((uVar1 >> ((ulong)(uVar2 >> 0x10) & 3)) >> ((ulong)(uVar2 >> 0xe) & 3))
                         << 1);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_print_info(void)

{
  byte bVar1;
  uint uVar2;
  bool bVar3;
  char *prefix;
  uint uVar4;
  uint uVar5;
  
                    /* Very wild guess: ddr_print_info() */
  if (ddrs.ddr_size_detect != '\0') {
    ddr_detect_size(&ddrs);
  }
  uVar2 = _DAT_da838768;
  bVar1 = ddrs.ddr_channel_set - 3;
  bVar3 = ddrs.ddr_channel_set == '\x06';
  if (ddrs.ddr_channel_set == '\x04') {
    uVar5 = 2;
  }
  else {
    uVar5 = _DAT_c8839080 >> 3 & 1;
  }
  for (uVar4 = 0; (int)uVar4 <= (int)(uint)(bVar1 < 2 || bVar3); uVar4 = uVar4 + 1) {
    serial_print_new(s_DDR_d90095e7,(ulong)uVar4,1,&CHAR_NUL);
    serial_print_new(&DAT_d900971e,
                     (ulong)(uint)(1 << (ulong)((uVar2 >> (ulong)(uVar4 * 3 & 0x1f) & 7) + 7)),1,
                     &DAT_d90092d9);
    if (ddrs.ddr_size_detect != '\0') {
      serial_puts(s__auto__d90092dc);
    }
    if (uVar5 == 2) {
      prefix = s__F1T__d90092ce;
    }
    else if (uVar5 == 1) {
      prefix = &DAT_d90092d4;
    }
    else {
      prefix = &DAT_d90092c9;
    }
    serial_print_new(prefix,(ulong)ddrs.ddr_timing_ind,1,&CHAR_NL);
  }
  _DAT_da100240 = _DAT_da100240 & 0xffff | (uint)ddrs.ddr_size << 0x10;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 ddr_init_dmc(void)

{
  undefined4 *puVar1;
  ulong uVar2;
  undefined4 uVar3;
  
                    /* As seen in Amlogic's old sources (again):
                           plat/gxb/ddr/sec_mmc_define.h:#define DMC_SEC_REG_BASE 0xda838400
                       So some of it must be DMC-related, which is why this function is named
                       'ddr_init_dmc'
                       
                       P.S. these DMC registers are documented in Amlogic's public S905 datasheet.
                        */
  ddr_set_dmc_ctrl_new(&ddrs);
                    /* (unsigned)(ddrs.ddr_channel_set - 4) > 3 (WeTek Play2 ACS) */
  if ((byte)(ddrs.ddr_channel_set - 4) < 3) {
    _DAT_da838740 = 0x128398a0;
    _DDR0_ADDRMAP_1_da838744 = 0x1ee683ca;
    _DAT_da838748 = 0x2b49ca30;
    _DDR0_ADDRMAP_3_da83874c = 0x39ace2f6;
    _DDR0_ADDRMAP_4_da838750 = 0x3c0db17d;
    _DAT_da838754 = 0x128398a0;
    _DDR1_ADDRMAP_1_da838758 = 0x1ee683ca;
    _DAT_da83875c = 0x2b49ca30;
    _DDR1_ADDRMAP_3_da838760 = 0x39ace2f6;
    puVar1 = (undefined4 *)&DAT_da838764;
    uVar3 = 0x3c0db17d;
  }
  else if ((byte)(ddrs.ddr_channel_set - 1) < 2) {
    if (((ddrs.ddr_type == '\0') || (acs_setting_d900a2e0.ddr_set_version < 3)) ||
       (ddrs.t_lpddr3_remap != '\x03')) {
      _DDR0_ADDRMAP_1_da838744 = 0x20f703eb;
      _DDR0_ADDRMAP_3_da83874c = 0x3bbd6717;
    }
    else {
      _DDR0_ADDRMAP_1_da838744 = 0x20f77fab;
      _DDR0_ADDRMAP_3_da83874c = 0x1bd6717;
    }
    puVar1 = (undefined4 *)&DDR0_ADDRMAP_4_da838750;
    uVar3 = 0xe359e;
  }
  else {
                    /* Rank0+1 diff. */
    if (ddrs.ddr_channel_set != 3) goto LAB_d9003c90;
    if (ddrs.ddr_type == 0) {
      _DDR0_ADDRMAP_1_da838744 = 0x20f703eb;
      _DDR0_ADDRMAP_3_da83874c = 0x3bbd6717;
      _DDR1_ADDRMAP_1_da838758 = 0x20f703eb;
      _DDR1_ADDRMAP_3_da838760 = 0x3bbd6717;
LAB_d9003c48:
      _DDR0_ADDRMAP_4_da838750 = 0x3c0e3580;
      uVar2 = 0x8764;
    }
    else {
      if ((2 < acs_setting_d900a2e0.ddr_set_version) && (ddrs.t_lpddr3_remap == 3)) {
        _DDR0_ADDRMAP_1_da838744 = 0x20f77fab;
        _DDR0_ADDRMAP_3_da83874c = 0x1bd6717;
        _DDR1_ADDRMAP_1_da838758 = 0x20f77fab;
        _DDR1_ADDRMAP_3_da838760 = 0x1bd6717;
        goto LAB_d9003c48;
      }
      _DDR0_ADDRMAP_1_da838744 = 0x20f703eb;
      _DDR0_ADDRMAP_3_da83874c = 0x3bbd6717;
      uVar2 = 0x8750;
    }
    puVar1 = (undefined4 *)(uVar2 | 0xda830000);
    uVar3 = 0x3c0e3580;
  }
  *puVar1 = uVar3;
LAB_d9003c90:
  _DAT_c8838118 = 0x440620;
  _DAT_c8836c08 = 0x2020;
  _DAT_c8836c00 = 5;
  _DAT_c88382e4 = 0x11;
                    /* DMC SEC */
  _DAT_da83841c = 0;
  _DAT_da838400 = 0x80000000;
  _DAT_da838438 = 0x55555555;
  _DAT_da8384d8 = 0x55555555;
  _DAT_da8384dc = 0x55555555;
  _DAT_da8384d0 = 0x15;
  _DAT_da8384d4 = 5;
                    /* VPU */
  _DAT_da838494 = 0xffffffff;
  _DAT_da8384cc = 0x55555555;
  _DAT_da8384c8 = 0x55555555;
                    /* VDEC */
  _DAT_da838448 = 0xffffffff;
  _DAT_da838444 = 0x55555555;
  _DAT_da838440 = 0x55555555;
                    /* HCODEC */
  _DAT_da838464 = 0xffffffff;
  _DAT_da838460 = 0x55555555;
  _DAT_da83845c = 0x55555555;
                    /* HEVC */
  _DAT_da838480 = 0xffffffff;
  _DAT_da83847c = 0x55555555;
  _DAT_da838478 = 0x55555555;
                    /* DMC_REQ_CTRL */
  _DAT_c8838000 = 0xffff;
  _DAT_da838640 = 0;
  _DAT_da838644 = 0;
  _DAT_da838648 = _DAT_c8834500;
  _DAT_da83864c = _DAT_c8834500;
  _DAT_c1107d40 = 0xbaadf00d;
                    /* As seen on old Amlogic sources, plat/gxb/ddr/ddr.c:
                           // PUT SOME CODE HERE TO TRY TO STOP BUS TRAFFIC
                           __asm__ volatile("NOP");
                           __asm__ volatile("DMB SY");
                           __asm__ volatile("ISB");
                       Which is possibly why we see DataMemoryBarrier and
                       InstructionSynchronizationBarrier here. */
  DataMemoryBarrier(3,3);
  InstructionSynchronizationBarrier();
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */
/* WARNING: Restarted to delay deadcode elimination for space: ram */

undefined8 ddr_init_pctl(int param_1)

{
  long lVar1;
  byte bVar2;
  byte bVar3;
  uint *puVar4;
  uint *puVar5;
  uint uVar6;
  ulong uVar7;
  long lVar8;
  uint tmp_var;
  char *suffix;
  ddr_set_t *pdVar9;
  uint ddr1_enabled;
  long ddrt_pointer_2__notsure;
  ushort pctl0_100ns_pck__and__dfistcfg1;
  ushort pctl0_1us_pack__and__dfistcfg0;
  ushort pctl0_init_us__and__dfitctrldelay;
  ushort pctl0_rsth_us__and__dfitphywrdata;
  ushort uVar10_pctl0_cmdtstaten__notsure;
  ushort uVar3_pctl0_dfitphywrlta__notsure;
  ushort uVar4_pctl0_dfitrddataen__notsure;
  ushort uVar5_pctl0_dfitphyrdlat__notsure;
  ushort uVar6_pctl0_dfitdramclkdis__notsure;
  ushort uVar7_pctl0_dfitdramclken__notsure;
  ushort uVar8_pctl0_dfitphyupdtype1__notsure;
  ushort uVar9_pctl0_dfitctrlupdmin__notsure;
  
LAB_d9003e84:
  do {
                    /* As seen in Amlogic's old sources:
                           plat/gxb/ddr/ddr_pub_define.h:#define DDR0_PUB_REG_BASE 0xc8836000
                       So most (all?) of it must be DDR pub-related, which is why this function is
                       named 'ddr_init_pctl'
                       
                       Also param_1 is set to 1 if the Rank setting is set to be automatic... */
    pctl0_rsth_us__and__dfitphywrdata = ddrs.t_pctl0_rsth_us;
    pctl0_init_us__and__dfitctrldelay = ddrs.t_pctl0_init_us;
    pctl0_100ns_pck__and__dfistcfg1 = ddrs.t_pctl0_100ns_pck;
    pctl0_1us_pack__and__dfistcfg0 = ddrs.t_pctl0_1us_pck;
                    /* ddr0_enabled == 1
                       ddr1_enabled == 0 */
    _ddr0_enabled = (uint)(((ulong)ddrs.ddr_dmc_ctrl ^ 0x80) >> 7) & 1;
    ddr1_enabled = (uint)(((ulong)ddrs.ddr_dmc_ctrl ^ 0x40) >> 6) & 1;
    BL21__DAT_d900a969 = 0;
    _DAT_c8838004 = 0xffffffff;
    _DAT_c8838008 = 0xffffffff;
    _DAT_c8838118 = 0x550620;
    _DAT_c8836c04 = 0xf;
    _DAT_c8836238 = 0x49494949;
    _DAT_c883623c = 0x49494949;
    _DAT_c88360ac = ddrs.t_pub_odtcr;
    _DAT_c883609c = (uint)ddrs.t_pub_mr[0];
    _DAT_c88360a0 = (uint)ddrs.t_pub_mr[1];
    _DAT_c88360a4 = (uint)ddrs.t_pub_mr[2];
    _DAT_c88360a8 = (uint)ddrs.t_pub_mr[3];
    if (ddrs.ddr_type == 0x2) {
      _DAT_c88361a0 = (uint)ddrs.t_pub_mr11;
    }
    _DAT_c883608c = ddrs.t_pub_dtpr[0];
    _DAT_c8836090 = ddrs.t_pub_dtpr[1];
    _DAT_c883600c = ddrs.t_pub_pgcr1;
    _DAT_c8836010 = ddrs.t_pub_pgcr2;
    _DAT_c8836014 = ddrs.t_pub_pgcr3;
    _DAT_c8836080 = ddrs.t_pub_dxccr;
    _DAT_c8836094 = ddrs.t_pub_dtpr[2];
    _DAT_c8836098 = ddrs.t_pub_dtpr[3];
    _DAT_c88360b0 = ddrs.t_pub_dtcr;
    do {
    } while ((_DAT_c8836018 & 1) == 0);
    _DAT_c883606c = 0;
    _DAT_c8836070 = 0;
    _DAT_c8836074 = 0;
    _DAT_c8836078 = 0;
    _DAT_c883607c = 0;
    _DAT_c8836284 = 0;
    _DAT_c8836288 = 0;
    _DAT_c883628c = 0x2400;
    _DAT_c8836304 = 0;
    _DAT_c8836308 = 0;
    _DAT_c883630c = 0x2400;
    _DAT_c8836384 = 0;
    _DAT_c8836388 = 0;
    _DAT_c883638c = 0x2400;
    _DAT_c8836404 = 0;
    _DAT_c8836408 = 0;
    _DAT_c883640c = 0x2400;
    _DAT_c8836088 = ddrs.t_pub_dcr;
    _DAT_c88360b4 = ddrs.t_pub_dtar;
    _DAT_c88360b8 = ddrs.t_pub_dtar | 8;
    _DAT_c88360bc = ddrs.t_pub_dtar | 0x10;
    _DAT_c88360c0 = ddrs.t_pub_dtar | 0x18;
    if (plls.pxp != 0) {
      _DAT_c8836004 = 0x581;
    }
    _DAT_c8836084 = ddrs.t_pub_dsgcr;
    do {
    } while ((_DAT_c8836018 & 1) == 0);
    uVar7 = 0;
    _DAT_d900a958_pctl_ddr1 = ddr1_enabled;
    do {
                    /* Done once. (WeTek Play2 ACS) */
      ddrt_pointer_2__notsure = (uVar7 & 0x3fffff) * 0x400;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88390c0) = (uint)pctl0_1us_pack__and__dfistcfg0;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88390cc) = (uint)pctl0_100ns_pck__and__dfistcfg1;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88390c4) = (uint)pctl0_init_us__and__dfitctrldelay;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88390c8) = (uint)pctl0_rsth_us__and__dfitphywrdata;
      tmp_var = 0;
      if (ddrs.ddr_2t_mode != 0) {
        tmp_var = 8;
      }
      *(uint *)(&DAT_c8839080 + ddrt_pointer_2__notsure) = tmp_var | ddrs.t_pctl0_mcfg;
      tmp_var = ddrs.t_pctl0_mcfg1;
      if (ddrs.ddr_channel_set == 0x4) {
        tmp_var = ddrs.t_pctl0_mcfg1 & 0xffffff00;
      }
      uVar6 = (int)uVar7 + 1;
      uVar7 = (ulong)uVar6;
      *(uint *)(ddrt_pointer_2__notsure + 0xc883907c) = tmp_var;
    } while (uVar6 <= ddr1_enabled);
    udelay(500);
    ddr1_enabled = _DAT_d900a958_pctl_ddr1;
    ddrt_pointer_2__notsure = _ddrt_pointer;
                    /* _ddr0_enabled    = (uint)(((ulong)ddrs.ddr_dmc_ctrl ^ 0x80) >> 7) & 1
                                        = (((0x200040 | 0x2D) ^ 0x80) >> 7) & 1
                                        = 0x1
                       Such a complex thing just for this... (WeTek Play2 ACS) */
    if (_ddr0_enabled != 0) {
      do {
      } while ((_DAT_c88392c0 & 1) == 0);
      _DAT_c8839044 = 1;
      do {
      } while ((_DAT_c8839048 & 1) == 0);
    }
    if (_DAT_d900a958_pctl_ddr1 != 0) {
      do {
      } while ((_DAT_c88396c0 & 1) == 0);
      _DAT_c8839444 = 1;
      do {
      } while ((_DAT_c8839448 & 1) == 0);
    }
    uVar7 = 0;
    do {
                    /* 0xE  => cfg_ddr_rfc
                       0x18 => cfg_ddr_refi_mddr3
                       
                       Good one, Amlogic.
                       
                       Later:
                       while (uVar18 <= uVar1)
                       ==> while (8 <= 0) */
      lVar1 = (uVar7 & 0x3fffff) * 0x400;
      *(uint *)(lVar1 + 0xc88390d8) = (uint)*(ushort *)(ddrt_pointer_2__notsure + 0xe);
      *(uint *)(lVar1 + 0xc8839148) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x18);
                    /* cfg_ddr_mrd */
      tmp_var = (uint)*(byte *)(ddrt_pointer_2__notsure + 8);
      if (6 < *(byte *)(ddrt_pointer_2__notsure + 8)) {
        tmp_var = 7;
      }
      *(uint *)(lVar1 + 0xc88390d4) = tmp_var;
      if (ddrs.ddr_type == '\0') {
                    /* cfg_ddr_rp */
        *(uint *)(lVar1 + 0xc88390dc) = (uint)*(byte *)(ddrt_pointer_2__notsure + 3);
                    /* cfg_ddr_cke + 1 */
        tmp_var = *(byte *)(ddrt_pointer_2__notsure + 0x14) + 1;
LAB_d9004314:
        *(uint *)(lVar1 + 0xc8839140) = tmp_var;
      }
      else {
                    /* Not ran (WeTek Play2 ACS) */
        if (ddrs.ddr_type == '\x01') {
          lVar8 = 0xc88390dc;
          tmp_var = (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x2a) |
                    ((uint)*(ushort *)(ddrt_pointer_2__notsure + 0x28) -
                    (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x2a)) * 0x10000;
LAB_d9004304:
          *(uint *)(lVar1 + lVar8) = tmp_var;
          tmp_var = (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x30);
          goto LAB_d9004314;
        }
        if (ddrs.ddr_type == '\x02') {
          *(uint *)(lVar1 + 0xc88390dc) =
               (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x2a) |
               ((uint)*(ushort *)(ddrt_pointer_2__notsure + 0x28) -
               (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x2a)) * 0x10000;
          lVar8 = 0xc8839144;
          tmp_var = (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x32);
          goto LAB_d9004304;
        }
      }
                    /* cfg_ddr_al */
      *(uint *)(lVar1 + 0xc88390e4) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x1c);
                    /* cfg_ddr_cwl */
      *(uint *)(lVar1 + 0xc88390ec) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x1b);
                    /* cfg_ddr_cl */
      *(uint *)(lVar1 + 0xc88390e8) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x19);
                    /* cfg_ddr_ras */
      *(uint *)(lVar1 + 0xc88390f0) = (uint)*(byte *)(ddrt_pointer_2__notsure + 5);
                    /* cfg_ddr_rc */
      *(uint *)(lVar1 + 0xc88390f4) = (uint)*(byte *)(ddrt_pointer_2__notsure + 7);
                    /* cfg_ddr_rcd */
      *(uint *)(lVar1 + 0xc88390f8) = (uint)*(byte *)(ddrt_pointer_2__notsure + 4);
                    /* cfg_ddr_rrd */
      *(uint *)(lVar1 + 0xc88390fc) = (uint)*(byte *)(ddrt_pointer_2__notsure + 6);
                    /* cfg_ddr_rtp */
      *(uint *)(lVar1 + 0xc8839100) = (uint)*(byte *)(ddrt_pointer_2__notsure + 1);
                    /* cfg_ddr_wr */
      *(uint *)(lVar1 + 0xc8839104) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x1a);
                    /* cfg_ddr_wtr */
      *(uint *)(lVar1 + 0xc8839108) = (uint)*(byte *)(ddrt_pointer_2__notsure + 2);
                    /* cfg_ddr_exsr */
      *(uint *)(lVar1 + 0xc883910c) = (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x22);
                    /* cfg_ddr_xp */
      *(uint *)(lVar1 + 0xc8839110) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0xd);
                    /* cfg_ddr_dqs */
      *(uint *)(lVar1 + 0xc8839120) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x1d);
                    /* cfg_ddr_rtw */
      *(uint *)(lVar1 + 0xc88390e0) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x16);
                    /* cfg_ddr_cksre */
      *(uint *)(lVar1 + 0xc8839124) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x1e);
                    /* cfg_ddr_cksrx */
      *(uint *)(lVar1 + 0xc8839128) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x1f);
                    /* cfg_ddr_mod */
      *(uint *)(lVar1 + 0xc8839130) = (uint)*(byte *)(ddrt_pointer_2__notsure + 9);
                    /* cfg_ddr_cke */
      *(uint *)(lVar1 + 0xc883912c) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x14);
                    /* cfg_ddr_zqcs */
      *(uint *)(lVar1 + 0xc8839118) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x20);
                    /* cfg_ddr_zqcl */
      *(uint *)(lVar1 + 0xc8839138) = (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x24);
                    /* cfg_ddr_xpdll */
      *(uint *)(lVar1 + 0xc8839114) = (uint)*(byte *)(ddrt_pointer_2__notsure + 0x21);
      tmp_var = (int)uVar7 + 1;
      uVar7 = (ulong)tmp_var;
                    /* cfg_ddr_zqcsi */
      *(uint *)(lVar1 + 0xc883911c) = (uint)*(ushort *)(ddrt_pointer_2__notsure + 0x26);
      uVar6 = _DAT_d900a958_pctl_ddr1;
      uVar10_pctl0_cmdtstaten__notsure = ddrs.t_pctl0_cmdtstaten;
      uVar9_pctl0_dfitctrlupdmin__notsure = ddrs.t_pctl0_dfitctrlupdmin;
      uVar8_pctl0_dfitphyupdtype1__notsure = ddrs.t_pctl0_dfitphyupdtype1;
      uVar7_pctl0_dfitdramclken__notsure = ddrs.t_pctl0_dfitdramclken;
      uVar6_pctl0_dfitdramclkdis__notsure = ddrs.t_pctl0_dfitdramclkdis;
      uVar5_pctl0_dfitphyrdlat__notsure = ddrs.t_pctl0_dfitphyrdlat;
      uVar4_pctl0_dfitrddataen__notsure = ddrs.t_pctl0_dfitrddataen;
      uVar3_pctl0_dfitphywrlta__notsure = ddrs.t_pctl0_dfitphywrlta;
      pctl0_rsth_us__and__dfitphywrdata = ddrs.t_pctl0_dfitphywrdata;
      pctl0_init_us__and__dfitctrldelay = ddrs.t_pctl0_dfitctrldelay;
      pctl0_100ns_pck__and__dfistcfg1 = ddrs.t_pctl0_dfistcfg1;
      pctl0_1us_pack__and__dfistcfg0 = ddrs.t_pctl0_dfistcfg0;
    } while (tmp_var <= ddr1_enabled);
    if (_ddr0_enabled != 0) {
      _DAT_c8839000 = (uint)ddrs.t_pctl0_scfg;
      _DAT_c8839004 = (uint)ddrs.t_pctl0_sctl;
    }
    if (_DAT_d900a958_pctl_ddr1 != 0) {
      _DAT_c8839400 = (uint)ddrs.t_pctl0_scfg;
      _DAT_c8839404 = (uint)ddrs.t_pctl0_sctl;
    }
    _DAT_c1107d40 = 0xdeadbeef;
    _DAT_c883c010 = 0x88776655;
    if (_ddr0_enabled != 0) {
      do {
      } while ((_DAT_c8839008 & 1) == 0);
    }
    if (_DAT_d900a958_pctl_ddr1 != 0) {
      do {
      } while ((_DAT_c8839408 & 1) == 0);
    }
                    /* Only ran once (WeTek Play2 ACS) */
    uVar7 = 0;
    do {
      ddrt_pointer_2__notsure = (uVar7 & 0x3fffff) * 0x400;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839084) = ddrs.t_pctl0_ppcfg;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88392c4) = (uint)pctl0_1us_pack__and__dfistcfg0;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88392c8) = (uint)pctl0_100ns_pck__and__dfistcfg1;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839240) = (uint)pctl0_init_us__and__dfitctrldelay;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839250) = (uint)pctl0_rsth_us__and__dfitphywrdata;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839254) = (uint)uVar3_pctl0_dfitphywrlta__notsure;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839260) = (uint)uVar4_pctl0_dfitrddataen__notsure;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839264) = (uint)uVar5_pctl0_dfitphyrdlat__notsure;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88392d4) = (uint)uVar6_pctl0_dfitdramclkdis__notsure;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88392d0) = (uint)uVar7_pctl0_dfitdramclken__notsure;
      *(uint *)(ddrt_pointer_2__notsure + 0xc88392f0) = ddrs.t_pctl0_dfilpcfg0;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839274) = (uint)uVar8_pctl0_dfitphyupdtype1__notsure;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839280) = (uint)uVar9_pctl0_dfitctrlupdmin__notsure;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839244) = ddrs.t_pctl0_dfiodtcfg;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839248) = ddrs.t_pctl0_dfiodtcfg1;
      ddr1_enabled = (int)uVar7 + 1;
      uVar7 = (ulong)ddr1_enabled;
      *(uint *)(ddrt_pointer_2__notsure + 0xc8839050) = (uint)uVar10_pctl0_cmdtstaten__notsure;
    } while (ddr1_enabled <= uVar6);
    bVar2 = DAT_d900a968;
    bVar3 = BL21__DAT_d900a969;
    if (plls.pxp != 0) goto LAB_d90046f8;
    _DAT_c8836244 = ddrs.t_pub_zq0pr;
    _DAT_c8836254 = ddrs.t_pub_zq1pr;
    _DAT_c8836264 = ddrs.t_pub_zq2pr;
    _DAT_c8836274 = ddrs.t_pub_zq3pr;
    _DAT_c8836004 = 3;
    do {
    } while ((_DAT_c8836018 & 1) == 0);
    _DAT_c8836240 = _DAT_c8836240 | 0x8000004;
    udelay(10);
    _DAT_c8836240 = _DAT_c8836240 & 0xf7fffffb;
    udelay(30);
                    /* (ddrs.ddr_channel_set - 5) > 2 because ddrs.ddr_channel_set is unsigned.
                       (WeTek Play2 ACS) */
    if ((byte)(ddrs.ddr_channel_set - 5) < 2) {
      _DAT_c8836380 = _DAT_c8836380 & 0xfffffffe;
      _DAT_c8836400 = _DAT_c8836400 & 0xfffffffe;
    }
    _DAT_c8836040 = ddrs.t_pub_acbdlr0;
    if ((ddrs.ddr_type == '\0') && (2 < acs_setting_d900a2e0.ddr_set_version)) {
      _DAT_c8836004 = 0xfff3;
      do {
        udelay(500);
        BL21__DAT_d900a969 = BL21__DAT_d900a969 + 1;
        DAT_d900a968 = DAT_d900a968 + 1;
        if (10 < BL21__DAT_d900a969) {
          if (param_1 == 0) goto LAB_d9003e84;
          suffix = s__PGSR0__0x_d900932b;
          goto LAB_d9004ddc;
        }
        if (100 < DAT_d900a968) {
          if (param_1 == 0) {
            return 0xffffffff;
          }
          serial_print_new(s_only_wl_Error_PGSR0__0x_d900931e,(ulong)_DAT_c8836018,0,&DAT_d9009339);
          serial_print_new(s_DX0GSR2__0x_d90092ee,(ulong)_DAT_c8836298,0,&DAT_d9009339);
          serial_print_new(s_DX1GSR2__0x_d90092fa,(ulong)_DAT_c8836318,0,&DAT_d9009339);
          serial_print_new(s_DX2GSR2__0x_d9009306,(ulong)_DAT_c8836398,0,&DAT_d9009339);
          serial_print_new(s_DX3GSR2__0x_d9009312,(ulong)_DAT_c8836418,0,&DAT_d9009339);
                    /* "DDR init fail, reset..." */
          serial_puts(s_LPDDR_init_fail__reset____d900933c + 2);
          reset_system();
          do {
                    /* WARNING: Do nothing block with infinite loop */
          } while( true );
        }
      } while ((_DAT_c8836018 != 0xc0000fff) && (_DAT_c8836018 != 0x80000fff));
    }
    bVar2 = DAT_d900a968;
    bVar3 = BL21__DAT_d900a969;
    if ((ddrs.ddr_type != '\x02') || (acs_setting_d900a2e0.ddr_set_version < 3)) {
LAB_d90046f8:
      BL21__DAT_d900a969 = bVar3;
      DAT_d900a968 = bVar2;
      if (ddrs.ddr_channel_set == '\x02') {
        _DAT_c88363b8 = _DAT_c88363b8 & 0xffffff00 | _DAT_c88363b8 >> 8;
        ddr1_enabled = _DAT_c88363c8 >> 3 & 7;
        tmp_var = _DAT_c88363c8 >> 0xe & 3;
        _DAT_c88363c8 = tmp_var << 0xc | ddr1_enabled << 3 | ddr1_enabled | tmp_var << 0xe;
        _DAT_c88363c0 = _DAT_c88363c0 & 0xffffff00 | _DAT_c88363c0 >> 8;
        _DAT_c8836438 = _DAT_c8836438 & 0xffffff00 | _DAT_c8836438 >> 8;
        ddr1_enabled = _DAT_c8836448 >> 3 & 7;
        tmp_var = _DAT_c8836448 >> 0xe & 3;
        _DAT_c8836448 = tmp_var << 0xc | ddr1_enabled << 3 | ddr1_enabled | tmp_var << 0xe;
        _DAT_c8836440 = _DAT_c8836440 & 0xffffff00 | _DAT_c8836440 >> 8;
        _DAT_c88362b8 = _DAT_c88362b8 & 0xffff00ff | _DAT_c88362b8 << 8;
        ddr1_enabled = _DAT_c88362c8 >> 0xc & 3;
        _DAT_c88362c8 =
             _DAT_c88362c8 & 7 | (_DAT_c88362c8 & 7) << 3 | ddr1_enabled << 0xc |
             ddr1_enabled << 0xe;
        _DAT_c88362c0 = _DAT_c88362c0 & 0xffff00ff | _DAT_c88362c0 << 8;
        _DAT_c8836338 = _DAT_c8836338 & 0xffff00ff | _DAT_c8836338 << 8;
        ddr1_enabled = _DAT_c8836348 >> 0xc & 3;
        _DAT_c8836348 =
             _DAT_c8836348 & 7 | (_DAT_c8836348 & 7) << 3 | ddr1_enabled << 0xc |
             ddr1_enabled << 0xe;
        _DAT_c8836340 = _DAT_c8836340 & 0xffff00ff | _DAT_c8836340 << 8;
      }
                    /* Ran (WeTek Play2 ACS) */
      if ((byte)(ddrs.ddr_channel_set - 1) < 3) {
        _DAT_c8836010 = ddrs.t_pub_pgcr2 & 0xefffffff;
      }
      if (((ddrs.ddr_2t_mode != '\0') && (ddrs.ddr_channel_set != '\x04')) &&
         ((ddrs.t_pub_dcr & 7) == 3)) {
        _DAT_c883603c = 0x1f;
      }
                    /* Not ran (WeTek Play2 ACS) */
      if (((1 < acs_setting_d900a2e0.ddr_set_version) &&
          (pdVar9 = &ddrs, ddrs.wr_adj_per[0] != '\0')) && (ddrs.rd_adj_per[0] != '\0')) {
        serial_puts(s_DQS_corr_enabled_d9009357);
        _DAT_c883603c = _DAT_c883603c & 0xff;
        if (_DAT_c883603c == 0) {
          _DAT_c883603c = 1;
        }
        _DAT_c883603c = (_DAT_c883603c * ddrs.wr_adj_per[0]) / 100 & 0xff;
        _DAT_c8836040 = _DAT_c8836040 & 0xff;
        if (_DAT_c8836040 == 0) {
          _DAT_c8836040 = 1;
        }
        _DAT_c8836040 = (_DAT_c8836040 * ddrs.wr_adj_per[1]) / 100 & 0x1f;
        puVar4 = (uint *)&DAT_c88362bc;
        do {
          tmp_var = *puVar4;
          ddr1_enabled = tmp_var & 0xff;
          if ((tmp_var & 0xff) == 0) {
            ddr1_enabled = 1;
          }
          uVar6 = tmp_var >> 8 & 0xff;
          if ((tmp_var >> 8 & 0xff) == 0) {
            uVar6 = 1;
          }
          tmp_var = (uVar6 * pdVar9->rd_adj_per[2]) / 100;
          puVar5 = puVar4 + 0x20;
          *puVar4 = tmp_var << 8 | tmp_var << 16 | (ddr1_enabled * pdVar9->wr_adj_per[2]) / 100;
          pdVar9 = (ddr_set_t *)&pdVar9->ddr_type;
          puVar4 = puVar5;
        } while (puVar5 != (uint *)0xc88364bc);
      }
      if (_ddr0_enabled != 0) {
        do {
        } while ((_DAT_c883904c & 1) == 0);
      }
      if (_DAT_d900a958_pctl_ddr1 != 0) {
        do {
        } while ((_DAT_c883944c & 1) == 0);
      }
      if (_ddr0_enabled != 0) {
        _DAT_c8839004 = 2;
      }
      if (_DAT_d900a958_pctl_ddr1 != 0) {
        _DAT_c8839404 = 2;
      }
      if (_ddr0_enabled != 0) {
        do {
        } while (_DAT_c8839008 != 3);
      }
      if (_DAT_d900a958_pctl_ddr1 != 0) {
        do {
        } while (_DAT_c8839408 != 3);
      }
      if (_ddr0_enabled != 0) {
        _DAT_c883808c = 0x880019d;
      }
      if (_DAT_d900a958_pctl_ddr1 != 0) {
        _DAT_c883808c = 0x880019f;
      }
                    /*                                         cfg_ddr_refi */
      _DAT_c8838090 = ddrs.ddr_clk / 0x14 | (uint)*(byte *)(_ddrt_pointer + 0x17) << 8 | 0x20100000;
      _DAT_c8836240 = _DAT_c8836240 & 0xfffffffb;
      return 0;
    }
    if ((ddrs.ddr_func >> 1 & 1) == 0) {
      _DAT_c8836004 = 0x173;
    }
    else {
      _DAT_c8836040 = 0x10;
      _DAT_c883604c = 0x1010;
      _DAT_c8836050 = 0x1010;
      _DAT_c8836054 = 0x1010;
      _DAT_c8836058 = 0x4040404;
      _DAT_c883605c = 0x4040404;
      _DAT_c8836060 = 0x4040404;
      _DAT_c88360ec = 0x141002;
      _DAT_c88360f0 = 0x23aaaa;
      _DAT_c8836004 = 0x177;
    }
    do {
      udelay(2);
    } while ((_DAT_c8836018 & 1) == 0);
    if (ddrs.t_lpddr3_wl == '\0') {
      _DAT_c8836004 = 0xf001;
      do {
        udelay(2);
      } while ((_DAT_c8836018 & 1) == 0);
    }
    else {
      _DAT_c8836004 = 0xfa03;
      do {
        udelay(2);
      } while ((_DAT_c8836018 & 1) == 0);
    }
    ddr1_enabled = ddrs.ddr_func & 2;
    bVar3 = BL21__DAT_d900a969;
    while( true ) {
      bVar3 = bVar3 + 1;
      bVar2 = (DAT_d900a968 + bVar3) - BL21__DAT_d900a969;
      if (10 < bVar3) break;
      if (100 < bVar2) {
        if (param_1 == 0) {
          DAT_d900a968 = bVar2;
          BL21__DAT_d900a969 = bVar3;
          return 0xffffffff;
        }
        DAT_d900a968 = bVar2;
        BL21__DAT_d900a969 = bVar3;
        serial_puts(s_LPDDR_init_fail__reset____d900933c);
        reset_system();
        do {
                    /* WARNING: Do nothing block with infinite loop */
        } while( true );
      }
      tmp_var = 0x1fbf;
      if (ddr1_enabled == 0) {
        tmp_var = 0xfbf;
      }
      if (_DAT_c8836018 == (tmp_var | 0xc0000000)) goto LAB_d90046f8;
      tmp_var = 0x1fbf;
      if (ddr1_enabled == 0) {
        tmp_var = 0xfbf;
      }
      if (_DAT_c8836018 == (tmp_var | 0xe0000000)) goto LAB_d90046f8;
      tmp_var = 0x1fbf;
      if (ddr1_enabled == 0) {
        tmp_var = 0xfbf;
      }
      if (_DAT_c8836018 == (tmp_var | 0xa0000000)) goto LAB_d90046f8;
      tmp_var = 0x1fbf;
      if (ddr1_enabled == 0) {
        tmp_var = 0xfbf;
      }
      if (_DAT_c8836018 == (tmp_var | 0x80000000)) goto LAB_d90046f8;
    }
    DAT_d900a968 = bVar2;
    if (param_1 != 0) {
      suffix = &DAT_d9009336;
      BL21__DAT_d900a969 = bVar3;
LAB_d9004ddc:
      serial_print_new(s_only_wl_Error_PGSR0__0x_d900931e + 0xd,(ulong)_DAT_c8836018,0,suffix);
      serial_print_new(s_DX0GSR2__0x_d90092ee,(ulong)_DAT_c8836298,0,suffix);
      serial_print_new(s_DX1GSR2__0x_d90092fa,(ulong)_DAT_c8836318,0,suffix);
      serial_print_new(s_DX2GSR2__0x_d9009306,(ulong)_DAT_c8836398,0,suffix);
      serial_print_new(s_DX3GSR2__0x_d9009312,(ulong)_DAT_c8836418,0,suffix);
    }
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_pre_init(void)

{
  ushort uVar1;
  ulong loop;
  uchar uVar2;
  uint ddr_type_1__notsure;
  int ddr_dual_rank_sel;
  ushort uVar3;
  int iVar4;
  uint ddr_type;
  ddr_timing_t *pointer_to_pointer_to_ddrt_notsure;
  byte some_ddrt_setting_member;
  uint tmp_val;
  
                    /* Wild guesses which were not relevant in the end:
                       > * it tries to fetch DDR clocks from 0xD900A00A - <some number>
                       > * it tries to fetch DDR timings from 0xD900A148 onwards (?)
                       > Some of those addresses may be pointing to acs.bin data. Not sure.
                       
                       0xD900A00A points to __ddr_setting.ddr_clk, and 0xD900A007 points 
                       to __ddr_setting.ddr_timing_ind, which equals to 0 by default.
                       
                       Ghidra sees chars, because ddr_timing_ind is an unsigned char. I've 
                       set those to be hexadecimal ints, but yeah...Amlogic keeps using 
                       unsigned chars instead of uint8_t everytime...
                       
                       Also 0xD900A007 can be equal to any of the CONFIG_DDR_TIMMING_* 
                       constants defined in arch/arm/include/asm/arch-gxb/ddr_define.h. */
  if ((ushort)(ddrs.ddr_clk - 200) < 0x14d) {
                    /* #define CONFIG_DDR_TIMMING_DDR3_7   0x07 */
    ddrs.ddr_timing_ind = 0x7;
  }
  else if ((ushort)(ddrs.ddr_clk - 0x215) < 0x86) {
                    /* #define CONFIG_DDR_TIMMING_DDR3_9   0x09 */
    ddrs.ddr_timing_ind = 0x9;
  }
  else if ((ushort)(ddrs.ddr_clk - 0x29b) < 0x85) {
                    /* #define CONFIG_DDR_TIMMING_DDR3_11  0x0B */
    ddrs.ddr_timing_ind = 0xb;
  }
  else if ((ushort)(ddrs.ddr_clk - 800) < 0x85) {
                    /* #define CONFIG_DDR_TIMMING_DDR3_13  0x0D
                       
                       This one gets ran on WeTek Play2. */
    ddrs.ddr_timing_ind = 0xd;
  }
  else if ((ushort)(ddrs.ddr_clk - 0x3a5) < 0x42b) {
                    /* #define CONFIG_DDR_TIMMING_DDR3_14  0x0E */
    ddrs.ddr_timing_ind = 0xe;
  }
  else {
    serial_puts(s_DDR_clk_setting_error__Reset____d9009369);
    reset_system();
  }
  if (2 < acs_setting_d900a2e0.ddr_set_version) {
                    /* Never ran (WeTek Play2 ACS) */
    if (ddrs.ddr_type == '\x01') {
      ddrs.ddr_timing_ind = 0x21;
    }
    else if (ddrs.ddr_type == '\x02') {
      ddrs.ddr_timing_ind = 0x31;
    }
  }
  loop = 0;
  do {
    if (ddrt[loop].identifier == ddrs.ddr_timing_ind) {
                    /* arch/arm/cpu/armv8/common/firmware/plat/gxb/ddr/ddr.c 
                       (before "BL2 Closed-source" commit):
                           p_ddr_timing = &__ddr_timming <open bracket> loop <closing bracket>
                       Sorry my keyboard is borked in X
                       
                       But yeah index=3 I guess. */
      _ddrt_pointer = ddrt + (loop & 0xffffffff);
      break;
    }
    loop = loop + 1;
  } while (loop != 7);
  if (_ddrt_pointer == (ddr_timing_t *)0x0) {
    serial_puts(s_DDR_Timing_err____d900938a);
    reset_system();
  }
  pointer_to_pointer_to_ddrt_notsure = _ddrt_pointer;
                    /* Never ran (WeTek Play2 ACS) */
  if ((2 < acs_setting_d900a2e0.ddr_set_version) && (ddrs.ddr_type == '\x02')) {
    ddrs.t_pub_dsgcr = 0x2064da;
    ddrs.t_pub_dxccr = 0x181ee4;
    ddrs.t_pub_dcr = 0x89;
    ddrs.t_pub_zq0pr = 0x4991d;
    ddrs.t_pub_zq1pr = 0x49919;
    ddrs.t_pub_zq2pr = 0x49919;
    ddrs.t_pub_zq3pr = 0x49919;
    ddrs.t_pctl0_mcfg = 0x6a2fe1;
    ddrs.t_pctl0_mcfg1 = 0x80200040;
    if ((ushort)(ddrs.ddr_clk - 200) < 200) {
      _ddrt_pointer->cfg_ddr_cl = '\x06';
      uVar2 = '\x03';
    }
    else if ((ushort)(ddrs.ddr_clk - 400) < 0x85) {
      _ddrt_pointer->cfg_ddr_cl = '\b';
      uVar2 = '\x04';
    }
    else if ((ushort)(ddrs.ddr_clk - 0x215) < 0x43) {
      _ddrt_pointer->cfg_ddr_cl = '\t';
      uVar2 = '\x05';
    }
    else {
      if ((ushort)(ddrs.ddr_clk - 600) < 0x43) {
        uVar2 = '\n';
      }
      else if ((ushort)(ddrs.ddr_clk - 0x29b) < 0x42) {
        uVar2 = '\v';
      }
      else {
        if (0x42 < (ushort)(ddrs.ddr_clk - 0x2dd)) {
          if ((ushort)(ddrs.ddr_clk - 800) < 0x85) {
            uVar2 = '\x0e';
          }
          else {
            if (0x42a < (ushort)(ddrs.ddr_clk - 0x3a5)) goto LAB_d90054b0;
            uVar2 = '\x10';
          }
          _ddrt_pointer->cfg_ddr_cl = uVar2;
          uVar2 = '\b';
          goto LAB_d90054ac;
        }
        uVar2 = '\f';
      }
      _ddrt_pointer->cfg_ddr_cl = uVar2;
      uVar2 = '\x06';
    }
LAB_d90054ac:
    pointer_to_pointer_to_ddrt_notsure->cfg_ddr_cwl = uVar2;
  }
LAB_d90054b0:
  ddrs.t_pctl0_ppcfg = 0x1e0;
  ddrs.t_pctl0_dfiodtcfg = 8;
  if (ddrs.ddr_channel_set == 0x1) {
    ddrs.t_pctl0_dfiodtcfg = 0x808;
    ddrs.t_pub_dtcr = 0x4100308f;
    tmp_val = 0x40;
LAB_d9005594:
                    /* Gets ran (WeTek Play2 ACS) */
    ddr_dual_rank_sel = 0;
  }
  else {
    if (ddrs.ddr_channel_set != 0x2) {
                    /* ddrs.ddr_channel_set == 0x3 (WeTek Play2 ACS) */
      if (ddrs.ddr_channel_set == 0x3) {
        ddrs.t_pctl0_dfiodtcfg = 0xc0c;
        ddrs.t_pub_dtcr = 0x4300308f;
        tmp_val = 0x200040;
      }
      else if (ddrs.ddr_channel_set == 0x5) {
        ddrs.t_pctl0_ppcfg = 0x1fd;
        ddrs.t_pub_dtcr = 0x4100308f;
        tmp_val = 0x30040;
      }
      else {
        if (ddrs.ddr_channel_set == 0x4) {
          ddrs.t_pctl0_ppcfg = 0x1fd;
          ddrs.ddr_2t_mode = '\x01';
          tmp_val = 0x130500;
          goto LAB_d90055a0;
        }
        if (ddrs.ddr_channel_set != 0x6) {
          tmp_val = 0;
          ddr_dual_rank_sel = 0;
          goto LAB_d90055b0;
        }
        ddrs.t_pctl0_ppcfg = 0x1fd;
        ddrs.t_pctl0_dfiodtcfg = 0xc0c;
        ddrs.t_pub_dtcr = 0x4300308f;
        tmp_val = 0x230040;
      }
      goto LAB_d9005594;
    }
    tmp_val = 0x400040;
LAB_d90055a0:
    ddr_dual_rank_sel = 1;
  }
LAB_d90055b0:
                    /* ddr_dmc_ctrl = 0x200040 | 0x2D */
  ddrs.ddr_dmc_ctrl = tmp_val | 0x2d;
  ddrs.t_pub_pgcr2 = ddrs.t_pub_pgcr2 & 0xefffffff;
  tmp_val = ddrs.t_pub_pgcr2 | ddr_dual_rank_sel << 28;
                    /* Never gets run (WeTek Play2 ACS) */
  ddr_type_1__notsure = (uint)ddrs.ddr_type;
  ddr_type = (uint)ddrs.ddr_type;
  if (ddrs.ddr_type == '\x02') {
    tmp_val = _ddrt_pointer->cfg_ddr_cwl + 1 + (uint)_ddrt_pointer->cfg_ddr_al;
    iVar4 = 4;
    ddr_dual_rank_sel = 3;
    if ((tmp_val & 1) == 0) {
      ddr_dual_rank_sel = iVar4;
    }
    ddrs.t_pctl0_dfitphywrlta = 0;
    if (ddr_type_1__notsure != 0) {
      ddrs.t_pctl0_dfitphywrlta =
           (ushort)((int)(tmp_val - ddr_dual_rank_sel) / (int)ddr_type_1__notsure);
    }
    uVar3 = _ddrt_pointer->cfg_ddr_tdqsck;
    tmp_val = (uint)_ddrt_pointer->cfg_ddr_cl + (uint)uVar3 + (uint)_ddrt_pointer->cfg_ddr_al;
    if ((tmp_val & 1) != 0) {
      iVar4 = 3;
    }
    ddrs.t_pctl0_dfitrddataen = 0;
    if (ddr_type_1__notsure != 0) {
      ddrs.t_pctl0_dfitrddataen = (ushort)((int)(tmp_val - iVar4) / (int)ddr_type);
    }
    if (((uint)_ddrt_pointer->cfg_ddr_cl + (uint)uVar3 + (uint)_ddrt_pointer->cfg_ddr_al & 1) == 0)
    {
      ddr_dual_rank_sel = _ddrt_pointer->cfg_ddr_tdqsckmax + 0x11;
    }
    else {
      ddr_dual_rank_sel = _ddrt_pointer->cfg_ddr_tdqsckmax + 0xf;
    }
    ddrs.t_pctl0_dfitphyrdlat = 0;
    if (ddr_type != 0) {
      ddrs.t_pctl0_dfitphyrdlat = (ushort)((int)(ddr_dual_rank_sel - (uint)uVar3) / (int)ddr_type);
    }
LAB_d90056f8:
    ddrs.t_pctl0_rsth_us = 0;
    ddrs.ddr_2t_mode = 0;
  }
  else {
                    /* ddrs.ddr_type == CONFIG_DDR_TYPE_DDR3 == 0 (WeTek Play2 ACS) */
    ddrs.t_pub_pgcr2 = tmp_val;
    if (ddrs.ddr_type == 0) {
                    /* (x & 1) == (x % 2) */
      tmp_val = (uint)_ddrt_pointer->cfg_ddr_cwl + (uint)_ddrt_pointer->cfg_ddr_al;
      iVar4 = 4;
      ddr_dual_rank_sel = iVar4;
                    /* Gets run (WeTek Play2 ACS) */
      if ((tmp_val & 1) != 0) {
        ddr_dual_rank_sel = 3;
      }
                    /* ((ddrt_pointer->cfg_ddr_cwl + ddrt_pointer->cfg_ddr_al) - ddr_dual_rank_sel)
                       / 2 */
      ddrs.t_pctl0_dfitphywrlta = (ushort)((int)(tmp_val - ddr_dual_rank_sel) / 2);
      tmp_val = (uint)_ddrt_pointer->cfg_ddr_cl + (uint)_ddrt_pointer->cfg_ddr_al;
      if ((tmp_val & 1) != 0) {
        iVar4 = 3;
      }
      ddrs.t_pctl0_dfitrddataen = (ushort)((int)(tmp_val - iVar4) / 2);
      if ((_ddrt_pointer->cfg_ddr_cl + _ddrt_pointer->cfg_ddr_al & 1) != 0) {
        ddrs.t_pctl0_dfitphyrdlat = 0xe;
      }
    }
    else if (ddr_type_1__notsure == 1) {
      tmp_val = _ddrt_pointer->cfg_ddr_cwl + 1 + (uint)_ddrt_pointer->cfg_ddr_al;
      iVar4 = 4;
      ddr_dual_rank_sel = iVar4;
      if ((tmp_val & 1) != 0) {
        ddr_dual_rank_sel = 3;
      }
      ddrs.t_pctl0_dfitphywrlta = (ushort)((int)(tmp_val - ddr_dual_rank_sel) / 2);
      uVar3 = _ddrt_pointer->cfg_ddr_tdqsck;
      tmp_val = (uint)_ddrt_pointer->cfg_ddr_cl + (uint)uVar3 + (uint)_ddrt_pointer->cfg_ddr_al;
      if ((tmp_val & 1) != 0) {
        iVar4 = 3;
      }
      ddrs.t_pctl0_dfitrddataen = (ushort)((int)(tmp_val - iVar4) / 2);
      if (((uint)_ddrt_pointer->cfg_ddr_cl + (uint)uVar3 + (uint)_ddrt_pointer->cfg_ddr_al & 1) == 0
         ) {
        ddr_dual_rank_sel = _ddrt_pointer->cfg_ddr_tdqsckmax + 17;
      }
      else {
        ddr_dual_rank_sel = _ddrt_pointer->cfg_ddr_tdqsckmax + 15;
      }
      ddrs.t_pctl0_dfitphyrdlat = (ushort)((int)(ddr_dual_rank_sel - (uint)uVar3) / 2);
      goto LAB_d90056f8;
    }
  }
                    /* 0xD - 4 = 0x9 */
  tmp_val = _ddrt_pointer->cfg_ddr_cl - 4;
  some_ddrt_setting_member = _ddrt_pointer->cfg_ddr_wr;
  if (some_ddrt_setting_member < 9) {
    ddr_type_1__notsure = some_ddrt_setting_member - 4;
  }
  else {
    ddr_type_1__notsure = (uint)(some_ddrt_setting_member >> 1);
  }
                    /* ddrs.t_pub_mr[0] = (((0x10 >> 1) & 7) << 9) | ((0x9 & 8) >> 1) | 
                                          ((0x9 & 7) << 4) | 0x1C00
                                        = 0x1C14 */
  ddrs.t_pub_mr[0] =
       (ushort)((ddr_type_1__notsure & 7) << 9) |
       (ushort)((int)(tmp_val & 8) >> 1) | (ushort)((tmp_val & 7) << 4) | 0x1c00;
  if (ddr_type == 0) {
    uVar3 = 0;
    if (_ddrt_pointer->cfg_ddr_al != 0) {
                    /* Never gets ran (WeTek Play2 ACS) */
      uVar3 = (ushort)(((uint)_ddrt_pointer->cfg_ddr_cl - (uint)_ddrt_pointer->cfg_ddr_al & 3) << 3)
      ;
    }
                    /* ddrs.t_pub_mr[1] = 0 << 1 | 0x80 | ((2 & 1) << 2) | ((0x2 >> 1 & 1) << 6) | 
                                          ((0x2 >> 2 & 1) << 9) | 0
                                        = 0xC0 */
    ddrs.t_pub_mr[1] =
         (ushort)ddrs.ddr_drv << 1 | 0x80 | (ushort)((ddrs.ddr_odt & 1) << 2) |
         (ushort)((ddrs.ddr_odt >> 1 & 1) << 6) | (ushort)((ddrs.ddr_odt >> 2 & 1) << 9) | uVar3;
                    /* ddrs.t_pub_mr[2] = ((0x9 - 5 & 7) << 3) | 0x40
                                        = 0x60 */
    ddrs.t_pub_mr[3] = 0;
    ddrs.t_pub_mr[2] = (ushort)((_ddrt_pointer->cfg_ddr_cwl - 5 & 7) << 3) | 0x40;
  }
  else {
    if (ddr_type == 1) {
      uVar1 = (_ddrt_pointer->cfg_ddr_wr - 2) * 0x20;
      ddrs.t_pub_mr[2] = _ddrt_pointer->cfg_ddr_cl - 2;
    }
    else {
      if (ddr_type != 2) goto LAB_d9005844;
      some_ddrt_setting_member = _ddrt_pointer->cfg_ddr_wr;
      if (some_ddrt_setting_member < 10) {
        tmp_val = some_ddrt_setting_member - 2;
      }
      else {
        tmp_val = some_ddrt_setting_member - 10;
      }
      uVar1 = (ushort)((tmp_val & 7) << 5);
      uVar3 = 0;
      if (9 < _ddrt_pointer->cfg_ddr_wr) {
        uVar3 = 0x10;
      }
      ddrs.t_pub_mr[2] = uVar3 | _ddrt_pointer->cfg_ddr_cl - 2 | 0x80;
    }
    ddrs.t_pub_mr[1] = uVar1 | 3;
    ddrs.t_pub_mr[3] = (ushort)ddrs.ddr_drv;
  }
LAB_d9005844:
                    /* ddrs.t_pub_dtpr[0] = 0xD << 8 | 0x7 << 4 | 0x7 | 0x25 << 0x10 | 0x7 << 0x16 |
                                            0xD << 0x1A
                                          = 0x35E50D77 */
  ddrs.t_pub_dtpr[0] =
       (uint)_ddrt_pointer->cfg_ddr_rp << 8 | (uint)_ddrt_pointer->cfg_ddr_wtr << 4 |
       (uint)_ddrt_pointer->cfg_ddr_rtp | (uint)_ddrt_pointer->cfg_ddr_ras << 0x10 |
       (uint)_ddrt_pointer->cfg_ddr_rrd << 0x16 | (uint)_ddrt_pointer->cfg_ddr_rcd << 0x1a;
                    /* ddrs.t_pub_dtpr[1] = 0x4 << 2 | 0x21 << 5 | 0x118 << 0xB | 0x28 << 0x14 | 
                                            0x7 << 0x1A
                                          = 0x1E88C430 */
  ddrs.t_pub_dtpr[1] =
       (uint)_ddrt_pointer->cfg_ddr_mod << 2 | (uint)_ddrt_pointer->cfg_ddr_faw << 5 |
       (uint)_ddrt_pointer->cfg_ddr_rfc << 11 | (uint)_ddrt_pointer->cfg_ddr_wlmrd << 20 |
       (uint)_ddrt_pointer->cfg_ddr_wlo << 26;
  if (ddr_type == 2) {
                    /* Not run (WeTek Play2 ACS) */
    ddrs.t_pub_dtpr[1] = ddrs.t_pub_dtpr[1] | (_ddrt_pointer->cfg_ddr_taond_aofd - 2) * 0x40000000;
  }
  ddrs.t_pub_dtpr[2] =
       (uint)_ddrt_pointer->cfg_ddr_dllk << 19 | (uint)_ddrt_pointer->cfg_ddr_xp << 10 |
       (uint)_ddrt_pointer->cfg_ddr_xs;
  ddrs.t_pub_dtpr[3] =
       (uint)_ddrt_pointer->cfg_ddr_rc << 6 | (uint)_ddrt_pointer->cfg_ddr_cke << 13 |
       (uint)_ddrt_pointer->cfg_ddr_mrd << 18;
  if ((ddr_type - 1 & 0xff) < 2) {
    ddrs.t_pub_dtpr[3] =
         (uint)_ddrt_pointer->cfg_ddr_tdqsck | (_ddrt_pointer->cfg_ddr_tdqsck + 2) * 8 |
         ddrs.t_pub_dtpr[3];
  }
  some_ddrt_setting_member = _ddrt_pointer->cfg_ddr_rrd;
  tmp_val = 0;
  if (some_ddrt_setting_member != 0) {
    tmp_val = (int)((uint)_ddrt_pointer->cfg_ddr_faw + (uint)some_ddrt_setting_member + -1) /
              (int)(uint)some_ddrt_setting_member;
  }
  ddrs.t_pctl0_mcfg = (tmp_val & 3) << 0x12 | ddrs.t_pctl0_mcfg & 0xfff3ffff;
  some_ddrt_setting_member = _ddrt_pointer->cfg_ddr_rrd;
  tmp_val = 0;
  if (some_ddrt_setting_member != 0) {
    tmp_val = (uint)_ddrt_pointer->cfg_ddr_faw / (uint)some_ddrt_setting_member;
  }
  ddr_type_1__notsure = 0;
  tmp_val = (uint)_ddrt_pointer->cfg_ddr_faw - tmp_val * some_ddrt_setting_member & 0xff;
  if (tmp_val != 0) {
    ddr_type_1__notsure = (some_ddrt_setting_member - tmp_val & 7) << 8;
  }
                    /* ddrs.t_pctl0_mcfg1 = ((_ddrt_pointer->cfg_ddr_rrd - ddr_chl_set & 7) << 8) |
                       ddrs.t_pctl0_mcfg1 & 0xfffffcff
                                          = ((0x7 - (0x21 / 0x7) & 7) << 8) | 0x80200000 &
                       0xFFFFFCFF */
  ddrs.t_pctl0_mcfg1 = ddr_type_1__notsure | ddrs.t_pctl0_mcfg1 & 0xfffffcff;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void ddr_test(void)

{
  undefined8 ret;
  ulong ret_;
  
  _DAT_c1300000 = 1;
  InstructionSynchronizationBarrier();
  DataMemoryBarrier(3,3);
                    /* Both memTestDataBus, memTestAddressBus, and memTestDevice are derived from a
                       public-domain memtest software. */
  ret = memTestDataBus((uint *)(ulong)ddrs.ddr_base_addr);
  if ((int)ret == 0) {
    serial_puts(s_DataBus_test_pass__d90093b5);
  }
  else {
    serial_puts(s_DataBus_test_failed____d900939d);
    reset_system();
  }
  ret_ = memTestAddressBus((uint *)(ulong)ddrs.ddr_base_addr,(ulong)((uint)ddrs.ddr_size << 0x14));
  if ((int)ret_ == 0) {
    serial_puts(s_AddrBus_test_pass__d90093e1);
  }
  else {
    serial_puts(s_AddrBus_test_failed____d90093c9);
    reset_system();
  }
  if (ddrs.ddr_full_test != 0) {
    watchdog_disable();
    ret_ = memTestDevice((uint *)(ulong)ddrs.ddr_base_addr,(ulong)((uint)ddrs.ddr_size << 0x14));
    if ((int)ret_ == 0) {
      serial_puts(s_Device_test_pass__d900940c);
    }
    else {
      serial_puts(s_Device_test_failed____d90093f5);
      reset_system();
    }
  }
  _DAT_c1300000 = 0;
  InstructionSynchronizationBarrier();
  DataMemoryBarrier(3,3);
  return;
}



undefined8 ddr_init(int param_1)

{
  bool bVar1;
  uchar uVar2;
  char *pcVar3;
  undefined8 ret;
  bool chl_auto;
  
                    /* About the function name: It's a guess. It is possible that they derived this
                       far from the original one found in their old sources, but it's also possible
                       that the name is actually incorrect, but *as far as I can tell* that's the
                       correct name. */
  ddr_init_pll();
  chl_auto = false;
                    /* arch/arm/include/asm/arch-gxb/ddr_define.h:#define CONFIG_DDR_CHL_AUTO 0xF
                       So: ddrs.ddr_channel_set == CONFIG_DDR_CHL_AUTO */
  if ((ddrs.ddr_channel_set == '\x0f') && (plls.pxp == 0)) {
    ddrs.ddr_channel_set = '\x03';
    chl_auto = true;
  }
  do {
                    /* Address 0xD900A968 doesn't exist...weird */
    DAT_d900a968 = 0;
                    /* The instructions below mean: 
                           pbVal3 => pointer to 0xD900941F + 2, so 0xD9009421
                                                ^^^^^^^^^^         ^^^^^^^^^^
                                                "LPDDR chl: "      "DDR chl: "
                       They redefine it to be a pointer to 0xD900941F later on if <contents of 
                       0xD900A001> - 1U is less than 2. 0xD900A000 is where 
                       the ddrs_ structure begins, so it must be a pointer to
                       __ddr_setting.ddr_type,
                       which can be either one of these (grepped in Amlogic U-Boot tree):
                       
                       arch/arm/include/asm/arch-gxb/ddr_define.h:#define CONFIG_DDR_TYPE_DDR3   0
                       arch/arm/include/asm/arch-gxb/ddr_define.h:#define CONFIG_DDR_TYPE_LPDDR2 1
                       arch/arm/include/asm/arch-gxb/ddr_define.h:#define CONFIG_DDR_TYPE_LPDDR3 2
                       
                       If it's equal to CONFIG_DDR_TYPE_DDR3 then `<contents of 0xD900A000> - 1U` 
                       will be equal to 255, which is < 2, while if it's equal to 
                       CONFIG_DDR_TYPE_LPDDR* it won't exceed 2. */
    pcVar3 = s_LPDDR_chl__d900941f + 2;
    if ((byte)(ddrs.ddr_type - 1) < 2) {
      pcVar3 = s_LPDDR_chl__d900941f;
    }
    serial_puts(pcVar3);
                    /* This serial_puts call below basically does this:
                           if      (contents of d900a000) - 1 == 0: serial_puts("Rank0 only")
                           <...>
                           else if (contents of d900a000) - 1 == 3: serial_puts("Rank0+1 diff")
                       0xD900A000 points to __ddr_setting.ddr_channel_set (acs), so it can be 
                       equal to:
                       
                       #define CONFIG_DDR0_RANK0_ONLY              1
                       #define CONFIG_DDR0_RANK01_SAME             2
                       #define CONFIG_DDR0_RANK01_DIFF             3
                       #define CONFIG_DDR01_SHARE_AC               4
                       #define CONFIG_DDR0_ONLY_16BIT              5
                       #define CONFIG_DDR0_RANK01_DIFF_16BIT       6
                       
                       linux-amlogic -- libera.chat:
                       * f_ looks at Amlogic staff.
                       <f_> Nice obfuscation attempt. Too bad I figured out how it worked :P */
    serial_puts(s_Rank0_only_d9009240 + (long)(int)(ddrs.ddr_channel_set - 1) * 0xd);
    ddr_pre_init();
    serial_print_new(s____d900942b,(ulong)ddrs.ddr_clk,1,s_MHz_d900942f);
                    /* bVar3 might be a switch to enable LPDDR init? Here it's set to false (==0) in
                       line 16
                       
                       EDIT: Maybe not... */
    if (!chl_auto) {
      serial_puts(&CHAR_NL);
      ddr_init_pctl(1);
      break;
    }
    ret = ddr_init_pctl(0);
    if ((int)ret == 0) {
      serial_puts(s____PASS_d9009433);
      break;
    }
    serial_puts(s____FAIL_d900943c);
    uVar2 = ddrs.ddr_channel_set + 0xff;
    bVar1 = ddrs.ddr_channel_set != '\0';
    ddrs.ddr_channel_set = uVar2;
  } while (bVar1);
  ddr_init_dmc();
  ddr_print_info();
  if ((plls.pxp == 0) && (param_1 != 0)) {
    ddr_test();
  }
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void pinmux_init(void)

{
  char *message;
  
  _DAT_c883444c = _DAT_c883444c | 0x3f00000;
  _DAT_c8834448 = _DAT_c8834448 | 0x3f00000;
  if ((_DAT_c8834450 >> 0x18 & 1) == 0) {
    _DAT_c8100014 = _DAT_c8100014 & 0xffffe7ff;
    _DAT_c88344d0 = _DAT_c88344d0 | 0x600;
    message = s__sdio_debug_board_detected_d9009445;
    _DAT_c88344b8 = _DAT_c88344b8 & 0xffff8fff;
  }
  else {
    message = s__no_sdio_debug_board_detected_d9009461;
  }
  serial_puts(message);
  return;
}



void bl2_does_nothing_at_all__notsure(void)

{
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void NDMA_set_table_position_secure(uint param_1,undefined4 param_2,undefined4 param_3)

{
  long lVar1;
  uint *puVar2;
  uint uVar3;
  
                    /* (!) Not sure about the function name (!) */
  lVar1 = (ulong)param_1 * 4;
  *(undefined4 *)(lVar1 + 0xd900a980) = param_2;
  *(undefined4 *)(lVar1 + 0xd900a970) = param_2;
  *(undefined4 *)(lVar1 + 0xd900a990) = param_3;
  switch((ulong)param_1) {
  case 0:
    uVar3 = _DAT_c88321e4 | 0x1000000;
    puVar2 = (uint *)&DAT_da8321e4;
    _DAT_da832060 = param_2;
    _DAT_da832068 = param_3;
    goto LAB_d9005e64;
  case 1:
    uVar3 = _DAT_c88321e4 | 0x2000000;
    _DAT_da83206c = param_2;
    _DAT_da832074 = param_3;
    break;
  case 2:
    uVar3 = _DAT_c88321e4 | 0x4000000;
    _DAT_da832078 = param_2;
    _DAT_da832080 = param_3;
    break;
  case 3:
    uVar3 = _DAT_c88321e4 | 0x8000000;
    _DAT_da832084 = param_2;
    _DAT_da83208c = param_3;
    break;
  default:
    return;
  }
  puVar2 = (uint *)&DAT_c88321e4;
LAB_d9005e64:
  *puVar2 = uVar3;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9005e6c(int param_1)

{
  _DAT_da8321c0 = _DAT_da8321c0 | 0x4000;
  _DAT_da8321e4 = 1 << (ulong)(param_1 + 8U & 0x1f) | _DAT_da8321e4;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9005eb4(uint param_1)

{
  if (*(int *)((ulong)param_1 * 4 + 0xd900a980) != 0) {
    do {
    } while ((0xff << (ulong)((param_1 & 3) << 3) & _DAT_da8321c8) != 0);
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9005ee8(int param_1)

{
  _DAT_c88321e4 = _DAT_c88321e4 & (1 << (ulong)(param_1 + 8U & 0x1f) ^ 0xffffffffU);
  if ((_DAT_c88321e4 & 0xf00) == 0) {
    _DAT_c88321c0 = _DAT_c88321c0 & 0xffffbfff;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9005f44(uint param_1,int param_2,int param_3,int param_4,int param_5,int param_6,
                 uint param_7,int param_8,uint param_9,undefined4 param_10,uint param_11,
                 undefined4 param_12_00,uint param_13,undefined4 param_14_00,int param_15,
                 undefined4 param_16,int param_17)

{
  int iVar1;
  long lVar2;
  int iVar3;
  uint *puVar4;
  
  lVar2 = (ulong)param_1 * 4;
  puVar4 = (uint *)(ulong)*(uint *)(lVar2 + 0xd900a970);
  *puVar4 = param_2 << 0x15 | 0x41000100;
  puVar4[1] = param_11;
  puVar4[2] = param_13;
  puVar4[3] = param_9 & 0x1ffffff;
  puVar4[4] = 0;
  iVar1 = *(int *)(lVar2 + 0xd900a990);
  iVar3 = *(int *)(lVar2 + 0xd900a970);
  puVar4[5] = 0;
  puVar4[6] = param_7 | param_8 << 4 | param_6 << 8 | param_5 << 10 | param_4 << 0xb |
              param_3 << 0xc | param_17 << 0xe | param_15 << 0x10;
  if (iVar3 == iVar1) {
    iVar3 = *(int *)(lVar2 + 0xd900a980);
  }
  else {
    iVar3 = iVar3 + 0x20;
  }
  *(int *)(lVar2 + 0xd900a970) = iVar3;
  _DAT_c88321c8 = param_1 << 8 | 1;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9005ff8(uint param_1,int param_2,int param_3,uint param_4,uint param_5,uint param_6,
                 int param_7)

{
  long lVar1;
  int iVar2;
  uint *puVar3;
  
  lVar1 = (ulong)param_1 * 4;
  puVar3 = (uint *)(ulong)*(uint *)(lVar1 + 0xd900a970);
  *puVar3 = param_2 << 0x15 | 0x41400000;
  puVar3[1] = param_6;
  puVar3[2] = 0;
  puVar3[3] = param_5 & 0x1ffffff;
  puVar3[4] = 0;
  puVar3[5] = 0;
  puVar3[6] = param_4 | param_7 << 4 | param_3 << 8;
  if (*(int *)(lVar1 + 0xd900a970) == *(int *)(lVar1 + 0xd900a990)) {
    iVar2 = *(int *)(lVar1 + 0xd900a980);
  }
  else {
    iVar2 = *(int *)(lVar1 + 0xd900a970) + 0x20;
  }
  *(int *)(lVar1 + 0xd900a970) = iVar2;
  _DAT_da8321c8 = param_1 << 8 | 1;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9006080(void)

{
  long lVar1;
  
  _DAT_da838438 = 0xd500d5;
  _DAT_da8384d8 = 0x5ff55fff;
  _DAT_da8384dc = 0x5ff55fff;
  _DAT_da8384d4 = 5;
  _DAT_da8384d0 = 0x157f15;
  _DAT_da838494 = 0xffffffff;
  _DAT_da8384cc = 0x55555555;
  _DAT_da8384c8 = 0x55555555;
  _DAT_da838448 = 0xffffffff;
  _DAT_da838444 = 0x55555555;
  _DAT_da838440 = 0x55555555;
  _DAT_da83844c = 0xffffffff;
  _DAT_da838450 = 0xffffffff;
  _DAT_da838454 = 0x55555555;
  _DAT_da838458 = 0x55555555;
  if ((_DAT_d900a740 != 0) || (lVar1 = _DAT_d900a748, _DAT_d900a748 != 0)) {
    _DAT_da838404 =
         ((int)_DAT_d900a740 + (int)_DAT_d900a748) - 1U & 0xffff0000 |
         (uint)((ulong)_DAT_d900a740 >> 0x10) & 0xffff;
    _DAT_da83841c = _DAT_da83841c | 1;
    lVar1 = 1;
  }
  if (((_DAT_d900a750 == 0) && (_DAT_d900a758 == 0)) && ((int)lVar1 == 0)) {
    return;
  }
  do {
  } while( true );
}



void FUN_d9006248(undefined8 *param_1,ulong param_2)

{
  long lVar1;
  undefined8 uVar2;
  
  lVar1 = (param_2 & 0xffffffff) * 0x10;
  uVar2 = param_1[1];
  *(undefined8 *)(&DAT_d900a740 + lVar1) = *param_1;
  *(undefined8 *)(&DAT_d900a748 + lVar1) = uVar2;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void parse_fip(long param_1)

{
  int iVar1;
  ulong uVar2;
  int *piVar3;
  undefined4 *puVar4;
  int *piVar5;
  undefined8 local_10;
  undefined8 local_8;
  
  piVar3 = (int *)(param_1 + 0x430);
  piVar5 = (int *)(param_1 + 0x10);
  if (*(int *)(param_1 + 0x400) == L'\x87654321') {
    serial_puts(s_New_fip_structure__d9009484);
  }
  do {
    if (*(long *)(piVar5 + 6) == 0) {
      return;
    }
    iVar1 = *piVar5;
    for (puVar4 = &DAT_d900a320; puVar4[1] != 0; puVar4 = puVar4 + 10) {
      if (iVar1 == puVar4[1]) {
        *puVar4 = 1;
        *(long *)(puVar4 + 2) = *(long *)(piVar5 + 4) + 0xc000;
        *(undefined8 *)(puVar4 + 4) = *(undefined8 *)(piVar5 + 6);
        if (*piVar3 == 0x12348765) {
          *(undefined8 *)(puVar4 + 6) = *(undefined8 *)(piVar3 + 2);
          local_10 = *(undefined8 *)(piVar3 + 8);
          local_8 = *(undefined8 *)(piVar3 + 10);
          if (iVar1 == 0x6d08d447) {
            _DAT_da100254 = (undefined4)*(undefined8 *)(piVar3 + 4);
            _DAT_da10024c = _DAT_da10024c & 0xffff | (int)(*(ulong *)(piVar3 + 6) >> 10) << 0x10;
            uVar2 = 0;
          }
          else {
            if (iVar1 != -0x761e2ffb) goto LAB_d90063c4;
            _DAT_da100250 = (undefined4)*(undefined8 *)(piVar3 + 4);
            _DAT_da10024c =
                 (uint)(*(ulong *)(piVar3 + 6) >> 10) & 0xffff | _DAT_da10024c & 0xffff0000;
            uVar2 = 1;
          }
          FUN_d9006248(&local_10,uVar2);
        }
      }
LAB_d90063c4:
    }
    piVar5 = piVar5 + 10;
    piVar3 = piVar3 + 0x14;
  } while( true );
}



void parse_bl30x(ulong param_1,undefined8 param_2,byte *image_str)

{
  long *sha2;
  undefined auVar1 [16];
  long local_30;
  undefined8 uStack_28;
  undefined8 local_20;
  undefined8 uStack_18;
  byte *local_10_imgstring;
  
  local_10_imgstring = image_str;
  FUN_d9008de8(param_1,param_2,image_str);
  local_30 = 0;
  uStack_28 = 0;
  local_20 = 0;
  uStack_18 = 0;
  ::sha2(param_1,(uint)param_2,(ulong)&local_30,0);
  sha2 = &local_30;
  auVar1 = send_bl30x((int)param_1,(uint)param_2,sha2,0x20,local_10_imgstring);
  FUN_d9008e04(auVar1._0_8_,auVar1._8_8_,sha2);
  return;
}



void FUN_d900644c(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  uint uVar1;
  undefined *puVar2;
  long lVar3;
  undefined8 extraout_x1;
  undefined8 uVar4;
  undefined8 uVar5;
  undefined auVar6 [16];
  
  puVar2 = FUN_d9001068();
  lVar3 = bl2_plat_get_bl31_ep_info();
  *(undefined **)(lVar3 + 0x18) = puVar2;
  *(undefined8 *)(lVar3 + 8) = param_1;
  *(uint *)(lVar3 + 4) = *(uint *)(lVar3 + 4) & 0xfffffffe;
  *(undefined4 *)(lVar3 + 0x10) = 0x2cd;
  auVar6 = watchdog_disable();
  FUN_d9008de8(auVar6._0_8_,auVar6._8_8_,param_3);
  uVar5 = 0;
  *(undefined8 *)(lVar3 + 0x18) = 1;
  FUN_d9008f94((undefined8 *)0xc0000000,lVar3);
  uVar4 = extraout_x1;
  uVar1 = get_boot_device();
  if (uVar1 == 5) {
    serial_puts(s_USB_mode__d9009498);
    FUN_d900644c(0xd9044504,uVar4,uVar5);
  }
  serial_puts(s__lock_failed__reset____d90091a0 + 0xe);
  reset_system();
  return;
}



void FUN_d90064c0(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  uint uVar1;
  
  uVar1 = get_boot_device();
  if (uVar1 == 5) {
    serial_puts(s_USB_mode__d9009498);
    FUN_d900644c(0xd9044504,param_2,param_3);
  }
  serial_puts(s__lock_failed__reset____d90091a0 + 0xe);
  reset_system();
  return;
}



ulong aml_check(int *param_1,int *param_2,undefined8 param_3,int param_4)

{
  uint uVar1;
  char *pcVar2;
  undefined8 extraout_x1;
  undefined8 uVar3;
  undefined auVar4 [16];
  
  FUN_d9008de8(param_1,param_2,param_3);
  uVar1 = aml_data_check(param_1,param_2,(uint)param_3,param_4);
  auVar4._8_8_ = param_2;
  auVar4._0_8_ = CONCAT44(0,uVar1);
  if (uVar1 != 0) {
    serial_puts(s_aml_log___SIG_CHK___d90094a3);
    serial_put_dec((long)(int)uVar1);
    serial_puts(s__for_address_0x_d90094b8);
    serial_put_hex((ulong)param_1,0x20);
    pcVar2 = &CHAR_NL;
    uVar3 = extraout_x1;
    serial_puts(&CHAR_NL);
    auVar4 = FUN_d90064c0(pcVar2,uVar3,param_3);
  }
  FUN_d9008e04(auVar4._0_8_,auVar4._8_8_,param_3);
  return CONCAT44(0,uVar1);
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void bl2_load_image(void)

{
  uint uVar1;
  uint uVar2;
  long lVar3;
  undefined8 uVar4;
  long lVar5;
  char *pcVar6;
  char *img_name;
  undefined *puVar7;
  long lVar8;
  uint uVar9;
  int *dst;
  undefined auVar10 [16];
  
  dump_ddr_data();
  FUN_d9008e20();
  storage_load(0xc000,(ulong)&DAT_01400000,0x4000,(byte *)s_fip_header_d90094c8);
  memcpy(0x1700000,(long)&DAT_01400000,0x4000);
  uVar2 = _DAT_c8100228;
  uVar9 = 1;
  if (_DAT_01400000 == L'\xaa640001') {
    uVar9 = (uint)(_DAT_01400004 != 0x12345678);
  }
  pcVar6 = (char *)0x4000;
  aml_check((int *)0x1700000,(int *)&DAT_01400000,0x4000,uVar9);
  parse_fip((long)&DAT_01400000);
  lVar8 = 0;
  FUN_d9006080();
  img_name = &s_bl30;
  puVar7 = (undefined *)0x0;
  do {
    if (*(int *)(img_name + -0x1c) == 0) {
      auVar10 = FUN_d90011f4();
      FUN_d9008de8(auVar10._0_8_,auVar10._8_8_,pcVar6);
      watchdog_disable();
      FUN_d9008f94((undefined8 *)0xc0000000,lVar8);
      _DAT_c8834528 = 0xffff87ff;
      _DAT_c88344f0 = 0xffff8700;
      _DAT_c88344c0 = 0xfff00000;
      return;
    }
    if (*(int *)(img_name + -0x20) != 0) {
      dst = (int *)0x1700000;
      if ((uVar2 & 0x10) == 0) {
        dst = *(int **)(img_name + -8);
      }
      storage_load(*(ulong *)(img_name + -0x18),(ulong)dst,*(ulong *)(img_name + -0x10),
                   (byte *)img_name);
      pcVar6 = (char *)(ulong)*(uint *)(img_name + -0x10);
      aml_check(dst,*(int **)(img_name + -8),pcVar6,uVar9);
      uVar1 = *(uint *)(img_name + -0x1c);
      if (uVar1 == 0x89e1d005) {
        lVar3 = *(long *)(puVar7 + 0x18);
        pcVar6 = *(char **)(img_name + -8);
        *(char **)(lVar3 + 8) = pcVar6;
        *(int *)(lVar3 + 0x10) = (int)*(undefined8 *)(img_name + -0x10);
        lVar5 = *(long *)(puVar7 + 0x10);
        if (lVar5 != 0) {
          *(char **)(lVar5 + 8) = pcVar6;
        }
        FUN_d9001220(lVar3,lVar5);
      }
      else if (uVar1 < 0x89e1d006) {
        if (uVar1 == 0x3dfd6697) {
LAB_d9006730:
          pcVar6 = img_name;
          parse_bl30x(*(ulong *)(img_name + -8),*(undefined8 *)(img_name + -0x10),(byte *)img_name);
        }
        else if (uVar1 == 0x6d08d447) {
          puVar7 = FUN_d9001068();
          lVar8 = bl2_plat_get_bl31_ep_info();
          *(undefined **)(lVar8 + 0x18) = puVar7;
          uVar4 = *(undefined8 *)(img_name + -8);
          lVar3 = *(long *)(puVar7 + 8);
          *(undefined8 *)(lVar3 + 8) = uVar4;
          pcVar6 = *(char **)(img_name + -0x10);
          *(int *)(lVar3 + 0x10) = (int)pcVar6;
          *(undefined8 *)(lVar8 + 8) = uVar4;
          FUN_d9001208(lVar3,lVar8);
        }
      }
      else if (uVar1 == 0xa7eed0d6) {
        lVar3 = *(long *)(puVar7 + 0x28);
        pcVar6 = *(char **)(img_name + -8);
        *(char **)(lVar3 + 8) = pcVar6;
        *(int *)(lVar3 + 0x10) = (int)*(undefined8 *)(img_name + -0x10);
        lVar5 = *(long *)(puVar7 + 0x20);
        if (lVar5 != 0) {
          *(char **)(lVar5 + 8) = pcVar6;
        }
        bl2_plat_set_bl33_ep_info(lVar3,lVar5);
      }
      else if (uVar1 == 0xaabbccdd) goto LAB_d9006730;
    }
    img_name = img_name + 0x28;
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90067c8(void)

{
  _DAT_c8834528 = 0xffff87ff;
  _DAT_c88344f0 = 0xffff8700;
  _DAT_c88344c0 = 0xfff00000;
  return;
}



/* WARNING: Removing unreachable block (ram,0xd90068e0) */
/* WARNING: Removing unreachable block (ram,0xd90068e4) */
/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 FUN_d90067fc(void)

{
  _DAT_d0074800 = 0x33800;
  return 0;
}



void FUN_d90068fc(uint param_1,undefined4 *param_2)

{
  uint uVar1;
  int iVar2;
  undefined4 uVar3;
  ulong uVar4;
  uint uVar5;
  ulong uVar6;
  long lVar7;
  byte local_20 [4];
  undefined local_1c;
  undefined local_1b;
  undefined local_1a;
  undefined local_19;
  undefined local_18;
  undefined local_17;
  undefined local_16;
  undefined local_15;
  undefined local_14;
  undefined local_13;
  undefined local_12;
  undefined local_11;
  undefined local_10;
  undefined local_f;
  undefined local_e;
  undefined local_d;
  undefined local_c;
  undefined local_b;
  undefined local_a;
  undefined local_9;
  undefined local_8;
  undefined local_7;
  undefined local_6;
  undefined local_5;
  undefined local_4;
  undefined local_3;
  undefined local_2;
  undefined local_1;
  
  local_1a = 0x78;
  local_19 = 0x78;
  local_17 = 0x76;
  local_e = 0x76;
  local_b = 0x76;
  local_20[0] = 4;
  local_20[1] = 4;
  local_16 = 0x74;
  local_14 = 8;
  local_13 = 8;
  local_d = 0x74;
  local_9 = 0x70;
  local_20[2] = 0x7c;
  local_20[3] = 0x7e;
  local_1c = 0;
  local_1b = 0x7c;
  local_18 = 0x7c;
  local_15 = 0x72;
  local_12 = 0;
  local_11 = 0;
  local_10 = 0xb;
  local_f = 0x7e;
  local_c = 0x10;
  local_a = 0x72;
  local_8 = 2;
  local_7 = 0;
  local_6 = 0x7e;
  local_5 = 0x7c;
  local_4 = 0;
  local_3 = 0;
  local_2 = 0;
  local_1 = 0;
  uVar1 = param_1;
  if (param_1 == 7) {
    param_1 = 0xff;
LAB_d9006a10:
    uVar4 = 0;
  }
  else {
    if (param_1 == 0xff) {
      uVar1 = 7;
      goto LAB_d9006a10;
    }
    uVar5 = 0;
    if (7 < param_1) goto LAB_d9006b84;
    if (param_1 == 0) {
      *param_2 = 0x1785c;
      param_2[1] = 0x178c5;
      param_2[2] = 0x33800;
      uVar4 = 3;
    }
    else {
      uVar4 = 0;
    }
  }
  lVar7 = 0;
  uVar6 = uVar4;
  do {
    param_2[uVar6] = 0x17855;
    iVar2 = (int)uVar6;
    param_2[iVar2 + 1] = 0x33802;
    param_2[iVar2 + 2] = (int)lVar7 + 4U | 0x1b800;
    param_2[iVar2 + 3] = 0x33802;
    param_2[iVar2 + 4] = local_20[uVar1 * 4 + (int)lVar7] | 0x13800;
    lVar7 = lVar7 + 1;
    param_2[iVar2 + 5] = 0x33800;
    uVar6 = (ulong)(iVar2 + 6);
  } while (lVar7 != 4);
  iVar2 = (int)uVar4;
  param_2[iVar2 + 0x18] = 0x17855;
  param_2[iVar2 + 0x19] = 0x33802;
  param_2[iVar2 + 0x1a] = 0x1b80d;
  param_2[iVar2 + 0x1b] = 0x33802;
  param_2[iVar2 + 0x1c] = 0x13800;
  uVar1 = iVar2 + 0x1e;
  param_2[iVar2 + 0x1d] = 0x33800;
  if (param_1 == 6) {
    param_2[uVar1] = 0x178b3;
    uVar1 = iVar2 + 0x20;
    param_2[iVar2 + 0x1f] = 0x33800;
LAB_d9006b48:
    param_2[uVar1] = 0x17826;
    param_2[uVar1 + 1] = 0x1785d;
    uVar5 = uVar1 + 3;
    uVar3 = 0x33800;
    uVar4 = (ulong)(uVar1 + 2);
  }
  else {
    if (param_1 != 0xff) goto LAB_d9006b48;
    param_2[uVar1] = 0x178ff;
    param_2[iVar2 + 0x1f] = 0x33802;
    uVar5 = iVar2 + 0x21;
    uVar4 = (ulong)(iVar2 + 0x20);
    if (DAT_d900a9a2 < '\0') {
      param_2[uVar4] = 0x17870;
      param_2[uVar5] = 0x33802;
      uVar5 = iVar2 + 0x23;
      param_2[iVar2 + 0x22] = 0x142c0d;
      goto LAB_d9006b84;
    }
    uVar3 = 0x10380d;
  }
  param_2[uVar4] = uVar3;
LAB_d9006b84:
  param_2[uVar5] = 0;
  return;
}



void FUN_d9006bf0(uint param_1,undefined4 *param_2)

{
  undefined4 *puVar1;
  long lVar2;
  uint uVar3;
  byte abStack_40 [64];
  
  memcpy((ulong)abStack_40,(long)&DAT_d90094d8,0x3f);
  if (param_1 == 0x15) {
LAB_d9006d5c:
    *param_2 = 0x1783b;
    param_2[1] = 0x178b9;
    param_2[2] = 0x17854;
    param_2[3] = 0x1b804;
    param_2[4] = 0x13800;
    param_2[5] = 0x17854;
    param_2[6] = 0x1b805;
    param_2[7] = 0x13800;
    param_2[8] = 0x17854;
    param_2[9] = 0x1b807;
    param_2[10] = 0x13800;
    uVar3 = 0x78d6;
  }
  else {
    if (param_1 == 0) {
      *param_2 = 0x1783b;
      param_2[1] = 0x178b9;
      uVar3 = 4;
      puVar1 = param_2 + 2;
      do {
        *puVar1 = 0x17854;
        puVar1[1] = uVar3 | 0x1b800;
        uVar3 = uVar3 + 1;
        puVar1[2] = 0x13800;
        puVar1 = puVar1 + 3;
      } while (uVar3 != 0xd);
      param_2[0x1d] = 0x178b6;
      lVar2 = 0x1e;
      goto LAB_d9006df0;
    }
    if (0x14 < param_1) {
      lVar2 = 0;
      if (param_1 != 0xff) goto LAB_d9006df0;
      goto LAB_d9006d5c;
    }
    *param_2 = 0x1783b;
    param_2[1] = 0x178b9;
    param_2[2] = 0x17854;
    uVar3 = param_1 * 3;
    param_2[3] = 0x1b804;
    param_2[4] = abStack_40[uVar3] | 0x13800;
    param_2[5] = 0x17854;
    param_2[6] = 0x1b805;
    param_2[7] = abStack_40[uVar3 + 1] | 0x13800;
    param_2[8] = 0x17854;
    param_2[9] = 0x1b807;
    param_2[10] = abStack_40[uVar3 + 2] | 0x13800;
    uVar3 = 0x78b6;
  }
  param_2[0xb] = uVar3 | 0x10000;
  lVar2 = 0xc;
LAB_d9006df0:
  param_2[lVar2] = 0;
  return;
}



void FUN_d9006e00(uint param_1,undefined4 *param_2)

{
  long lVar1;
  
  if (param_1 == 0xff) {
    param_1 = 0;
  }
  else {
    lVar1 = 0;
    if (7 < param_1) goto LAB_d9006ec8;
  }
  *param_2 = 0x178ef;
  param_2[1] = 0x1b889;
  param_2[2] = 0x33803;
  param_2[3] = param_1 | 0x13800;
  param_2[4] = 0x13800;
  param_2[5] = 0x13800;
  param_2[6] = 0x13800;
  param_2[7] = 0x33802;
  if (DAT_d900a9a2 < '\0') {
    param_2[8] = 0x17870;
    param_2[9] = 0x33802;
    param_2[10] = 0x142c0d;
    lVar1 = 0xb;
  }
  else {
    param_2[8] = 0x10380d;
    lVar1 = 9;
  }
LAB_d9006ec8:
  param_2[lVar1] = 0;
  return;
}



void FUN_d9006ed0(uint param_1,undefined4 *param_2)

{
  long lVar1;
  undefined4 *puVar2;
  byte local_50 [64];
  byte local_10 [16];
  
  local_10[0] = 0xa7;
  local_10[1] = 0xa4;
  local_10[2] = 0xa5;
  local_10[3] = 0xa6;
  memcpy((ulong)local_50,(long)&DAT_d9009518,0x3c);
  if (param_1 == 0xff) {
    param_1 = 0;
  }
  else {
    lVar1 = 0;
    if (0xe < param_1) goto LAB_d9006fbc;
  }
  lVar1 = 0;
  puVar2 = param_2;
  do {
    *puVar2 = 0x178a1;
    puVar2[1] = 0x1b800;
    puVar2[2] = local_10[lVar1] | 0x1b800;
    puVar2[3] = 0x33802;
    puVar2[4] = local_50[param_1 * 4 + (int)lVar1] | 0x13800;
    lVar1 = lVar1 + 1;
    puVar2[5] = 0x33808;
    puVar2 = puVar2 + 6;
  } while (lVar1 != 4);
  lVar1 = 0x18;
LAB_d9006fbc:
  param_2[lVar1] = 0;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9006fcc(void)

{
  uint *puVar1;
  uint uVar2;
  long lVar3;
  uint local_200 [128];
  
  if (_DAT_d900a9a6 != 0) {
    if (_DAT_d900a9a4 == 0x45) {
      FUN_d9006bf0(_DAT_d900a95c,local_200);
    }
    else if (_DAT_d900a9a4 < 0x46) {
      if (_DAT_d900a9a4 == 0x2c) {
        FUN_d9006e00(_DAT_d900a95c,local_200);
      }
    }
    else if (_DAT_d900a9a4 == 0x98) {
      FUN_d90068fc(_DAT_d900a95c,local_200);
    }
    else if (_DAT_d900a9a4 == 0xec) {
      FUN_d9006ed0(_DAT_d900a95c,local_200);
    }
    _DAT_d900a95c = _DAT_d900a95c + 1;
    if (_DAT_d900a9a6 <= _DAT_d900a95c) {
      _DAT_d900a95c = 0;
    }
    lVar3 = 0;
    uVar2 = _DAT_d0074800;
    do {
      _DAT_d0074800 = uVar2;
      if ((_DAT_d0074800 >> 0x16 & 0x1f) == 0x1f) {
        return;
      }
      puVar1 = (uint *)((long)local_200 + lVar3);
      lVar3 = lVar3 + 4;
      uVar2 = *puVar1;
    } while (*puVar1 != 0);
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 FUN_d90070d8(void)

{
  uint uVar1;
  uint uVar2;
  long lVar3;
  uint uVar4;
  
  uVar1 = _DAT_d900a9a0 & 0x3f;
  memset(0x1800200,0,(ulong)uVar1 << 3);
  lVar3 = (ulong)uVar1 << 3;
  inv_dcache_range(0x1800200,lVar3);
  do {
  } while ((_DAT_d0074800 >> 0x16 & 0x1f) != 0);
  _DAT_d0074800 = 0x33800;
  do {
    FUN_d9008e60(0x1800200,lVar3);
  } while (*(long *)((ulong)(uVar1 - 1) * 8 + 0x1800200) == 0);
  uVar4 = 0;
  while( true ) {
    if (uVar1 <= uVar4) {
      return 0;
    }
    uVar2 = *(uint *)(ulong)((uVar4 + 0x300040) * 8);
    if ((uVar2 >> 0x18 & 0x3f) == 0x3f) break;
    if ((uVar4 == 0) && ((uVar2 & 0xc000ffff) != 0xc000aa55)) {
      return 0x83;
    }
    uVar4 = uVar4 + 1;
  }
  if ((_DAT_d900a9a0 & 0x1000000) != 0) {
    FUN_d90067fc();
  }
  if (9 < (uVar2 >> 0x10 & 0x3f)) {
    return 0x82;
  }
  return 0x85;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int FUN_d9007484(uint param_1,uint param_2,uint param_3)

{
  uint uVar1;
  byte bVar2;
  uint uVar3;
  ulong uVar4;
  int iVar5;
  int iVar6;
  uint uVar7;
  uint uVar8;
  uint uVar9;
  
  uVar4 = (ulong)_DAT_d900a9a0 & 0x3fffff;
  if (((uint)uVar4 >> 0xd & 1) == 0) {
    iVar5 = 0x400;
    if (((uint)(uVar4 >> 0xe) & 7) < 2) {
      iVar5 = 0x200;
    }
  }
  else {
    iVar5 = ((uint)(uVar4 >> 6) & 0x7f) << 3;
  }
  uVar3 = iVar5 * ((uint)uVar4 & 0x3f);
  iVar5 = 0x200;
  if (0xfff < uVar3) {
    iVar5 = 0x100;
  }
  inv_dcache_range((ulong)param_2,(ulong)param_3);
  uVar8 = 0;
  while( true ) {
    if (param_3 <= uVar8) {
      return 0;
    }
    uVar1 = param_1 + 1;
    if (_DAT_d900a764 != 0) {
      uVar9 = _DAT_d900a768 >> 1;
      uVar7 = 0;
      if (uVar9 != 0) {
        uVar7 = param_1 / uVar9;
      }
      if (_DAT_d900a764 < 10) {
        uVar9 = param_1 - uVar7 * uVar9;
        if (_DAT_d900a764 == 6) {
          bVar2 = (&DAT_d900a410)[uVar9];
        }
        else {
          bVar2 = (&DAT_d900a490)[uVar9];
        }
        param_1 = (uint)bVar2 + uVar7 * _DAT_d900a768;
      }
      else if (_DAT_d900a764 == 0x28) {
        param_1 = uVar7 * _DAT_d900a768 + (param_1 - uVar7 * uVar9) * 2;
      }
    }
    uVar9 = 0;
    uVar7 = param_1;
    while (iVar6 = FUN_d90070d8(), iVar6 == 0x82) {
      uVar7 = uVar7 + iVar5;
      if (0x3ff < uVar7 - param_1) {
        if (_DAT_d900a9a6 <= uVar9) {
          return 0x82;
        }
        uVar9 = uVar9 + 1;
        FUN_d9006fcc();
        uVar7 = param_1;
      }
    }
    if (iVar6 != 0) break;
    uVar8 = uVar8 + uVar3;
    param_1 = uVar1;
  }
  return iVar6;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int nfio_init(void)

{
  int iVar1;
  uint uVar2;
  undefined auStack_10 [6];
  byte local_a;
  
  efuse_read(0,8,(ulong)auStack_10);
  _DAT_d900a9a0 = 0xebec01;
  DAT_d900a9a3 = 0x80;
  FUN_d90067c8();
  _DAT_d0074000 = 0x90200201;
  uVar2 = (DAT_d900a9a3 >> 3 & 3) << 10;
  _DAT_d0074804 = uVar2 | 0x80000064;
  if ((local_a >> 5 & 1) != 0) {
    _DAT_d0074804 = uVar2 | 0x80020064;
  }
  iVar1 = FUN_d90067fc();
  if (iVar1 != 0) {
    return iVar1;
  }
  _DAT_d0074800 = 0x33800;
  _DAT_d900a9a4 = (ushort)(byte)_DAT_d0074810;
  if ((_DAT_d0074810 & 0xff) == 0x45) {
    _DAT_d900a9a6 = 0x16;
  }
  else if ((_DAT_d0074810 & 0xff) < 0x46) {
    uVar2 = _DAT_d0074810 & 0xff;
    if (((uVar2 == 7) || (uVar2 < 8)) || ((uVar2 == 0x20 || (uVar2 != 0x2c)))) {
LAB_d90077f0:
      _DAT_d900a9a6 = 0;
    }
    else {
LAB_d90077dc:
      _DAT_d900a9a6 = 8;
    }
  }
  else {
    uVar2 = _DAT_d0074810 & 0xff;
    if (uVar2 == 0x98) goto LAB_d90077dc;
    if (((uVar2 < 0x99) || (uVar2 == 0xad)) || (uVar2 != 0xec)) goto LAB_d90077f0;
    _DAT_d900a9a6 = 0xf;
  }
  iVar1 = FUN_d9007484(0,0x1800000,0x180);
  if (iVar1 == 0x82) {
    if (_DAT_d900a9a4 == 0x45) {
      _DAT_d900a9a6 = 0;
      _DAT_d900a9a0 = CONCAT13(DAT_d900a9a3,_DAT_d900a9a0) | 0x1000000;
    }
    else {
      if (((DAT_d900a9a3 >> 3 & 3) != 0) || ((_DAT_d900a9a4 != 0xec && (_DAT_d900a9a4 != 0x98))))
      goto LAB_d90078ac;
      _DAT_d900a9a0 = CONCAT13(DAT_d900a9a3 & 0xe0 | DAT_d900a9a3 & 7 | 0x10,_DAT_d900a9a0);
      _DAT_d0074804 = _DAT_d0074804 | 0x800;
      _DAT_c88344f0 = _DAT_c88344f0 & 0xffff7fff;
    }
    iVar1 = FUN_d9007484(0,0x1800000,0x180);
  }
LAB_d90078ac:
  DAT_d900a9a3 = (byte)((uint)_DAT_01800000 >> 0x18);
  _DAT_d900a9a0 = (undefined3)_DAT_01800000;
  _DAT_d900a9a6 = _DAT_01800006;
  _DAT_d900a9a4 = _DAT_01800004;
  _DAT_d900a768 = _DAT_01800060;
  _DAT_d900a764 = _DAT_0180005c;
  DAT_d900a9a3 = DAT_d900a9a3 & 0x9f;
  return iVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

int nand_read(undefined8 param_1,uint param_2,uint param_3,uint param_4)

{
  uint uVar1;
  uint uVar2;
  int iVar3;
  
  iVar3 = 0x400;
  if (((uint)(((ulong)_DAT_d900a9a0 & 0x3fffff) >> 0xe) & 7) < 2) {
    iVar3 = 0x200;
  }
  if ((param_2 & 0x3fff) == 0) {
    uVar1 = iVar3 * ((uint)((ulong)_DAT_d900a9a0 & 0x3fffff) & 0x3f);
    uVar2 = 0;
    if (uVar1 != 0) {
      uVar2 = param_2 / uVar1;
    }
    iVar3 = FUN_d9007484(uVar2 + 1,param_3,param_4);
    return iVar3;
  }
  return 0x86;
}



void FUN_d9007950(long param_1,long param_2)

{
  long lVar1;
  uint uVar2;
  long lVar3;
  
  lVar1 = 0;
  lVar3 = 0;
  for (uVar2 = 0; uVar2 < *(uint *)(param_1 + 0x408); uVar2 = uVar2 + 1) {
    lVar3 = ((ulong)*(uint *)(param_2 + lVar1) + lVar3) - (ulong)*(uint *)(param_1 + lVar1);
    *(int *)(param_2 + lVar1) = (int)lVar3;
    lVar3 = lVar3 >> 0x20;
    lVar1 = lVar1 + 4;
  }
  return;
}



void FUN_d9007990(uint *param_1,uint *param_2,long param_3,uint *param_4)

{
  long lVar1;
  uint uVar2;
  uint uVar3;
  uint uVar4;
  long lVar5;
  ulong uVar6;
  uint uVar7;
  ulong uVar8;
  
  for (uVar4 = 0; uVar4 < param_1[0x102]; uVar4 = uVar4 + 1) {
    param_2[uVar4] = 0;
  }
  for (uVar4 = 0; uVar4 < param_1[0x102]; uVar4 = uVar4 + 1) {
    uVar2 = *(uint *)(param_3 + (ulong)uVar4 * 4);
    uVar6 = (ulong)*param_2 + (ulong)uVar2 * (ulong)*param_4;
    uVar3 = param_1[0x101] * (int)uVar6;
    uVar8 = (uVar6 & 0xffffffff) + (ulong)uVar3 * (ulong)*param_1;
    uVar7 = 1;
    lVar5 = 0;
    while( true ) {
      lVar1 = lVar5 + 4;
      if (param_1[0x102] <= uVar7) break;
      uVar7 = uVar7 + 1;
      uVar6 = (ulong)*(uint *)((long)param_2 + lVar1) +
              (ulong)uVar2 * (ulong)*(uint *)((long)param_4 + lVar1) + (uVar6 >> 0x20);
      uVar8 = (uVar8 >> 0x20) + (ulong)uVar3 * (ulong)*(uint *)((long)param_1 + lVar1) +
              (uVar6 & 0xffffffff);
      *(int *)((long)param_2 + lVar5) = (int)uVar8;
      lVar5 = lVar1;
    }
    uVar8 = (uVar8 >> 0x20) + (uVar6 >> 0x20);
    param_2[uVar7 - 1] = (uint)uVar8;
    if (uVar8 >> 0x20 != 0) {
      FUN_d9007950((long)param_1,(long)param_2);
    }
  }
  return;
}



void FUN_d9007a8c(uint *param_1,undefined *param_2,uint *param_3)

{
  uint *puVar1;
  uint *puVar2;
  uint uVar3;
  undefined4 uVar4;
  uint uVar5;
  long lVar6;
  ulong uVar7;
  long lVar8;
  uint *puVar9;
  uint *puVar10;
  uint *puVar11;
  int iVar12;
  
  uVar3 = param_1[0x102];
  puVar1 = param_3 + (ulong)uVar3 * 2;
  lVar6 = 0;
  while( true ) {
    if (param_1[0x102] <= (uint)lVar6) break;
    uVar5 = ((param_1[0x102] + 0x3fffffff) - (uint)lVar6) * 4;
    param_3[lVar6] =
         (uint)(byte)param_2[uVar5 + 3] | (uint)(byte)param_2[uVar5] << 0x18 |
         (uint)(byte)param_2[uVar5 + 1] << 0x10 | (uint)(byte)param_2[uVar5 + 2] << 8;
    lVar6 = lVar6 + 1;
  }
  iVar12 = 0;
  for (uVar5 = param_1[0x80]; 0 < (int)uVar5; uVar5 = (int)uVar5 >> 1) {
    iVar12 = iVar12 + 1;
  }
  FUN_d9007990(param_1,puVar1,(long)param_3,param_1 + 0x81);
  puVar9 = puVar1;
  puVar11 = param_3 + uVar3;
  for (uVar5 = iVar12 - 2; -1 < (int)uVar5; uVar5 = uVar5 - 1) {
    FUN_d9007990(param_1,puVar11,(long)puVar9,puVar9);
    puVar2 = puVar9;
    if (puVar9 == puVar1) {
      puVar2 = param_3;
    }
    puVar10 = puVar2;
    puVar9 = puVar11;
    if ((1 << (ulong)(uVar5 & 0x1f) & param_1[0x80]) != 0) {
      FUN_d9007990(param_1,puVar2,(long)puVar11,puVar1);
      puVar10 = puVar11;
      puVar9 = puVar2;
    }
    puVar11 = puVar10;
  }
  memset((ulong)puVar1,0,((ulong)param_1[0x102] & 0x3fffffff) << 2);
  *puVar1 = 1;
  FUN_d9007990(param_1,puVar11,(long)puVar9,puVar1);
  do {
    uVar7 = (ulong)param_1[0x102];
    do {
      if ((int)uVar7 == 0) break;
      uVar7 = (ulong)((int)uVar7 - 1);
      if (puVar11[uVar7] < param_1[uVar7]) {
        iVar12 = param_1[0x102] - 1;
        lVar6 = (long)iVar12;
        lVar8 = 0;
        for (; -1 < iVar12; iVar12 = iVar12 + -1) {
          uVar4 = *(undefined4 *)((long)puVar11 + lVar8 + lVar6 * 4);
          *param_2 = (char)((uint)uVar4 >> 0x18);
          param_2[1] = (char)((uint)uVar4 >> 0x10);
          param_2[2] = (char)((uint)uVar4 >> 8);
          param_2[3] = (char)uVar4;
          lVar8 = lVar8 + -4;
          param_2 = param_2 + 4;
        }
        return;
      }
    } while (puVar11[uVar7] <= param_1[uVar7]);
    FUN_d9007950((long)param_1,(long)puVar11);
  } while( true );
}



bool FUN_d9007c78(uint *param_1,long param_2,long param_3,uint *param_4)

{
  uint uVar1;
  int iVar2;
  ulong uVar3;
  uint uVar4;
  ulong uVar5;
  byte local_210 [512];
  uint *local_10;
  
  iVar2 = param_1[0x102] << 5;
  if (((iVar2 == 0x400) || (iVar2 == 0x800)) || (iVar2 == 0x1000)) {
    uVar4 = param_1[0x102] * 4;
    local_10 = param_4;
    memcpy((ulong)local_210,param_2,(ulong)uVar4);
    FUN_d9007a8c(param_1,local_210,local_10);
    uVar5 = (ulong)(uVar4 - 0x20);
    uVar4 = (uint)(byte)(local_210[1] ^ 1 | local_210[0]);
    for (uVar3 = 0; uVar3 < uVar5 - 0x16; uVar3 = uVar3 + 1) {
      uVar4 = uVar4 | (byte)~local_210[uVar3 + 2];
    }
    uVar1 = FUN_d9008b00((long)(local_210 + (uVar5 - 0x14)),(long)&DAT_d9009558,0x14);
    if ((uVar1 | uVar4) == 0) {
      iVar2 = FUN_d9008b00((long)(local_210 + uVar5),param_3,0x20);
      return iVar2 == 0;
    }
  }
  return false;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void customer_id_check_new(void)

{
  if ((_DAT_d9013d08 != 0) && (_DAT_d9013d08 != plls.lCustomerID)) {
    serial_puts(s__ERROR__Customer_ID_not_match__d9009570);
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void print_version(void)

{
  serial_puts(s__TE__d9009590);
  serial_put_dec((ulong)_DAT_c1109988);
  serial_puts(s__BL2_d9009596);
  serial_puts(s_Built___09_01_42__Oct_11_2016__g_d9009010);
  serial_puts(&DAT_d900959d);
  return;
}



void bl21_handler_new(void)

{
                    /* I think this runs a BL21 function */
  (*(code *)&bl21_board_init_new)();
  return;
}



void bl2_main(undefined8 param_1,undefined8 param_2,ulong param_3)

{
  undefined auVar1 [16];
  
                    /* BL1 has a watchdog (see docs/watchdog.md). either ddr_init() or
                       bl2_load_images() disables it. */
  FUN_d90085fc();
  pinmux_init();
  customer_id_check_new();
  print_version();
  bl21_handler_new();
  auVar1 = bl2_arch_setup();
  bl2_usb_handler(auVar1._0_8_,auVar1._8_8_,param_3);
  bl2_does_nothing_at_all__notsure();
  ddr_init(1);
  bl2_platform_setup();
  bl2_load_image();
  serial_puts(s_NEVER_BE_HERE_d90095a0);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}



undefined8 FUN_d9007e70(long param_1,ulong param_2,long param_3)

{
  ulong uVar1;
  long lVar2;
  
  FUN_d9008de8(param_1,param_2,param_3);
  lVar2 = param_1 + (ulong)(DAT_d900a510 - 0xc000);
  uVar1 = memcpy(param_2,lVar2,param_3);
  FUN_d9008e04(uVar1,lVar2,param_3);
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 bl2_usb_handler(undefined8 param_1,undefined8 param_2,ulong param_3)

{
  int iVar1;
  uint uVar2;
  undefined4 uVar3;
  uint uVar4;
  ulong uVar5;
  uint uVar6;
  long lVar7;
  ulong uVar8;
  
                    /* Looks like BL2 USB-related stuff...which is a "special mode" when sending BL2
                       with the burning tool. Not sure...
                        */
  uVar2 = _DAT_da10001c;
  uVar4 = get_boot_device();
  uVar6 = uVar2 >> 0xc & 0xf;
  if ((uVar4 != 5) && (uVar6 != 2)) {
    if (uVar6 != 1) {
      return 0;
    }
    serial_puts(s_Skip_usb__d90095be);
    _DAT_da10001c = uVar2 & 0xffff0fff | 0x2000;
    uVar8 = (ulong)_DAT_d904050c;
    if (_DAT_d904050c == 0x52800022) {
      uVar5 = 0x3d4c;
    }
    else if (_DAT_d904050c == 0x940004fe) {
      uVar5 = 0x3e24;
    }
    else {
      uVar5 = 0x3df0;
    }
    goto LAB_d90081a0;
  }
  serial_puts(s_BL2_USB__d90095b4);
  iVar1 = _DAT_d900c000;
  param_3 = 0x7856efab;
  _DAT_d900c000 = 0x7856efab;
  if ((_DAT_d900c004 == 0x200) && (iVar1 == 0x3412cdab)) {
    uVar3 = 0xe3;
    if (_DAT_d900c008 - 0xc0deU < 4) {
      param_3 = (long)(char)(&switchD_d9007f88::switchdataD_d90095b0)[_DAT_d900c008 - 0xc0deU] * 4 +
                0xd9007f8c;
      switch(_DAT_d900c008) {
      case 0xc0de:
        uVar3 = 0xe4;
        break;
      case 0xc0df:
        ddr_init(0);
        _DAT_d900c018 = FUN_d9001a7c();
        uVar3 = 0xe5;
        if (_DAT_d900c018 != 0) {
          uVar3 = 0;
        }
        break;
      case 0xc0e0:
        if (_DAT_d900c018 == 1) {
          param_3 = (ulong)_DAT_d900c01c;
          _DAT_d900c028 = 0;
          uVar6 = _DAT_d900c020 & 3;
          for (lVar7 = 0; (int)_DAT_d900c020 >> 2 != (uint)lVar7; lVar7 = lVar7 + 1) {
            _DAT_d900c028 = _DAT_d900c028 + *(int *)(param_3 + lVar7 * 4);
          }
          lVar7 = (ulong)(uint)((int)_DAT_d900c020 >> 2) * 4;
          if (uVar6 == 1) {
            uVar6 = (uint)*(byte *)(param_3 + lVar7);
LAB_d90080c4:
            _DAT_d900c028 = _DAT_d900c028 + uVar6;
          }
          else {
            if (uVar6 == 2) {
              uVar6 = (uint)*(ushort *)(param_3 + lVar7);
              goto LAB_d90080c4;
            }
            if (uVar6 == 3) {
              uVar6 = *(uint *)(param_3 + lVar7) & 0xffffff;
              goto LAB_d90080c4;
            }
          }
          uVar3 = 0xe6;
          if (_DAT_d900c028 == _DAT_d900c024) {
            uVar3 = 0;
          }
        }
        else {
          uVar3 = 0xe8;
        }
        break;
      case 0xc0e1:
        _DAT_da10025c = _DAT_da10025c | 0x80000000;
        if (_DAT_d900c018 == 1) {
          DAT_d900a510 = _DAT_d900c01c;
          bl2_platform_setup();
          bl2_load_image();
        }
        else {
          if (_DAT_d900c018 != 2) {
            uVar3 = 0xe9;
            break;
          }
          (*(code *)(ulong)_DAT_d900c01c)();
        }
        uVar3 = 0;
      }
    }
  }
  else {
    _DAT_d900c00c = 0xe2;
    uVar3 = _DAT_d900c00c;
  }
  _DAT_d900c00c = uVar3;
  uVar8 = (ulong)_DAT_d904050c;
  if (_DAT_d904050c == 0x52800022) {
    uVar5 = 0x4640;
  }
  else if (_DAT_d904050c == 0x940004fe) {
    uVar5 = 0x4538;
  }
  else {
    uVar5 = 0x4504;
  }
LAB_d90081a0:
  FUN_d900644c(uVar5 | 0xd9040000,uVar8,param_3);
  return 0;
}



void bl2_arch_setup(void)

{
  write_cpacr(0x300000);
  saradc_ch1_get();
  pll_init();
  return;
}



void FUN_d90081d4(uint param_1)

{
  if ((param_1 & 0x3ffff) == 0) {
    serial_print_new(&DAT_d90095c9,(ulong)(param_1 << 2),param_1 & 0x3ffff,&CHAR_NUL);
    return;
  }
  return;
}



undefined8 FUN_d90081fc(char *param_1,ulong param_2,uint param_3,uint param_4)

{
                    /* "  -W[0xSOMETHING,R:0xSOMETHING" */
  serial_puts(s__d90095cd);
  serial_puts(param_1);
  serial_print_new(s__W_0x_d90095d0,param_2,0,&CHAR_NUL);
  serial_print_new(s___0x_d90095d6,(ulong)param_3,0,&CHAR_NUL);
  serial_print_new(s__R_0x_d90095db,(ulong)param_4,0,&CHAR_NL);
  return 1;
}



undefined8 memTestDataBus(uint *address)

{
  undefined8 ret;
  uint pattern;
  int iVar1;
  
  iVar1 = 0x20;
  ret = 0;
  pattern = 1;
  do {
    *address = pattern;
    if (*address != pattern) {
      ret = FUN_d90081fc(s_DATA_d90095e1,(ulong)address & 0xffffffff,pattern,*address);
    }
    iVar1 = iVar1 + -1;
    pattern = pattern << 1;
  } while (iVar1 != 0);
  return ret;
}



ulong memTestAddressBus(uint *param_1,ulong param_2)

{
  uint uVar1;
  ulong uVar2;
  ulong uVar3;
  uint uVar4;
  uint uVar5;
  ulong uVar6;
  
  uVar1 = ((uint)(param_2 >> 2) & 0x3fffffff) - 1;
  uVar3 = 1;
  while( true ) {
    uVar4 = (uint)uVar3 & uVar1;
    uVar2 = (ulong)uVar4;
    if (uVar4 == 0) break;
    param_1[uVar3] = 0xaaaaaaaa;
    uVar3 = (ulong)((uint)uVar3 << 1);
  }
  *param_1 = 0x55555555;
  uVar3 = 1;
  while (uVar4 = (uint)uVar3, (uVar4 & uVar1) != 0) {
    if (param_1[uVar3] != 0xaaaaaaaa) {
      uVar2 = FUN_d90081fc(&DAT_d90095e6,(ulong)(uVar4 << 2),0xaaaaaaaa,param_1[uVar3]);
    }
    uVar3 = (ulong)(uVar4 << 1);
  }
  *param_1 = 0xaaaaaaaa;
  uVar3 = 1;
  while (uVar4 = (uint)uVar3, (uVar4 & uVar1) != 0) {
    param_1[uVar3] = 0x55555555;
    if (*param_1 != 0xaaaaaaaa) {
      uVar2 = FUN_d90081fc(s_ADDR2_d90095eb,(ulong)(uVar4 << 2),0x55555555,*param_1);
    }
    uVar6 = 1;
    while (uVar5 = (uint)uVar6, (uVar5 & uVar1) != 0) {
      if ((param_1[uVar6] != 0xaaaaaaaa) && (uVar5 != uVar4)) {
        uVar2 = FUN_d90081fc(s_ADDR3_d90095f1,(ulong)(uVar4 << 2),0x55555555,param_1[uVar6]);
      }
      uVar6 = (ulong)(uVar5 << 1);
    }
    param_1[uVar3] = 0xaaaaaaaa;
    uVar3 = (ulong)(uVar4 << 1);
  }
  return uVar2;
}



ulong memTestDevice(uint *param_1,ulong param_2)

{
  uint uVar1;
  uint uVar2;
  long lVar3;
  ulong uVar4;
  uint uVar5;
  uint *puVar6;
  
  uVar5 = (uint)(param_2 >> 2) & 0x3fffffff;
  serial_print_new(s__Total_Size_0x_d90095f7,param_2,0,&CHAR_NL);
  for (lVar3 = 0; uVar2 = (uint)lVar3, uVar2 < uVar5; lVar3 = lVar3 + 1) {
    param_1[lVar3] = uVar2 + 1;
    FUN_d90081d4(uVar2);
  }
  uVar4 = 0;
  serial_puts(&CHAR_NL);
  puVar6 = param_1;
  uVar2 = 0;
  while (uVar1 = uVar2 + 1, uVar2 != uVar5) {
    if (*puVar6 != uVar1) {
      uVar4 = FUN_d90081fc(s_FULL_d9009606,(ulong)(uVar2 << 2),uVar1,*puVar6);
      uVar4 = uVar4 & 0xffffffff;
    }
    *puVar6 = -uVar2 - 2;
    FUN_d90081d4(uVar2);
    puVar6 = puVar6 + 1;
    uVar2 = uVar1;
  }
  serial_puts(&CHAR_NL);
  for (uVar2 = 0; uVar2 != uVar5; uVar2 = uVar2 + 1) {
    if (*param_1 != -uVar2 - 2) {
      uVar4 = FUN_d90081fc(s_FULL2_d900960b,(ulong)(uVar2 << 2),-uVar2 - 2,*param_1);
      uVar4 = uVar4 & 0xffffffff;
    }
    FUN_d90081d4(uVar2);
    param_1 = param_1 + 1;
  }
  serial_puts(&CHAR_NL);
  return uVar4;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9008588(void)

{
  int iVar1;
  int iVar2;
  int iVar3;
  
                    /* Very much undocumented. */
  iVar1 = mmio_read_X__notsure((undefined4 *)0xc1107d4c);
  iVar2 = mmio_read_X__notsure((undefined4 *)0xda8345a8);
  if (iVar2 == 0x11111111) {
    iVar3 = 10;
  }
  else {
    iVar3 = 0xb;
    if (iVar2 != 0x13311111) {
      iVar3 = 0xc;
    }
  }
  _DAT_d900a960 = iVar1 << 0x18 | iVar3 << 8;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90085fc(void)

{
  FUN_d9008588();
  _DAT_d900a788 = _DAT_d900a960 >> 0x18;
  _DAT_d900a78c = _DAT_d900a960 >> 8 & 0xff;
  return;
}



void FUN_d9008634(ulong *param_1,ulong param_2,ulong *param_3,uint param_4)

{
  uint uVar1;
  ulong uVar2;
  uint uVar3;
  ulong *puVar4;
  ulong uVar5;
  char *pcVar6;
  uint uVar7;
  long lVar8;
  ulong uVar9;
  ulong unaff_x20;
  uint *unaff_x23;
  long *unaff_x26;
  undefined auVar10 [16];
  
  uVar1 = param_4 * -9 + 0x27;
  if (param_4 < 4) {
    unaff_x26 = (long *)&DAT_d900a930;
    unaff_x23 = (uint *)0xd900a964;
    goto LAB_d90086ac;
  }
  puVar4 = (ulong *)0xc2;
  pcVar6 = s_level____3_d9009652;
  do {
    auVar10 = __assert((byte *)s_init_xlation_table_d90096f0,s_lib_aarch64_xlat_tables_c_d9009638,
                       (ulong)puVar4,(byte *)pcVar6);
    do {
      uVar9 = (ulong)puVar4 | 3;
      param_1 = (ulong *)FUN_d9008634(auVar10._0_8_,auVar10._8_8_,puVar4,param_4 + 1);
      param_2 = unaff_x20;
      do {
        while( true ) {
          *param_3 = uVar9;
          param_3 = param_3 + 1;
          while( true ) {
            if ((param_1[1] == 0) || ((param_2 & (long)(0x1ff << (ulong)(uVar1 & 0x1f))) == 0)) {
              return;
            }
LAB_d90086ac:
            auVar10._8_8_ = param_2;
            auVar10._0_8_ = param_1;
            uVar9 = *param_1;
            if (param_2 < uVar9 + param_1[1]) break;
            param_1 = param_1 + 3;
          }
          unaff_x20 = param_2 + (uint)(1 << (ulong)(uVar1 & 0x1f));
          if (uVar9 < unaff_x20) break;
          uVar9 = 0;
          param_2 = unaff_x20;
        }
        if ((param_2 < uVar9) || (uVar9 + param_1[1] < unaff_x20)) break;
        uVar7 = *(uint *)(param_1 + 2);
        puVar4 = param_1;
        while( true ) {
          if ((puVar4[4] == 0) || (uVar9 = puVar4[3], unaff_x20 <= uVar9)) break;
          uVar5 = puVar4[4] + uVar9;
          if (((param_2 < uVar5) && ((uVar7 & *(uint *)(puVar4 + 5)) != uVar7)) &&
             ((param_2 < uVar9 || (uVar7 = uVar7 & *(uint *)(puVar4 + 5), uVar5 < unaff_x20))))
          goto LAB_d90087ac;
          puVar4 = puVar4 + 3;
        }
        if ((int)uVar7 < 0) break;
        uVar5 = 3;
        if (param_4 != 3) {
          uVar5 = 1;
        }
        uVar2 = 0;
        if ((uVar7 & 4) != 0) {
          uVar2 = 0x20;
        }
        uVar9 = 0x80;
        if ((uVar7 & 2) != 0) {
          uVar9 = 0;
        }
        uVar9 = uVar5 | param_2 | uVar2 | uVar9;
        if ((uVar7 & 1) == 0) {
          uVar5 = 0x604;
LAB_d900879c:
          uVar9 = uVar9 | uVar5 | 0x40000000000000;
        }
        else {
          if ((uVar7 & 2) != 0) {
            uVar5 = 0x700;
            goto LAB_d900879c;
          }
          uVar9 = uVar9 | 0x700;
        }
        param_2 = unaff_x20;
      } while (uVar9 != 0xffffffffffffffff);
LAB_d90087ac:
      uVar3 = *unaff_x23;
      lVar8 = *unaff_x26;
      uVar7 = uVar3 + 1;
      *unaff_x23 = uVar7;
      puVar4 = (ulong *)(lVar8 + ((ulong)uVar3 & 0x7fffff) * 0x1000);
    } while (uVar7 < 5);
    puVar4 = (ulong *)0xe5;
    pcVar6 = s_next_xlat____MAX_XLAT_TABLES_d900965d;
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void mmap_add_region(ulong param_1,ulong param_2,undefined4 param_3)

{
  ulong *puVar1;
  ulong extraout_x1;
  ulong uVar2;
  char *pcVar3;
  ulong *puVar4;
  
  if ((param_1 & 0xfff) == 0) {
    uVar2 = param_2;
    if ((param_2 & 0xfff) == 0) goto LAB_d90088a4;
    uVar2 = 0x61;
    pcVar3 = s_IS_PAGE_ALIGNED_size__d9009690;
  }
  else {
    uVar2 = 0x60;
    pcVar3 = s_IS_PAGE_ALIGNED_base__d900967a;
  }
  do {
    __assert((byte *)s_mmap_add_region_d9009628,s_lib_aarch64_xlat_tables_c_d9009638,uVar2,
             (byte *)pcVar3);
    uVar2 = extraout_x1;
LAB_d90088a4:
    if (uVar2 == 0) {
      return;
    }
    puVar1 = (ulong *)&DAT_d900a798;
    do {
      puVar4 = puVar1;
      if (param_1 <= *puVar4) break;
      puVar1 = puVar4 + 3;
    } while (puVar4[1] != 0);
    FUN_d9008b88((undefined *)(puVar4 + 3),(long)puVar4,0xd900a918 - (long)puVar4);
    if (_DAT_d900a920 == 0) {
      *puVar4 = param_1;
      puVar4[1] = param_2;
      *(undefined4 *)(puVar4 + 2) = param_3;
      return;
    }
    uVar2 = 0x73;
    pcVar3 = s_mm_last__size____0_d90096a6;
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900892c(void)

{
  memset(0x1600000,0,0x4000);
  memset(0x15ff000,0,4);
  _DAT_d900a930 = 0x1600000;
  _DAT_d900a938 = 0x15ff000;
  FUN_d9008634((ulong *)&DAT_d900a798,0,(ulong *)0x15ff000,1);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900899c(void)

{
  ulong uVar1;
  undefined8 uVar2;
  char *pcVar3;
  
  uVar1 = read_current_el();
  if ((uVar1 >> 2 & 3) == 1) {
    uVar1 = read_sctlr_el1();
    if ((uVar1 & 1) == 0) goto LAB_d90089fc;
    uVar1 = 0x10f;
    pcVar3 = s__read_sctlr_el1_____SCTLR_M_BIT__d90096c5;
  }
  else {
    uVar1 = 0x10e;
    pcVar3 = s_IS_IN_EL_1__d90096b9;
  }
  __assert((byte *)s_enable_mmu_el1_d9009618,s_lib_aarch64_xlat_tables_c_d9009638,uVar1,
           (byte *)pcVar3);
LAB_d90089fc:
  write_mair_el1(0x4ff);
  FUN_d9008fc4();
  write_tcr_el1(0x3520);
  FUN_00000800(_DAT_d900a938);
  FUN_d9008f84();
  FUN_d9008f8c();
  uVar2 = read_sctlr_el1();
  write_sctlr_el1((ulong)((uint)uVar2 | 0x81007));
  FUN_d9008f8c();
  return;
}



void __assert(byte *param_1,undefined8 param_2,ulong param_3,byte *param_4)

{
  serial_puts(s_ASSERT__d9009710);
  serial_puts((char *)param_1);
  serial_puts(&DAT_d9009719);
  serial_put_dec(param_3 & 0xffffffff);
  serial_puts(&DAT_d900971c);
  serial_puts((char *)param_4);
  serial_puts(s_ABORT_d9009708 + 5);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}



ulong memset(ulong param_1,undefined param_2,long param_3)

{
  long lVar1;
  
  for (lVar1 = 0; lVar1 != param_3; lVar1 = lVar1 + 1) {
    *(undefined *)(param_1 + lVar1) = param_2;
  }
  inv_dcache_range(param_1,-1);
  FUN_d9008e60(param_1,-1);
  return param_1;
}



int FUN_d9008b00(long param_1,long param_2,long param_3)

{
  byte *pbVar1;
  long lVar2;
  uint uVar3;
  
  lVar2 = 0;
  do {
    if (lVar2 == param_3) {
      return 0;
    }
    pbVar1 = (byte *)(param_1 + lVar2);
    lVar2 = lVar2 + 1;
    uVar3 = (uint)*(byte *)(param_2 + lVar2 + -1);
  } while (*pbVar1 == uVar3);
  return *pbVar1 - uVar3;
}



ulong memcpy(ulong param_1,long param_2,long param_3)

{
  long lVar1;
  
  for (lVar1 = 0; lVar1 != param_3; lVar1 = lVar1 + 1) {
    *(undefined *)(param_1 + lVar1) = *(undefined *)(param_2 + lVar1);
  }
  inv_dcache_range(param_1,-1);
  FUN_d9008e60(param_1,-1);
  return param_1;
}



undefined * FUN_d9008b88(undefined *param_1,long param_2,ulong param_3)

{
  undefined *puVar1;
  undefined *puVar2;
  
  if (param_3 <= (ulong)((long)param_1 - param_2)) {
    puVar1 = (undefined *)memcpy((ulong)param_1,param_2,param_3);
    return puVar1;
  }
  puVar1 = (undefined *)(param_2 + param_3);
  puVar2 = param_1 + param_3;
  while (puVar2 != param_1) {
    puVar1 = puVar1 + -1;
    puVar2 = puVar2 + -1;
    *puVar2 = *puVar1;
  }
  inv_dcache_range((ulong)puVar2,param_3);
  FUN_d9008e60((ulong)puVar2,param_3);
  return puVar2;
}



int strcmp(long param_1,long param_2)

{
  byte bVar1;
  long lVar2;
  
  lVar2 = 0;
  do {
    bVar1 = *(byte *)(param_1 + lVar2);
    if ((uint)bVar1 != (uint)*(byte *)(param_2 + lVar2)) {
      return (uint)bVar1 - (uint)*(byte *)(param_2 + lVar2);
    }
    lVar2 = lVar2 + 1;
  } while (bVar1 != 0);
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void serial_putc(char c)

{
  if ((byte)c == L'\n') {
    do {
    } while ((_DAT_c81004cc >> 0x15 & 1) != 0);
  }
  do {
  } while ((_DAT_c81004cc >> 0x15 & 1) != 0);
  _DAT_c81004c0 = (uint)(byte)c;
  return;
}



void serial_puts(char *param_1)

{
  for (; *param_1 != '\0'; param_1 = param_1 + 1) {
    serial_putc(*param_1);
  }
  return;
}



void serial_put_hex(ulong param_1,int param_2)

{
  uint uVar1;
  uint uVar2;
  char cVar3;
  ulong uVar4;
  
  for (uVar2 = param_2 - 4; -1 < (int)uVar2; uVar2 = uVar2 - 4) {
    uVar4 = param_1 >> ((ulong)uVar2 & 0x3f);
    cVar3 = '0';
    if (uVar4 != 0) {
      uVar1 = (uint)uVar4 & 0xf;
      cVar3 = (char)uVar1;
      if (uVar1 < 10) {
        cVar3 = cVar3 + '0';
      }
      else {
        cVar3 = cVar3 + 'W';
      }
    }
    serial_putc(cVar3);
  }
  return;
}



void serial_put_dec(ulong param_1)

{
  uint uVar1;
  ulong uVar2;
  ulong uVar3;
  int iVar4;
  ulong uVar5;
  char local_10 [16];
  
  local_10[0] = '0';
  uVar3 = 0;
  do {
    uVar2 = param_1 / 10;
    local_10[uVar3] = (char)param_1 + (char)uVar2 * -10 + '0';
    uVar5 = uVar3 & 0xffffffff;
    uVar3 = uVar3 + 1;
    param_1 = uVar2;
  } while (uVar2 != 0);
  do {
    iVar4 = (int)uVar5;
    uVar1 = iVar4 - 1;
    uVar5 = (ulong)uVar1;
    serial_putc(local_10[iVar4]);
  } while (uVar1 != 0xffffffff);
  return;
}



void serial_print_new(char *prefix,ulong integer,int hex_or_dec,char *suffix)

{
                    /* FUN_d9008d58(byte *param_1, ulong param_2, int param_3, byte *param_4);
                                            ^ string     ^ number     ^ hex/dec switch ^ extra
                       string
                       
                       Not sure about the name..... */
  if (prefix != (char *)0x0) {
    serial_puts(prefix);
  }
  if (hex_or_dec == 0) {
    serial_put_hex(integer & 0xffffffff,0x20);
  }
  else {
    serial_put_dec(integer & 0xffffffff);
  }
  if (suffix != (char *)0x0) {
    serial_puts(suffix);
    return;
  }
  return;
}



undefined4 mmio_read_X__notsure(undefined4 *param_1)

{
  return *param_1;
}



undefined8 platform_set_coherent_stack(void)

{
  return DAT_d9008dd0;
}



bool platform_is_primary_cpu(short param_1)

{
  return param_1 == 0;
}



void FUN_d9008de8(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  ulong uVar1;
  
  uVar1 = sctlr_el1;
  sctlr_el1 = uVar1 & 0xfffffffffffffffa;
  InstructionSynchronizationBarrier();
  FUN_d9008f74(1,5,param_3);
  return;
}



void FUN_d9008e04(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  ulong uVar1;
  
  uVar1 = sctlr_el1;
  sctlr_el1 = uVar1 | 5;
  InstructionSynchronizationBarrier();
  FUN_d9008f74(1,5,param_3);
  return;
}



void FUN_d9008e20(void)

{
  return;
}



void inv_dcache_range(ulong param_1,long param_2)

{
  ulong uVar1;
  long lVar2;
  
  uVar1 = ctr_el0;
  lVar2 = 4L << (uVar1 >> 0x10 & 0xf);
  uVar1 = param_1 & (lVar2 - 1U ^ 0xffffffffffffffff);
  do {
    DC_CIVAC(uVar1);
    uVar1 = uVar1 + lVar2;
  } while (uVar1 < param_1 + param_2);
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  return;
}



void FUN_d9008e60(ulong param_1,long param_2)

{
  ulong uVar1;
  long lVar2;
  
  uVar1 = ctr_el0;
  lVar2 = 4L << (uVar1 >> 0x10 & 0xf);
  uVar1 = param_1 & (lVar2 - 1U ^ 0xffffffffffffffff);
  do {
    DC_IVAC(uVar1);
    uVar1 = uVar1 + lVar2;
  } while (uVar1 < param_1 + param_2);
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  return;
}



void FUN_d9008e94(long param_1,undefined8 param_2,undefined8 param_3,long param_4)

{
  undefined8 uVar1;
  ulong in_x9;
  ulong uVar2;
  
  if (param_4 != 0) {
    uVar2 = 0;
    do {
      if (1 < (in_x9 >> (uVar2 + (uVar2 >> 1) & 0x3f) & 7)) {
        csselr_el1 = uVar2;
        InstructionSynchronizationBarrier();
        uVar1 = ccsidr_el1;
        UnkSytemRegWrite(0,3,3,0xf,4,0);
                    /* WARNING: Could not recover jumptable at 0xd9008ef4. Too many branches */
                    /* WARNING: Treating indirect jump as call */
        (*(code *)(param_1 * 0x20 + 0xd9008f14))();
        return;
      }
      uVar2 = uVar2 + 2;
    } while ((long)uVar2 < param_4);
    csselr_el1 = 0;
    UnkSytemRegWrite(0,3,3,0xf,4,0);
    InstructionSynchronizationBarrier();
  }
  return;
}



void FUN_d9008f74(long param_1,undefined8 param_2,undefined8 param_3)

{
  ulong uVar1;
  
  uVar1 = clidr_el1;
  FUN_d9008e94(param_1,param_2,param_3,(uVar1 >> 0x18 & 7) << 1);
  return;
}



void FUN_d9008f84(void)

{
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  return;
}



void FUN_d9008f8c(void)

{
  InstructionSynchronizationBarrier();
  return;
}



void FUN_d9008f94(undefined8 *param_1,long param_2)

{
  undefined8 *puVar1;
  
  CallSecureMonitor(0);
  puVar1 = (undefined8 *)((long)param_1 + param_2);
  for (; 0xf < (long)puVar1 - (long)param_1; param_1 = param_1 + 2) {
    *param_1 = 0;
    param_1[1] = 0;
  }
  for (; param_1 != puVar1; param_1 = (undefined8 *)((long)param_1 + 1)) {
    *(undefined *)param_1 = 0;
  }
  return;
}



void zeromem16(undefined8 *param_1,long param_2)

{
  undefined8 *puVar1;
  
  puVar1 = (undefined8 *)((long)param_1 + param_2);
  for (; 0xf < (long)puVar1 - (long)param_1; param_1 = param_1 + 2) {
    *param_1 = 0;
    param_1[1] = 0;
  }
  for (; param_1 != puVar1; param_1 = (undefined8 *)((long)param_1 + 1)) {
    *(undefined *)param_1 = 0;
  }
  return;
}



void FUN_d9008fc4(void)

{
  TLBI_VMALLE1();
  return;
}



undefined8 read_current_el(void)

{
  undefined8 uVar1;
  
  uVar1 = currentel;
  return uVar1;
}



undefined8 read_id_aa64pfr0_el1(void)

{
  undefined8 uVar1;
  
  uVar1 = id_aa64pfr0_el1;
  return uVar1;
}



void write_mair_el1(undefined8 param_1)

{
  mair_el1 = param_1;
  return;
}



undefined8 read_sctlr_el1(void)

{
  undefined8 uVar1;
  
  uVar1 = sctlr_el1;
  return uVar1;
}



void write_sctlr_el1(undefined8 param_1)

{
  sctlr_el1 = param_1;
  return;
}



void write_tcr_el1(undefined8 param_1)

{
  tcr_el1 = param_1;
  return;
}



void FUN_00000800(undefined8 param_1)

{
  ttbr0_el1 = param_1;
  return;
}



void write_cpacr(undefined8 param_1)

{
                    /* Guess: seems like this was originally manually written in ARM Assembly,
                       according to Amlogic's old sources. */
  cpacr_el1 = param_1;
  return;
}


