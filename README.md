# Reversing Amlogic BL2

*Efforts to reverse-engineer Amlogic's proprietary BL2*

This repository contains documentation of Amlogic bootloader firmware stages.  
Mainly BL2, but also BL1/BootROM.

## Chat

I am in the [#linux-amlogic](https://web.libera.chat/#linux-amlogic) IRC channel 
over on Libera.Chat or the [#postmarketos-lowlevel](https://postmarketos.org/irc) 
IRC channel on [OFTC](https://oftc.net), or 
[#amlogic-postmarketos](https://webchat.oftc.net/?channels=amlogic-postmarketos) 
(also on OFTC). I'm also in the [#u-boot](https://web.libera.chat/#u-boot) 
Libera.Chat IRC channel or [#ml-mainline](https://webchat.oftc.net/?channels=ml-mainline) 
on OFTC, but Amlogic bootloader reverse engineering are a less on-topic on those. 
You can also [private message me](https://vitali64.duckdns.org/contact.html).

## Progress

**Support for all SoCs in [U-Boot SPL](https://git.vitali64.duckdns.org/misc/u-boot-kii-pro.git/tree/?h=wip/spl)
is still very much a work-in-progress!** This is especially true for anything 
newer than GXBB/S905 or GXL/S905X.

* [GXBB](./docs/gxbb/status.md): **Mostly done** 
* [GXL](./docs/gxl/status.md): **Mostly done**
* AXG: **TODO** (no AXG board, but very similar to GXL)
* G12B: **WIP**
* SM1: **TODO**

The following boards were tested:

* *Videostrong KII Pro (S905 version)*: Boots from SD card
* *Libre Computer AML-S905X-CC (1 GB version)*: Boots from SD card
* *Libre Computer AML-S805X-AC (512 MB version)*: Cannot boot from SPI or eMMC

## Pages

* [Amlogic TF-A mangles UART output](./docs/uart_mangle.md)
* [Boot process](./docs/boot_process.md)
* [GX BootROM notes](./docs/gx-bootrom.md)

* GXL / [Differences between 2017 BL2 and 2019 BL2](./docs/gxl/diffs_2017_2019.md)
* G12 / [How to extract code from aml_ddr.fw](./docs/g12/aml_ddr_extract.md)
