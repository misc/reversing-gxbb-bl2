# GX BootROM notes

Sample bootROM UART log of a successful boot on Amlogic S805Y:

`GXL:BL1:9ac50e:bb16dc;FEAT:BDFD71BC:0;POC:3;RCY:0;EMMC:0;READ:0;0.0;0.0;CHK:0;`

### Status messages

* `GXL:BL1:9ac50e:bb16dc`: Indicates SoC generation, git commit hash (?)
* `FEAT`: Dump of `SECUREBOOT_FLAG` (`0xc8100228`):
* `POC`: POC pin
* `RCY`: If HDMI boot dongle is plugged in: `boot@USB → 1`, `boot@SDC → 2`, else `0`

### Boot device status

* `EMMC`/`SD`/`SPI`/`USB`/`NAND`: Supported boot devices
* `CHK`: Image/Payload checks
    * `0`: Success, no error
    * `A7`: Wrong magic (!= `@AML`) or version number
    * `C7`: Wrong header size or digest offset
    * `CF`: Payload size or offset too big

### EFUSE/OTP

Where many settings are set, e.g. regarding SecureBoot.  A copy of it is 
available at `0xd9013c00` (in AHB SRAM).

On a secureboot-enabled device, it can be dumped easily using 
[amlogic-usbdl](https://fredericb.info/2021/02/amlogic-usbdl-unsigned-code-loader-for-amlogic-bootrom.html).

It is as follows:

* **0xd9013c04 ~ 0xd9013c09** → Boot behaviour settings
    * **Supported boot devices** (`0x4`):
        * Bit 4: Disable USB boot
        * Bit 5: Disable SPI boot
        * Bit 6: Disable SD card boot
        * Bit 7: Disable eMMC/NAND boot
    * **Debugging and recovery** (`0x5`):
        * Bit 0: Disable HDMI boot
        * Bit 1: Obfuscate UART logs
        * Bit 2: Disable JTAG
    * **SecureBoot** (`0x6`):
        * Bit 2: Enable SecureBoot
    * **Encryption** (`0x7`):
        * Bit 4: Enable AES-256 encryption of BL2
    * **Restrictions** (`0x8`):
        * Bit 0: Enable Anti-Rollback (prevents downgrades)
        * Bit 1: Protect USB mode behind a password
* **0xd9013c30 ~ 0xd9013c3f** → Root hash (likely SHA-256)
* **0xd9013c50 ~ 0xd9013c6f** → BL2 AES key (AES-256)
* **0xd9013c80 ~ 0xd9013c9f** → USB password hash (likely SHA-256)

## SecureBoot

When SecureBoot mode is enabled, the bootROM will refuse to load unsigned code, 
and in some cases the binaries on eMMC/NAND/... will be encrypted.

This is used extensively in the Android TV box/stick market, specifically the 
Google-certified ones, to prevent users from looking at the firmware and from 
easily replacing them.

In this mode:

* The BootROM expects BL2 to be encrypted with AES-256-CBC, and the key is in 
  OTP memory
* It also expects various `@KEY` headers, presumably for signature checks
