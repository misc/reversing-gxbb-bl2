# aml_ddr.fw

aml_ddr.fw is a proprietary AArch64 binary ran by BL2 and containing most of 
the DDR init code. As such, a lot (but not all!) of the BL2 DDR init code has 
been moved there, presumably because of space restrictions.

Besides, some DDR init code remains in BL2, e.g. PLL init.

## Import aml_ddr.fw in Ghidra

`aml_ddr.fw` cannot be fed into Ghidra directly. It seems to have some sort 
of header and is LZ4-compressed.

Strip the header:

```sh
$ dd if=aml_ddr.fw of=aml_ddr.stripped.fw bs=1 skip=96
```

And decompress it:

```sh
$ cat aml_ddr.stripped.fw | lz4 > aml_ddr.final.fw
```

When importing into ghidra it will display this notice:

```
The file aml_ddr.final.fw seems to have nested files in it.  Select an import mode:

             [ Single file ] [ Batch ] [ File System ] [ Cancel ]
```

Choose File System, then lz4f_decompressed, then set the language to AARCH64 
v8A little endian, and the base address to 0xfffd0060.

[IRC #linux-amlogic on 2023-07-26](https://libera.irclog.whitequark.org/linux-amlogic/2023-07-26#34664244;)
