There are multiple differences between 2 'major' GXL BL2 revisions, built in 
2017 and 2018 respectively. This page aims to list a major change Amlogic made.

## CPU Architecture

These 2 revisions target separate arch. Running objdump with -maarch64 shows:

```asm
;
; p212/bl2.bin `Built : 11:58:42, May 27 2017. gxl gc3c9a84 - xiaobo.gu@droid05`
; This is valid AArch64 assembly
;

0000000000000000 <.data>:
       0:       14000002        b       0x8             ; Jumps to bl2_entrypoint which is supposed to be at 0xD9001008
       4:       d900a310        stlur   x16, [x24, #10]
       8:       aa0003f4        mov     x20, x0
       c:       aa0103f5        mov     x21, x1
      10:       d53800a0        mrs     x0, mpidr_el1
      14:       9400204b        bl      0x8140
```

```asm
;
; lepotato/bl2.bin `Built : 16:20:27, Apr 19 2018. gxl g9478cf1 - jenkins@walle02-sh`
; This is invalid AArch64 assembly, which means this definitely doesn't target AArch64.
;

0000000000000000 <.data>:
       0:       14000200        b       0x800      ; Notice how it jumps to 0xD9001800. Also notice how this has nothing to do with bl2_entrypoint. Most likely bad disassembly
       4:       d900a698        .inst   0xd900a698 ; undefined
       8:       14000000        b       0x8        ; Is this a loop?
       c:       d503201f        nop
      10:       d503201f        nop
      14:       d503201f        nop
      18:       d503201f        nop
      1c:       d503201f        nop
```

Ghidra can't find a single function in the lepotato bl2.bin but successfully finds all of 
them in the P212 bl2.bin when the language is set to AARCH64-default. But it does find 
functions when the language is set to ARM-v7-default, which means lepotato's bl2.bin is 
targeted for ARMv7/AArch32.

Latest GXL BL21 is also targeted for AArch32. There's a config constant CONFIG_BL21_T32 
which, if set, compiles BL21 for AArch32:

```make
#
# arch/arm/cpu/armv8/gxl/firmware/bl21/Makefile
#

# Build architecture
ifdef CONFIG_BL21_T32
ARCH            := aarch32
else
ARCH    := aarch64
endif
```

This constant is set in arch/arm/include/asm/arch-gxl/cpu.h:

````c
/*
 * arch/arm/include/asm/arch-gxl/cpu.h
 */
#define CONFIG_BL21_T32           1
```

Which effectively makes it compile BL21 to target AArch32 instead of AArch64, 
and thus this means BL2 binaries newer than P212 BL2 binaries runs in AArch32 instead 
of AArch64. Meanwhile GXBB BL2 binaries all target AArch64, and GXL BL1 also targets 
AArch64. 

It looks like this was made to save a bit of space somehow?
