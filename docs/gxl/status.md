# U-Boot SPL on GXL

## Progress

The primary goal is to be able to boot U-Boot as BL33 and mainline linux.

Broken - Doesn't work

Untested - Needs testing

Works - Implemented and functional

TODO - Needs someone to write the solving code

```
    DDR initialisation: Partial (not all rank modes are supported yet)
        DDR3: Works
        DDR4: Works 
    PLL initialisation: Works
    Power initialisation: Works
    UART: Works
    Storage
        eMMC: Broken
        SD: Works
        SPI: Untested
        NAND: Untested
        USB: TODO 
```

## Log

```
GXL:BL1:9ac50e:bb16dc;FEAT:ADFC318C:0;POC:0;RCY:0;USB:0;SPI:0;CHK:A7;EMMC:400;NAND:81;SD:0;READ:0;0.0;CHK:0;
<de՝}Յ>
SPL: Restarting execution at EL3

<debug_uart>

U-Boot SPL 2023.10-rc3-00106-g83b2ac0c2c-dirty (Nov 23 2023 - 16:48:02 +0100)
Trying to boot from MMC1
NOTICE:  BL31: v2.9(debug):v2.8-1741-g84de50c7d-dirty
NOTICE:  BL31: Built : 15:45:51, Nov 23 2023
INFO:    ARM GICv2 driver initialized
mpu_config_enable:ok
[Image: gxl_v1.1.3308-45470c4 2018-04-12 16:22:58 jenkins@walle02-sh]
OPS=0x84
21 0d 84 00 97 d2 f2 c8 c8 5f 33 10 ea b7 86 fa
[4.099065 Inits done]
secure task start!
high task start!
low task start!
INFO:    BL31: Initializing runtime services
INFO:    BL31: cortex_a53: CPU workaround for erratum 855873 was applied
WARNING: BL31: cortex_a53: CPU workaround for erratum 1530924 was missing!
INFO:    BL31: Preparing for EL3 exit to normal world
INFO:    Entry point address = 0x1000000
INFO:    SPSR = 0x3c9

<debug_uart>


U-Boot 2023.10-rc3-00106-g83b2ac0c2c-dirty (Nov 23 2023 - 16:48:02 +0100) libretech-cc

Model: Libre Computer AML-S905X-CC
SoC:   Amlogic Meson GXL (S905X) Revision 21:d (84:2)
DRAM:  2 GiB (effective 1 GiB)
Core:  168 devices, 27 uclasses, devicetree: separate
MMC:   mmc@72000: 0, mmc@74000: 1
Loading Environment from nowhere... OK
In:    serial
Out:   vidconsole,serial
Err:   vidconsole,serial
Net:   eth0: ethernet@c9410000
Hit any key to stop autoboot:  0
switch to partitions #0, OK
mmc0 is current device
Scanning mmc 0:1...
Found /extlinux/extlinux.conf
Retrieving file: /extlinux/extlinux.conf
boot prev kernel
1:      postmarketOS
Enter choice: 1:        postmarketOS
Retrieving file: /vmlinuz
Retrieving file: /initramfs
append: console=null PMOS_NO_OUTPUT_REDIRECT
Retrieving file: /meson-gxbb-kii-pro.dtb
Moving Image from 0x8080000 to 0x8200000, end=9a00000
## Flattened Device Tree blob at 08008000
   Booting using the fdt blob at 0x8008000
Working FDT set to 8008000
   Loading Ramdisk to 3ac32000, end 3af322be ... OK
   Loading Device Tree to 000000003ac28000, end 000000003ac31c37 ... OK
Working FDT set to 3ac28000

Starting kernel ...
```

## Known issues

* eMMC doesn't work 
