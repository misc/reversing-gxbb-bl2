# U-Boot SPL on GXBB

## Progress

The primary goal is to be able to boot U-Boot as BL33 and mainline linux.

Broken - Doesn't work

Untested - Needs testing

Works - Implemented and functional

TODO - Needs someone to write the solving code

```
    DDR initialisation: Works
        DDR3: Works
        LPDDR2/3: Untested, not implemented
    PLL initialisation: Works
    Power initialisation: Works
    UART: Works
    Storage
        eMMC: Broken
        SD: Works
        SPI: Untested
        NAND: Untested
        USB: TODO 
```

## Log

```
GXBB:BL1:08dafd:0a8993;FEAT:EDFC318C;POC:3;RCY:0;EMMC:0;READ:0;CHK:F3;SD:0;READ:0;CHK:0;
<debug_uart>
SPL: Restarting execution at EL3

<debug_uart>

U-Boot SPL 2023.10-rc3-00109-g55d37e8bc2-dirty (Jan 16 2024 - 19:07:11 +0100)
Trying to boot from MMC1
Trying to send the SCP firmware
Trying to send the SCP firmware
[Image: gxb_v1.1.3135-18177fc 2016-06-24 11:00:09 yun.cai@droid06]
OPS=0x13
a4 f3 ce 38 6e 4c 4e 8e ed 41 1a 8 [0.910845 Inits done]
secure task start!
high task start!
low task start!
NOTICE:  BL31: lts-v2.8.8(release):
NOTICE:  BL31: Built : 18:10:21, Sep 20 2023
INFO:    ARM GICv2 driver initialized
INFO:    BL31: Initializing runtime services
INFO:    BL31: Preparing for EL3 exit to normal world
INFO:    Entry point address = 0x1000000
INFO:    SPSR = 0x3c9

<debug_uart>


U-Boot 2023.10-rc3-00109-g55d37e8bc2-dirty (Jan 16 2024 - 19:07:11 +0100) kii-pro

Model: Videostrong KII Pro
SoC:   Amlogic Meson GXBB (S905) Revision 1f:c (13:1)
DRAM:  1 GiB (effective 2 GiB)
Core:  162 devices, 23 uclasses, devicetree: separate
MMC:   mmc@70000: 0, mmc@72000: 1, mmc@74000: 2
Loading Environment from nowhere... OK
In:    usbkbd,serial
Out:   vidconsole,serial
Err:   vidconsole,serial
Net:
Warning: ethernet@c9410000 (eth0) using random MAC address - 1e:e1:c6:e3:33:c1
eth0: ethernet@c9410000
Hit any key to stop autoboot:  0
switch to partitions #0, OK
mmc1 is current device
Scanning mmc 1:1...
Found /extlinux/extlinux.conf
Retrieving file: /extlinux/extlinux.conf
boot prev kernel
1:      postmarketOS
Enter choice: 1:        postmarketOS
Retrieving file: /vmlinuz
Retrieving file: /initramfs
append: console=ttyAML0,115200n8 MOS_NO_OUTPUT_REDIRECT
Retrieving file: /meson-gxbb-kii-pro.dtb
Moving Image from 0x8080000 to 0x8200000, end=9a60000
## Flattened Device Tree blob at 08008000
   Booting using the fdt blob at 0x8008000
Working FDT set to 8008000
   Loading Ramdisk to 3fcf9000, end 3fffff68 ... OK
   Loading Device Tree to 000000003fcef000, end 000000003fcf8c51 ... OK
Working FDT set to 3fcef000

Starting kernel ...
```

## Known issues

* <del>In some cases loading the SCP firmware requires multiple tries. This could 
  be a problem with the SCP firmware and not SPL</del>
  **Fixed**: https://git.vitali64.duckdns.org/misc/u-boot-kii-pro.git/commit/?h=wip/spl&id=453434e37ca9b04e1ba55206a2ff00e67b80e437
* <del>Various crashes?</del>
  **Fixed**: https://git.vitali64.duckdns.org/misc/u-boot-kii-pro.git/commit/?h=wip/spl&id=c173e2876bef98aed989bf5a8ff38df11c8d8a66
* eMMC doesn't work 
