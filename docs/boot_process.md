Amlogic SoCs use a proprietary fork of Arm Trusted Firmware-A with added 
GPL-2.0+-licensed plugins for board-specific init, and a BL1 implementing 
a proprietary 'worldcup' USB protocol:


        ╭─────────────────────────────────────────────────────────────────────────────────────────────────┤ Secure World ├───────╮
        │                                                                                                                        │
        │       ╭─────┌────────────────╮           ╭─────┌───────────────────────╮                                               │
        │       │ BL1 │ AP Trusted ROM │──┤ L&R ├──┤ BL2 │ Trusted Boot firmware │                                               │
        │       ╰─────└────────────────╯           ╰─────└───────────────────────╯                                               │
        │                                             │                                                                          │
        │          ╭───────┤ Loads and runs¹ ├────────┤                                                                          │
        │          │                                  ╰┬──────────┤ Loads and runs ├──────────╮                                  │
        │          │                                   │                                      │                                  │
        │       ╭──┴──────┌──────────────╮             │                                   ╭──┴───┌──────────────────────╮       │
        │       │ SCP_BL2 │ SCP Firmware │             │                                   │ BL31 │ EL3 Runtime Software │       │
        │       ╰─────────└──────────────╯             │                                   ╰──────└──────────────────────╯       │
        │                                              │                                      │                                  │
        │                                              │                     ╭────┤ Runs ├────╯                                  │
        │                                              │                     │                                                   │
        │                                              ╰──────────┤ Loads ├──┴────────────────╮                                  │
        │                                                                                     │                                  │
        ╰─────────────────────────────────────────────────────────────────────────────────────│──────────────────────────────────╯
                                                                                              │
                                                                                       ╭──────│───────────┤ Normal World ├───────╮
                                                                                       │      │                                  │
                                                                                       │   ╭──┴───┌──────────────────────╮       │
                                                                                       │   │ BL33 │ Non-trusted Firmware │       │
                                                                                       │   │      │    Usually U-Boot    │       │
                                                                                       │   ╰──────└──────────────────────╯       │
                                                                                       │      │                                  │
                                                                                       │  ╭───┴───╮                              │
                                                                                       │  │  ...  │                              │
                                                                                       │  ╰───────╯                              │
                                                                                       ╰─────────────────────────────────────────╯

        ¹: SCP_BL2 is ran on the SCP.
           Also note that on GXL, BL31 runs the SCP firmware instead of BL2.

There are multiple 'plugins':
* BL2: BL21 plugin, ACS
* SCP_BL2: BL301 plugin

Another ASCII representation:

                              ___________________________________                      ___________________________________
                             |                                   |                    |                                   |_______________________________________________________________________
                             |   BL1: AP Trusted ROM             | ----------------\  |   BL2: Trusted Boot Firmware      |                        |                                              |
                             |     Does arch and platform init   |  Loads and runs  ) |     Inits DRAM and PLL and loads  |   BL21: Board Plugin   |   ACS: C struct injected in BL2 binary       |
                             |     and loads BL2 (or USB mode)   | ----------------/  |     further images into DRAM      |    Power init          |     DRAM settings, timings, and PLL config   |
                             |___________________________________|                    |___________________________________|________________________|______________________________________________|
      _______________________|                                   |                                  |   |
     |                       |   SCP_BL2/BL30: SCP Firmware      |  /-------------------------------+   |
     |   BL301: Board Plugin |     Platform-specific SCP init    | (  Sends to SCP and runs             |
     |    DVFS and suspend   |     for power management          |  \-----------------------------------+
     |_______________________|___________________________________|                                  |   |
                             |                                   |                                  |   |
           +---------------- |   BL31: EL3 Runtime Software      |  /-------------------------------+   |
           |                 |     Inits runtime services such   | (  Loads and runs                    |
           |   +------------ |     as PSCI and invokes BL33      |  \-----------------------------------+
           |   |             |___________________________________|                                  |   | 
           |   |             |                                   |                                  |   |
           |   +----------\  |   BL33: Non-trusted Firmware      |  /-------------------------------+   |
           |         Runs  ) |     Bootloader (U-Boot in most    | (  Loads                             |
           +--------------/  |     cases) which loads Linux      |  \-----------------------------------+
                             |___________________________________|
