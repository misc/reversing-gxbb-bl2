# Amlogic TF-A UART logs obfuscation on some boards (OTP?)

A ZTE B700V7-2017 user reported that it mangled UART logs:

```
.......................................................................................set vcck to 1200 mv
set vddee to 1200 mv

[U-Boot logs ...]
```

They successfully dumped BL1. We fed the dump into Ghidra:

```c
/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

uint serial_putc(uint c)

{
  byte *efuse_data;
  
                    /* Thanks to arzamas-16[m] for helping! */
  efuse_data = (byte *)get_efuse_mirror_ptr();
  if ((efuse_data[5] >> 1 & 1) != 0) {
    c = L'.';
  }
  do {
  } while ((_DAT_c81004cc >> 0x15 & 1) != 0);
  _DAT_c81004c0 = c & 0xff;
  return c;
}
```

It's known to happen on:
- ZTE B700V7
- ZTE ZXV10 B860H V5 
