typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned int    uint;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined8;



undefined8 load_image__notsure(undefined8 param_1)

{
  elr_el3 = param_1;
  spsr_el3 = 0x1c5;
  ExceptionReturn();
  return 0x1c5;
}



void FUN_d9004390(void)

{
  bool bVar1;
  uint uVar2;
  undefined8 uVar4;
  ulong uVar5;
  ulong uVar6;
  ulong uVar7;
  ulong uVar8;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  uint uVar3;
  
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  uVar6 = clidr_el1;
  uVar7 = (uVar6 & 0x7000000) >> 0x17;
  if (uVar7 != 0) {
    uVar11 = 0;
    do {
      if (1 < (uVar6 >> (uVar11 + (uVar11 >> 1) & 0x3f) & 7)) {
        uVar4 = daif;
        uVar5 = daif;
        daif = uVar5 | 0x80;
        csselr_el1 = uVar11;
        InstructionSynchronizationBarrier();
        uVar5 = ccsidr_el1;
        daif = uVar4;
        uVar8 = uVar5 >> 3 & 0x3ff;
        uVar2 = (uint)uVar8 | (uint)uVar8 >> 1;
        uVar3 = uVar2 | uVar2 >> 2;
        uVar2 = uVar3 | uVar3 >> 4 | uVar2 >> 8;
        uVar2 = ((uVar2 & 0xaaaaaaaa) >> 1) + (uVar2 & 0x55555555);
        uVar2 = ((uVar2 & 0xcccccccc) >> 2) + (uVar2 & 0x33333333);
        uVar2 = ((uVar2 & 0xf0f0f0f0) >> 4) + (uVar2 & 0xf0f0f0f);
        uVar10 = uVar8;
        uVar9 = uVar5 >> 0xd & 0x7fff;
        do {
          do {
            DC_CISW(uVar11 | uVar10 << ((ulong)(0x20 - ((uVar2 >> 8) + (uVar2 & 0xff00ff))) & 0x3f)
                    | uVar9 << (uVar5 & 7) + 4);
            bVar1 = 0 < (long)uVar10;
            uVar10 = uVar10 - 1;
          } while (bVar1);
          bVar1 = 0 < (long)uVar9;
          uVar10 = uVar8;
          uVar9 = uVar9 - 1;
        } while (bVar1);
      }
      uVar11 = uVar11 + 2;
    } while ((long)uVar11 < (long)uVar7);
  }
  csselr_el1 = 0;
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  InstructionSynchronizationBarrier();
  return;
}



void FUN_d900445c(void)

{
  IC_IALLUIS();
  InstructionSynchronizationBarrier();
  TLBI_ALLE3();
  return;
}



ulong FUN_d900446c(void)

{
  ulong uVar1;
  
  uVar1 = sctlr_el3;
  sctlr_el3 = uVar1 & 0xffffffffffffeffb;
  DataMemoryBarrier(3,3);
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  InstructionSynchronizationBarrier();
  return uVar1 & 0xffffffffffffeffb;
}



ulong FUN_d900448c(void)

{
  ulong uVar1;
  
  uVar1 = sctlr_el3;
  sctlr_el3 = uVar1 | 0x1004;
  DataMemoryBarrier(3,3);
  UnkSytemRegWrite(0,3,3,0xf,4,0);
  InstructionSynchronizationBarrier();
  return uVar1 | 0x1004;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90044ac(uint param_1)

{
  _SEC_SYS_CPU_POR_CFG0 = param_1 & 0xf | _SEC_SYS_CPU_POR_CFG0 & 0xfffffff0;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 bl1_main(void)

{
  uint uVar1;
  byte bVar2;
  uint uVar3;
  int iVar4;
  int ret;
  undefined4 te;
  byte *pbVar5;
  uint rcy;
  int local_20 [2];
  undefined4 local_18 [2];
  uint local_10;
  uint poc;
  
  FUN_d90059f0(5000);
  iVar4 = get_te();
  FUN_d900d4b4(0x10,iVar4);
  serial_puts(s_BL1__d900d600);
                    /* WARNING: Read-only address (ram,0xd900c580) is written */
  uRam00000000d900c580 = 0xa53a53;
  efuse__notsure();
  FUN_d9005750();
  get_secureboot_flag(local_18);
  serial_log(s_FEAT_d900d605,local_18[0],0x3b);
  pbVar5 = get_efuse_mirror__clone_questionmark();
  uVar3 = _DAT_c1107d54;
  uVar1 = _DAT_c1107d54 >> 6 & 1;
  poc = _DAT_c1107d54 >> 6 & 3;
  serial_log(s_POC_d900d60a,poc,0x3b);
                    /* WARNING: Read-only address (ram,0xd900c580) is written */
  uRam00000000d900c580 = uVar3;
  uVar3 = uVar3 >> 7;
  rcy = 0;
  if ((pbVar5[5] & 1) == 0) {
    rcy = FUN_d9007864(0xffffffa4);
    rcy = rcy & 0xff;
    serial_log(s_RCY_d900d60e,rcy,0x3b);
                    /* WARNING: Read-only address (ram,0xd900c580) is written */
    uRam00000000d900c580 = rcy;
  }
  _SEC_SYS_CPU_POR_CFG0 = _SEC_SYS_CPU_POR_CFG0 & 0xffffff0f | ((rcy & 3) << 2 | poc) << 4;
  te = get_te();
  FUN_d900d4b4(0x14,te);
  local_10 = uVar1 ^ 1;
  poc = 0;
  do {
    FUN_d9005a3c();
    if (((pbVar5[4] >> 6 & 1) == 0) && (rcy == 2)) {
      FUN_d90044ac(4);
      FUN_d9007518();
      do {
        ret = get_te();
      } while ((uint)(ret - iVar4) < 50000);
      ret = check_if_device_exists(0xd0072000);
      serial_log(s_SD_d900d612,ret,L';');
      if (ret == 0) {
        ret = read(0xd0072000,1,0xd9000000,0xc000);
        te = get_te();
        FUN_d900d4b4(0x18,te);
        serial_log(s_READ_d900d615,ret,0x3b);
        if (ret == 0) {
          ret = check__notsure(0xd9000000,0xc000,local_20);
          serial_log(s_CHK_d900d61a,ret,0x3b);
          if (ret == 0) {
LAB_d9004a0c:
            uRam00000000d900c580 = poc | 0xa530000;
                    /* WARNING: Read-only address (ram,0xd900c580) is written */
            set_rng_sec_config();
            load_image__notsure(local_20[0] + L'\xd9000000');
            return 1;
          }
        }
      }
    }
    if (((pbVar5[4] >> 4 & 1) == 0) && ((rcy == 1 || (local_10 != 0)))) {
      FUN_d90044ac(5);
      serial_log(s_boot_USB_d900d728 + 5,uVar1 + rcy * 2,0x3b);
      FUN_d9007df0(rcy == 0);
    }
    if (((pbVar5[4] >> 5 & 1) == 0) && ((uVar3 & 1) == 0)) {
      FUN_d90044ac(3);
      FUN_d9007974();
      FUN_d90079d4();
      ret = FUN_d9007a0c(uVar3 & 1,0xd9000000,0xc000);
      te = get_te();
      FUN_d900d4b4(0x18,te);
      serial_log(s_SPI_d900d61e,ret,0x3b);
      if (ret == 0) {
        ret = check__notsure(0xd9000000,0xc000,local_20);
        serial_log(s_CHK_d900d61a,ret,0x3b);
        if (ret == 0) goto LAB_d9004a0c;
      }
    }
    if (-1 < (char)pbVar5[4]) {
      FUN_d9007518();
      ret = check_if_device_exists(0xd0074000);
      serial_log(s_EMMC_d900d622,ret,L';');
      if (ret == 0) {
        FUN_d90044ac(1);
        ret = read(0xd0074000,0,0xd9000000,0xc000);
LAB_d9004858:
        serial_log(s_READ_d900d615,ret,L';');
      }
      else {
        FUN_d90044ac(2);
        FUN_d9005ac8();
        ret = FUN_d90069c8(0xd0074000);
        serial_log(s_NAND_d900d627,ret,L';');
        if (ret == 0) {
          ret = nand_read(1,0xd9000000,0xc000);
          goto LAB_d9004858;
        }
      }
      te = get_te();
      FUN_d900d4b4(0x18,te);
      if ((pbVar5[6] >> 5 & 1) != 0) {
        FUN_d9004d74(0xd9000000,0xd9000000,0xc000);
      }
      if (ret == 0) {
        ret = check__notsure(0xd9000000,0xc000,local_20);
        serial_log(s_CHK_d900d61a,ret,0x3b);
        if (ret == 0) goto LAB_d9004a0c;
      }
    }
    if (((pbVar5[4] >> 6 & 1) == 0) && (rcy != 2)) {
      FUN_d90044ac(4);
      FUN_d9007518();
      do {
        ret = get_te();
      } while ((uint)(ret - iVar4) < 50000);
      ret = check_if_device_exists(0xd0072000);
      serial_log(s_SD_d900d612,ret,0x3b);
      if (ret == 0) {
        ret = read(0xd0072000,1,0xd9000000,0xc000);
        te = get_te();
        FUN_d900d4b4(0x18,te);
        serial_log(s_READ_d900d615,ret,0x3b);
        if (ret == 0) {
          ret = check__notsure(0xd9000000,0xc000,local_20);
          serial_log(s_CHK_d900d61a,ret,0x3b);
          if (ret == 0) goto LAB_d9004a0c;
        }
      }
    }
    bVar2 = pbVar5[4];
    if ((((bVar2 >> 4 & 1) == 0) && (uVar1 != 0)) && (rcy != 1)) {
      FUN_d90044ac(5);
      serial_log(s_boot_USB_d900d728 + 5,8,0x3b);
      FUN_d9007df0(bVar2 >> 4 & 1);
    }
    poc = poc + 1;
    serial_log(s_LOOP_d900d62c,poc,0x3b);
  } while( true );
}



void serial_puts(char *param_1)

{
  for (; *param_1 != '\0'; param_1 = param_1 + 1) {
    if (*param_1 == '\n') {
      serial_putc(0xd);
    }
    serial_putc(*param_1);
  }
  return;
}



void FUN_d9004a94(uint param_1)

{
  uint uVar1;
  int iVar2;
  uint uVar3;
  
  serial_putc(0x30);
  serial_putc(0x78);
  uVar3 = 0x1c;
  do {
    uVar1 = param_1 >> (ulong)(uVar3 & 0x1f) & 0xf;
    iVar2 = uVar1 + 0x37;
    if (uVar1 < 10) {
      iVar2 = uVar1 + 0x30;
    }
    serial_putc(iVar2);
    uVar3 = uVar3 - 4;
  } while (uVar3 != 0xfffffffc);
  return;
}



void FUN_d9004aec(long param_1,int param_2)

{
  char cVar1;
  byte bVar2;
  uint uVar3;
  
  for (uVar3 = 0; (int)uVar3 < param_2; uVar3 = uVar3 + 1 & 0xffff) {
    bVar2 = *(byte *)(param_1 + (ulong)(ushort)uVar3) >> 4;
    cVar1 = bVar2 + 0x37;
    if (bVar2 < 10) {
      cVar1 = bVar2 + 0x30;
    }
    serial_putc(cVar1);
    bVar2 = *(byte *)(param_1 + (ulong)(ushort)uVar3) & 0xf;
    cVar1 = bVar2 + 0x37;
    if (bVar2 < 10) {
      cVar1 = bVar2 + 0x30;
    }
    serial_putc(cVar1);
  }
  return;
}



int FUN_d9004b6c(long param_1,long boot_mode,long string_len)

{
  byte *pbVar1;
  byte *pbVar2;
  int iVar3;
  long i;
  
  i = 0;
  do {
    if (i == string_len) {
      return 0;
    }
    pbVar1 = (byte *)(param_1 + i);
    pbVar2 = (byte *)(boot_mode + i);
    i = i + 1;
    iVar3 = (uint)*pbVar1 - (uint)*pbVar2;
  } while (iVar3 == 0);
  return iVar3;
}



void FUN_d9004b9c(undefined4 *param_1,undefined4 *param_2,long param_3)

{
  undefined4 *puVar1;
  undefined4 *puVar2;
  undefined4 *puVar3;
  long lVar4;
  
  puVar1 = (undefined4 *)((long)param_1 + param_3);
  puVar3 = puVar1;
  if (((((ulong)param_2 ^ (ulong)param_1) & 3) == 0) &&
     (puVar2 = (undefined4 *)((long)param_1 + 3U & 0xfffffffffffffffc), puVar2 <= puVar1)) {
    puVar3 = puVar2;
  }
  for (; param_1 < puVar3; param_1 = (undefined4 *)((long)param_1 + 1)) {
    *(undefined *)param_1 = *(undefined *)param_2;
    param_2 = (undefined4 *)((long)param_2 + 1);
  }
  for (; param_1 < (undefined4 *)((ulong)puVar1 & 0xfffffffffffffffc); param_1 = param_1 + 1) {
    *param_1 = *param_2;
    param_2 = param_2 + 1;
  }
  for (lVar4 = 0; (undefined4 *)((long)param_1 + lVar4) < puVar1; lVar4 = lVar4 + 1) {
    *(undefined *)((long)param_1 + lVar4) = *(undefined *)((long)param_2 + lVar4);
  }
  return;
}



void FUN_d9004c14(uint *param_1,byte param_2,long param_3)

{
  uint *puVar1;
  uint *puVar2;
  uint *puVar3;
  
  puVar1 = (uint *)((long)param_1 + param_3);
  puVar3 = (uint *)((long)param_1 + 3U & 0xfffffffffffffffc);
  puVar2 = puVar1;
  if (puVar3 <= puVar1) {
    puVar2 = puVar3;
  }
  for (; param_1 < puVar2; param_1 = (uint *)((long)param_1 + 1)) {
    *(byte *)param_1 = param_2;
  }
  for (; param_1 < (uint *)((ulong)puVar1 & 0xfffffffffffffffc); param_1 = param_1 + 1) {
    *param_1 = (uint)param_2 << 0x10 | (uint)param_2 << 8 | (uint)param_2 | (uint)param_2 << 0x18;
  }
  for (; param_1 < puVar1; param_1 = (uint *)((long)param_1 + 1)) {
    *(byte *)param_1 = param_2;
  }
  return;
}



void strlcpy(char *dst,char *src,int len)

{
  long i;
  
  if (0 < len) {
    for (i = 0; (len - (int)i != 1 && (src[i] != '\0')); i = i + 1) {
      dst[i] = src[i];
    }
    dst[i] = '\0';
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

long FUN_d9004cb4(int param_1)

{
  long lVar1;
  
  if (_DAT_d8fd0800 + param_1 < 0x4001) {
    lVar1 = (long)_DAT_d8fd0800 + 0xd8fd0808;
    if (lVar1 != 0) {
      _DAT_d8fd0800 = _DAT_d8fd0800 + param_1;
    }
  }
  else {
    lVar1 = 0;
  }
  return lVar1;
}



void FUN_d9004ce8(void)

{
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9004cec(void)

{
  _DAT_d8fd0800 = 0;
  return;
}



void FUN_d9004cfc(long param_1,long param_2,undefined8 param_3)

{
  long lVar1;
  int iVar2;
  ulong uVar3;
  undefined auStack_50 [32];
  undefined auStack_30 [16];
  undefined auStack_20 [16];
  undefined8 local_10;
  
  local_10 = param_3;
  FUN_d9007a38(auStack_20,auStack_50);
  checksum_check(auStack_30);
  iVar2 = (int)local_10;
  for (uVar3 = 0; (int)uVar3 != iVar2 / 0x10; uVar3 = (ulong)((int)uVar3 + 1)) {
    lVar1 = (uVar3 & 0x3fffffff) * 0x10;
    FUN_d9007b0c(auStack_20,auStack_30,param_1 + lVar1,param_2 + lVar1);
  }
  return;
}



void FUN_d9004d74(long param_1,long param_2,undefined8 param_3)

{
  long lVar1;
  int iVar2;
  ulong uVar3;
  undefined auStack_20 [16];
  undefined8 local_10;
  
  local_10 = param_3;
  lVar1 = get_efuse_mirror();
  FUN_d9007ba0(auStack_20,lVar1 + 0x70);
  iVar2 = (int)local_10;
  for (uVar3 = 0; (int)uVar3 != iVar2 / 8; uVar3 = (ulong)((int)uVar3 + 1)) {
    lVar1 = (uVar3 & 0x7fffffff) * 8;
    FUN_d9007c68(auStack_20,param_1 + lVar1,param_2 + lVar1);
  }
  return;
}



uint header_check__notsure(int *dev_address,long switch,int *param_3)

{
  undefined8 *puVar1;
  uint uVar2;
  bool bVar3;
  int iVar4;
  undefined4 uVar5;
  uint uVar6;
  byte *pbVar7;
  long lVar8;
  long lVar9;
  undefined8 uVar10;
  undefined *puVar11;
  long lVar12;
  short *psVar13;
  byte bVar14;
  undefined auStack_2b0 [8];
  ulong local_2a8;
  undefined auStack_2a0 [24];
  undefined auStack_288 [248];
  undefined auStack_190 [172];
  undefined auStack_e4 [36];
  undefined8 auStack_c0 [16];
  undefined auStack_40 [32];
  undefined auStack_20 [32];
  
  pbVar7 = get_efuse_mirror__clone_questionmark();
  if (*dev_address != 0x4c4d4140) {
    return 0xf3;
  }
  if (*(char *)((long)dev_address + 10) != 1) {
    return 0xf3;
  }
  if (*(char *)((long)dev_address + 11) != 0) {
    return 0xf3;
  }
  if (*(char *)(dev_address + 2) != '@') {
    return 0x10a;
  }
  if (dev_address[5] != L'@') {
    return 0x10a;
  }
  if (dev_address[4] == 0) {
    iVar4 = 0;
    if ((pbVar7[6] & 3) == 3) {
      bVar14 = 0;
      goto LAB_d9004ef4;
    }
  }
  else {
    if (3 < (uint)dev_address[4]) {
      return 0x154;
    }
    if (dev_address[6] != 0x200) {
      return 0x113;
    }
    bVar14 = 1;
    if (switch == 0) {
      if (dev_address[1] != 0x8c8) {
        return 0x132;
      }
      if (dev_address[0xb] + dev_address[7] != 0x8c8) {
        return 0x132;
      }
      if (dev_address[9] != 0x240) {
        return 0x138;
      }
      if (dev_address[10] != 0x4e8) {
        return 0x138;
      }
      if (dev_address[0xd] != 0x728) {
        return 0x13e;
      }
      if (dev_address[0xe] != 0x1a0) {
        return 0x13e;
      }
    }
LAB_d9004ef4:
    pbVar7 = get_efuse_mirror__clone_questionmark();
    if ((pbVar7[6] >> 2 & 1) != bVar14) {
      return 0x15b;
    }
    uVar6 = dev_address[7];
    uVar2 = dev_address[5];
    FUN_d9005794(auStack_190,0x100);
    FUN_d90057f0(auStack_190,dev_address,*(undefined *)(dev_address + 2));
    FUN_d90057f0(auStack_190,(long)dev_address + (ulong)uVar6,dev_address[0xb]);
    FUN_d9005804(auStack_190);
    if (bVar14 != 0) {
      FUN_d900a124(auStack_2b0,0,0);
      if (dev_address != (int *)0x0) {
        uVar6 = dev_address[9];
        lVar8 = (long)dev_address + (ulong)*(ushort *)((long)dev_address + (ulong)uVar6 + 0x18);
        psVar13 = (short *)((long)dev_address +
                           (ulong)*(ushort *)((long)dev_address + (ulong)uVar6 + 0x14));
        FUN_d9005818(lVar8,*(undefined2 *)((long)dev_address + (ulong)uVar6 + 0x1a),auStack_20,0);
        for (iVar4 = 0; iVar4 < (int)(uint)*(byte *)((long)dev_address + (ulong)uVar6 + 0x10);
            iVar4 = iVar4 + 1) {
          if (*psVar13 == 1) {
            puVar11 = auStack_2a0;
          }
          else {
            if (*psVar13 != 2) {
              do {
                    /* WARNING: Do nothing block with infinite loop */
              } while( true );
            }
            puVar11 = auStack_288;
          }
          FUN_d900b460(puVar11,lVar8,psVar13[2]);
          lVar8 = lVar8 + (ulong)(ushort)psVar13[2];
          psVar13 = psVar13 + 4;
        }
        lVar8 = FUN_d900b3e4(auStack_2a0);
        local_2a8 = lVar8 + 7U >> 3;
        if (((local_2a8 == 0x100) || (local_2a8 == 0x200)) || (local_2a8 == 0x80)) {
          bVar3 = false;
          do {
            if (switch == 0) {
              lVar8 = (long)dev_address + (ulong)(uint)dev_address[0xd];
              if (bVar3) {
                if (param_3 != (int *)0x0) {
                  *param_3 = dev_address[1];
                }
                uVar6 = header_check__notsure
                                  ((int *)((long)dev_address + (ulong)(uint)dev_address[1]),
                                   lVar8 + 0xd0,param_3);
                if (uVar6 == 0) {
                  return 0;
                }
                return uVar6 | 0x400;
              }
              if ((((*(int *)((long)dev_address + (ulong)(uint)dev_address[0xd]) != 0x584d4b40) ||
                   (*(char *)(lVar8 + 4) != '\x01')) ||
                  ((*(short *)(lVar8 + 6) != 0x10 ||
                   ((0x3fef < *(int *)(lVar8 + 8) - 0x11U || (*(char *)(lVar8 + 0xc) != '\x04'))))))
                 || (*(char *)(lVar8 + 0xd) != '0')) {
                return 0x1a8;
              }
              for (iVar4 = 0; iVar4 < (int)(uint)*(byte *)(lVar8 + 0xc); iVar4 = iVar4 + 1) {
                lVar12 = (long)iVar4;
                lVar9 = lVar8 + lVar12 * 0x30;
                if ((byte)(*(char *)(lVar9 + 0x19) - 5U) < 4) {
                  puVar1 = (undefined8 *)(lVar8 + lVar12 * 0x30 + 0x20);
                  uVar10 = puVar1[1];
                  auStack_c0[lVar12 * 4] = *puVar1;
                  auStack_c0[lVar12 * 4 + 1] = uVar10;
                  uVar10 = puVar1[3];
                  auStack_c0[lVar12 * 4 + 2] = puVar1[2];
                  auStack_c0[lVar12 * 4 + 3] = uVar10;
                }
                else {
                  FUN_d9005818(lVar8 + (ulong)*(ushort *)(lVar9 + 0x28),
                               *(undefined2 *)(lVar9 + 0x2a),auStack_c0 + lVar12 * 4,0);
                }
              }
              FUN_d9005818(auStack_c0,(ulong)*(byte *)(lVar8 + 0xc) << 5,auStack_40,0);
              lVar9 = get_efuse_mirror();
              iVar4 = FUN_d9004b6c(auStack_40,lVar9 + 0x30,0x20);
              if (iVar4 != 0) {
                return 0x1a8;
              }
              iVar4 = FUN_d90053b4(lVar8,auStack_20,1);
              if (iVar4 != 0) {
                return 0x1b0;
              }
            }
            else {
              if (bVar3) {
                if (param_3 != (int *)0x0) {
                  *param_3 = *param_3 + dev_address[0xd];
                }
                return 0;
              }
              iVar4 = FUN_d90053b4(switch,auStack_20);
              if (iVar4 != 0) {
                return 0x191;
              }
            }
            iVar4 = FUN_d900a4c8(auStack_2b0,0,0xb,0x20,auStack_e4,(long)dev_address + (ulong)uVar2)
            ;
            if (iVar4 != 0) {
              if (switch == 0) {
                uVar5 = get_te();
                uVar10 = 0x24;
              }
              else {
                uVar5 = get_te();
                uVar10 = 0x20;
              }
              FUN_d900d4b4(uVar10,uVar5);
              return 0x1cb;
            }
            if (switch == 0) {
              uVar5 = get_te();
              uVar10 = 0x24;
            }
            else {
              uVar5 = get_te();
              uVar10 = 0x20;
            }
            FUN_d900d4b4(uVar10,uVar5);
            bVar3 = true;
          } while( true );
        }
      }
      return 0x180;
    }
    iVar4 = FUN_d9004b6c(auStack_e4,(long)dev_address + (ulong)(uint)dev_address[5],0x20);
  }
  if ((iVar4 == 0) && (param_3 != (int *)0x0)) {
    *param_3 = dev_address[0xd];
  }
  else {
    uVar6 = 0x17a;
    if (iVar4 != 0) goto LAB_d9004f88;
  }
  uVar6 = 0;
LAB_d9004f88:
  uVar5 = get_te();
  FUN_d900d4b4(0x24,uVar5);
  return uVar6;
}



int check__notsure(long address,ulong len,int *param_3)

{
  undefined4 te;
  int ret;
  long efuse_mirror;
  byte *efuse_mirror_2;
  undefined4 secureboot_flag;
  undefined4 secureboot_flag_2;
  
  efuse_mirror = get_efuse_mirror();
  efuse_mirror_2 = get_efuse_mirror__clone_questionmark();
  secureboot_flag = 0;
  get_secureboot_flag(&secureboot_flag);
  if ((secureboot_flag._3_1_ >> 5 & 1) != 0) {
    FUN_d9004390();
    FUN_d900445c();
    FUN_d900448c();
  }
  if ((efuse_mirror_2[7] >> 4 & 1) != 0) {
    FUN_d9007a38(&secureboot_flag,efuse_mirror + 0x50);
    checksum_check(0);
    for (ret = 0; ret != (int)((len & 0xffffffff) >> 4); ret = ret + 1) {
      efuse_mirror = address + (long)ret * 0x10;
      FUN_d9007b0c(&secureboot_flag,0,efuse_mirror,efuse_mirror);
    }
    te = get_te();
    FUN_d900d4b4(0x1c,te);
  }
  ret = header_check__notsure((int *)(address + 0x10),0,param_3);
  if ((ret == 0) && (param_3 != (int *)0x0)) {
    *param_3 = *param_3 + 0x10;
  }
  secureboot_flag_2 = 0;
  get_secureboot_flag();
  if ((secureboot_flag_2._3_1_ >> 5 & 1) != 0) {
    FUN_d9004390();
    FUN_d900445c();
    FUN_d900446c();
  }
  return ret;
}



undefined8 FUN_d90053b4(long param_1,undefined8 param_2,int param_3)

{
  byte bVar1;
  byte *pbVar2;
  undefined8 uVar3;
  uint uVar4;
  uint uVar5;
  
  pbVar2 = get_efuse_mirror__clone_questionmark();
  uVar4 = 0xe;
  if (param_3 != 0) {
    bVar1 = pbVar2[5];
    uVar4 = bVar1 >> 4 & 1;
    if ((bVar1 >> 5 & 1) != 0) {
      uVar4 = uVar4 | 2;
    }
    if ((bVar1 >> 6 & 1) != 0) {
      uVar4 = uVar4 | 4;
    }
    if ((char)bVar1 < '\0') {
      uVar4 = uVar4 | 8;
    }
  }
  bVar1 = *(byte *)(param_1 + 0xc);
  uVar5 = 0;
  while( true ) {
    if ((int)(uint)bVar1 <= (int)uVar5) {
      return 0xffffffff;
    }
    if ((((int)uVar4 >> (uVar5 & 0x1f) & 1U) == 0) &&
       (uVar3 = FUN_d9004b6c(param_2,param_1 + (long)(int)uVar5 * 0x30 + 0x20,0x20), (int)uVar3 == 0
       )) break;
    uVar5 = uVar5 + 1;
  }
  return uVar3;
}



byte FUN_d9005460(int param_1)

{
  undefined4 local_10;
  
  local_10 = 0;
  get_secureboot_flag(&local_10);
  if (param_1 == 1) {
  }
  else {
    if (param_1 == 0) {
      return local_10._1_1_ >> 3 & 1;
    }
    if (param_1 != 2) {
      return 0;
    }
    local_10._0_1_ = local_10._2_1_;
  }
  return (byte)local_10 >> 1 & 1;
}



undefined8 FUN_d90054bc(long param_1,ulong param_2,uint param_3)

{
  bool bVar1;
  undefined4 uVar2;
  int iVar3;
  long lVar4;
  long lVar5;
  uint uVar6;
  undefined8 local_70;
  undefined8 uStack_68;
  undefined8 local_60;
  uint local_58;
  uint auStack_54 [5];
  undefined8 local_40;
  undefined8 uStack_38;
  undefined8 local_30;
  undefined4 local_28;
  undefined8 local_20;
  undefined8 uStack_18;
  undefined8 local_10;
  uint local_8;
  
  param_2 = param_2 & 0xffffffff;
  iVar3 = FUN_d9005460(param_3);
  if (iVar3 == 0) {
    return 1;
  }
  if (param_1 == 0) {
    return 0;
  }
  bVar1 = 0xf < (int)param_2 - 1U;
  local_8 = (uint)bVar1;
  if (local_8 != 0) {
    return 0;
  }
  if (3 < param_3) {
    return 0;
  }
  local_20 = 0;
  uStack_18 = 0;
  local_10 = 0;
  FUN_d9004b9c(auStack_54,param_1,param_2);
  lVar4 = get_efuse_mirror();
  *(uint *)((long)auStack_54 + param_2) = (uint)bVar1;
  if (lVar4 != 0) {
    if (param_3 < 2) {
      uVar2 = *(undefined4 *)(lVar4 + 0x9c);
    }
    else {
      if (param_3 != 2) goto LAB_d9005564;
      uVar2 = *(undefined4 *)(lVar4 + 0xbc);
    }
    *(undefined4 *)((long)auStack_54 + param_2) = uVar2;
  }
LAB_d9005564:
  iVar3 = 0x21;
  while (iVar3 = iVar3 + -1, iVar3 != 0) {
    local_60 = local_10;
    local_58 = local_8;
    local_70 = local_20;
    uStack_68 = uStack_18;
    FUN_d9005818(&local_70,(int)param_2 + 0x20,&local_20,1);
  }
  lVar4 = get_efuse_mirror();
  if (lVar4 != 0) {
    if (param_3 < 2) {
      local_40 = *(undefined8 *)(lVar4 + 0x80);
      uStack_38 = *(undefined8 *)(lVar4 + 0x88);
      lVar5 = lVar4 + 0x80;
      local_30 = *(undefined8 *)(lVar4 + 0x90);
    }
    else {
      if (param_3 != 2) goto LAB_d90055ec;
      local_40 = *(undefined8 *)(lVar4 + 0xa0);
      uStack_38 = *(undefined8 *)(lVar4 + 0xa8);
      lVar5 = lVar4 + 0xa0;
      local_30 = *(undefined8 *)(lVar4 + 0xb0);
    }
    local_28 = *(undefined4 *)(lVar5 + 0x18);
  }
LAB_d90055ec:
  iVar3 = FUN_d9004b6c(&local_40,&local_20,0x1c);
  if (iVar3 != 0) {
    if (param_3 == 0) {
      DAT_d8fd4808 = DAT_d8fd4808 + 1;
      uVar6 = (uint)DAT_d8fd4808;
    }
    else if (param_3 == 1) {
      DAT_d8fd4809 = DAT_d8fd4809 + 1;
      uVar6 = (uint)DAT_d8fd4809;
    }
    else {
      DAT_d8fd480a = DAT_d8fd480a + 1;
      uVar6 = (uint)DAT_d8fd480a;
    }
    for (iVar3 = 0; iVar3 < 1 << (ulong)(uVar6 - 1 & 0x1f); iVar3 = iVar3 + 1) {
      FUN_d9005a3c();
      FUN_d90059c8(1000000);
    }
    if (2 < uVar6) {
      serial_puts(s_auth_failed__reboot____d900d631);
      FUN_d9005a70();
    }
    return 0;
  }
  if (param_3 == 0) {
    DAT_d8fd4808 = 0;
  }
  else if (param_3 == 1) {
    DAT_d8fd4809 = 0;
  }
  else {
    DAT_d8fd480a = 0;
  }
  return 1;
}



void FUN_d90056dc(long param_1)

{
  undefined *puVar1;
  char cVar2;
  long lVar3;
  
  lVar3 = 0;
  do {
    puVar1 = (undefined *)(param_1 + lVar3);
    lVar3 = lVar3 + 1;
    serial_putc(*puVar1);
  } while (lVar3 != 5);
  cVar2 = *(char *)(param_1 + 0xe);
  if (cVar2 != '+') {
    cVar2 = *(char *)(param_1 + 5);
  }
  serial_putc(cVar2);
  return;
}



void efuse__notsure(void)

{
  char *efuse_mirror;
  
  efuse_mirror = (char *)get_efuse_mirror();
  if (efuse_mirror != (char *)0x0) {
    strlcpy(efuse_mirror + 240,s_08dafda0fd31778_d900d648,0x10);
    return;
  }
  return;
}



void FUN_d9005750(void)

{
  long lVar1;
  
  lVar1 = get_efuse_mirror();
  FUN_d90056dc(s_08dafda0fd31778_d900d694);
  serial_putc(0x3a);
  if (lVar1 != 0) {
    FUN_d90056dc(lVar1 + 0xe0);
  }
  serial_putc(0x3b);
  return;
}



void FUN_d9005794(undefined8 param_1,undefined8 param_2)

{
  byte *pbVar1;
  
  pbVar1 = get_efuse_mirror__clone_questionmark();
  if ((pbVar1 != (byte *)0x0) && ((pbVar1[7] >> 5 & 1) != 0)) {
    DAT_d8fd480b = '\x01';
  }
  if (DAT_d8fd480b != '\0') {
    FUN_d9009d90(param_1);
    return;
  }
  FUN_d90099fc(param_1,param_2);
  return;
}



void FUN_d90057f0(void)

{
  if (DAT_d8fd480b != '\0') {
    FUN_d9009f2c();
    return;
  }
  FUN_d9009adc();
  return;
}



void FUN_d9005804(void)

{
  if (DAT_d8fd480b != '\0') {
    FUN_d900a038();
    return;
  }
  FUN_d9009bac();
  return;
}



void FUN_d9005818(undefined8 param_1,undefined4 param_2,undefined8 param_3,int param_4)

{
  int iVar1;
  undefined8 uVar2;
  undefined auStack_d0 [208];
  
  iVar1 = 0x100;
  if (param_4 != 0) {
    iVar1 = 0xe0;
  }
  FUN_d9005794(auStack_d0,iVar1);
  FUN_d90057f0(auStack_d0,param_1,param_2);
  uVar2 = FUN_d9005804(auStack_d0);
  FUN_d9004b9c(param_3,uVar2,iVar1 >> 3);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

uint serial_putc(uint param_1)

{
  byte *pbVar1;
  
  pbVar1 = get_efuse_mirror__clone_questionmark();
  if ((pbVar1[5] >> 1 & 1) != 0) {
    param_1 = L'.';
  }
  do {
  } while ((_DAT_c81004cc >> 0x15 & 1) != 0);
  _DAT_c81004c0 = param_1 & 0xff;
  return param_1;
}



undefined8 FUN_d90058e4(char *param_1)

{
  for (; *param_1 != '\0'; param_1 = param_1 + 1) {
    serial_putc();
  }
  return 1;
}



undefined8 serial_log(char *prefix,int return,char separator)

{
  uint uVar1;
  int iVar2;
  uint uVar3;
  
  for (; *prefix != '\0'; prefix = prefix + 1) {
    serial_putc();
  }
  serial_putc(L':');
  uVar3 = 7;
  do {
    if ((return >> ((uVar3 & 7) << 2) & 0xfU) != 0) break;
    uVar3 = uVar3 - 1;
  } while (uVar3 != 0);
  do {
    uVar1 = return >> ((uVar3 & 7) << 2) & 0xf;
    iVar2 = uVar1 + 0x37;
    if (uVar1 < 10) {
      iVar2 = uVar1 + 0x30;
    }
    serial_putc(iVar2);
    uVar3 = uVar3 - 1;
  } while (uVar3 != 0xffffffff);
  if (separator != '\0') {
    serial_putc(separator);
  }
  return 1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 get_te(void)

{
  return _DAT_c1109988;
}



/* WARNING: Removing unreachable block (ram,0xd90059ec) */

void FUN_d90059c8(void)

{
                    /* WARNING: Do nothing block with infinite loop */
  do {
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90059f0(undefined4 param_1)

{
  _DAT_c11098d8 = param_1;
  _DAT_c11098dc = 0;
  _DAT_c11098d0 = 0x3245dbf;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9005a3c(void)

{
  _DAT_c11098dc = 0;
  return;
}



void FUN_d9005a70(void)

{
  int iVar1;
  
  do {
    iVar1 = 100;
    do {
      iVar1 = iVar1 + -1;
    } while (iVar1 != 0);
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9005ac8(void)

{
  _DAT_c8834528 = 0xffff87ff;
  _DAT_c88344f0 = 0xffff8700;
  _DAT_c88344c0 = 0xfff00000;
  return;
}



/* WARNING: Removing unreachable block (ram,0xd9005be0) */
/* WARNING: Removing unreachable block (ram,0xd9005be4) */
/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 FUN_d9005afc(void)

{
  _DAT_d0074800 = 0x33800;
  return 0;
}



void FUN_d9005bfc(uint param_1,undefined4 *param_2)

{
  uint uVar1;
  int iVar2;
  undefined4 uVar3;
  ulong uVar4;
  uint uVar5;
  ulong uVar6;
  long lVar7;
  byte local_20 [4];
  undefined local_1c;
  undefined local_1b;
  undefined local_1a;
  undefined local_19;
  undefined local_18;
  undefined local_17;
  undefined local_16;
  undefined local_15;
  undefined local_14;
  undefined local_13;
  undefined local_12;
  undefined local_11;
  undefined local_10;
  undefined local_f;
  undefined local_e;
  undefined local_d;
  undefined local_c;
  undefined local_b;
  undefined local_a;
  undefined local_9;
  undefined local_8;
  undefined local_7;
  undefined local_6;
  undefined local_5;
  undefined local_4;
  undefined local_3;
  undefined local_2;
  undefined local_1;
  
  local_1a = 0x78;
  local_19 = 0x78;
  local_17 = 0x76;
  local_e = 0x76;
  local_b = 0x76;
  local_20[0] = 4;
  local_20[1] = 4;
  local_16 = 0x74;
  local_14 = 8;
  local_13 = 8;
  local_d = 0x74;
  local_9 = 0x70;
  local_20[2] = 0x7c;
  local_20[3] = 0x7e;
  local_1c = 0;
  local_1b = 0x7c;
  local_18 = 0x7c;
  local_15 = 0x72;
  local_12 = 0;
  local_11 = 0;
  local_10 = 0xb;
  local_f = 0x7e;
  local_c = 0x10;
  local_a = 0x72;
  local_8 = 2;
  local_7 = 0;
  local_6 = 0x7e;
  local_5 = 0x7c;
  local_4 = 0;
  local_3 = 0;
  local_2 = 0;
  local_1 = 0;
  uVar1 = param_1;
  if (param_1 == 7) {
    param_1 = 0xff;
LAB_d9005d10:
    uVar4 = 0;
  }
  else {
    if (param_1 == 0xff) {
      uVar1 = 7;
      goto LAB_d9005d10;
    }
    uVar5 = 0;
    if (7 < param_1) goto LAB_d9005e84;
    if (param_1 == 0) {
      *param_2 = 0x1785c;
      param_2[1] = 0x178c5;
      param_2[2] = 0x33800;
      uVar4 = 3;
    }
    else {
      uVar4 = 0;
    }
  }
  lVar7 = 0;
  uVar6 = uVar4;
  do {
    param_2[uVar6] = 0x17855;
    iVar2 = (int)uVar6;
    param_2[iVar2 + 1] = 0x33802;
    param_2[iVar2 + 2] = (int)lVar7 + 4U | 0x1b800;
    param_2[iVar2 + 3] = 0x33802;
    param_2[iVar2 + 4] = local_20[uVar1 * 4 + (int)lVar7] | 0x13800;
    lVar7 = lVar7 + 1;
    param_2[iVar2 + 5] = 0x33800;
    uVar6 = (ulong)(iVar2 + 6);
  } while (lVar7 != 4);
  iVar2 = (int)uVar4;
  param_2[iVar2 + 0x18] = 0x17855;
  param_2[iVar2 + 0x19] = 0x33802;
  param_2[iVar2 + 0x1a] = 0x1b80d;
  param_2[iVar2 + 0x1b] = 0x33802;
  param_2[iVar2 + 0x1c] = 0x13800;
  uVar1 = iVar2 + 0x1e;
  param_2[iVar2 + 0x1d] = 0x33800;
  if (param_1 == 6) {
    param_2[uVar1] = 0x178b3;
    uVar1 = iVar2 + 0x20;
    param_2[iVar2 + 0x1f] = 0x33800;
LAB_d9005e48:
    param_2[uVar1] = 0x17826;
    param_2[uVar1 + 1] = 0x1785d;
    uVar5 = uVar1 + 3;
    uVar3 = 0x33800;
    uVar4 = (ulong)(uVar1 + 2);
  }
  else {
    if (param_1 != 0xff) goto LAB_d9005e48;
    param_2[uVar1] = 0x178ff;
    param_2[iVar2 + 0x1f] = 0x33802;
    uVar5 = iVar2 + 0x21;
    uVar4 = (ulong)(iVar2 + 0x20);
    if (DAT_d8fd4812 < '\0') {
      param_2[uVar4] = 0x17870;
      param_2[uVar5] = 0x33802;
      uVar5 = iVar2 + 0x23;
      param_2[iVar2 + 0x22] = 0x142c0d;
      goto LAB_d9005e84;
    }
    uVar3 = 0x10380d;
  }
  param_2[uVar4] = uVar3;
LAB_d9005e84:
  param_2[uVar5] = 0;
  return;
}



void FUN_d9005ef0(uint param_1,undefined4 *param_2)

{
  undefined4 *puVar1;
  long lVar2;
  uint uVar3;
  byte abStack_40 [64];
  
  FUN_d9004b9c(abStack_40,&DAT_d900d6a8,0x3f);
  if (param_1 == 0x15) {
LAB_d900605c:
    *param_2 = 0x1783b;
    param_2[1] = 0x178b9;
    param_2[2] = 0x17854;
    param_2[3] = 0x1b804;
    param_2[4] = 0x13800;
    param_2[5] = 0x17854;
    param_2[6] = 0x1b805;
    param_2[7] = 0x13800;
    param_2[8] = 0x17854;
    param_2[9] = 0x1b807;
    param_2[10] = 0x13800;
    uVar3 = 0x78d6;
  }
  else {
    if (param_1 == 0) {
      *param_2 = 0x1783b;
      param_2[1] = 0x178b9;
      uVar3 = 4;
      puVar1 = param_2 + 2;
      do {
        *puVar1 = 0x17854;
        puVar1[1] = uVar3 | 0x1b800;
        uVar3 = uVar3 + 1;
        puVar1[2] = 0x13800;
        puVar1 = puVar1 + 3;
      } while (uVar3 != 0xd);
      param_2[0x1d] = 0x178b6;
      lVar2 = 0x1e;
      goto LAB_d90060f0;
    }
    if (0x14 < param_1) {
      lVar2 = 0;
      if (param_1 != 0xff) goto LAB_d90060f0;
      goto LAB_d900605c;
    }
    *param_2 = 0x1783b;
    param_2[1] = 0x178b9;
    param_2[2] = 0x17854;
    param_1 = param_1 * 3;
    param_2[3] = 0x1b804;
    param_2[4] = abStack_40[param_1] | 0x13800;
    param_2[5] = 0x17854;
    param_2[6] = 0x1b805;
    param_2[7] = abStack_40[param_1 + 1] | 0x13800;
    param_2[8] = 0x17854;
    param_2[9] = 0x1b807;
    param_2[10] = abStack_40[param_1 + 2] | 0x13800;
    uVar3 = 0x78b6;
  }
  param_2[0xb] = uVar3 | 0x10000;
  lVar2 = 0xc;
LAB_d90060f0:
  param_2[lVar2] = 0;
  return;
}



void FUN_d9006100(uint param_1,undefined4 *param_2)

{
  long lVar1;
  
  if (param_1 == 0xff) {
    param_1 = 0;
  }
  else {
    lVar1 = 0;
    if (7 < param_1) goto LAB_d90061c8;
  }
  *param_2 = 0x178ef;
  param_2[1] = 0x1b889;
  param_2[2] = 0x33803;
  param_2[3] = param_1 | 0x13800;
  param_2[4] = 0x13800;
  param_2[5] = 0x13800;
  param_2[6] = 0x13800;
  param_2[7] = 0x33802;
  if (DAT_d8fd4812 < '\0') {
    param_2[8] = 0x17870;
    param_2[9] = 0x33802;
    param_2[10] = 0x142c0d;
    lVar1 = 0xb;
  }
  else {
    param_2[8] = 0x10380d;
    lVar1 = 9;
  }
LAB_d90061c8:
  param_2[lVar1] = 0;
  return;
}



void FUN_d90061d4(uint param_1,undefined4 *param_2)

{
  long lVar1;
  undefined4 *puVar2;
  byte local_50 [64];
  byte local_10 [16];
  
  local_10[0] = 0xa7;
  local_10[1] = 0xa4;
  local_10[2] = 0xa5;
  local_10[3] = 0xa6;
  FUN_d9004b9c(local_50,&DAT_d900d6e8,0x3c);
  if (param_1 == 0xff) {
    param_1 = 0;
  }
  else {
    lVar1 = 0;
    if (0xe < param_1) goto LAB_d90062c0;
  }
  lVar1 = 0;
  puVar2 = param_2;
  do {
    *puVar2 = 0x178a1;
    puVar2[1] = 0x1b800;
    puVar2[2] = local_10[lVar1] | 0x1b800;
    puVar2[3] = 0x33802;
    puVar2[4] = local_50[param_1 * 4 + (int)lVar1] | 0x13800;
    lVar1 = lVar1 + 1;
    puVar2[5] = 0x33808;
    puVar2 = puVar2 + 6;
  } while (lVar1 != 4);
  lVar1 = 0x18;
LAB_d90062c0:
  param_2[lVar1] = 0;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90062d0(int param_1,long param_2)

{
  byte bVar1;
  long lVar2;
  uint uVar3;
  ulong uVar4;
  
  if ((param_1 == 0xff) || (param_1 == 0)) {
    _DAT_d8fd4818 = 0;
  }
  uVar3 = 0;
  while( true ) {
    lVar2 = (ulong)(uVar3 + _DAT_d8fd4818) * 2;
    bVar1 = *(byte *)(lVar2 + 0xd900c418);
    uVar4 = (ulong)uVar3;
    if (bVar1 == 0) break;
    uVar3 = uVar3 + 1;
    *(uint *)(param_2 + uVar4 * 4) = *(byte *)(lVar2 + 0xd900c419) | 0x3800 | (uint)bVar1 << 0xe;
  }
  *(undefined4 *)(param_2 + uVar4 * 4) = 0;
  _DAT_d8fd4818 = uVar3 + _DAT_d8fd4818 + 1;
  return;
}



void FUN_d9006340(undefined8 param_1,long param_2)

{
  long lVar1;
  long lVar2;
  uint uVar3;
  uint uVar4;
  
  lVar1 = get_efuse_mirror();
  lVar2 = 0;
  uVar3 = 0;
  do {
    uVar4 = (uint)*(byte *)(lVar1 + 0x120 + lVar2);
    if (uVar4 == 0) break;
    uVar3 = uVar3 + 1;
    *(uint *)(param_2 + lVar2 * 2) =
         (uint)*(byte *)(lVar1 + 0x120 + lVar2 + 1) | uVar4 << 0xe | 0x3800;
    lVar2 = lVar2 + 2;
  } while (uVar3 != 8);
  *(undefined4 *)(param_2 + (ulong)uVar3 * 4) = 0;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900639c(uint param_1)

{
  uint *puVar1;
  uint uVar2;
  long lVar3;
  uint local_200 [128];
  
  if (_DAT_d8fd4816 != 0) {
    uVar2 = param_1;
    if ((param_1 != 0) && (uVar2 = _DAT_d8fd481c, param_1 == 2)) {
      uVar2 = 0xff;
    }
    _DAT_d8fd481c = uVar2;
    if (_DAT_d8fd4814 == 0x98) {
      FUN_d9005bfc(_DAT_d8fd481c,local_200);
    }
    else if (_DAT_d8fd4814 < 0x99) {
      if (_DAT_d8fd4814 == 0x2c) {
        FUN_d9006100(_DAT_d8fd481c,local_200);
      }
      else if (_DAT_d8fd4814 == 0x45) {
        FUN_d9005ef0(_DAT_d8fd481c,local_200);
      }
    }
    else if (_DAT_d8fd4814 == 0x100) {
      FUN_d90062d0(_DAT_d8fd481c,local_200);
    }
    else if (_DAT_d8fd4814 == 0x101) {
      FUN_d9006340(_DAT_d8fd481c,local_200);
    }
    else if (_DAT_d8fd4814 == 0xec) {
      FUN_d90061d4(_DAT_d8fd481c,local_200);
    }
    _DAT_d8fd481c = _DAT_d8fd481c + 1;
    if (_DAT_d8fd4816 <= _DAT_d8fd481c) {
      _DAT_d8fd481c = 0;
    }
    lVar3 = 0;
    uVar2 = _DAT_d0074800;
    do {
      _DAT_d0074800 = uVar2;
      if ((_DAT_d0074800 >> 0x16 & 0x1f) == 0x1f) {
        return;
      }
      puVar1 = (uint *)((long)local_200 + lVar3);
      lVar3 = lVar3 + 4;
      uVar2 = *puVar1;
    } while (*puVar1 != 0);
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 FUN_d90064d4(undefined8 param_1,undefined4 param_2)

{
  uint uVar1;
  uint uVar2;
  uint uVar3;
  ulong uVar4;
  undefined4 *puVar5;
  
  uVar4 = (ulong)_DAT_d8fd4810 & 0x3fffff;
  uVar1 = (uint)uVar4 & 0x3f;
  if (((uint)uVar4 >> 0xd & 1) == 0) {
    uVar3 = 0x80;
    if (((uint)(uVar4 >> 0xe) & 7) < 2) {
      uVar3 = 0x40;
    }
    uVar4 = (ulong)uVar3;
  }
  else {
    uVar4 = uVar4 >> 6 & 0x7f;
  }
  do {
  } while ((_DAT_d0074800 >> 0x16 & 0x1f) != 0);
  _DAT_d007480c = 0xd900c600;
  _DAT_d0074800 = 0x33800;
  _DAT_d0074808 = param_2;
  for (uVar3 = 0; uVar2 = 0, uVar3 < uVar1; uVar3 = uVar3 + 1) {
    for (; uVar2 < (int)uVar4 + 1U; uVar2 = uVar2 + 1) {
      while ((_DAT_d0074818 >> 0xb & 1) == 0) {
        if ((_DAT_d0074800 >> 0x1b & 1) != 0) {
          _DAT_d0074800 = 0x80000000;
          return 0x81;
        }
      }
      puVar5 = (undefined4 *)(ulong)_DAT_d007481c;
      if (puVar5 < (undefined4 *)0xd9014000) {
        *puVar5 = _DAT_d0074820;
        puVar5[1] = _DAT_d0074824;
      }
    }
  }
  while( true ) {
    if (uVar1 <= uVar2) {
      return 0;
    }
    uVar3 = *(uint *)(ulong)((uVar2 + 0x1b2018c0) * 8);
    if ((uVar3 >> 0x18 & 0x3f) == 0x3f) break;
    if ((uVar2 == 0) && ((uVar3 & 0xc000ffff) != 0xc000aa55)) {
      return 0x83;
    }
    uVar2 = uVar2 + 1;
  }
  if ((_DAT_d8fd4810 & 0x1000000) != 0) {
    FUN_d9005afc();
  }
  if (9 < (uVar3 >> 0x10 & 0x3f)) {
    return 0x82;
  }
  return 0x85;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 nand_read(uint param_1,int param_2,uint param_3)

{
  byte bVar1;
  ulong uVar2;
  int iVar3;
  undefined8 uVar4;
  uint uVar5;
  byte *pbVar6;
  int iVar7;
  uint uVar8;
  uint uVar9;
  
  uVar2 = (ulong)_DAT_d8fd4810 & 0x3fffff;
  if (((uint)uVar2 >> 0xd & 1) == 0) {
    iVar3 = 0x400;
    if (((uint)(uVar2 >> 0xe) & 7) < 2) {
      iVar3 = 0x200;
    }
  }
  else {
    iVar3 = ((uint)(uVar2 >> 6) & 0x7f) << 3;
  }
  pbVar6 = (byte *)0xd900c408;
  uVar5 = 0;
LAB_d9006920:
  if (uVar5 < param_3) {
    if ((DAT_d8fd4813 >> 2 & 1) == 0) {
      uVar8 = param_1 + 1;
      uVar9 = param_1;
    }
    else {
      bVar1 = *pbVar6;
      pbVar6 = pbVar6 + 1;
      uVar8 = param_1;
      uVar9 = (uint)bVar1;
    }
    param_1 = uVar8;
    uVar8 = 0;
    while( true ) {
      iVar7 = 0;
      do {
        uVar4 = FUN_d90064d4(iVar7 + uVar9,uVar5 + param_2);
        if ((int)uVar4 != 0x82) {
          if ((int)uVar4 != 0) {
            return uVar4;
          }
          uVar5 = uVar5 + iVar3 * ((uint)uVar2 & 0x3f);
          goto LAB_d9006920;
        }
        iVar7 = iVar7 + 0x100;
      } while (iVar7 != 0x400);
      if (_DAT_d8fd4816 <= uVar8) break;
      FUN_d900639c(1);
      uVar8 = uVar8 + 1;
    }
  }
  else {
    uVar4 = 0;
  }
  return uVar4;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

ulong FUN_d90069c8(undefined4 *param_1)

{
  uint uVar1;
  byte *pbVar2;
  ulong uVar3;
  
  pbVar2 = get_efuse_mirror__clone_questionmark();
  _DAT_d8fd4810 =
       CONCAT13((char)((pbVar2[7] & 1) << 4),CONCAT12((char)((pbVar2[6] >> 7 ^ 1) << 7),0xec01)) |
       0x806b0000;
  *param_1 = 0x81000201;
  _DAT_d0074804 = (_DAT_d8fd4810 >> 0x1b & 3) << 10 | 0x80004064;
  uVar3 = FUN_d9005afc();
  if ((int)uVar3 != 0) {
    return uVar3;
  }
  if ((pbVar2[7] >> 1 & 1) == 0) {
    _DAT_d0074800 = 0x33800;
    _DAT_d8fd4814 = (ushort)_DAT_d0074810 & 0xff;
  }
  else {
    _DAT_d8fd4814 = 0x101;
  }
  _DAT_d8fd4816 = pbVar2[6] >> 6 & 1;
  if ((pbVar2[6] >> 6 & 1) != 0) {
    if (_DAT_d8fd4814 == 0x45) {
      _DAT_d8fd4816 = 0x16;
    }
    else {
      if (_DAT_d8fd4814 < 0x46) {
        if ((((_DAT_d8fd4814 == 7) || (_DAT_d8fd4814 < 8)) || (_DAT_d8fd4814 == 0x20)) ||
           (_DAT_d8fd4814 != 0x2c)) goto LAB_d9006bd0;
      }
      else if (_DAT_d8fd4814 != 0x98) {
        if (_DAT_d8fd4814 < 0x99) {
LAB_d9006bd0:
          _DAT_d8fd4816 = 0;
        }
        else if (_DAT_d8fd4814 == 0xec) {
          _DAT_d8fd4816 = 0xf;
        }
        else {
          _DAT_d8fd4816 = 1;
          if (_DAT_d8fd4814 != 0x101) goto LAB_d9006bd0;
        }
        goto LAB_d9006be4;
      }
      _DAT_d8fd4816 = 8;
    }
  }
LAB_d9006be4:
  uVar1 = nand_read(0,0xd900c400,0x180);
  uVar3 = (ulong)uVar1;
  if (uVar1 == 0x82) {
    if (_DAT_d8fd4814 == 0x45) {
      _DAT_d8fd4816 = 0;
      _DAT_d8fd4810 = _DAT_d8fd4810 | 0x1000000;
    }
    else {
      if (((DAT_d8fd4813 >> 3 & 3) != 0) || ((_DAT_d8fd4814 != 0xec && (_DAT_d8fd4814 != 0x98))))
      goto LAB_d9006ca8;
      _DAT_d8fd4810 = CONCAT13(DAT_d8fd4813 & 0xe0 | DAT_d8fd4813 & 7 | 0x10,_DAT_d8fd4810);
      _DAT_d0074804 = _DAT_d0074804 | 0x800;
      _DAT_c88344f0 = _DAT_c88344f0 & 0xffff7fff;
    }
    uVar3 = nand_read(0,0xd900c400,0x180);
    uVar3 = uVar3 & 0xffffffff;
  }
LAB_d9006ca8:
  if ((pbVar2[6] >> 5 & 1) != 0) {
    FUN_d9004d74(0xd900c400,0xd900c400,0x180);
  }
  _DAT_d8fd4816 = 0x9280;
  _DAT_d8fd4814 = 0xe0;
  _DAT_d8fd4810 = 0x990073a0;
  return uVar3;
}



uint FUN_d9006d20(long param_1,undefined4 *param_2)

{
  uint uVar1;
  
  *(undefined4 *)(param_1 + 0x50) = *param_2;
  *(undefined4 *)(param_1 + 0x54) = param_2[1];
  uVar1 = 0;
  if ((*(uint *)(param_1 + 0x50) >> 0x1e & 1) != 0) {
    uVar1 = *(uint *)(param_1 + 0x48) & 0x1fff;
  }
  param_2[3] = *(undefined4 *)(param_1 + 0x5c);
  return uVar1;
}



void FUN_d9006d54(undefined8 param_1)

{
  uint uVar1;
  undefined4 local_10;
  uint local_c;
  
  uVar1 = DAT_d8fd4820 >> 4 & 7;
  if (uVar1 == 2) {
    uVar1 = 0;
  }
  else {
    uVar1 = uVar1 + 1 & 7;
  }
  DAT_d8fd4820 = DAT_d8fd4820 & 0x80 | DAT_d8fd4820 & 0xf | (byte)(uVar1 << 4);
  local_c = uVar1 * 0x900 | 0x3b30000;
  local_10 = 0x6007400;
  FUN_d9006d20(param_1,&local_10);
  return;
}



uint read_from_mmc(long src,int sd_or_emmc_switch,int dst,ulong size)

{
  uint uVar1;
  uint sd_or_emmc;
  int iVar2;
  int iVar3;
  int offset_notsure;
  long src_offset;
  
                    /* Write?? */
  *(undefined4 *)(src + 0x50) = 0x11047201;
  offset_notsure = 0x200;
  if ((byte)(DAT_d8fd4820 >> 7 | DAT_d8fd4821 >> 1 & 1) != 0) {
    offset_notsure = 1;
  }
  iVar2 = 0;
  iVar3 = dst + -0x200;
  sd_or_emmc = 0;
  while( true ) {
    if (sd_or_emmc == ((uint)(size >> 9) & 0x7fffff)) {
      FUN_d9004b9c(dst + sd_or_emmc * 0x200 + -0x200,src + 0x600,0x200);
      return 0;
    }
                    /* in the if block: +512 offset if using an SD card. */
    src_offset = src + 1024;
    if ((sd_or_emmc & 1) != 0) {
      src_offset = src + 1536;
    }
    *(int *)(src + 0x58) = (int)src_offset;
    *(int *)(src + 0x54) = iVar2 + offset_notsure * sd_or_emmc_switch;
    if (sd_or_emmc != 0) {
      src_offset = src + 1536;
      if ((sd_or_emmc & 1) != 0) {
        src_offset = src + 1024;
      }
      FUN_d9004b9c(iVar3,src_offset,0x200);
    }
    if ((*(uint *)(src + 0x50) >> 0x1e & 1) != 0) break;
    iVar2 = iVar2 + offset_notsure;
    uVar1 = *(uint *)(src + 0x5c) & 0xfcf80000;
    iVar3 = iVar3 + 0x200;
    if (uVar1 != 0) {
      return uVar1;
    }
    sd_or_emmc = sd_or_emmc + 1;
  }
  return *(uint *)(src + 0x48) & 0x1fff;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9006eec(undefined8 param_1)

{
  int iVar1;
  int iVar2;
  uint uVar3;
  uint uVar4;
  undefined4 local_10;
  int local_c;
  undefined4 local_4;
  
  iVar2 = 2;
  DAT_d8fd4821 = DAT_d8fd4821 | 1;
  do {
    local_c = 0;
    local_10 = 0x17000;
    FUN_d9006d20(param_1,&local_10);
    local_c = 0x1aa;
    local_10 = 0x8007000;
    iVar1 = FUN_d9006d20(param_1,&local_10);
    if (iVar1 == 0) {
      DAT_d8fd4821 = DAT_d8fd4821 & 0xfc | DAT_d8fd4821 & 1 | ((local_4 & 0x1ff) == 0x1aa) << 1;
      break;
    }
    iVar2 = iVar2 + -1;
  } while (iVar2 != 0);
  uVar3 = 0;
  while( true ) {
    uVar4 = 5000;
    if ((DAT_d8fd4820 >> 3 & 1) != 0) {
      uVar4 = 10;
    }
    if (uVar4 <= uVar3) goto LAB_d9007080;
    local_c = 0;
    local_10 = 0x37007000;
    iVar2 = FUN_d9006d20(param_1,&local_10);
    if (iVar2 != 0) {
      return;
    }
    local_c = 0x200000;
    if ((DAT_d8fd4821 >> 1 & 1) != 0) {
      local_c = 0x40200000;
    }
    local_10 = 0x29107000;
    iVar2 = FUN_d9006d20(param_1,&local_10);
    if (iVar2 != 0) {
      return;
    }
    if ((int)local_4 < 0) break;
    uVar3 = uVar3 + 1;
  }
  DAT_d8fd4820 = DAT_d8fd4820 & 0x7f | (byte)((local_4 >> 0x1e & 1) << 7);
LAB_d9007080:
  local_10 = 0x2207000;
  local_c = 0;
  iVar2 = FUN_d9006d20(param_1,&local_10);
  if (iVar2 == 0) {
    local_c = 0x10000;
    local_10 = 0x3007000;
    iVar2 = FUN_d9006d20(param_1,&local_10);
    if (iVar2 == 0) {
      _DAT_d8fd4822 = local_4._2_2_;
      local_c = (uint)local_4._2_2_ << 0x10;
      local_10 = 0x9207000;
      iVar2 = FUN_d9006d20(param_1,&local_10);
      if (iVar2 == 0) {
        local_c = (uint)_DAT_d8fd4822 << 0x10;
        local_10 = 0x7007400;
        iVar2 = FUN_d9006d20(param_1,&local_10);
        if (iVar2 == 0) {
          local_c = (uint)_DAT_d8fd4822 << 0x10;
          local_10 = 0x37007000;
          iVar2 = FUN_d9006d20(param_1,&local_10);
          if (iVar2 == 0) {
            local_c = (DAT_d8fd4820 & 7) << 1;
            local_10 = 0x6007000;
            FUN_d9006d20(param_1,&local_10);
          }
        }
      }
    }
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

ulong FUN_d9007210(undefined8 param_1)

{
  int iVar1;
  uint uVar2;
  uint uVar3;
  int iVar4;
  ulong uVar5;
  ulong uVar6;
  undefined4 local_10;
  uint local_c;
  uint local_4;
  
  iVar4 = 2;
  do {
    local_c = 0;
    local_10 = 0x17000;
    FUN_d9006d20(param_1,&local_10);
    local_c = 0x40ff8000;
    local_10 = 0x1107000;
    iVar1 = FUN_d9006d20(param_1,&local_10);
    if (iVar1 == 0) {
      uVar3 = 0;
      goto LAB_d90072c8;
    }
    iVar4 = iVar4 + -1;
  } while (iVar4 != 0);
  uVar2 = FUN_d9006eec(param_1);
LAB_d90074f8:
  return (ulong)uVar2;
LAB_d90072c8:
  uVar2 = 5000;
  if ((DAT_d8fd4820 >> 3 & 1) != 0) {
    uVar2 = 10;
  }
  if (uVar2 <= uVar3) {
LAB_d9007340:
    _DAT_d8fd4822 = 1;
    local_10 = 0x2207000;
    local_c = 0;
    uVar5 = FUN_d9006d20(param_1,&local_10);
    uVar6 = uVar5 & 0xffffffff;
    if ((int)uVar5 != 0) {
      return uVar6;
    }
    local_c = (uint)_DAT_d8fd4822 << 0x10;
    local_10._2_2_ = (undefined2)(uVar5 >> 0x10);
    local_10._0_1_ = (undefined)uVar5;
    local_10 = CONCAT13((char)(uVar6 >> 0x18),
                        (int3)CONCAT22(local_10._2_2_,
                                       CONCAT11((char)(uVar6 >> 8),(undefined)local_10))) &
               0xc0ff0fff | 0x3007000;
    uVar3 = FUN_d9006d20(param_1,&local_10);
    if (uVar3 != 0) {
      return (ulong)uVar3;
    }
    local_c = (uint)_DAT_d8fd4822 << 0x10;
    local_10 = 0x9207000;
    uVar5 = FUN_d9006d20(param_1,&local_10);
    uVar6 = uVar5 & 0xffffffff;
    if ((int)uVar5 != 0) {
      return uVar6;
    }
    local_c = (uint)_DAT_d8fd4822 << 0x10;
    local_10._2_2_ = (undefined2)(uVar5 >> 0x10);
    local_10._0_1_ = (undefined)uVar5;
    local_10 = CONCAT13((char)(uVar6 >> 0x18),
                        (int3)CONCAT22(local_10._2_2_,
                                       CONCAT11((char)(uVar6 >> 8),(undefined)local_10))) &
               0xc0ff0fff | 0x7007400;
    uVar5 = FUN_d9006d20(param_1,&local_10);
    uVar6 = uVar5 & 0xffffffff;
    if ((int)uVar5 == 0) {
      local_c = (DAT_d8fd4820 & 7) << 8 | 0x3b70000;
      local_10._2_2_ = (undefined2)(uVar5 >> 0x10);
      local_10._0_1_ = (undefined)uVar5;
      local_10 = CONCAT13((char)(uVar6 >> 0x18),
                          (int3)CONCAT22(local_10._2_2_,
                                         CONCAT11((char)(uVar6 >> 8),(undefined)local_10))) &
                 0xc0ff0fff | 0x6007400;
      iVar4 = FUN_d9006d20(param_1,&local_10);
      if (iVar4 == 0) {
        local_c = (uint)_DAT_d8fd4822 << 0x10;
        local_10 = (uint)uVar6 & 0xc0ff0fff | 0xd007000;
        uVar3 = FUN_d9006d20(param_1,&local_10);
        uVar6 = (ulong)uVar3;
        if (uVar3 != 0) {
          return uVar6;
        }
        if ((local_4 >> 7 & 1) == 0) {
          return uVar6;
        }
      }
      DAT_d8fd4820 = DAT_d8fd4820 & 0xf8;
      return uVar6;
    }
    return uVar6;
  }
  if ((int)local_4 < 0) {
    DAT_d8fd4820 = DAT_d8fd4820 & 0x7f | (byte)((local_4 >> 0x1e & 1) << 7);
    goto LAB_d9007340;
  }
  local_c = 0x40ff8000;
  local_10 = 0x1107000;
  uVar2 = FUN_d9006d20(param_1,&local_10);
  if (uVar2 != 0) goto LAB_d90074f8;
  uVar3 = uVar3 + 1;
  goto LAB_d90072c8;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007518(void)

{
  _DAT_c8834528 = 0xffffffff;
  _DAT_c88344f0 = 0xffffffff;
  _DAT_c88344c0 = 0xc00c0000;
  _DAT_c88344b8 = 0xfc00;
  _DAT_c8834448 = _DAT_c8834448 & 0xfffffdff;
  _DAT_c883444c = _DAT_c883444c & 0xfffffdff;
  FUN_d90059c8(10);
  _DAT_c883444c = _DAT_c883444c | 0x200;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void check_if_device_exists(undefined4 *param_1)

{
  *param_1 = 0x100023c;
  FUN_d90059c8(0xfa);
  _DAT_d8fd4820 = (uint)param_1[0x12] >> 0x13 & 1;
  param_1[0x11] = _DAT_d8fd4820 | 0x4790;
  FUN_d9007210(param_1);
  param_1[0x11] = _DAT_d8fd4820 & 3 | 0x4790;
  *param_1 = 0x1000204;
  _DAT_da100244 = _DAT_d8fd4820;
  return;
}



undefined8 read(undefined8 src,undefined4 sd_or_emmc,undefined4 dst,undefined4 len)

{
  undefined8 ret;
  int i;
  
                    /*  FUN_d9007690(0xd0072000,1,0xd9000000,0xc000); */
  i = 3;
  do {
    ret = read_from_mmc(src,sd_or_emmc,dst,len);
    if ((int)ret == 0) {
      return 0;
    }
    if ((DAT_d8fd4821 & 1) != 0) {
      return ret;
    }
    ret = FUN_d9006d54(src);
    if ((int)ret != 0) {
      return ret;
    }
    i = i + -1;
  } while (i != 0);
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007714(void)

{
  _DAT_c883443c = _DAT_c883443c & 0xffbfffff;
  FUN_d90059c8(3);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007738(void)

{
  _DAT_c883443c = _DAT_c883443c | 0x400000;
  FUN_d90059c8(3);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900775c(byte param_1)

{
  if ((_DAT_c883443c >> 0x15 & 1) != (uint)param_1) {
    _DAT_c883443c = _DAT_c883443c ^ 0x200000;
  }
  FUN_d90059c8(3);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

uint FUN_d900778c(void)

{
  return _DAT_c8834444 >> 0x15 & 1;
}



void FUN_d90077a0(void)

{
  FUN_d9007738();
  FUN_d900775c(1);
  FUN_d900775c(0);
  return;
}



void FUN_d90077c0(void)

{
  FUN_d9007714();
  FUN_d9007738();
  FUN_d900775c(0);
  FUN_d900775c(1);
  return;
}



void FUN_d90077e4(undefined param_1)

{
  FUN_d9007714();
  FUN_d900775c(param_1);
  FUN_d9007738();
  FUN_d900778c();
  return;
}



char FUN_d9007810(byte param_1)

{
  char cVar1;
  uint uVar2;
  char cVar3;
  
  uVar2 = 7;
  cVar3 = '\0';
  do {
    cVar1 = FUN_d90077e4((int)(uint)param_1 >> (uVar2 & 0x1f) & 1);
    uVar2 = uVar2 - 1;
    cVar3 = cVar3 * '\x02' + cVar1;
  } while (uVar2 != 0xffffffff);
  return cVar3;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 FUN_d9007864(byte param_1)

{
  char cVar1;
  undefined uVar2;
  long lVar3;
  undefined local_10 [8];
  undefined local_8;
  
  _DAT_c88344b4 = _DAT_c88344b4 & 0xfcffffff;
  FUN_d90077a0();
  FUN_d9007810(param_1);
  cVar1 = FUN_d90077e4(1);
  if (cVar1 == '\0') {
    FUN_d9007810(0xfffffff8);
    cVar1 = FUN_d90077e4(1);
    if (cVar1 == '\0') {
      FUN_d90077c0();
      FUN_d90077a0();
      FUN_d9007810(param_1 | 1);
      cVar1 = FUN_d90077e4(1);
      if (cVar1 == '\0') {
        lVar3 = 0;
        do {
          uVar2 = FUN_d9007810(0xffffffff);
          local_10[lVar3] = uVar2;
          FUN_d90077e4(((uint)lVar3 & 0xff) == 7);
          lVar3 = lVar3 + 1;
        } while (lVar3 != 8);
        FUN_d90077c0();
        local_8 = 0;
        cVar1 = FUN_d9004b6c(local_10,s_boot_USB_d900d728,8);
        if (cVar1 == '\0') {
          return 1;
        }
        cVar1 = FUN_d9004b6c(local_10,s_boot_SDC_d900d731,8);
        if (cVar1 != '\0') {
          return 0;
        }
        return 2;
      }
    }
  }
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007974(void)

{
  _DAT_c8834528 = 0xffff87ff;
  _DAT_c88344f0 = 0xffff8700;
  _DAT_c88344c0 = _DAT_c88344c0 & 0x7e2fffff;
  _DAT_c88344c4 = _DAT_c88344c4 | 0xf;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90079d4(void)

{
  uint uVar1;
  byte *pbVar2;
  
  pbVar2 = get_efuse_mirror__clone_questionmark();
  if ((pbVar2[6] & 1) == 0) {
    uVar1 = 0xb313;
  }
  else {
    uVar1 = 0xa949;
  }
  _DAT_c1108c88 = uVar1 | 0x2a0000;
  return;
}



undefined8 FUN_d9007a0c(uint param_1,undefined4 param_2,undefined4 param_3)

{
  FUN_d9004b9c(param_2,param_1 | 0xcc000000,param_3);
  return 0;
}



/* WARNING: Removing unreachable block (ram,0xd9007a84) */
/* WARNING: Removing unreachable block (ram,0xd9007a8c) */
/* WARNING: Removing unreachable block (ram,0xd9007ab0) */

void FUN_d9007a38(void)

{
                    /* WARNING: Do nothing block with infinite loop */
  do {
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void checksum_check(long param_1)

{
  undefined4 sha_ctrl;
  undefined4 *sha_ctrl_reg;
  undefined4 *puVar1;
  
  sha_ctrl_reg = (undefined4 *)&DAT_da8320b8;
  do {
    if (param_1 == 0) {
      sha_ctrl = 0;
    }
    else {
      sha_ctrl = *(undefined4 *)((long)sha_ctrl_reg + param_1 + -0xda8320b8);
    }
    puVar1 = sha_ctrl_reg + 1;
    *sha_ctrl_reg = sha_ctrl;
    sha_ctrl_reg = puVar1;
  } while (puVar1 != (undefined4 *)&DAT_da8320c8);
  _DAT_da832000 = _DAT_da832000 | 2;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007b0c(undefined8 param_1,undefined8 param_2,undefined4 *param_3,undefined4 *param_4)

{
  _DAT_da832004 = *param_3;
  _DAT_da832008 = param_3[1];
  _DAT_da83200c = param_3[2];
  _DAT_da832010 = param_3[3];
  do {
  } while ((_DAT_da8321c0 >> 0xc & 1) != 0);
  *param_4 = _DAT_da832098;
  param_4[1] = _DAT_da83209c;
  param_4[2] = _DAT_da8320a0;
  param_4[3] = _DAT_da8320a4;
  return;
}



/* WARNING: Removing unreachable block (ram,0xd9007bdc) */
/* WARNING: Removing unreachable block (ram,0xd9007c54) */
/* WARNING: Removing unreachable block (ram,0xd9007c64) */

void FUN_d9007ba0(void)

{
                    /* WARNING: Do nothing block with infinite loop */
  do {
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007c68(undefined8 param_1,undefined4 *param_2,undefined4 *param_3)

{
  _DAT_da8320b8 = *param_2;
  _DAT_da8320bc = param_2[1];
  do {
  } while ((_DAT_da832028 & 0x4000) != 0);
  _DAT_da832028 = _DAT_da832028 | 1;
  *param_3 = _DAT_da8320b8;
  param_3[1] = _DAT_da8320bc;
  return;
}



/* WARNING: Unknown calling convention -- yet parameter storage is locked */

byte * get_efuse_mirror__clone_questionmark(void)

{
  return &DAT_d9013c00;
}



undefined * get_efuse_mirror(void)

{
  return &DAT_d9013c00;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 get_secureboot_flag(undefined4 *param_1)

{
  *param_1 = _DAT_c8100228;
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void set_rng_sec_config(void)

{
  _RNG_SEC_CONFIG_REG2__c8834008 = 0x6d95d5c;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007d58(void)

{
  uint uVar1;
  uint uVar2;
  
  _DAT_c1104408 = 4;
  _DAT_c8100094 = _DAT_c8100094 | 0xc0000000;
  _DAT_c8100010 = _DAT_c8100010 | 0x1000;
  _DAT_c0000000 = _DAT_c0000000 | 0x8000;
  uVar1 = _DAT_c0000004 & 0xfe000000;
  uVar2 = _DAT_c0000004 & 0x3f7fff;
  _DAT_c0000004 = uVar1 | _DAT_c0000004 & 0x3fffff | 0x1400000 | 0x8000;
  FUN_d90059c8(500);
  _DAT_c0000004 = uVar1 | uVar2 | 0x1400000;
  FUN_d90059c8(500);
  return;
}



undefined8 FUN_d9007df0(undefined8 param_1)

{
  int iVar1;
  
  FUN_d9007d58();
  FUN_d9007e48(param_1);
  iVar1 = s_Err_sha_d900d8f8();
  while (iVar1 == 0) {
    FUN_d9005a3c();
    iVar1 = FUN_d9007f04();
  }
  return 0;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 check_password(void)

{
  undefined4 uVar1;
  
  uVar1 = 1;
  if (_DAT_d8fd4828 != 0) {
    uVar1 = _DAT_d8fd482c;
  }
  return uVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 s_Err_sha_d900d8f8(void)

{
  undefined8 uVar1;
  
  if ((_DAT_c9000040 & 0xfffff000) == 0x4f543000) {
    _DAT_c9000008 = _DAT_c9000008 & 0xfffffffe;
    FUN_d9008664();
    _DAT_c9000804 = _DAT_c9000804 & 0xfffffffd;
    _DAT_c9000008 = _DAT_c9000008 | 1;
    uVar1 = 0;
  }
  else {
    uVar1 = 0xffffffff;
  }
  return uVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007e48(int param_1)

{
  byte *pbVar1;
  
  pbVar1 = get_efuse_mirror__clone_questionmark();
  _DAT_d8fd4828 = pbVar1[8] >> 1 & 1;
  if ((pbVar1[8] >> 1 & 1) != 0) {
    _DAT_d8fd4828 = 1;
  }
  _DAT_d8fd4834 = 0;
  _DAT_d8fd482c = (uint)(_DAT_d8fd4828 == 0);
  _DAT_d8fd4838 = 1;
  _DAT_d8fd4840 = 0xd900c400;
  _DAT_d8fd4848 = 30000;
  if (param_1 == 0) {
    _DAT_d8fd4848 = 5000000;
  }
  _DAT_d8fd4830 = param_1;
  _DAT_d8fd484c = get_te();
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007ee8(void)

{
  if (_DAT_d8fd4830 == 1) {
    _DAT_d8fd4838 = 0;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d9007f04(void)

{
  int iVar1;
  undefined8 uVar2;
  
  if (_DAT_d8fd4838 != 0) {
    iVar1 = get_te();
    if (_DAT_d8fd4848 < (uint)(iVar1 - _DAT_d8fd484c)) {
      return 2;
    }
  }
  uVar2 = FUN_d9008584();
  return uVar2;
}



void FUN_d9007f54(long param_1)

{
  undefined4 uVar1;
  
  *(undefined *)(param_1 + 0x39) = 1;
  uVar1 = 2;
  if (*(char *)(param_1 + 0x38) == '\0') {
    uVar1 = 1;
  }
  FUN_d90089d0(param_1,uVar1);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9007f70(long param_1,char *param_2)

{
  char cVar1;
  ushort uVar2;
  ushort uVar3;
  undefined8 *puVar4;
  undefined2 uVar5;
  undefined8 uVar6;
  undefined8 *puVar7;
  uint uVar8;
  uint uVar9;
  
  uVar5 = DAT_d900d7d0;
  uVar6 = _DAT_d900d7c8;
  puVar4 = _DAT_d8fd4840;
  cVar1 = param_2[1];
  uVar2 = *(ushort *)(param_2 + 6);
  uVar9 = (uint)uVar2;
  if (cVar1 == '\b') {
    if (*param_2 != -0x80) {
      return;
    }
    *(byte *)_DAT_d8fd4840 = 1;
    *(undefined8 **)(param_1 + 0x10) = puVar4;
    uVar9 = 1;
    goto LAB_d9008138;
  }
  if (cVar1 == '\t') {
    if (*param_2 != '\0') {
      return;
    }
    *(undefined4 *)(param_1 + 0x18) = 0;
    *(undefined8 *)(param_1 + 0x10) = 0;
    *(byte *)(param_1 + 0x3a) = *(byte *)(param_1 + 0x3a) | 1;
    return;
  }
  if (cVar1 != '\x06') {
    return;
  }
  if (*param_2 != -0x80) {
    return;
  }
  uVar3 = *(ushort *)(param_2 + 2) >> 8;
  _DAT_d8fd4838 = 0;
  uVar8 = (uint)uVar2;
  if (uVar3 == 2) {
    if (9 < uVar8) {
      FUN_d9004b9c((byte *)((long)_DAT_d8fd4840 + 9),&DAT_d900d760,0x17);
      uVar9 = 0x20;
    }
    puVar4 = _DAT_d8fd4840;
    *_DAT_d8fd4840 = DAT_d900d778;
    *(byte *)(puVar4 + 1) = DAT_d900d780;
    *(undefined8 **)(param_1 + 0x10) = puVar4;
    *(uint *)(param_1 + 0x18) = uVar9;
    return;
  }
  if (uVar3 != 3) {
    if (uVar3 != 1) {
      _DAT_d8fd4838 = 0;
      return;
    }
    uVar9 = (uint)uVar2;
    if (0x11 < uVar2) {
      uVar9 = 0x12;
    }
    uVar6 = FUN_d9004b9c(_DAT_d8fd4840,&DAT_d900d748,(short)uVar9);
    *(undefined8 *)(param_1 + 0x10) = uVar6;
    *(uint *)(param_1 + 0x18) = uVar9;
    return;
  }
  cVar1 = (char)*(ushort *)(param_2 + 2);
  if (cVar1 == '\x01') {
    puVar7 = &DAT_d900d7a0;
LAB_d90080a8:
    uVar6 = puVar7[1];
    *_DAT_d8fd4840 = *puVar7;
    puVar4[1] = uVar6;
  }
  else if (cVar1 == '\0') {
    *(undefined4 *)_DAT_d8fd4840 = DAT_d900d798;
  }
  else {
    if (cVar1 == '\x02') {
      puVar7 = &DAT_d900d7b0;
      goto LAB_d90080a8;
    }
    *_DAT_d8fd4840 = DAT_d900d7c0;
    puVar4[1] = uVar6;
    *(undefined2 *)(puVar4 + 2) = uVar5;
  }
  puVar4 = _DAT_d8fd4840;
  *(undefined8 **)(param_1 + 0x10) = _DAT_d8fd4840;
  uVar9 = (uint)*(byte *)puVar4;
  if (uVar8 < *(byte *)puVar4) {
    uVar9 = uVar8;
  }
LAB_d9008138:
  *(uint *)(param_1 + 0x18) = uVar9;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void usb_mode(long param_1,char *param_2)

{
  byte bVar1;
  ushort uVar2;
  undefined4 uVar3;
  undefined *puVar4;
  bool bVar5;
  int iVar6;
  uint uVar7;
  undefined8 uVar8;
  uint uVar9;
  uint uVar10;
  uint password_check;
  
  puVar4 = _DAT_d8fd4840;
  bVar1 = param_2[1];
  uVar2 = *(ushort *)(param_2 + 6);
  uVar7 = (uint)uVar2;
  if (bVar1 == 6) {
LAB_d9008254:
    if (*param_2 != '@') {
      return;
    }
LAB_d900826c:
    *(undefined **)(param_1 + 0x10) = _DAT_d8fd4840;
  }
  else {
    uVar9 = (uint)*(ushort *)(param_2 + 4);
    uVar10 = (uint)*(ushort *)(param_2 + 2);
    if (bVar1 < 7) {
      if ((4 < bVar1) || (2 < bVar1)) goto LAB_d9008254;
      if (bVar1 == 1) {
        if (*param_2 != '@') {
          return;
        }
        iVar6 = check_password();
        if (iVar6 != 0) {
          *(ulong *)(param_1 + 0x10) = (ulong)(uVar9 + uVar10 * 0x10000);
          goto LAB_d900827c;
        }
        goto LAB_d900826c;
      }
      bVar5 = bVar1 == 2;
    }
    else {
      if (0x12 < bVar1) {
        if (bVar1 == 0x20) {
          *_DAT_d8fd4840 = 2;
          uVar3 = _DAT_d8fd4828;
          puVar4[1] = 0;
          puVar4[4] = (char)uVar3;
          uVar3 = _DAT_d8fd482c;
          puVar4[2] = 0;
          puVar4[3] = 0;
          puVar4[5] = (char)uVar3;
          puVar4[6] = 0;
          puVar4[7] = 0;
          *(undefined **)(param_1 + 0x10) = puVar4;
          if (8 < uVar7) {
            uVar7 = 8;
          }
          *(uint *)(param_1 + 0x18) = uVar7;
          _DAT_d8fd4838 = 0;
          return;
        }
        if (bVar1 != 0x35) {
          return;
        }
        *(uint *)(param_1 + 0x18) = uVar7;
        *(undefined **)(param_1 + 0x10) = puVar4;
        _DAT_d8fd482c = 0;
        return;
      }
      if (0x10 < bVar1) {
        *(uint *)(param_1 + 0x28) = uVar10;
        *(uint *)(param_1 + 0x2c) = uVar9;
        goto LAB_d900826c;
      }
      bVar5 = bVar1 == 7;
    }
    if (!bVar5) {
      return;
    }
    if (*param_2 != -0x40) {
      return;
    }
    iVar6 = check_password();
    if (iVar6 == 0) {
                    /* BAD PASSWORD */
      password_check = 0xd900d7d8;
    }
    else {
      password_check = uVar9 + uVar10 * 0x10000;
    }
    uVar8 = FUN_d9004b9c(_DAT_d8fd4840,password_check,uVar2);
    *(undefined8 *)(param_1 + 0x10) = uVar8;
  }
LAB_d900827c:
  *(uint *)(param_1 + 0x18) = uVar7;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90082e8(long param_1,char *param_2)

{
  uint uVar1;
  byte bVar2;
  ushort uVar3;
  ushort uVar4;
  uint uVar5;
  int iVar6;
  int iVar7;
  long lVar8;
  uint *puVar9;
  uint **ppuVar10;
  uint *puVar11;
  int local_10 [4];
  
  bVar2 = param_2[1];
  uVar3 = *(ushort *)(param_2 + 4);
  uVar4 = *(ushort *)(param_2 + 2);
  if (bVar2 == 5) {
    if (*param_2 != '@') {
      return;
    }
    iVar6 = check_password();
    if (iVar6 == 0) {
      return;
    }
    iVar6 = (uint)uVar3 + (uint)uVar4 * 0x10000;
    if ((**(uint **)(param_1 + 0x10) >> 4 & 1) == 0) {
      FUN_d9008ba8();
    }
    iVar7 = check__notsure(iVar6,0xc000,local_10);
    if (iVar7 != 0) {
      return;
    }
    load_image__notsure(iVar6 + local_10[0]);
    return;
  }
  if (5 < bVar2) {
    if (bVar2 < 0x11) {
      return;
    }
    if (bVar2 < 0x13) {
      iVar6 = check_password();
      puVar9 = _DAT_d8fd4840;
      if (iVar6 == 0) {
        return;
      }
      uVar5 = *_DAT_d8fd4840;
      *(bool *)(param_1 + 0x38) = bVar2 == 0x11;
      *(ulong *)(param_1 + 0x20) = (ulong)uVar5;
      *(uint *)(param_1 + 0x30) = puVar9[1];
      FUN_d9007f54(param_1);
      return;
    }
    if (bVar2 != 0x35) {
      return;
    }
    _DAT_d8fd482c = FUN_d90054bc(*(undefined8 *)(param_1 + 0x10),*(undefined2 *)(param_2 + 6),1);
    if (_DAT_d8fd482c != 0) {
      _DAT_d8fd482c = 1;
      _DAT_d8fd4834 = 0;
      return;
    }
    _DAT_d8fd4834 = _DAT_d8fd4834 + 1;
    if (_DAT_d8fd4834 < 4) {
      _DAT_d8fd482c = 0;
      return;
    }
    FUN_d9005a70();
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
  }
  if (bVar2 == 3) {
    iVar6 = check_password();
    if (iVar6 == 0) {
      return;
    }
    lVar8 = *(long *)(param_1 + 0x10);
    for (uVar5 = 0; uVar5 < *(uint *)(param_1 + 0x18); uVar5 = uVar5 + 8) {
      *(undefined4 *)(ulong)*(uint *)(lVar8 + (ulong)uVar5) =
           *(undefined4 *)(lVar8 + (ulong)(uVar5 + 4));
    }
    return;
  }
  if (bVar2 != 4) {
    return;
  }
  iVar6 = check_password();
  if (iVar6 == 0) {
    return;
  }
  ppuVar10 = *(uint ***)(param_1 + 0x10);
  puVar9 = *ppuVar10;
  uVar1 = *(uint *)((long)ppuVar10 + 4);
  uVar5 = *(uint *)(ppuVar10 + 1);
  puVar11 = *(uint **)((long)ppuVar10 + 0xc);
  switch(uVar4) {
  case 0:
    *puVar9 = uVar1;
    return;
  case 1:
    uVar5 = uVar5 & uVar1;
    break;
  case 2:
    uVar5 = *puVar9 | uVar5;
    break;
  case 3:
    uVar5 = uVar1 & (uVar5 ^ 0xffffffff);
    break;
  case 4:
    uVar5 = *puVar9 & (uVar5 ^ 0xffffffff) | uVar5 & uVar1;
    goto LAB_d9008400;
  case 5:
    uVar5 = *puVar11;
LAB_d9008400:
    *puVar9 = uVar5;
    return;
  case 6:
    uVar5 = *puVar11 & uVar5;
    break;
  case 7:
    for (lVar8 = 0; uVar1 != (uint)lVar8; lVar8 = lVar8 + 1) {
      puVar9[lVar8] = puVar11[lVar8];
    }
  default:
    goto switchD_d90083b4_caseD_8;
  }
  *puVar9 = uVar5;
switchD_d90083b4_caseD_8:
  return;
}



void FUN_d9008514(long param_1)

{
  int iVar1;
  
  iVar1 = *(int *)(param_1 + 0x2c) + -1;
  *(undefined *)(param_1 + 0x39) = 0;
  *(int *)(param_1 + 0x2c) = iVar1;
  *(int *)(param_1 + 0x30) = *(int *)(param_1 + 0x30) - *(int *)(param_1 + 0x34);
  if (iVar1 != 0) {
    *(long *)(param_1 + 0x20) = *(long *)(param_1 + 0x20) + (long)*(int *)(param_1 + 0x28);
    FUN_d9007f54();
    return;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9008550(void)

{
  _DAT_c9000004 = 0xffffffff;
  _DAT_c9000014 = 0xffffffff;
  _DAT_c9000018 = 0xf0000816;
  return;
}



int FUN_d9008584(void)

{
  int iVar1;
  int iVar2;
  
  iVar1 = FUN_d9009474();
  iVar2 = FUN_d90094a8();
  return iVar1 + iVar2;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90085ac(void)

{
  int iVar1;
  
  iVar1 = 0x186a1;
  do {
    FUN_d90059c8(10);
    iVar1 = iVar1 + -1;
    if (iVar1 == 0) {
      return;
    }
  } while (-1 < (int)_DAT_c9000010);
  _DAT_c9000010 = _DAT_c9000010 | 1;
  iVar1 = 0xf4241;
  do {
    iVar1 = iVar1 + -1;
  } while (iVar1 != 0);
  FUN_d90059c8(1000);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900862c(uint param_1)

{
  int iVar1;
  
  _DAT_c9000010 = (param_1 & 0x1f) << 6 | 0x20;
  iVar1 = 0x2711;
  do {
    iVar1 = iVar1 + -1;
  } while (iVar1 != 0);
  FUN_d90059c8(1);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9008664(void)

{
  int *piVar1;
  int iVar2;
  int *piVar3;
  
  FUN_d90085ac();
  _DAT_c900000c = _DAT_c900000c & 0xdffffcaf | 0x40000000;
  FUN_d90085ac();
  _DAT_c9000008 = 0;
  FUN_d9008550();
  if ((_DAT_c9000014 & 1) == 0) {
    _DAT_c9000800 = _DAT_c9000800 & 0xffffe7fc;
    _DAT_c9000024 = 0x100;
    _DAT_c9000028 = 0x1000100;
    _DAT_c9000e00 = _DAT_c9000014 & 1;
    FUN_d900862c(0x10);
    _DAT_c9000010 = 0x10;
    iVar2 = 0x2711;
    do {
      iVar2 = iVar2 + -1;
    } while (iVar2 != 0);
    FUN_d90059c8(1);
    _DAT_c9000010 = 8;
    _DAT_c9000810 = 0;
    _DAT_c9000814 = 0;
    _DAT_c9000818 = 0xffffffff;
    _DAT_c900081c = 0;
    piVar3 = (int *)&DAT_c9000900;
    do {
      piVar1 = piVar3 + 8;
      iVar2 = 0;
      if (*piVar3 < 0) {
        iVar2 = 0x48000000;
      }
      *piVar3 = iVar2;
      iVar2 = 0;
      if (piVar3[0x80] < 0) {
        iVar2 = 0x48000000;
      }
      piVar3[0x80] = iVar2;
      piVar3[4] = 0;
      piVar3[0x84] = 0;
      piVar3[5] = 0;
      piVar3[0x85] = 0;
      piVar3 = piVar1;
    } while (piVar1 != (int *)0xc9000980);
    _DAT_c9000018 = 0;
    _DAT_c9000014 = 0xffffffff;
    FUN_d9008550();
    _DAT_c9000018 = _DAT_c9000018 | 0xe3800;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d9008834(void)

{
  undefined8 uVar1;
  
  if ((_DAT_c9000040 & 0xfffff000) == 0x4f543000) {
    _DAT_c9000008 = _DAT_c9000008 & 0xfffffffe;
    FUN_d9008664();
    _DAT_c9000804 = _DAT_c9000804 & 0xfffffffd;
    _DAT_c9000008 = _DAT_c9000008 | 1;
    uVar1 = 0;
  }
  else {
    uVar1 = 0xffffffff;
  }
  return uVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90088e4(byte *param_1)

{
  uint uVar1;
  byte bVar2;
  uint uVar3;
  uint uVar4;
  int iVar5;
  uint uVar6;
  
  uVar4 = *(uint *)(param_1 + 0x18) & 0x7ffff;
  bVar2 = param_1[1];
  *(uint *)(param_1 + 0x20) =
       *(uint *)(param_1 + 0x20) & 0xfff80000 | *(uint *)(param_1 + 0x18) & 0x7ffff;
  uVar6 = *(ushort *)(param_1 + 2) >> 4 & 0x7ff;
  if ((bVar2 & 1) == 0) {
    iVar5 = *param_1 + 0x58;
  }
  else {
    iVar5 = *param_1 + 0x48;
  }
  iVar5 = iVar5 * 0x20;
  uVar1 = *(uint *)((long)(iVar5 + 0x10) + 0xc9000000);
  if (uVar4 == 0) {
    if ((bVar2 & 1) != 0) {
      uVar6 = 0;
    }
    uVar6 = uVar1 & 0xe0000000 | uVar6 | 0x80000;
  }
  else {
    uVar3 = 0;
    if ((*(ushort *)(param_1 + 2) >> 4 & 0x7ff) != 0) {
      uVar3 = (int)(uVar4 + (uVar6 - 1)) / (int)uVar6;
    }
    if (((bVar2 & 1) == 0) || (uVar6 <= uVar4)) {
      uVar4 = uVar4 - (*(uint *)(param_1 + 0x1c) & 0x7ffff);
    }
    uVar6 = uVar1 & 0xe0000000 | (uVar3 & 0x3ff) << 0x13 | uVar4 & 0x7ffff;
  }
  *(uint *)((long)(iVar5 + 0x10) + 0xc9000000) = uVar6;
  *(uint *)((long)iVar5 + 0xc9000000) = *(uint *)((long)iVar5 + 0xc9000000) | 0x84000000;
  if ((bVar2 & 1) != 0) {
    _DAT_c9000014 = _DAT_c9000014 & 0xffffffdf;
    _DAT_c9000018 = _DAT_c9000018 | 0x20;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d90089d0(long param_1,uint param_2)

{
  int iVar1;
  uint uVar2;
  byte bVar3;
  uint uVar4;
  long lVar5;
  
  lVar5 = (long)(int)param_2 * 0x28;
  *(char *)(lVar5 + 0xd8fd4850) = (char)param_2;
  if (param_2 == 0) {
    if (*(int *)(param_1 + 8) != 2) {
      if (*(int *)(param_1 + 8) != 3) {
        return 0xffffffff;
      }
      if (((*(byte *)(param_1 + 0x3a) & 1) != 0) || (*(int *)(param_1 + 0x18) == 0)) {
        (&DAT_d8fd4851)[lVar5] = (&DAT_d8fd4851)[lVar5] | 1;
        *(undefined4 *)(param_1 + 8) = 4;
      }
    }
    *(undefined8 *)(&DAT_d8fd4858 + lVar5) = *(undefined8 *)(param_1 + 0x10);
    *(undefined8 *)(&DAT_d8fd4860 + lVar5) = *(undefined8 *)(param_1 + 0x10);
    uVar4 = *(uint *)(param_1 + 0x18) & 0x7ffff;
    *(uint *)(&DAT_d8fd4868 + lVar5) = *(uint *)(&DAT_d8fd4868 + lVar5) & 0xfff80000 | uVar4;
    uVar2 = *(uint *)(&DAT_d8fd486c + lVar5);
    *(uint *)(&DAT_d8fd486c + lVar5) = uVar2 & 0xfff80000;
    (&DAT_d8fd486e)[lVar5] = (byte)((uVar2 & 0xfff80000) >> 0x10) & 0xf7;
    *(uint *)(&DAT_d8fd4870 + lVar5) = *(uint *)(&DAT_d8fd4870 + lVar5) & 0xfff80000 | uVar4;
    (&DAT_d8fd4852)[lVar5] = (&DAT_d8fd4852)[lVar5] & 0xf;
    *(byte *)(lVar5 + 0xd8fd4853) = *(byte *)(lVar5 + 0xd8fd4853) & 0x80 | 4;
  }
  else {
    *(undefined8 *)(&DAT_d8fd4858 + lVar5) = *(undefined8 *)(param_1 + 0x20);
    *(undefined8 *)(&DAT_d8fd4860 + lVar5) = *(undefined8 *)(param_1 + 0x20);
    uVar4 = *(uint *)(param_1 + 0x28);
    if ((int)*(uint *)(param_1 + 0x30) < (int)*(uint *)(param_1 + 0x28)) {
      uVar4 = *(uint *)(param_1 + 0x30);
    }
    *(uint *)(&DAT_d8fd4868 + lVar5) =
         *(uint *)(&DAT_d8fd4868 + lVar5) & 0xfff80000 | uVar4 & 0x7ffff;
    uVar2 = *(uint *)(&DAT_d8fd486c + lVar5);
    *(uint *)(&DAT_d8fd486c + lVar5) = uVar2 & 0xfff80000;
    (&DAT_d8fd486e)[lVar5] = (byte)((uVar2 & 0xfff80000) >> 0x10) & 0xf7;
    *(uint *)(&DAT_d8fd4870 + lVar5) =
         *(uint *)(&DAT_d8fd4870 + lVar5) & 0xfff80000 | uVar4 & 0x7ffff;
    bVar3 = (&DAT_d8fd4851)[lVar5];
    (&DAT_d8fd4852)[lVar5] = (&DAT_d8fd4852)[lVar5] & 0xf;
    *(byte *)(lVar5 + 0xd8fd4853) = *(byte *)(lVar5 + 0xd8fd4853) & 0x80 | 0x20;
    (&DAT_d8fd4851)[lVar5] = bVar3 & 0xfe | param_2 == 1;
    if (param_2 == 1) {
      iVar1 = (param_2 & 0xff) + 0x48;
      uVar4 = 2;
    }
    else {
      iVar1 = (param_2 & 0xff) + 0x58;
      uVar4 = 0x40000;
    }
    uVar2 = *(uint *)((long)(iVar1 * 0x20) + 0xc9000000);
    if ((uVar2 >> 0xf & 1) == 0) {
      *(uint *)((long)(iVar1 * 0x20) + 0xc9000000) =
           uVar2 & 0xfc300000 | uVar2 & 0x3f800 | 0x80200 | 0x10008000;
    }
    _DAT_c900081c = uVar4 | _DAT_c900081c;
  }
  FUN_d90088e4();
  return 0;
}



void FUN_d9008ba8(void)

{
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9008bac(void)

{
  _DAT_c9000b10 = 0x60080018;
  _DAT_c9000b00 = 0x80008000;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9008bec(int *param_1)

{
  if (*param_1 != 5) {
    *param_1 = 4;
    _DAT_d8fd4868 = _DAT_d8fd4868 & 0xfff80000;
    _DAT_d8fd486c = _DAT_d8fd486c & 0xfff80000;
    DAT_d8fd4851 = DAT_d8fd4851 | 1;
    FUN_d90088e4();
    FUN_d9008bac();
    return;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d9008c48(void)

{
  if (_DAT_d8fd48f8 == 4) {
    _DAT_d8fd4858 = 0;
    _DAT_d8fd4868 = _DAT_d8fd4868 & 0xfff80000;
    _DAT_d8fd4860 = 0;
  }
  else {
    if ((_DAT_d8fd4868 & 0x7ffff) != 0) {
      if ((DAT_d8fd4851 & 1) == 0) {
        FUN_d9008bec(&DAT_d8fd48f8);
      }
      else if (((_DAT_c9000910 & 0x7f) == 0) && (_DAT_d8fd48f8 != 5)) {
        _DAT_d8fd48f8 = 4;
        _DAT_d8fd4868 = _DAT_d8fd4868 & 0xfff80000;
        DAT_d8fd4851 = DAT_d8fd4851 & 0xfe;
        _DAT_d8fd486c = _DAT_d8fd486c & 0xfff80000;
        FUN_d90088e4();
        FUN_d9008bac();
      }
      return 0;
    }
    _DAT_d8fd486c = (uint)((ushort)(_DAT_d8fd486c >> 0x10) & 0xfff8 | 8) << 0x10;
    FUN_d90088e4();
  }
  return 1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9008d24(void)

{
  ulong uVar1;
  byte bVar2;
  undefined8 local_10;
  
  bVar2 = DAT_d8fd492a;
  switch(_DAT_d8fd48f8) {
  case 1:
    local_10 = _DAT_d8fd48f0;
    uVar1 = local_10;
    bVar2 = DAT_d8fd492a & 0xfe;
    if ((DAT_d8fd492a >> 1 & 1) != 0) {
      DAT_d8fd492a = DAT_d8fd492a & 0xfc;
      local_10._0_1_ = (char)_DAT_d8fd48f0;
      _DAT_d8fd48fc = 0;
      if ((char)local_10 < '\0') {
        DAT_d8fd4851 = DAT_d8fd4851 | 1;
        _DAT_d8fd48f8 = 2;
      }
      else {
        DAT_d8fd4851 = DAT_d8fd4851 & 0xfe;
        _DAT_d8fd48f8 = 3;
      }
      local_10 = uVar1;
      if ((_DAT_d8fd48f0 & 0x60) == 0) {
        local_10._1_1_ = (undefined)(_DAT_d8fd48f0 >> 8);
        switch(local_10._1_1_) {
        case 0:
          _DAT_d8fd4900 = &DAT_d8fd48fc;
          _DAT_d8fd48fc = 0;
          _DAT_d8fd4908 = 2;
          break;
        case 5:
          if ((char)local_10 != '\0') {
            _DAT_d8fd48fc = 0;
            return;
          }
          local_10._2_2_ = (ushort)(_DAT_d8fd48f0 >> 0x10);
          _DAT_c9000800 = (local_10._2_2_ & 0x7f) << 4 | _DAT_c9000800;
          FUN_d9008bec(&DAT_d8fd48f8);
          return;
        case 9:
        case 0xb:
          DAT_d8fd492a = DAT_d8fd492a | 1;
        default:
          FUN_d9007f70(&DAT_d8fd48f0,&local_10);
        }
      }
      else {
        usb_mode(&DAT_d8fd48f0,&local_10);
      }
      FUN_d90089d0(&DAT_d8fd48f0,0);
      bVar2 = DAT_d8fd492a;
    }
    break;
  case 2:
    if ((_DAT_d8fd4870 & 0x7ffff) <= (_DAT_d8fd486c & 0x7ffff)) {
      FUN_d9008c48();
      bVar2 = DAT_d8fd492a;
    }
    break;
  case 3:
    FUN_d9008c48();
    FUN_d90082e8(&DAT_d8fd48f0,&DAT_d8fd48f0);
    bVar2 = DAT_d8fd492a;
    break;
  case 4:
    FUN_d9008c48();
    _DAT_d8fd48f8 = 1;
    DAT_d8fd4851 = DAT_d8fd4851 & 0xfe | 4;
    bVar2 = DAT_d8fd492a;
  }
  DAT_d8fd492a = bVar2;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9008efc(ulong param_1,ushort param_2)

{
  ulong uVar1;
  uint uVar2;
  
  uVar1 = param_1 & 3;
  if (uVar1 == 0) {
    for (; (int)uVar1 < (int)(uint)param_2; uVar1 = uVar1 + 4) {
      *(uint *)(param_1 + uVar1) = _DAT_c9001000;
    }
  }
  else {
    uVar2 = 0;
    for (uVar1 = 0; (int)uVar1 < (int)(uint)param_2; uVar1 = uVar1 + 1) {
      if ((uVar1 & 3) == 0) {
        uVar2 = _DAT_c9001000;
      }
      *(char *)(param_1 + uVar1) = (char)uVar2;
      uVar2 = uVar2 >> 8;
    }
  }
  return;
}



void FUN_d9008f60(byte *param_1)

{
  byte *pbVar1;
  byte *pbVar2;
  byte *pbVar3;
  uint uVar4;
  byte bVar5;
  byte bVar6;
  byte *pbVar7;
  uint uVar8;
  
  pbVar7 = *(byte **)(param_1 + 0x10);
  if ((*(uint *)(param_1 + 0x1c) & 0x7ffff) < (*(uint *)(param_1 + 0x18) & 0x7ffff)) {
    uVar8 = (*(uint *)(param_1 + 0x18) & 0x7ffff) - (*(uint *)(param_1 + 0x1c) & 0x7ffff);
    bVar5 = *param_1;
    uVar4 = *(ushort *)(param_1 + 2) >> 4 & 0x7ff;
    if ((int)uVar8 < (int)uVar4) {
      uVar4 = uVar8;
    }
    for (uVar8 = 0; uVar8 != uVar4 + 3 >> 2; uVar8 = uVar8 + 1) {
      pbVar1 = pbVar7 + 2;
      pbVar2 = pbVar7 + 1;
      bVar6 = *pbVar7;
      pbVar3 = pbVar7 + 3;
      pbVar7 = pbVar7 + 4;
      *(uint *)((long)(int)((bVar5 + 1) * 0x1000) + 0xc9000000) =
           (uint)*pbVar1 << 0x10 | (uint)*pbVar2 << 8 | (uint)bVar6 | (uint)*pbVar3 << 0x18;
    }
    *(uint *)(param_1 + 0x1c) =
         *(uint *)(param_1 + 0x1c) & 0xfff80000 |
         uVar4 + (*(uint *)(param_1 + 0x1c) & 0x7ffff) & 0x7ffff;
    *(ulong *)(param_1 + 0x10) = *(long *)(param_1 + 0x10) + (ulong)uVar4;
  }
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d900900c(undefined4 *param_1)

{
  *param_1 = _DAT_c9001000;
  param_1[1] = _DAT_c9001000;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d9009030(void)

{
  uint uVar1;
  uint uVar2;
  long lVar3;
  ulong uVar4;
  
  uVar2 = _DAT_c9000020;
  _DAT_c9000018 = _DAT_c9000018 & 0xffffffef;
  if ((_DAT_c9000020 & 0xf) == 0) {
    lVar3 = 0xd8fd4850;
  }
  else {
    lVar3 = (long)(int)(_DAT_c9000020 & 0xf) * 0x28 + 0xd8fd4850;
  }
  uVar1 = _DAT_c9000020 >> 0x11 & 0xf;
  if (uVar1 == 2) {
    uVar4 = (ulong)(_DAT_c9000020 >> 4) & 0x7ff;
    if (((int)uVar4 != 0) && (*(long *)(lVar3 + 0x10) != 0)) {
      FUN_d9008efc(*(long *)(lVar3 + 0x10),uVar4);
      *(uint *)(lVar3 + 0x1c) =
           *(uint *)(lVar3 + 0x1c) & 0xfff80000 |
           (*(uint *)(lVar3 + 0x1c) & 0x7ffff) + (int)uVar4 & 0x7ffff;
      *(ulong *)(lVar3 + 0x10) = *(long *)(lVar3 + 0x10) + uVar4;
    }
  }
  else if (uVar1 == 6) {
    FUN_d900900c(&DAT_d8fd48f0);
    DAT_d8fd492a = DAT_d8fd492a | 2;
    *(uint *)(lVar3 + 0x1c) =
         *(uint *)(lVar3 + 0x1c) & 0xfff80000 |
         (*(uint *)(lVar3 + 0x1c) & 0x7ffff) + (uVar2 >> 4 & 0x7ff) & 0x7ffff;
  }
  _DAT_c9000018 = _DAT_c9000018 | 0x10;
  _DAT_c9000014 = 0x10;
  return 1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d9009158(void)

{
  long lVar1;
  uint uVar2;
  uint uVar3;
  long lVar4;
  int iVar5;
  
  lVar4 = 0;
  do {
    lVar1 = lVar4 * 0x28;
    if (((((int)lVar4 == 0) || (((&DAT_d8fd4851)[lVar1] & 1) != 0)) &&
        (*(int *)((lVar4 + 0x6480048) * 0x20) < 0)) &&
       ((((&DAT_d8fd4852)[lVar1] & 3) != 3 || ((*(uint *)(&DAT_d8fd4868 + lVar1) & 0x7ffff) != 0))))
    {
      uVar2 = (*(uint *)(&DAT_d8fd4868 + lVar1) & 0x7ffff) -
              (*(uint *)(&DAT_d8fd486c + lVar1) & 0x7ffff);
      uVar3 = *(ushort *)(&DAT_d8fd4852 + lVar1) >> 4 & 0x7ff;
      if (uVar3 < uVar2) {
        uVar2 = uVar3;
      }
      while ((*(uint *)(&DAT_d8fd486c + lVar1) & 0x7ffff) <
             (*(uint *)(&DAT_d8fd4868 + lVar1) & 0x7ffff)) {
        iVar5 = 1000;
        while( true ) {
          iVar5 = iVar5 + -1;
          if (iVar5 == -1) goto LAB_d900918c;
          if (((_DAT_c900002c >> 0x10 & 0xff) != 0) && (uVar2 + 3 >> 2 < (_DAT_c900002c & 0xffff)))
          break;
          FUN_d90059c8(100);
        }
        if (iVar5 == 0) break;
        FUN_d9008f60(lVar1 + 0xd8fd4850);
        uVar2 = (*(uint *)(&DAT_d8fd4868 + lVar1) & 0x7ffff) -
                (*(uint *)(&DAT_d8fd486c + lVar1) & 0x7ffff);
        uVar3 = *(ushort *)(&DAT_d8fd4852 + lVar1) >> 4 & 0x7ff;
        if (uVar3 < uVar2) {
          uVar2 = uVar3;
        }
      }
    }
LAB_d900918c:
    lVar4 = lVar4 + 1;
    if (lVar4 == 4) {
      _DAT_c9000014 = 0x20;
      return 1;
    }
  } while( true );
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d90092b4(void)

{
  _DAT_c9000900 = _DAT_c9000900 & 0xfffff800;
  _DAT_c9000b00 = _DAT_c9000b00 | 0x80000000;
  _DAT_c9000804 = _DAT_c9000804 | 0x100;
  _DAT_d8fd48f8 = 1;
  _DAT_c900000c = _DAT_c900000c & 0xffffc000 | _DAT_c900000c & 0x3ff | 0x1400;
  _DAT_c9000014 = 0x2000;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d9009348(void)

{
  int *piVar1;
  
  _DAT_c9000804 = _DAT_c9000804 & 0xfffffffe;
  piVar1 = (int *)&DAT_c9000900;
  do {
    if (*piVar1 < 0) {
      *piVar1 = 0x48000000;
    }
    piVar1 = piVar1 + 8;
  } while (piVar1 != (int *)0xc9000980);
  _DAT_c9000b00 = 0x8000000;
  _DAT_c9000b20 = 0x8000000;
  _DAT_c9000b40 = 0x8000000;
  _DAT_c9000b60 = 0x8000000;
  FUN_d900862c(0);
  _DAT_c9000010 = 8;
  _DAT_c900081c = 0x10001;
  _DAT_c9000814 = 0xf;
  _DAT_c9000810 = 0xf;
  _DAT_c9000800 = _DAT_c9000800 & 0xfffff80f;
  FUN_d9008bac();
  _DAT_c9000014 = 0x1000;
  return 1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d900944c(void)

{
  FUN_d9007ee8();
  _DAT_c9000014 = 0x800;
  return 1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined4 FUN_d9009474(void)

{
  undefined4 uVar1;
  
  uVar1 = 0;
  if ((_DAT_c9000004 != 0) && (uVar1 = 0xb, (_DAT_c9000004 >> 2 & 1) == 0)) {
    uVar1 = 0;
  }
  return uVar1;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

undefined8 FUN_d90094a8(void)

{
  uint uVar1;
  long lVar2;
  uint uVar3;
  uint uVar4;
  int iVar5;
  uint uVar6;
  ulong uVar7;
  
  uVar3 = _DAT_c9000014 & _DAT_c9000018;
  uVar4 = _DAT_c9000014;
  if (uVar3 != 0) {
    if ((uVar3 >> 4 & 1) != 0) {
      FUN_d9009030();
    }
    if ((uVar3 >> 5 & 1) != 0) {
      FUN_d9009158();
    }
    if ((uVar3 >> 0xc & 1) != 0) {
      FUN_d9009348();
    }
    if ((uVar3 >> 0xb & 1) != 0) {
      FUN_d900944c();
    }
    if ((uVar3 >> 0xd & 1) != 0) {
      FUN_d90092b4();
    }
    if ((uVar3 >> 0x12 & 1) != 0) {
      uVar6 = _DAT_c9000818 & _DAT_c900081c;
      _DAT_c9000014 = 0x40000;
      _DAT_c9000818 = 0xffff;
      uVar7 = 0x908;
      uVar4 = 0;
      for (uVar6 = uVar6 & 0xffff; uVar6 != 0; uVar6 = uVar6 >> 1) {
        if ((uVar6 & 1) != 0) {
          uVar1 = *(uint *)(uVar7 + 0xc9000000) & _DAT_c900081c;
          if ((uVar1 & 1) != 0) {
            _DAT_c9000018 = _DAT_c9000018 & 0xffffffdf;
            *(undefined4 *)(uVar7 + 0xc9000000) = 1;
            if (uVar4 == 0) {
              FUN_d9008d24();
            }
            else {
              lVar2 = (long)(int)uVar4 * 0x28;
              _DAT_d8fd4924 = *(uint *)(&DAT_d8fd4868 + lVar2) & 0x7ffff;
              if (((*(uint *)((long)((int)uVar7 + 8) + 0xc9000000) & 0x1fffffff) == 0) &&
                 ((*(uint *)(&DAT_d8fd486c + lVar2) & 0x7ffff) == _DAT_d8fd4924)) {
                *(undefined8 *)(&DAT_d8fd4858 + lVar2) = 0;
                *(undefined8 *)(&DAT_d8fd4860 + lVar2) = 0;
                *(uint *)(&DAT_d8fd4868 + lVar2) = *(uint *)(&DAT_d8fd4868 + lVar2) & 0xfff80000;
              }
              FUN_d9008514(&DAT_d8fd48f0);
            }
          }
          if ((uVar1 >> 1 & 1) != 0) {
            *(undefined4 *)(uVar7 + 0xc9000000) = 2;
          }
          if ((uVar1 >> 2 & 1) != 0) {
            *(undefined4 *)(uVar7 + 0xc9000000) = 4;
          }
          if ((uVar1 >> 3 & 1) != 0) {
            _DAT_c9000018 = _DAT_c9000018 & 0xffffffdf | 0x60;
            _DAT_c9000804 = _DAT_c9000804 | 0x80;
            (&DAT_d8fd4851)[(ulong)uVar4 * 0x28] = (&DAT_d8fd4851)[(ulong)uVar4 * 0x28] | 4;
            *(undefined4 *)(uVar7 + 0xc9000000) = 8;
          }
          if ((uVar1 >> 4 & 1) != 0) {
            *(undefined4 *)(uVar7 + 0xc9000000) = 0x10;
          }
          if ((uVar1 >> 5 & 1) != 0) {
            *(undefined4 *)(uVar7 + 0xc9000000) = 0x20;
          }
          if ((uVar1 >> 6 & 1) != 0) {
            *(undefined4 *)(uVar7 + 0xc9000000) = 0x40;
          }
        }
        uVar4 = uVar4 + 1;
        uVar7 = (ulong)((int)uVar7 + 0x20);
      }
    }
    uVar4 = uVar3;
    if ((uVar3 >> 0x13 & 1) != 0) {
      uVar3 = _DAT_c900081c & _DAT_c9000818;
      _DAT_c9000014 = 0x80000;
      _DAT_c9000818 = 0xffff0000;
      uVar7 = 0xb08;
      iVar5 = 0;
      for (uVar3 = uVar3 >> 0x10; uVar3 != 0; uVar3 = uVar3 >> 1) {
        if ((uVar3 & 1) != 0) {
          uVar6 = *(uint *)(uVar7 + 0xc9000000) & _DAT_c9000814;
          if ((uVar6 & 1) != 0) {
            *(undefined4 *)(uVar7 + 0xc9000000) = 1;
            if (iVar5 == 0) {
              FUN_d9008d24();
            }
            else {
              lVar2 = (long)iVar5 * 0x28;
              *(undefined8 *)(&DAT_d8fd4858 + lVar2) = 0;
              _DAT_d8fd4924 = *(uint *)(&DAT_d8fd486c + lVar2) & 0x7ffff;
              *(undefined8 *)(&DAT_d8fd4860 + lVar2) = 0;
              *(uint *)(&DAT_d8fd4868 + lVar2) = *(uint *)(&DAT_d8fd4868 + lVar2) & 0xfff80000;
              FUN_d9008514(&DAT_d8fd48f0);
            }
          }
          if ((uVar6 >> 1 & 1) != 0) {
            *(undefined4 *)(uVar7 + 0xc9000000) = 2;
          }
          if ((uVar6 >> 2 & 1) != 0) {
            *(undefined4 *)(uVar7 + 0xc9000000) = 4;
          }
          if ((uVar6 >> 3 & 1) != 0) {
            FUN_d9008d24();
            *(undefined4 *)(uVar7 + 0xc9000000) = 8;
          }
        }
        iVar5 = iVar5 + 1;
        uVar7 = (ulong)((int)uVar7 + 0x20);
      }
    }
  }
  _DAT_c9000014 = uVar4;
  return 0;
}



void FUN_d9009824(long param_1,long param_2,int param_3)

{
  int iVar1;
  long lVar2;
  long lVar3;
  uint *puVar4;
  long lVar5;
  long lVar6;
  uint uVar7;
  int iVar8;
  uint uVar9;
  uint local_120 [64];
  uint local_20 [8];
  
  for (iVar8 = 0; iVar8 < param_3; iVar8 = iVar8 + 1) {
    lVar5 = param_2 + (iVar8 << 6);
    lVar3 = 0;
    do {
      lVar6 = (long)((int)lVar3 << 2);
      lVar2 = lVar5 + lVar6;
      local_120[lVar3] =
           (uint)*(byte *)(lVar2 + 3) | (uint)*(byte *)(lVar5 + lVar6) << 0x18 |
           (uint)*(byte *)(lVar2 + 2) << 8 | (uint)*(byte *)(lVar2 + 1) << 0x10;
      lVar3 = lVar3 + 1;
      puVar4 = local_120;
    } while (lVar3 != 0x10);
    do {
      uVar7 = puVar4[0xe];
      uVar9 = puVar4[1];
      puVar4[0x10] = *puVar4 + puVar4[9] +
                     ((uVar7 >> 0x13 | uVar7 << 0xd) ^ (uVar7 >> 0x11 | uVar7 << 0xf) ^ uVar7 >> 10)
                     + ((uVar9 >> 0x12 | uVar9 << 0xe) ^ (uVar9 >> 7 | uVar9 << 0x19) ^ uVar9 >> 3);
      puVar4 = puVar4 + 1;
    } while (puVar4 != local_120 + 0x30);
    lVar5 = 0;
    do {
      *(undefined4 *)((long)local_20 + lVar5) = *(undefined4 *)(param_1 + lVar5);
      lVar5 = lVar5 + 4;
    } while (lVar5 != 0x20);
    lVar5 = 0;
    do {
      uVar7 = local_20[2];
      local_20[2] = local_20[1];
      local_20[1] = local_20[0];
      uVar9 = local_20[6];
      local_20[6] = local_20[5];
      local_20[5] = local_20[4];
      iVar1 = *(int *)((long)local_120 + lVar5) + *(int *)((long)&DAT_d900d7f8 + lVar5) +
              ((local_20[5] >> 0xb | local_20[5] << 0x15) ^ (local_20[5] >> 6 | local_20[5] << 0x1a)
              ^ (local_20[5] >> 0x19 | local_20[5] << 7)) +
              (uVar9 & (local_20[5] ^ 0xffffffff) ^ local_20[6] & local_20[5]) + local_20[7];
      local_20[4] = local_20[3] + iVar1;
      lVar5 = lVar5 + 4;
      local_20[0] = ((uVar7 ^ local_20[2]) & local_20[1] ^ uVar7 & local_20[2]) +
                    ((local_20[1] >> 0xd | local_20[1] << 0x13) ^
                     (local_20[1] >> 2 | local_20[1] << 0x1e) ^
                    (local_20[1] >> 0x16 | local_20[1] << 10)) + iVar1;
      local_20[7] = uVar9;
      local_20[3] = uVar7;
    } while (lVar5 != 0x100);
    lVar5 = 0;
    do {
      *(int *)(param_1 + lVar5) = *(int *)(param_1 + lVar5) + *(int *)((long)local_20 + lVar5);
      lVar5 = lVar5 + 4;
    } while (lVar5 != 0x20);
  }
  return;
}



void FUN_d90099fc(undefined4 *param_1,int param_2)

{
  undefined4 uVar1;
  
  if (param_2 == 0xe0) {
    *param_1 = 0xc1059ed8;
    param_1[1] = 0x367cd507;
    param_1[2] = 0x3070dd17;
    param_1[3] = 0xf70e5939;
    param_1[4] = 0xffc00b31;
    param_1[5] = 0x68581511;
    param_1[6] = 0x64f98fa7;
    uVar1 = 0xbefa4fa4;
  }
  else {
    if (param_2 != 0x100) goto LAB_d9009acc;
    *param_1 = 0x6a09e667;
    param_1[1] = 0xbb67ae85;
    param_1[2] = 0x3c6ef372;
    param_1[3] = 0xa54ff53a;
    param_1[4] = 0x510e527f;
    param_1[5] = 0x9b05688c;
    param_1[6] = 0x1f83d9ab;
    uVar1 = 0x5be0cd19;
  }
  param_1[7] = uVar1;
LAB_d9009acc:
  param_1[10] = param_2;
  param_1[9] = 0;
  param_1[8] = 0;
  return;
}



void FUN_d9009adc(long param_1,long param_2,uint param_3)

{
  uint uVar1;
  uint uVar2;
  
  uVar2 = 0x40 - *(uint *)(param_1 + 0x24);
  uVar1 = param_3;
  if (uVar2 < param_3) {
    uVar1 = uVar2;
  }
  FUN_d9004b9c(param_1 + (ulong)*(uint *)(param_1 + 0x24) + 0x2c,param_2,(ulong)uVar1);
  uVar2 = param_3 + *(int *)(param_1 + 0x24);
  if (uVar2 < 0x40) {
    *(uint *)(param_1 + 0x24) = uVar2;
  }
  else {
    param_2 = param_2 + (ulong)uVar1;
    uVar2 = param_3 - uVar1 >> 6;
    FUN_d9009824(param_1,param_1 + 0x2c,1);
    FUN_d9009824(param_1,param_2,(ulong)uVar2);
    uVar1 = param_3 - uVar1 & 0x3f;
    FUN_d9004b9c(param_1 + 0x2c,param_2 + (ulong)uVar2 * 0x40,uVar1);
    *(uint *)(param_1 + 0x24) = uVar1;
    *(uint *)(param_1 + 0x20) = *(int *)(param_1 + 0x20) + (uVar2 + 1) * 0x40;
  }
  return;
}



long FUN_d9009bac(long param_1)

{
  long lVar1;
  uint uVar2;
  int iVar3;
  undefined4 uVar4;
  int iVar5;
  long lVar6;
  
  uVar2 = *(uint *)(param_1 + 0x24);
  iVar5 = 2;
  if ((uVar2 & 0x3f) < 0x38) {
    iVar5 = 1;
  }
  iVar3 = *(int *)(param_1 + 0x20);
  FUN_d9004c14(param_1 + (ulong)uVar2 + 0x2c,0,iVar5 * 0x40 - uVar2);
  iVar3 = (uVar2 + iVar3) * 8;
  *(undefined *)(param_1 + (ulong)*(uint *)(param_1 + 0x24) + 0x2c) = 0x80;
  lVar6 = param_1 + (ulong)(uint)(iVar5 * 0x40);
  *(char *)(lVar6 + 0x2b) = (char)iVar3;
  *(char *)(lVar6 + 0x2a) = (char)((uint)iVar3 >> 8);
  *(char *)(lVar6 + 0x29) = (char)((uint)iVar3 >> 0x10);
  *(char *)(lVar6 + 0x28) = (char)((uint)iVar3 >> 0x18);
  FUN_d9009824(param_1,param_1 + 0x2c,iVar5);
  iVar5 = 8;
  if (*(int *)(param_1 + 0x28) == 0xe0) {
    iVar5 = 7;
  }
  lVar6 = 0;
  do {
    uVar4 = *(undefined4 *)(param_1 + lVar6 * 4);
    lVar1 = param_1 + ((int)lVar6 << 2);
    *(char *)(lVar1 + 0xaf) = (char)uVar4;
    *(char *)(lVar1 + 0xae) = (char)((uint)uVar4 >> 8);
    lVar6 = lVar6 + 1;
    *(char *)(lVar1 + 0xad) = (char)((uint)uVar4 >> 0x10);
    *(char *)(lVar1 + 0xac) = (char)((uint)uVar4 >> 0x18);
  } while ((int)lVar6 < iVar5);
  return param_1 + 0xac;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9009c9c(undefined4 *param_1,uint param_2,char param_3)

{
  uint uVar1;
  undefined4 *puVar2;
  uint uVar3;
  
  if ((param_3 == '\0') && ((param_2 & 0x3f) != 0)) {
    serial_puts(s_Err_sha_d900d8f8);
    return;
  }
  while (param_2 != 0) {
    uVar3 = 0;
    puVar2 = param_1;
    if (0x3f < param_2) {
      uVar3 = param_2 - 0x40;
      param_2 = 0x40;
    }
    do {
      if (param_2 < 4) {
        _DAT_da8320c8 = _DAT_da8320c8 & 0xffc3ffff | 0x300000 | (param_2 - 1) * 0x40000;
        param_2 = 0;
      }
      else {
        param_2 = param_2 - 4;
        if ((param_2 == 0) &&
           ((uVar1 = _DAT_da8320c8 & 0xffcfffff, uVar3 != 0 ||
            (_DAT_da8320c8 = uVar1 | 0x300000, param_3 == '\0')))) {
          _DAT_da8320c8 = uVar1 | 0x200000;
        }
      }
      param_1 = puVar2 + 1;
      _DAT_da8320cc = *puVar2;
      puVar2 = param_1;
    } while (param_2 != 0);
    do {
    } while ((int)_DAT_da8320c8 < 0);
    _DAT_da8320c8 = _DAT_da8320c8 & 0xffcfffff | 0x400000;
    param_2 = uVar3;
  }
  return;
}



/* WARNING: Removing unreachable block (ram,0xd9009ef4) */
/* WARNING: Removing unreachable block (ram,0xd9009efc) */
/* WARNING: Removing unreachable block (ram,0xd9009f20) */
/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9009d90(void)

{
  if (_DAT_d8fd4930 == 0) {
    do {
    } while( true );
  }
  serial_puts(s_Err_sha_d900d8f8);
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

void FUN_d9009f2c(long param_1,long param_2,uint param_3)

{
  ulong uVar1;
  uint uVar2;
  long lVar3;
  uint uVar4;
  
  lVar3 = _DAT_d8fd4930;
  if (_DAT_d8fd4930 != param_1) {
    serial_puts(s_Err_sha_d900d8f8);
    return;
  }
  uVar2 = *(uint *)(_DAT_d8fd4930 + 0x24);
  uVar4 = 0;
  if (uVar2 != 0) {
    uVar4 = 0x40 - uVar2;
    FUN_d9004b9c(_DAT_d8fd4930 + (ulong)uVar2 + 0x2c,param_2,uVar4);
    param_3 = param_3 - uVar4;
    *(uint *)(lVar3 + 0x24) = *(int *)(lVar3 + 0x24) + uVar4;
  }
  if (*(int *)(lVar3 + 0x24) == 0x40) {
    if (param_3 == 0) {
      return;
    }
    FUN_d9009c9c(lVar3 + 0x2c,0x40,0);
    *(undefined4 *)(lVar3 + 0x24) = 0;
    if (param_3 < 0x41) goto LAB_d900a010;
  }
  else if (param_3 < 0x41) {
    if (param_3 == 0) {
      return;
    }
    goto LAB_d900a010;
  }
  uVar2 = param_3 & 0x3f;
  if (uVar2 == 0) {
    uVar2 = 0x40;
  }
  uVar1 = (ulong)uVar4;
  uVar4 = uVar4 + (param_3 - uVar2);
  FUN_d9009c9c(param_2 + uVar1,param_3 - uVar2,0);
  param_3 = uVar2;
LAB_d900a010:
  FUN_d9004b9c(lVar3 + 0x2c,param_2 + (ulong)uVar4,param_3);
  *(uint *)(lVar3 + 0x24) = param_3;
  return;
}



/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */

long FUN_d900a038(long param_1)

{
  long lVar1;
  undefined4 *puVar2;
  
  lVar1 = _DAT_d8fd4930;
  if (_DAT_d8fd4930 == param_1) {
    param_1 = _DAT_d8fd4930 + 0xac;
    if (*(int *)(_DAT_d8fd4930 + 0x24) - 1U < 0x40) {
      FUN_d9009c9c(_DAT_d8fd4930 + 0x2c,*(int *)(_DAT_d8fd4930 + 0x24),1);
      _DAT_da832094 = _DAT_da832094 & 0xffffffbf;
      puVar2 = (undefined4 *)&DAT_da832098;
      do {
        *(undefined4 *)((long)puVar2 + lVar1 + -0xda831fec) = *puVar2;
        puVar2 = puVar2 + 1;
      } while (puVar2 != (undefined4 *)&DAT_da8320b8);
      _DAT_d8fd4930 = 0;
    }
    else {
      serial_puts(s_Err_sha_d900d8f8);
    }
  }
  else {
    serial_puts(s_Err_sha_d900d8f8);
    param_1 = param_1 + 0xac;
  }
  return param_1;
}



void FUN_d900a124(long param_1,undefined4 param_2,undefined4 param_3)

{
  FUN_d9004c14(param_1,0,0x120);
  *(undefined4 *)(param_1 + 0x118) = param_2;
  *(undefined4 *)(param_1 + 0x11c) = param_3;
  return;
}



int FUN_d900a164(long param_1,long param_2,undefined8 param_3)

{
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  undefined auStack_30 [32];
  long local_10;
  
  local_10 = param_2;
  FUN_d900b05c(auStack_30);
  iVar1 = FUN_d900b460(auStack_30,local_10,*(undefined8 *)(param_1 + 8));
  if (iVar1 == 0) {
    local_10 = param_1 + 0x10;
    iVar1 = FUN_d900b974(auStack_30,local_10);
    if (-1 < iVar1) {
      FUN_d900b074(auStack_30);
      return -0x4080;
    }
    uVar3 = *(undefined8 *)(param_1 + 8);
    iVar1 = FUN_d900c8d0(auStack_30,auStack_30,param_1 + 0x28,local_10,param_1 + 0xd0);
    if (iVar1 == 0) {
      iVar1 = FUN_d900b50c(auStack_30,param_3,uVar3);
    }
  }
  FUN_d900b074(auStack_30);
  iVar2 = 0;
  if (iVar1 != 0) {
    iVar2 = iVar1 + -0x4280;
  }
  return iVar2;
}



int FUN_d900a228(long param_1,int param_2,int param_3,uint param_4,undefined8 param_5,
                undefined8 param_6)

{
  char cVar1;
  byte bVar2;
  bool bVar3;
  int iVar4;
  char *pcVar5;
  char *pcVar6;
  char *pcVar7;
  long lVar8;
  char acStack_201 [17];
  char local_1f0 [5];
  byte local_1eb [491];
  
  if (((*(int *)(param_1 + 0x118) != 0) || (lVar8 = *(long *)(param_1 + 8), 0x1f0 < lVar8 - 0x10U))
     || (param_2 != 0)) {
    return -0x4080;
  }
  iVar4 = FUN_d900a164(param_1,param_6,acStack_201 + 1);
  if (iVar4 != 0) {
    return iVar4;
  }
  if (acStack_201[1] != '\0') {
    return -0x4100;
  }
  if (acStack_201[2] != '\x01') {
    return -0x4100;
  }
  for (pcVar7 = acStack_201 + 3; *pcVar7 != '\0'; pcVar7 = pcVar7 + 1) {
    if (acStack_201 + lVar8 <= pcVar7) {
      return -0x4100;
    }
    if (*pcVar7 != -1) {
      return -0x4100;
    }
  }
  pcVar5 = pcVar7 + 1;
  pcVar6 = acStack_201 + (lVar8 - (long)pcVar5) + 1;
  if ((pcVar6 != (char *)0x21) || (param_3 != 5)) {
    if (pcVar6 == (char *)0x22) {
      cVar1 = pcVar7[0xe];
      pcVar7[0xe] = '\0';
      iVar4 = FUN_d9004b6c(pcVar5,&DAT_d900d901,0x12);
      if (iVar4 != 0) {
        return -0x4380;
      }
      if ((((cVar1 == '\x02') && (param_3 == 2)) || ((cVar1 == '\x04' && (param_3 == 3)))) ||
         ((cVar1 == '\x05' && (param_3 == 4)))) {
        pcVar5 = pcVar7 + 0x13;
        pcVar6 = (char *)0x10;
        goto LAB_d900a314;
      }
    }
    if ((pcVar6 != (char *)0x23) || (param_3 != 5)) {
      if (pcVar6 == (char *)0x2f) {
        if (pcVar7[0xf] == '\x04') {
          bVar3 = param_3 == 0xe;
          goto LAB_d900a424;
        }
      }
      else if (pcVar6 == (char *)0x33) {
        if (pcVar7[0xf] == '\x01') {
          bVar3 = param_3 == 0xb;
          goto LAB_d900a424;
        }
      }
      else if (pcVar6 == (char *)0x43) {
        if (pcVar7[0xf] == '\x02') {
          bVar3 = param_3 == 0xc;
LAB_d900a424:
          if (bVar3) {
            cVar1 = pcVar7[2];
            bVar2 = pcVar7[0x13];
            pcVar7[2] = '\x11';
            pcVar7[0xf] = '\0';
            if (bVar2 != (byte)(cVar1 - 0x11U)) {
              return -0x4380;
            }
            iVar4 = FUN_d9004b6c(pcVar5,&DAT_d900d924,0x12);
            if (iVar4 != 0) {
              return -0x4380;
            }
            pcVar5 = pcVar7 + 0x14;
            pcVar6 = (char *)(ulong)bVar2;
            goto LAB_d900a314;
          }
        }
      }
      else if ((pcVar6 == (char *)0x53) && (pcVar7[0xf] == '\x03')) {
        bVar3 = param_3 == 0xd;
        goto LAB_d900a424;
      }
      if ((pcVar6 != (char *)(ulong)param_4) || (param_3 != 0)) {
        return -0x4100;
      }
      goto LAB_d900a314;
    }
    iVar4 = FUN_d9004b6c(pcVar5,&DAT_d900d914,0xf);
    if (iVar4 != 0) {
      return -0x4380;
    }
    pcVar5 = pcVar7 + 0x10;
  }
  else {
    iVar4 = FUN_d9004b6c(pcVar5,&DAT_d900d938,0xd);
    if (iVar4 != 0) {
      return -0x4380;
    }
    pcVar5 = pcVar7 + 0xe;
  }
  pcVar6 = (char *)0x14;
LAB_d900a314:
  iVar4 = FUN_d9004b6c(pcVar5,param_5,pcVar6);
  if (iVar4 != 0) {
    return -0x4380;
  }
  return 0;
}



undefined8 FUN_d900a4c8(long param_1)

{
  undefined8 uVar1;
  
  if (*(int *)(param_1 + 0x118) == 0) {
    uVar1 = FUN_d900a228();
    FUN_d9004cec();
  }
  else {
    uVar1 = 0xffffbf00;
  }
  return uVar1;
}



void FUN_d900a4f8(long param_1)

{
  FUN_d900b074(param_1 + 0x100);
  FUN_d900b074(param_1 + 0xe8);
  FUN_d900b074(param_1 + 0xd0);
  FUN_d900b074(param_1 + 0xb8);
  FUN_d900b074(param_1 + 0xa0);
  FUN_d900b074(param_1 + 0x88);
  FUN_d900b074(param_1 + 0x70);
  FUN_d900b074(param_1 + 0x58);
  FUN_d900b074(param_1 + 0x40);
  FUN_d900b074(param_1 + 0x28);
  FUN_d900b074(param_1 + 0x10);
  return;
}



undefined4 FUN_d900a568(ulong *param_1,int param_2,byte param_3)

{
  undefined4 uVar1;
  uint uVar2;
  ulong uVar3;
  
  uVar2 = param_3 - 0x30;
  uVar3 = (ulong)(int)uVar2;
  if (9 < (uVar2 & 0xff)) {
    uVar3 = 0xff;
  }
  *param_1 = uVar3;
  uVar2 = (uint)param_3;
  if ((param_3 - 0x41 & 0xff) < 6) {
    *param_1 = (long)(int)(uVar2 - 0x37);
  }
  if ((uVar2 - 0x61 & 0xff) < 6) {
    *param_1 = (long)(int)(uVar2 - 0x57);
  }
  uVar1 = 0xfffffffa;
  if (*param_1 < (ulong)(long)param_2) {
    uVar1 = 0;
  }
  return uVar1;
}



void FUN_d900a5d4(long param_1,long param_2,long param_3)

{
  ulong *puVar1;
  long lVar2;
  ulong uVar3;
  long lVar4;
  ulong uVar5;
  ulong uVar6;
  ulong uVar7;
  
  lVar2 = 0;
  uVar3 = 0;
  lVar4 = 0;
  while (lVar4 != param_1) {
    uVar5 = *(ulong *)(param_3 + lVar2);
    lVar4 = lVar4 + 1;
    uVar6 = uVar5 - uVar3;
    *(ulong *)(param_3 + lVar2) = uVar6;
    uVar7 = *(ulong *)(param_2 + lVar2);
    uVar3 = (ulong)(uVar5 < uVar3);
    *(ulong *)(param_3 + lVar2) = uVar6 - uVar7;
    if (uVar6 < uVar7) {
      uVar3 = uVar3 + 1;
    }
    lVar2 = lVar2 + 8;
  }
  puVar1 = (ulong *)(param_3 + lVar4 * 8);
  for (; uVar3 != 0; uVar3 = (ulong)(uVar5 < uVar3)) {
    uVar5 = *puVar1;
    *puVar1 = uVar5 - uVar3;
    puVar1 = puVar1 + 1;
  }
  return;
}



void FUN_d900a640(ulong param_1,ulong *param_2,ulong *param_3,ulong param_4)

{
  ulong *puVar1;
  ulong uVar2;
  ulong *puVar3;
  long lVar4;
  long lVar5;
  ulong uVar6;
  ulong uVar7;
  ulong uVar8;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  ulong uVar12;
  ulong uVar13;
  ulong uVar14;
  ulong uVar15;
  
  uVar2 = param_4 >> 0x20;
  uVar7 = 0;
  uVar6 = param_4 & 0xffffffff;
  puVar3 = param_3;
  puVar1 = param_2;
  uVar8 = param_1;
  while (0xf < uVar8) {
    uVar8 = uVar8 - 0x10;
    uVar10 = *puVar1 & 0xffffffff;
    uVar9 = *puVar1 >> 0x20;
    uVar12 = uVar9 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar12 << 0x20;
    uVar11 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar12 >> 0x20) + (uVar14 >> 0x20) + uVar9 * uVar2;
    uVar9 = uVar11 + uVar13;
    if (uVar11 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar11 = uVar9 + uVar7;
    if (uVar9 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = *puVar3;
    if (uVar11 < uVar7) {
      uVar10 = uVar10 + 1;
    }
    uVar11 = uVar11 + uVar9;
    *puVar3 = uVar11;
    if (uVar11 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[1] & 0xffffffff;
    uVar7 = puVar1[1] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[1];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[1] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[2] & 0xffffffff;
    uVar7 = puVar1[2] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[2];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[2] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[3] & 0xffffffff;
    uVar7 = puVar1[3] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[3];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[3] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[4] & 0xffffffff;
    uVar7 = puVar1[4] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[4];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[4] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[5] & 0xffffffff;
    uVar7 = puVar1[5] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[5];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[5] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[6] & 0xffffffff;
    uVar7 = puVar1[6] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[6];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[6] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[7] & 0xffffffff;
    uVar7 = puVar1[7] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[7];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[7] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[8] & 0xffffffff;
    uVar7 = puVar1[8] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[8];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[8] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[9] & 0xffffffff;
    uVar7 = puVar1[9] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[9];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[9] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[10] & 0xffffffff;
    uVar7 = puVar1[10] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[10];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[10] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[0xb] & 0xffffffff;
    uVar7 = puVar1[0xb] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[0xb];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[0xb] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[0xc] & 0xffffffff;
    uVar7 = puVar1[0xc] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[0xc];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[0xc] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[0xd] & 0xffffffff;
    uVar7 = puVar1[0xd] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[0xd];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[0xd] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[0xe] & 0xffffffff;
    uVar7 = puVar1[0xe] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[0xe];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[0xe] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[0xf] & 0xffffffff;
    uVar7 = puVar1[0xf] >> 0x20;
    uVar12 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar12 << 0x20;
    uVar11 = uVar15 + uVar9 * uVar6;
    uVar7 = (uVar12 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar9 = uVar11 + uVar13;
    if (uVar11 < uVar15) {
      uVar7 = uVar7 + 1;
    }
    uVar11 = uVar10 + uVar9;
    if (uVar9 < uVar13) {
      uVar7 = uVar7 + 1;
    }
    uVar9 = puVar3[0xf];
    if (uVar11 < uVar10) {
      uVar7 = uVar7 + 1;
    }
    uVar11 = uVar11 + uVar9;
    puVar3[0xf] = uVar11;
    puVar1 = puVar1 + 0x10;
    if (uVar11 < uVar9) {
      uVar7 = uVar7 + 1;
    }
    puVar3 = puVar3 + 0x10;
  }
  param_3 = param_3 + (param_1 & 0xfffffffffffffff0);
  uVar6 = param_4 & 0xffffffff;
  puVar3 = param_3;
  puVar1 = param_2 + (param_1 & 0xfffffffffffffff0);
  uVar8 = param_1 & 0xf;
  while (7 < uVar8) {
    uVar8 = uVar8 - 8;
    uVar10 = *puVar1 & 0xffffffff;
    uVar9 = *puVar1 >> 0x20;
    uVar12 = uVar9 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar12 << 0x20;
    uVar11 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar12 >> 0x20) + (uVar14 >> 0x20) + uVar9 * uVar2;
    uVar9 = uVar11 + uVar13;
    if (uVar11 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar11 = uVar9 + uVar7;
    if (uVar9 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = *puVar3;
    if (uVar11 < uVar7) {
      uVar10 = uVar10 + 1;
    }
    uVar11 = uVar11 + uVar9;
    *puVar3 = uVar11;
    if (uVar11 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[1] & 0xffffffff;
    uVar7 = puVar1[1] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[1];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[1] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[2] & 0xffffffff;
    uVar7 = puVar1[2] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[2];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[2] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[3] & 0xffffffff;
    uVar7 = puVar1[3] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[3];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[3] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[4] & 0xffffffff;
    uVar7 = puVar1[4] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[4];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[4] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[5] & 0xffffffff;
    uVar7 = puVar1[5] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar9 * uVar6;
    uVar9 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[5];
    if (uVar12 < uVar15) {
      uVar9 = uVar9 + 1;
    }
    uVar12 = uVar10 + uVar7;
    if (uVar7 < uVar13) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[5] = uVar7;
    if (uVar12 < uVar10) {
      uVar9 = uVar9 + 1;
    }
    if (uVar7 < uVar11) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = puVar1[6] & 0xffffffff;
    uVar7 = puVar1[6] >> 0x20;
    uVar11 = uVar7 * uVar6;
    uVar14 = uVar10 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar11 << 0x20;
    uVar12 = uVar15 + uVar10 * uVar6;
    uVar10 = (uVar11 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar7 = uVar12 + uVar13;
    uVar11 = puVar3[6];
    if (uVar12 < uVar15) {
      uVar10 = uVar10 + 1;
    }
    uVar12 = uVar9 + uVar7;
    if (uVar7 < uVar13) {
      uVar10 = uVar10 + 1;
    }
    uVar7 = uVar12 + uVar11;
    puVar3[6] = uVar7;
    if (uVar12 < uVar9) {
      uVar10 = uVar10 + 1;
    }
    if (uVar7 < uVar11) {
      uVar10 = uVar10 + 1;
    }
    uVar9 = puVar1[7] & 0xffffffff;
    uVar7 = puVar1[7] >> 0x20;
    uVar12 = uVar7 * uVar6;
    uVar14 = uVar9 * uVar2;
    uVar15 = uVar14 << 0x20;
    uVar13 = uVar12 << 0x20;
    uVar11 = uVar15 + uVar9 * uVar6;
    uVar7 = (uVar12 >> 0x20) + (uVar14 >> 0x20) + uVar7 * uVar2;
    uVar9 = uVar11 + uVar13;
    if (uVar11 < uVar15) {
      uVar7 = uVar7 + 1;
    }
    uVar11 = uVar10 + uVar9;
    if (uVar9 < uVar13) {
      uVar7 = uVar7 + 1;
    }
    uVar9 = puVar3[7];
    if (uVar11 < uVar10) {
      uVar7 = uVar7 + 1;
    }
    uVar11 = uVar11 + uVar9;
    puVar3[7] = uVar11;
    puVar1 = puVar1 + 8;
    if (uVar11 < uVar9) {
      uVar7 = uVar7 + 1;
    }
    puVar3 = puVar3 + 8;
  }
  lVar4 = ((param_1 & 0xf) >> 3) * 0x40;
  lVar5 = 0;
  uVar8 = param_1 & 7;
  while (uVar8 != 0) {
    uVar6 = *(ulong *)((long)(param_2 + (param_1 & 0xfffffffffffffff0)) + lVar5 + lVar4);
    uVar8 = uVar8 - 1;
    uVar9 = uVar6 & 0xffffffff;
    uVar6 = uVar6 >> 0x20;
    uVar11 = uVar6 * (param_4 & 0xffffffff);
    uVar13 = uVar9 * uVar2;
    uVar14 = uVar13 << 0x20;
    uVar12 = uVar11 << 0x20;
    uVar10 = uVar14 + uVar9 * (param_4 & 0xffffffff);
    uVar9 = (uVar11 >> 0x20) + (uVar13 >> 0x20) + uVar6 * uVar2;
    uVar6 = uVar10 + uVar12;
    if (uVar10 < uVar14) {
      uVar9 = uVar9 + 1;
    }
    uVar10 = uVar6 + uVar7;
    if (uVar6 < uVar12) {
      uVar9 = uVar9 + 1;
    }
    if (uVar10 < uVar7) {
      uVar9 = uVar9 + 1;
    }
    uVar7 = *(ulong *)((long)param_3 + lVar5 + lVar4);
    uVar10 = uVar10 + uVar7;
    *(ulong *)((long)param_3 + lVar5 + lVar4) = uVar10;
    if (uVar10 < uVar7) {
      uVar9 = uVar9 + 1;
    }
    lVar5 = lVar5 + 8;
    uVar7 = uVar9;
  }
  puVar1 = param_3 + (param_1 & 8) + (param_1 & 7);
  do {
    uVar8 = *puVar1;
    *puVar1 = uVar7 + uVar8;
    uVar7 = (ulong)(uVar7 + uVar8 < uVar7);
    puVar1 = puVar1 + 1;
  } while (uVar7 != 0);
  return;
}



void FUN_d900b05c(undefined4 *param_1)

{
  if (param_1 != (undefined4 *)0x0) {
    *param_1 = 1;
    *(undefined8 *)(param_1 + 2) = 0;
    *(undefined8 *)(param_1 + 4) = 0;
  }
  return;
}



void FUN_d900b074(undefined4 *param_1)

{
  if (param_1 != (undefined4 *)0x0) {
    if (*(long *)(param_1 + 4) != 0) {
      FUN_d9004c14(*(long *)(param_1 + 4),0,*(long *)(param_1 + 2) << 3);
      FUN_d9004ce8(*(undefined8 *)(param_1 + 4));
    }
    *param_1 = 1;
    *(undefined8 *)(param_1 + 2) = 0;
    *(undefined8 *)(param_1 + 4) = 0;
  }
  return;
}



undefined8 FUN_d900b0c4(long param_1,ulong param_2)

{
  long lVar1;
  
  if (param_2 < 0x2711) {
    if (param_2 <= *(ulong *)(param_1 + 8)) {
      return 0;
    }
    lVar1 = FUN_d9004cb4((int)param_2 << 3);
    if (lVar1 != 0) {
      FUN_d9004c14(lVar1,0,param_2 << 3);
      if (*(long *)(param_1 + 0x10) != 0) {
        FUN_d9004b9c(lVar1,*(long *)(param_1 + 0x10),*(long *)(param_1 + 8) << 3);
        FUN_d9004c14(*(undefined8 *)(param_1 + 0x10),0,*(long *)(param_1 + 8) << 3);
        FUN_d9004ce8(*(undefined8 *)(param_1 + 0x10));
      }
      *(ulong *)(param_1 + 8) = param_2;
      *(long *)(param_1 + 0x10) = lVar1;
      return 0;
    }
  }
  return 0xfffffff0;
}



int FUN_d900b170(undefined4 *param_1,undefined4 *param_2)

{
  long lVar1;
  long lVar2;
  int iVar3;
  long lVar4;
  
  if (param_1 == param_2) {
    iVar3 = 0;
  }
  else {
    lVar4 = *(long *)(param_2 + 2) * 8;
    lVar2 = *(long *)(param_2 + 2);
    do {
      lVar1 = lVar2;
      lVar4 = lVar4 + -8;
      if (lVar1 + -1 == 0) break;
      lVar2 = lVar1 + -1;
    } while (*(long *)(*(long *)(param_2 + 4) + lVar4) == 0);
    *param_1 = *param_2;
    iVar3 = FUN_d900b0c4(param_1,lVar1);
    if (iVar3 == 0) {
      FUN_d9004c14(*(undefined8 *)(param_1 + 4),0,*(long *)(param_1 + 2) << 3);
      FUN_d9004b9c(*(undefined8 *)(param_1 + 4),*(undefined8 *)(param_2 + 4),lVar1 * 8);
    }
  }
  return iVar3;
}



int FUN_d900b248(undefined4 *param_1,ulong param_2)

{
  int iVar1;
  undefined4 uVar2;
  
  iVar1 = FUN_d900b0c4(param_1,1);
  if (iVar1 == 0) {
    FUN_d9004c14(*(undefined8 *)(param_1 + 4),0,*(long *)(param_1 + 2) << 3);
    **(long **)(param_1 + 4) = (param_2 ^ (long)param_2 >> 0x3f) - ((long)param_2 >> 0x3f);
    uVar2 = 1;
    if ((long)param_2 < 0) {
      uVar2 = 0xffffffff;
    }
    *param_1 = uVar2;
  }
  return iVar1;
}



int FUN_d900b2e8(long param_1,ulong param_2,byte param_3)

{
  int iVar1;
  long lVar2;
  
  if (1 < param_3) {
    return -4;
  }
  if ((ulong)(*(long *)(param_1 + 8) << 6) <= param_2) {
    if (param_3 == 0) {
      return 0;
    }
    iVar1 = FUN_d900b0c4(param_1,(param_2 >> 6) + 1);
    if (iVar1 != 0) {
      return iVar1;
    }
  }
  lVar2 = (param_2 >> 6) * 8;
  *(ulong *)(*(long *)(param_1 + 0x10) + lVar2) =
       (long)~(1 << (ulong)((uint)param_2 & 0x1f)) & *(ulong *)(*(long *)(param_1 + 0x10) + lVar2) |
       (long)(int)((uint)param_3 << (ulong)((uint)param_2 & 0x1f));
  return 0;
}



long FUN_d900b38c(long param_1)

{
  long lVar1;
  long lVar2;
  long lVar3;
  
  lVar3 = 0;
  lVar2 = 0;
  do {
    if (lVar3 == *(long *)(param_1 + 8)) {
      return 0;
    }
    lVar1 = lVar2;
    do {
      if ((*(ulong *)(*(long *)(param_1 + 0x10) + lVar3 * 8) >>
           ((ulong)(uint)((int)lVar1 - (int)lVar2) & 0x3f) & 1) != 0) {
        return lVar1;
      }
      lVar1 = lVar1 + 1;
    } while (lVar1 != lVar2 + 0x40);
    lVar3 = lVar3 + 1;
    lVar2 = lVar1;
  } while( true );
}



long FUN_d900b3e4(long param_1)

{
  ulong uVar1;
  long lVar2;
  long lVar3;
  ulong uVar4;
  ulong uVar5;
  
  lVar2 = *(long *)(param_1 + 8);
  lVar3 = lVar2 * 8;
  do {
    lVar2 = lVar2 + -1;
    lVar3 = lVar3 + -8;
    if (lVar2 == 0) break;
  } while (*(long *)(*(long *)(param_1 + 0x10) + lVar3) == 0);
  uVar4 = 0x3f;
  do {
    uVar5 = *(ulong *)(*(long *)(param_1 + 0x10) + lVar2 * 8) >> (uVar4 & 0x3f);
    uVar1 = uVar4 + 1;
    if ((uVar5 & 1) != 0) break;
    uVar4 = uVar4 - 1;
    uVar1 = uVar5 & 1;
  } while (uVar4 != 0xffffffffffffffff);
  return uVar1 + lVar2 * 0x40;
}



ulong FUN_d900b444(void)

{
  long lVar1;
  
  lVar1 = FUN_d900b3e4();
  return lVar1 + 7U >> 3;
}



void FUN_d900b460(long param_1,long param_2,ulong param_3)

{
  int iVar1;
  ulong uVar2;
  ulong uVar3;
  ulong uVar4;
  ulong uVar5;
  
  for (uVar5 = 0; (uVar5 != param_3 && (*(char *)(param_2 + uVar5) == '\0')); uVar5 = uVar5 + 1) {
  }
  iVar1 = FUN_d900b0c4(param_1,(param_3 + 7) - uVar5 >> 3);
  if ((iVar1 == 0) && (iVar1 = FUN_d900b248(param_1,0), uVar3 = param_3, iVar1 == 0)) {
    while (uVar2 = param_3 - uVar3, uVar5 < uVar3) {
      uVar3 = uVar3 - 1;
      uVar4 = uVar2 & 0xfffffffffffffff8;
      *(ulong *)(*(long *)(param_1 + 0x10) + uVar4) =
           *(ulong *)(*(long *)(param_1 + 0x10) + uVar4) |
           (ulong)*(byte *)(param_2 + uVar3) << ((uVar2 & 7) << 3);
    }
  }
  return;
}



undefined8 FUN_d900b50c(long param_1,long param_2,ulong param_3)

{
  ulong uVar1;
  undefined8 uVar2;
  ulong uVar3;
  
  uVar1 = FUN_d900b444();
  if (param_3 < uVar1) {
    uVar2 = 0xfffffff8;
  }
  else {
    FUN_d9004c14(param_2,0,param_3);
    for (uVar3 = 0; uVar3 != uVar1; uVar3 = uVar3 + 1) {
      *(char *)(((param_2 + param_3) - uVar3) + -1) =
           (char)(*(ulong *)(*(long *)(param_1 + 0x10) + (uVar3 & 0xfffffffffffffff8)) >>
                 ((uVar3 & 7) << 3));
    }
    uVar2 = 0;
  }
  return uVar2;
}



undefined8 FUN_d900b598(long param_1,ulong param_2)

{
  long lVar1;
  undefined8 uVar2;
  ulong uVar3;
  ulong uVar4;
  ulong uVar5;
  ulong uVar6;
  
  uVar5 = param_2 >> 6;
  uVar6 = param_2 & 0x3f;
  lVar1 = FUN_d900b3e4();
  if ((lVar1 + param_2 <= (ulong)(*(long *)(param_1 + 8) * 0x40)) ||
     (uVar2 = FUN_d900b0c4(param_1,lVar1 + param_2 + 0x3f >> 6), (int)uVar2 == 0)) {
    if (uVar5 != 0) {
      uVar3 = *(ulong *)(param_1 + 8);
      lVar1 = uVar3 * 8;
      for (; lVar1 = lVar1 + -8, uVar5 < uVar3; uVar3 = uVar3 - 1) {
        *(undefined8 *)(*(long *)(param_1 + 0x10) + lVar1) =
             *(undefined8 *)(*(long *)(param_1 + 0x10) + lVar1 + uVar5 * -8);
      }
      lVar1 = uVar3 << 3;
      for (; lVar1 = lVar1 + -8, uVar3 != 0; uVar3 = uVar3 - 1) {
        *(undefined8 *)(*(long *)(param_1 + 0x10) + lVar1) = 0;
      }
    }
    if (uVar6 != 0) {
      lVar1 = uVar5 << 3;
      uVar3 = 0;
      for (; uVar5 < *(ulong *)(param_1 + 8); uVar5 = uVar5 + 1) {
        uVar4 = *(ulong *)(*(long *)(param_1 + 0x10) + lVar1);
        *(ulong *)(*(long *)(param_1 + 0x10) + lVar1) = uVar4 << uVar6;
        *(ulong *)(*(long *)(param_1 + 0x10) + lVar1) =
             *(ulong *)(*(long *)(param_1 + 0x10) + lVar1) | uVar3;
        lVar1 = lVar1 + 8;
        uVar3 = uVar4 >> ((ulong)(0x40 - (int)uVar6) & 0x3f);
      }
    }
    uVar2 = 0;
  }
  return uVar2;
}



undefined8 FUN_d900b6b0(long param_1,ulong param_2)

{
  undefined8 uVar1;
  long lVar2;
  ulong uVar3;
  long lVar4;
  ulong uVar5;
  
  uVar3 = param_2 >> 6;
  param_2 = param_2 & 0x3f;
  if ((uVar3 <= *(ulong *)(param_1 + 8)) && ((uVar3 != *(ulong *)(param_1 + 8) || (param_2 == 0))))
  {
    if (uVar3 != 0) {
      for (uVar5 = 0; uVar5 < *(long *)(param_1 + 8) - uVar3; uVar5 = uVar5 + 1) {
        *(undefined8 *)(*(long *)(param_1 + 0x10) + uVar5 * 8) =
             *(undefined8 *)(*(long *)(param_1 + 0x10) + (uVar5 + uVar3) * 8);
      }
      lVar2 = uVar5 << 3;
      for (; uVar5 < *(ulong *)(param_1 + 8); uVar5 = uVar5 + 1) {
        *(undefined8 *)(*(long *)(param_1 + 0x10) + lVar2) = 0;
        lVar2 = lVar2 + 8;
      }
    }
    if (param_2 != 0) {
      lVar4 = *(long *)(param_1 + 8);
      lVar2 = lVar4 * 8;
      uVar3 = 0;
      for (; lVar2 = lVar2 + -8, lVar4 != 0; lVar4 = lVar4 + -1) {
        uVar5 = *(ulong *)(*(long *)(param_1 + 0x10) + lVar2);
        *(ulong *)(*(long *)(param_1 + 0x10) + lVar2) = uVar5 >> param_2;
        *(ulong *)(*(long *)(param_1 + 0x10) + lVar2) =
             *(ulong *)(*(long *)(param_1 + 0x10) + lVar2) | uVar3;
        uVar3 = uVar5 << ((ulong)(0x40 - (int)param_2) & 0x3f);
      }
    }
    return 0;
  }
  uVar1 = FUN_d900b248(param_1,0);
  return uVar1;
}



undefined8 FUN_d900b784(long param_1,long param_2)

{
  long lVar1;
  undefined8 uVar2;
  ulong uVar3;
  ulong uVar4;
  ulong uVar5;
  
  uVar3 = *(ulong *)(param_1 + 8);
  lVar1 = uVar3 * 8;
  while ((lVar1 = lVar1 + -8, uVar3 != 0 && (*(long *)(*(long *)(param_1 + 0x10) + lVar1) == 0))) {
    uVar3 = uVar3 - 1;
  }
  uVar4 = *(ulong *)(param_2 + 8);
  lVar1 = uVar4 * 8;
  while ((lVar1 = lVar1 + -8, uVar4 != 0 && (*(long *)(*(long *)(param_2 + 0x10) + lVar1) == 0))) {
    uVar4 = uVar4 - 1;
  }
  uVar2 = 0;
  if (((uVar4 | uVar3) != 0) && (uVar2 = 1, uVar3 <= uVar4)) {
    if (uVar3 < uVar4) {
LAB_d900b83c:
      uVar2 = 0xffffffff;
    }
    else {
      lVar1 = uVar3 * 8;
      for (; lVar1 = lVar1 + -8, uVar3 != 0; uVar3 = uVar3 - 1) {
        uVar5 = *(ulong *)(*(long *)(param_1 + 0x10) + lVar1);
        uVar4 = *(ulong *)(*(long *)(param_2 + 0x10) + lVar1);
        if (uVar4 < uVar5) {
          return 1;
        }
        if (uVar5 < uVar4) goto LAB_d900b83c;
      }
      uVar2 = 0;
    }
  }
  return uVar2;
}



void FUN_d900b844(long param_1,long param_2,long param_3,long param_4,long param_5)

{
  ulong uVar1;
  int iVar2;
  long lVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  long lVar6;
  ulong uVar7;
  long *plVar8;
  ulong uVar9;
  long lVar10;
  long *plVar11;
  
  FUN_d9004c14(*(undefined8 *)(param_5 + 0x10),0,*(long *)(param_5 + 8) << 3);
  uVar9 = *(ulong *)(param_3 + 8);
  plVar11 = *(long **)(param_5 + 0x10);
  uVar1 = *(ulong *)(param_2 + 8);
  if (uVar9 < *(ulong *)(param_2 + 8)) {
    uVar1 = uVar9;
  }
  plVar8 = plVar11;
  for (uVar7 = 0; uVar7 != uVar9; uVar7 = uVar7 + 1) {
    lVar10 = *(long *)(*(long *)(param_1 + 0x10) + uVar7 * 8);
    lVar6 = **(long **)(param_2 + 0x10);
    lVar3 = *plVar8;
    FUN_d900a640(uVar1,*(long **)(param_2 + 0x10),plVar8,lVar10);
    FUN_d900a640(uVar9,*(undefined8 *)(param_3 + 0x10),plVar8,(lVar3 + lVar10 * lVar6) * param_4);
    *plVar8 = lVar10;
    (plVar8 + 1)[uVar9 + 1] = 0;
    plVar8 = plVar8 + 1;
  }
  FUN_d9004b9c(*(long *)(param_1 + 0x10),plVar11 + uVar9,(uVar9 + 1) * 8);
  iVar2 = FUN_d900b784(param_1,param_3);
  if (iVar2 < 0) {
    uVar4 = *(undefined8 *)(param_1 + 0x10);
    uVar5 = *(undefined8 *)(param_5 + 0x10);
  }
  else {
    uVar4 = *(undefined8 *)(param_3 + 0x10);
    uVar5 = *(undefined8 *)(param_1 + 0x10);
  }
  FUN_d900a5d4(uVar7,uVar4,uVar5);
  return;
}



int FUN_d900b974(int *param_1,int *param_2)

{
  int iVar1;
  int iVar2;
  long lVar3;
  ulong uVar4;
  ulong uVar5;
  
  uVar4 = *(ulong *)(param_1 + 2);
  lVar3 = uVar4 * 8;
  while ((lVar3 = lVar3 + -8, uVar4 != 0 && (*(long *)(*(long *)(param_1 + 4) + lVar3) == 0))) {
    uVar4 = uVar4 - 1;
  }
  uVar5 = *(ulong *)(param_2 + 2);
  lVar3 = uVar5 * 8;
  while ((lVar3 = lVar3 + -8, uVar5 != 0 && (*(long *)(*(long *)(param_2 + 4) + lVar3) == 0))) {
    uVar5 = uVar5 - 1;
  }
  iVar2 = 0;
  if ((uVar5 | uVar4) != 0) {
    if (uVar5 < uVar4) {
      iVar2 = *param_1;
    }
    else {
      iVar2 = *param_2;
      if (uVar4 < uVar5) {
        iVar2 = -iVar2;
      }
      else {
        iVar1 = *param_1;
        if ((iVar1 < 1) || (-1 < iVar2)) {
          if ((iVar2 < 1) || (-1 < iVar1)) {
            lVar3 = uVar4 * 8;
            for (; lVar3 = lVar3 + -8, uVar4 != 0; uVar4 = uVar4 - 1) {
              if (*(ulong *)(*(long *)(param_2 + 4) + lVar3) <
                  *(ulong *)(*(long *)(param_1 + 4) + lVar3)) {
                return iVar1;
              }
              if (*(ulong *)(*(long *)(param_1 + 4) + lVar3) <
                  *(ulong *)(*(long *)(param_2 + 4) + lVar3)) {
                return -iVar1;
              }
            }
            iVar2 = 0;
          }
          else {
            iVar2 = -1;
          }
        }
        else {
          iVar2 = 1;
        }
      }
    }
  }
  return iVar2;
}



void FUN_d900ba68(undefined8 param_1,ulong param_2)

{
  undefined4 local_20 [2];
  undefined8 local_18;
  long *local_10;
  long local_8;
  
  local_8 = (param_2 ^ (long)param_2 >> 0x3f) - ((long)param_2 >> 0x3f);
  local_20[0] = 0xffffffff;
  if (-1 < (long)param_2) {
    local_20[0] = 1;
  }
  local_18 = 1;
  local_10 = &local_8;
  FUN_d900b974(param_1,local_20);
  return;
}



int FUN_d900baac(undefined4 *param_1,undefined4 *param_2,undefined4 *param_3)

{
  ulong uVar1;
  ulong uVar2;
  bool bVar3;
  int iVar4;
  int iVar5;
  long lVar6;
  long lVar7;
  ulong *puVar8;
  ulong uVar9;
  long lVar10;
  ulong uVar11;
  ulong uVar12;
  
  if (((param_1 == param_3) || (bVar3 = param_1 == param_2, param_2 = param_3, bVar3)) ||
     (iVar4 = FUN_d900b170(), iVar4 == 0)) {
    *param_1 = 1;
    uVar11 = *(ulong *)(param_2 + 2);
    lVar6 = uVar11 * 8;
    while ((lVar6 = lVar6 + -8, uVar11 != 0 && (*(long *)(*(long *)(param_2 + 4) + lVar6) == 0))) {
      uVar11 = uVar11 - 1;
    }
    iVar4 = FUN_d900b0c4(param_1,uVar11);
    if (iVar4 == 0) {
      lVar6 = 0;
      lVar7 = *(long *)(param_1 + 4);
      lVar10 = *(long *)(param_2 + 4);
      uVar9 = 0;
      uVar12 = 0;
      while (uVar9 != uVar11) {
        uVar9 = uVar9 + 1;
        uVar1 = uVar12 + *(long *)(lVar7 + lVar6);
        *(ulong *)(lVar7 + lVar6) = uVar1;
        uVar2 = uVar1 + *(long *)(lVar10 + lVar6);
        *(ulong *)(lVar7 + lVar6) = uVar2;
        uVar12 = (ulong)(uVar1 < uVar12);
        if (uVar2 < *(ulong *)(lVar10 + lVar6)) {
          uVar12 = uVar12 + 1;
        }
        lVar6 = lVar6 + 8;
      }
      lVar6 = uVar11 * 8;
      puVar8 = (ulong *)(lVar7 + lVar6);
      for (; uVar12 != 0; uVar12 = (ulong)(uVar12 + uVar9 < uVar12)) {
        bVar3 = *(ulong *)(param_1 + 2) <= uVar11;
        uVar11 = uVar11 + 1;
        if (bVar3) {
          iVar5 = FUN_d900b0c4(param_1,uVar11);
          if (iVar5 != 0) {
            return iVar5;
          }
          puVar8 = (ulong *)(*(long *)(param_1 + 4) + lVar6);
        }
        uVar9 = *puVar8;
        lVar6 = lVar6 + 8;
        *puVar8 = uVar12 + uVar9;
        puVar8 = puVar8 + 1;
      }
    }
  }
  return iVar4;
}



int FUN_d900bbf0(undefined4 *param_1,undefined4 *param_2,undefined4 *param_3)

{
  int iVar1;
  long lVar2;
  long lVar3;
  undefined4 local_20 [2];
  undefined8 local_18;
  undefined8 local_10;
  
  iVar1 = FUN_d900b784(param_2,param_3);
  if (iVar1 < 0) {
    return -10;
  }
  local_20[0] = 1;
  local_18 = 0;
  local_10 = 0;
  if (param_1 == param_3) {
    iVar1 = FUN_d900b170(local_20,param_1);
    if (iVar1 != 0) goto LAB_d900bcac;
    param_3 = local_20;
  }
  if ((param_1 == param_2) || (iVar1 = FUN_d900b170(param_1,param_2), iVar1 == 0)) {
    *param_1 = 1;
    lVar2 = *(long *)(param_3 + 2);
    lVar3 = lVar2 * 8;
    while ((lVar3 = lVar3 + -8, lVar2 != 0 && (*(long *)(*(long *)(param_3 + 4) + lVar3) == 0))) {
      lVar2 = lVar2 + -1;
    }
    iVar1 = 0;
    FUN_d900a5d4(lVar2,*(undefined8 *)(param_3 + 4),*(undefined8 *)(param_1 + 4));
  }
LAB_d900bcac:
  FUN_d900b074(local_20);
  return iVar1;
}



void FUN_d900bcd0(int *param_1,int *param_2,int *param_3)

{
  int iVar1;
  int iVar2;
  
  iVar1 = *param_2;
  if (iVar1 * *param_3 < 0) {
    iVar2 = FUN_d900b784(param_2,param_3);
    if (iVar2 < 0) {
      iVar2 = FUN_d900bbf0(param_1,param_3,param_2);
      iVar1 = -iVar1;
    }
    else {
      iVar2 = FUN_d900bbf0(param_1,param_2,param_3);
    }
  }
  else {
    iVar2 = FUN_d900baac();
  }
  if (iVar2 == 0) {
    *param_1 = iVar1;
  }
  return;
}



void FUN_d900bd58(int *param_1,int *param_2,int *param_3)

{
  int iVar1;
  int iVar2;
  
  iVar1 = *param_2;
  if (iVar1 * *param_3 < 1) {
    iVar2 = FUN_d900baac();
  }
  else {
    iVar2 = FUN_d900b784(param_2,param_3);
    if (iVar2 < 0) {
      iVar2 = FUN_d900bbf0(param_1,param_3,param_2);
      iVar1 = -iVar1;
    }
    else {
      iVar2 = FUN_d900bbf0(param_1,param_2,param_3);
    }
  }
  if (iVar2 == 0) {
    *param_1 = iVar1;
  }
  return;
}



void FUN_d900bde4(undefined8 param_1,undefined8 param_2,ulong param_3)

{
  undefined4 local_20 [2];
  undefined8 local_18;
  long *local_10;
  long local_8;
  
  local_8 = (param_3 ^ (long)param_3 >> 0x3f) - ((long)param_3 >> 0x3f);
  local_20[0] = 0xffffffff;
  if (-1 < (long)param_3) {
    local_20[0] = 1;
  }
  local_18 = 1;
  local_10 = &local_8;
  FUN_d900bcd0(param_1,param_2,local_20);
  return;
}



void FUN_d900be28(undefined8 param_1,undefined8 param_2,ulong param_3)

{
  undefined4 local_20 [2];
  undefined8 local_18;
  long *local_10;
  long local_8;
  
  local_8 = (param_3 ^ (long)param_3 >> 0x3f) - ((long)param_3 >> 0x3f);
  local_20[0] = 0xffffffff;
  if (-1 < (long)param_3) {
    local_20[0] = 1;
  }
  local_18 = 1;
  local_10 = &local_8;
  FUN_d900bd58(param_1,param_2,local_20);
  return;
}



int FUN_d900be6c(int *param_1,int *param_2,int *param_3)

{
  int iVar1;
  long lVar2;
  long lVar3;
  long lVar4;
  int local_30 [2];
  undefined8 local_28;
  undefined8 local_20;
  int local_18 [2];
  undefined8 local_10;
  undefined8 local_8;
  
  local_18[0] = 1;
  local_10 = 0;
  local_8 = 0;
  local_30[0] = 1;
  local_28 = 0;
  local_20 = 0;
  if (param_1 == param_2) {
    iVar1 = FUN_d900b170(local_18);
    if (iVar1 != 0) goto LAB_d900bfa0;
    param_2 = local_18;
  }
  if (param_1 == param_3) {
    iVar1 = FUN_d900b170(local_30,param_1);
    if (iVar1 != 0) goto LAB_d900bfa0;
    param_3 = local_30;
  }
  lVar4 = *(long *)(param_2 + 2);
  lVar2 = lVar4 * 8;
  while ((lVar2 = lVar2 + -8, lVar4 != 0 && (*(long *)(*(long *)(param_2 + 4) + lVar2) == 0))) {
    lVar4 = lVar4 + -1;
  }
  lVar3 = *(long *)(param_3 + 2);
  lVar2 = lVar3 * 8;
  while ((lVar2 = lVar2 + -8, lVar3 != 0 && (*(long *)(*(long *)(param_3 + 4) + lVar2) == 0))) {
    lVar3 = lVar3 + -1;
  }
  iVar1 = FUN_d900b0c4(param_1,lVar3 + lVar4);
  if (iVar1 == 0) {
    iVar1 = FUN_d900b248(param_1,0);
    lVar2 = lVar3 << 3;
    if (iVar1 == 0) {
      for (; lVar2 = lVar2 + -8, lVar3 != 0; lVar3 = lVar3 + -1) {
        FUN_d900a640(lVar4,*(undefined8 *)(param_2 + 4),*(long *)(param_1 + 4) + lVar2,
                     *(undefined8 *)(*(long *)(param_3 + 4) + lVar2));
      }
      *param_1 = *param_2 * *param_3;
    }
  }
LAB_d900bfa0:
  FUN_d900b074(local_30);
  FUN_d900b074(local_18);
  return iVar1;
}



void FUN_d900bfcc(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  undefined4 local_20 [2];
  undefined8 local_18;
  undefined8 *local_10;
  undefined8 local_8;
  
  local_20[0] = 1;
  local_18 = 1;
  local_10 = &local_8;
  local_8 = param_3;
  FUN_d900be6c(param_1,param_2,local_20);
  return;
}



int FUN_d900c000(int *param_1,int param_2,char *param_3)

{
  int iVar1;
  int iVar2;
  long lVar3;
  long lVar4;
  long lVar5;
  ulong uVar6;
  undefined4 local_20 [2];
  undefined8 local_18;
  undefined8 local_10;
  long local_8;
  
  if (0xe < param_2 - 2U) {
    return -4;
  }
  local_20[0] = 1;
  local_18 = 0;
  local_10 = 0;
  lVar3 = FUN_d900d540(param_3);
  if (param_2 == 0x10) {
    iVar1 = FUN_d900b0c4(param_1,lVar3 * 4 + 0x3fU >> 6);
    if ((iVar1 != 0) || (iVar2 = FUN_d900b248(param_1,0), lVar5 = lVar3, iVar1 = iVar2, iVar2 != 0))
    goto LAB_d900c1a0;
    while (uVar6 = lVar3 - lVar5, lVar5 != 0) {
      if ((lVar5 == 1) && (*param_3 == '-')) {
        *param_1 = -1;
        iVar1 = iVar2;
        goto LAB_d900c1a0;
      }
      lVar5 = lVar5 + -1;
      iVar1 = FUN_d900a568(&local_8,0x10,param_3[lVar5]);
      if (iVar1 != 0) goto LAB_d900c1a0;
      lVar4 = (uVar6 >> 4) * 8;
      *(ulong *)(*(long *)(param_1 + 4) + lVar4) =
           *(ulong *)(*(long *)(param_1 + 4) + lVar4) | local_8 << ((uVar6 & 0xf) << 2);
    }
  }
  else {
    iVar1 = FUN_d900b248(param_1,0);
    if (iVar1 != 0) goto LAB_d900c1a0;
    for (lVar5 = 0; lVar5 != lVar3; lVar5 = lVar5 + 1) {
      if ((lVar5 == 0) && (*param_3 == '-')) {
        *param_1 = -1;
      }
      else {
        iVar1 = FUN_d900a568(&local_8,param_2,param_3[lVar5]);
        if ((iVar1 != 0) || (iVar1 = FUN_d900bfcc(local_20,param_1,(long)param_2), iVar1 != 0))
        goto LAB_d900c1a0;
        if (*param_1 == 1) {
          iVar1 = FUN_d900bde4();
        }
        else {
          iVar1 = FUN_d900be28(param_1,local_20,local_8);
        }
        if (iVar1 != 0) goto LAB_d900c1a0;
      }
    }
  }
  iVar1 = 0;
LAB_d900c1a0:
  FUN_d900b074(local_20);
  return iVar1;
}



int FUN_d900c1cc(int *param_1,undefined4 *param_2,int *param_3,int *param_4)

{
  long lVar1;
  int iVar2;
  int iVar3;
  int iVar4;
  ulong uVar5;
  ulong uVar6;
  undefined8 uVar7;
  ulong uVar8;
  long lVar9;
  ulong uVar10;
  ulong uVar11;
  ulong uVar12;
  ulong uVar13;
  ulong uVar14;
  long lVar15;
  long lVar16;
  ulong uVar17;
  undefined4 local_a0 [2];
  undefined8 local_98;
  undefined8 *local_90;
  undefined4 local_88 [2];
  undefined8 local_80;
  undefined8 *local_78;
  undefined4 local_70 [2];
  undefined8 local_68;
  long local_60;
  undefined4 local_58 [2];
  long local_50;
  long local_48;
  int local_40 [2];
  long local_38;
  long local_30;
  long local_20;
  long local_18;
  long local_10;
  long local_8;
  
  iVar2 = FUN_d900ba68(param_4,0);
  if (iVar2 == 0) {
    return L'\xfffffff4';
  }
  local_40[0] = 1;
  local_38 = 0;
  local_30 = 0;
  local_58[0] = 1;
  local_50 = 0;
  local_48 = 0;
  local_70[0] = 1;
  local_68 = 0;
  local_60 = 0;
  local_88[0] = 1;
  local_80 = 0;
  local_78 = (undefined8 *)0x0;
  local_a0[0] = 1;
  local_98 = 0;
  local_90 = (undefined8 *)0x0;
  iVar2 = FUN_d900b784(param_3,param_4);
  if (iVar2 < 0) {
    if (((param_1 == (int *)0x0) || (iVar2 = FUN_d900b248(param_1,0), iVar2 == 0)) &&
       ((param_2 == (undefined4 *)0x0 || (iVar2 = FUN_d900b170(param_2,param_3), iVar2 == 0)))) {
      return 0;
    }
  }
  else {
    iVar2 = FUN_d900b170(local_40,param_3);
    if ((iVar2 == 0) && (iVar2 = FUN_d900b170(local_58,param_4), iVar2 == 0)) {
      local_58[0] = 1;
      local_40[0] = 1;
      iVar2 = FUN_d900b0c4(local_70,*(long *)(param_3 + 2) + 2);
      if ((iVar2 == 0) &&
         (((iVar2 = FUN_d900b248(local_70,0), iVar2 == 0 &&
           (iVar2 = FUN_d900b0c4(local_88,2), iVar2 == 0)) &&
          (iVar2 = FUN_d900b0c4(local_a0,3), iVar2 == 0)))) {
        uVar5 = FUN_d900b3e4(local_58);
        if ((uVar5 & 0x3f) == 0x3f) {
          lVar16 = 0;
        }
        else {
          lVar16 = 0x3f - (uVar5 & 0x3f);
          iVar2 = FUN_d900b598(local_40,lVar16);
          if ((iVar2 != 0) || (iVar2 = FUN_d900b598(local_58,lVar16), iVar2 != 0))
          goto LAB_d900c6e4;
        }
        lVar15 = local_38;
        lVar1 = local_50;
        uVar5 = local_38 - 1;
        uVar17 = local_50 - 1;
        local_8 = uVar5 - uVar17;
        local_20 = local_8 * 0x40;
        iVar3 = FUN_d900b598(local_58,local_20);
        lVar9 = local_8 << 3;
        iVar2 = iVar3;
        if (iVar3 == 0) {
          while (local_8 = lVar9, iVar2 = FUN_d900b974(local_40,local_58), -1 < iVar2) {
            *(long *)(local_60 + local_8) = *(long *)(local_60 + local_8) + 1;
            FUN_d900bd58(local_40,local_40,local_58);
            lVar9 = local_8;
          }
          FUN_d900b6b0(local_58,local_20);
          local_20 = (lVar15 - lVar1) * 0x40;
          local_18 = uVar17 * 8;
          lVar15 = lVar15 * 8;
          local_10 = lVar1 * -8;
          while( true ) {
            local_20 = local_20 + -0x40;
            lVar15 = lVar15 + -8;
            if (uVar5 <= uVar17) break;
            uVar12 = *(ulong *)(local_30 + lVar15);
            uVar11 = *(ulong *)(local_48 + local_18);
            if (uVar12 < uVar11) {
              uVar6 = uVar11 >> 0x20;
              uVar14 = 0;
              if (uVar6 != 0) {
                uVar14 = uVar12 / uVar6;
              }
              uVar10 = *(ulong *)(local_30 + lVar15 + -8);
              uVar12 = uVar10 >> 0x20 | uVar12 - uVar14 * uVar6 << 0x20;
              uVar8 = uVar14 * (uVar11 & 0xffffffff);
              if (uVar12 < uVar8) {
                do {
                  uVar12 = uVar12 + uVar11;
                  uVar14 = uVar14 - 1;
                  if (uVar8 <= uVar12) break;
                } while (uVar11 <= uVar12);
              }
              uVar13 = 0;
              if (uVar6 != 0) {
                uVar13 = (uVar12 - uVar8) / uVar6;
              }
              uVar12 = uVar10 & 0xffffffff | (uVar12 - uVar8) - uVar13 * uVar6 << 0x20;
              uVar6 = uVar13 * (uVar11 & 0xffffffff);
              if (uVar12 < uVar6) {
                do {
                  uVar12 = uVar12 + uVar11;
                  uVar13 = uVar13 - 1;
                  if (uVar6 <= uVar12) break;
                } while (uVar11 <= uVar12);
              }
              *(ulong *)(local_60 + local_10 + lVar15) = uVar13 | uVar14 << 0x20;
            }
            else {
              *(undefined8 *)(local_60 + local_10 + lVar15) = 0xffffffffffffffff;
            }
            lVar1 = lVar15 + local_10;
            *(long *)(local_60 + lVar1) = *(long *)(local_60 + lVar1) + 1;
            do {
              *(long *)(local_60 + lVar1) = *(long *)(local_60 + lVar1) + -1;
              iVar2 = FUN_d900b248(local_88,0);
              if (iVar2 != 0) goto LAB_d900c6e4;
              uVar7 = 0;
              if (uVar17 != 0) {
                uVar7 = *(undefined8 *)(local_48 + local_18 + -8);
              }
              *local_78 = uVar7;
              local_78[1] = *(undefined8 *)(local_48 + local_18);
              iVar2 = FUN_d900bfcc(local_88,local_88,*(undefined8 *)(local_60 + lVar1));
              if ((iVar2 != 0) || (iVar2 = FUN_d900b248(local_a0,0), iVar2 != 0)) goto LAB_d900c6e4;
              if (uVar5 == 1) {
                uVar7 = 0;
              }
              else {
                uVar7 = *(undefined8 *)(local_30 + lVar15 + -0x10);
              }
              *local_90 = uVar7;
              local_90[1] = *(undefined8 *)(local_30 + lVar15 + -8);
              local_90[2] = *(undefined8 *)(local_30 + lVar15);
              iVar2 = FUN_d900b974(local_88,local_a0);
            } while (0 < iVar2);
            iVar2 = FUN_d900bfcc(local_88,local_58,*(undefined8 *)(local_60 + lVar1));
            if (((iVar2 != 0) || (iVar2 = FUN_d900b598(local_88,local_20), iVar2 != 0)) ||
               (iVar2 = FUN_d900bd58(local_40,local_40,local_88), iVar2 != 0)) goto LAB_d900c6e4;
            iVar2 = FUN_d900ba68(local_40,0);
            if (iVar2 < 0) {
              iVar2 = FUN_d900b170(local_88,local_58);
              if (((iVar2 != 0) || (iVar2 = FUN_d900b598(local_88,local_20), iVar2 != 0)) ||
                 (iVar2 = FUN_d900bcd0(local_40,local_40,local_88), iVar2 != 0)) goto LAB_d900c6e4;
              *(long *)(local_60 + lVar1) = *(long *)(local_60 + lVar1) + -1;
            }
            uVar5 = uVar5 - 1;
          }
          if (param_1 != (int *)0x0) {
            FUN_d900b170(param_1,local_70);
            *param_1 = *param_3 * *param_4;
          }
          iVar2 = iVar3;
          if (param_2 != (undefined4 *)0x0) {
            FUN_d900b6b0(local_40,lVar16);
            local_40[0] = *param_3;
            FUN_d900b170(param_2,local_40);
            iVar4 = FUN_d900ba68(param_2,0);
            if (iVar4 == 0) {
              *param_2 = 1;
            }
          }
        }
      }
    }
  }
LAB_d900c6e4:
  FUN_d900b074(local_40);
  FUN_d900b074(local_58);
  FUN_d900b074(local_70);
  FUN_d900b074(local_88);
  FUN_d900b074(local_a0);
  return iVar2;
}



void FUN_d900c734(void)

{
  FUN_d900c1cc();
  return;
}



undefined8 FUN_d900c778(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  int iVar1;
  undefined8 uVar2;
  
  iVar1 = FUN_d900ba68(param_3,0);
  if (iVar1 < 0) {
    return 0xfffffff6;
  }
  uVar2 = FUN_d900c1cc(0,param_1,param_2,param_3);
  while( true ) {
    if ((int)uVar2 != 0) {
      return uVar2;
    }
    iVar1 = FUN_d900ba68(param_1,0);
    if (-1 < iVar1) break;
    uVar2 = FUN_d900bcd0(param_1,param_1,param_3);
  }
  do {
    iVar1 = FUN_d900b974(param_1,param_3);
    if (iVar1 < 0) {
      return 0;
    }
    uVar2 = FUN_d900bd58(param_1,param_1,param_3);
  } while ((int)uVar2 == 0);
  return uVar2;
}



int FUN_d900c8d0(undefined4 *param_1,int *param_2,long param_3,long param_4,undefined8 *param_5)

{
  int iVar1;
  int iVar2;
  ulong uVar3;
  ulong uVar4;
  undefined *puVar5;
  undefined *puVar6;
  ulong uVar7;
  int iVar8;
  ulong uVar9;
  long lVar10;
  ulong uVar11;
  ulong uVar12;
  undefined auStack_ca0 [24];
  undefined auStack_c88 [3048];
  undefined4 local_a0 [2];
  undefined8 local_98;
  undefined8 *local_90;
  int local_88 [2];
  undefined8 local_80;
  undefined8 local_78;
  undefined4 local_70 [2];
  undefined8 local_68;
  undefined8 local_60;
  undefined8 local_58;
  undefined8 local_50;
  long local_48;
  undefined8 local_40 [2];
  ulong local_30;
  uint local_28;
  ulong local_20;
  undefined *local_18;
  ulong local_10;
  
  iVar1 = FUN_d900ba68(param_4,0);
  if (((iVar1 < 0) || (uVar11 = **(ulong **)(param_4 + 0x10), (uVar11 & 1) == 0)) ||
     (iVar1 = FUN_d900ba68(param_3,0), iVar1 < 0)) {
    return -4;
  }
  local_58 = CONCAT44(local_58._4_4_,1);
  local_70[0] = 1;
  local_50 = 0;
  local_48 = 0;
  local_68 = 0;
  local_60 = 0;
  FUN_d9004c14(auStack_ca0,0,0xc00);
  uVar3 = FUN_d900b3e4(param_3);
  uVar9 = 6;
  if (((uVar3 < 0x2a0) && (uVar9 = 5, uVar3 < 0xf0)) &&
     ((uVar9 = 4, uVar3 < 0x50 && (uVar9 = 3, uVar3 < 0x18)))) {
    uVar9 = 1;
  }
  lVar10 = *(long *)(param_4 + 8) + 1;
  iVar1 = FUN_d900b0c4(param_1,lVar10);
  iVar8 = (int)uVar9;
  if (((iVar1 == 0) && (iVar1 = FUN_d900b0c4(auStack_c88,lVar10), iVar1 == 0)) &&
     (iVar1 = FUN_d900b0c4(local_70,lVar10 * 2), iVar1 == 0)) {
    local_28 = (uint)(*param_2 == -1);
    local_88[0] = 1;
    local_80 = 0;
    local_78 = 0;
    if (local_28 != 0) {
      iVar1 = FUN_d900b170(local_88,param_2);
      if (iVar1 != 0) goto LAB_d900ce10;
      param_2 = local_88;
    }
    local_88[0] = 1;
    if ((param_5 == (undefined8 *)0x0) || (param_5[2] == 0)) {
      iVar1 = FUN_d900b248(&local_58,1);
      if ((iVar1 != 0) ||
         ((iVar1 = FUN_d900b598(&local_58,*(long *)(param_4 + 8) << 7), iVar1 != 0 ||
          (iVar1 = FUN_d900c778(&local_58,&local_58,param_4), iVar1 != 0)))) goto LAB_d900ce10;
      if (param_5 != (undefined8 *)0x0) {
        *param_5 = local_58;
        param_5[1] = local_50;
        param_5[2] = local_48;
      }
    }
    else {
      local_58 = *param_5;
      local_50 = param_5[1];
      local_48 = param_5[2];
    }
    iVar1 = FUN_d900b974(param_2,param_4);
    if (iVar1 < 0) {
      FUN_d900b170(auStack_c88,param_2);
    }
    else {
      FUN_d900c778(auStack_c88,param_2,param_4);
    }
    lVar10 = uVar11 + (uVar11 + 2 & 4) * 2;
    lVar10 = (2 - lVar10 * uVar11) * lVar10;
    lVar10 = (2 - lVar10 * uVar11) * lVar10;
    lVar10 = (2 - lVar10 * uVar11) * lVar10;
    lVar10 = -(lVar10 * (2 - lVar10 * uVar11));
    FUN_d900b844(auStack_c88,&local_58,param_4,lVar10,local_70);
    iVar2 = FUN_d900b170(param_1,&local_58);
    iVar1 = iVar2;
    if (iVar2 == 0) {
      local_90 = local_40;
      local_40[0] = 1;
      local_a0[0] = 1;
      local_98 = 1;
      FUN_d900b844(param_1,local_a0,param_4,lVar10,local_70);
      if (uVar9 != 1) {
        local_30 = 1L << ((ulong)(iVar8 - 1) & 0x3f);
        puVar5 = auStack_ca0 + local_30 * 0x18;
        iVar1 = FUN_d900b0c4(puVar5,*(long *)(param_4 + 8) + 1);
        if ((iVar1 != 0) || (iVar1 = FUN_d900b170(puVar5,auStack_c88), iVar1 != 0))
        goto LAB_d900ce10;
        puVar6 = (undefined *)0x0;
        while (puVar6 < (undefined *)(uVar9 - 1)) {
          local_18 = puVar6;
          FUN_d900b844(puVar5,puVar5,param_4,lVar10,local_70);
          puVar6 = local_18 + 1;
        }
        local_20 = 1L << uVar9;
        puVar5 = auStack_ca0 + local_30 * 0x18;
        uVar11 = local_30;
        while (uVar11 = uVar11 + 1, uVar11 < local_20) {
          puVar6 = puVar5 + 0x18;
          local_18 = puVar5;
          iVar1 = FUN_d900b0c4(puVar6,*(long *)(param_4 + 8) + 1);
          if ((iVar1 != 0) || (iVar1 = FUN_d900b170(puVar6,local_18), iVar1 != 0))
          goto LAB_d900ce10;
          FUN_d900b844(puVar6,auStack_c88,param_4,lVar10,local_70);
          puVar5 = puVar6;
        }
      }
      uVar3 = 0;
      uVar11 = 0;
      uVar12 = 0;
      uVar7 = 0;
      puVar5 = *(undefined **)(param_3 + 8);
LAB_d900cc8c:
      if (uVar7 == 0) {
        if (puVar5 == (undefined *)0x0) goto code_r0xd900cc98;
        uVar7 = 0x40;
        puVar5 = puVar5 + -1;
      }
      uVar7 = uVar7 - 1;
      uVar4 = *(ulong *)(*(long *)(param_3 + 0x10) + (long)puVar5 * 8) >> (uVar7 & 0x3f) & 1;
      if ((uVar4 | uVar3) != 0) {
        if ((uVar4 != 0) || (uVar3 != 1)) goto LAB_d900cce0;
        local_18 = puVar5;
        local_10 = uVar7;
        FUN_d900b844(param_1,param_1,param_4,lVar10,local_70);
        goto LAB_d900cd60;
      }
      uVar3 = 0;
      goto LAB_d900cc8c;
    }
  }
LAB_d900ce10:
  for (uVar11 = 1L << ((ulong)(iVar8 - 1) & 0x3f); uVar11 < (ulong)(1L << uVar9);
      uVar11 = uVar11 + 1) {
    FUN_d900b074(auStack_ca0 + uVar11 * 0x18);
  }
  FUN_d900b074(auStack_c88);
  FUN_d900b074(local_70);
  FUN_d900b074(local_88);
  if (param_5 == (undefined8 *)0x0) {
    FUN_d900b074(&local_58);
  }
  return iVar1;
code_r0xd900cc98:
  local_30 = 1L << uVar9;
  for (uVar3 = 0; uVar3 != uVar11; uVar3 = uVar3 + 1) {
    FUN_d900b844(param_1,param_1,param_4,lVar10,local_70);
    uVar12 = uVar12 << 1;
    if ((local_30 & uVar12) != 0) {
      FUN_d900b844(param_1,auStack_c88,param_4,lVar10,local_70);
    }
  }
  local_90 = local_40;
  local_40[0] = 1;
  local_a0[0] = 1;
  local_98 = 1;
  FUN_d900b844(param_1,local_a0,param_4,lVar10,local_70);
  iVar1 = iVar2;
  if (local_28 != 0) {
    *param_1 = 0xffffffff;
    FUN_d900bcd0(param_1,param_4,param_1);
  }
  goto LAB_d900ce10;
LAB_d900cce0:
  uVar11 = uVar11 + 1;
  uVar12 = uVar12 | uVar4 << ((ulong)(uint)(iVar8 - (int)uVar11) & 0x3f);
  uVar3 = 2;
  if (uVar11 == uVar9) {
    uVar11 = 0;
    local_10 = uVar7;
    local_18 = puVar5;
    do {
      uVar11 = uVar11 + 1;
      FUN_d900b844(param_1,param_1,param_4,lVar10,local_70);
    } while (uVar11 < uVar9);
    FUN_d900b844(param_1,auStack_ca0 + uVar12 * 0x18,param_4,lVar10,local_70);
    uVar12 = 0;
    uVar11 = 0;
LAB_d900cd60:
    uVar3 = 1;
    uVar7 = local_10;
    puVar5 = local_18;
  }
  goto LAB_d900cc8c;
}



int FUN_d900ce90(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  int iVar1;
  ulong uVar2;
  ulong uVar3;
  undefined8 uVar4;
  undefined4 *puVar5;
  undefined4 local_60 [2];
  undefined8 local_58;
  undefined8 local_50;
  undefined4 local_48 [2];
  undefined8 local_40;
  undefined8 local_38;
  undefined4 local_30 [2];
  undefined8 local_28;
  undefined8 local_20;
  undefined8 local_10;
  
  local_30[0] = 1;
  local_28 = 0;
  local_20 = 0;
  local_48[0] = 1;
  local_40 = 0;
  local_38 = 0;
  local_60[0] = 1;
  local_58 = 0;
  local_50 = 0;
  local_10 = param_3;
  iVar1 = FUN_d900b170(local_48);
  if (iVar1 == 0) {
    iVar1 = FUN_d900b170(local_60,local_10);
    if (iVar1 == 0) {
      uVar2 = FUN_d900b38c(local_48);
      uVar3 = FUN_d900b38c(local_60);
      if (uVar2 < uVar3) {
        uVar3 = uVar2;
      }
      iVar1 = FUN_d900b6b0(local_48,uVar3);
      if (iVar1 == 0) {
        iVar1 = FUN_d900b6b0(local_60,uVar3);
        if (iVar1 == 0) {
          local_60[0] = 1;
          local_48[0] = 1;
          do {
            iVar1 = FUN_d900ba68(local_48,0);
            if (iVar1 == 0) {
              iVar1 = FUN_d900b598(local_60,uVar3);
              if (iVar1 == 0) {
                iVar1 = FUN_d900b170(param_1,local_60);
              }
              break;
            }
            uVar4 = FUN_d900b38c(local_48);
            iVar1 = FUN_d900b6b0(local_48,uVar4);
            if (iVar1 != 0) break;
            uVar4 = FUN_d900b38c(local_60);
            iVar1 = FUN_d900b6b0(local_60,uVar4);
            if (iVar1 != 0) break;
            iVar1 = FUN_d900b974(local_48,local_60);
            if (iVar1 < 0) {
              iVar1 = FUN_d900bbf0(local_60,local_60,local_48);
              if (iVar1 != 0) break;
              puVar5 = local_60;
            }
            else {
              iVar1 = FUN_d900bbf0(local_48,local_48,local_60);
              if (iVar1 != 0) break;
              puVar5 = local_48;
            }
            iVar1 = FUN_d900b6b0(puVar5,1);
          } while (iVar1 == 0);
        }
      }
    }
  }
  FUN_d900b074(local_30);
  FUN_d900b074(local_48);
  FUN_d900b074(local_60);
  return iVar1;
}



void FUN_d900d03c(long param_1,long param_2,code *param_3,undefined8 param_4)

{
  int iVar1;
  
  iVar1 = FUN_d900b0c4(param_1,param_2 + 7U >> 3);
  if ((iVar1 == 0) && (iVar1 = FUN_d900b248(param_1,0), iVar1 == 0)) {
    (*param_3)(param_4,*(undefined8 *)(param_1 + 0x10),param_2);
  }
  return;
}



int FUN_d900d09c(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  int iVar1;
  int iVar2;
  undefined4 *puVar3;
  undefined4 *puVar4;
  undefined4 local_e0 [2];
  undefined8 local_d8;
  ulong *local_d0;
  undefined4 local_c8 [2];
  undefined8 local_c0;
  ulong *local_b8;
  undefined4 local_b0 [2];
  undefined8 local_a8;
  ulong *local_a0;
  undefined4 local_98 [2];
  undefined8 local_90;
  undefined8 local_88;
  undefined4 local_80 [2];
  undefined8 local_78;
  ulong *local_70;
  undefined4 local_68 [2];
  undefined8 local_60;
  ulong *local_58;
  undefined4 local_50 [2];
  undefined8 local_48;
  ulong *local_40;
  undefined4 local_38 [2];
  undefined8 local_30;
  undefined8 local_28;
  undefined4 local_20 [2];
  undefined8 local_18;
  undefined8 local_10;
  
  iVar1 = FUN_d900ba68(param_3,0);
  if (iVar1 < 1) {
    return -4;
  }
  local_38[0] = 1;
  local_30 = 0;
  local_28 = 0;
  local_50[0] = 1;
  local_48 = 0;
  local_40 = (ulong *)0x0;
  local_68[0] = 1;
  local_60 = 0;
  local_58 = (ulong *)0x0;
  local_80[0] = 1;
  local_78 = 0;
  local_70 = (ulong *)0x0;
  local_20[0] = 1;
  local_18 = 0;
  local_10 = 0;
  local_98[0] = 1;
  local_90 = 0;
  local_88 = 0;
  local_b0[0] = 1;
  local_a8 = 0;
  local_a0 = (ulong *)0x0;
  local_c8[0] = 1;
  local_c0 = 0;
  local_b8 = (ulong *)0x0;
  local_e0[0] = 1;
  local_d8 = 0;
  local_d0 = (ulong *)0x0;
  iVar1 = FUN_d900ce90(local_20,param_2,param_3);
  if (iVar1 == 0) {
    iVar2 = FUN_d900ba68(local_20,1);
    iVar1 = -0xe;
    if (((((iVar2 == 0) && (iVar1 = FUN_d900c778(local_38,param_2,param_3), iVar1 == 0)) &&
         (iVar1 = FUN_d900b170(local_50,local_38), iVar1 == 0)) &&
        ((iVar1 = FUN_d900b170(local_98,param_3), iVar1 == 0 &&
         (iVar1 = FUN_d900b170(local_b0,param_3), iVar1 == 0)))) &&
       ((iVar1 = FUN_d900b248(local_68,1), iVar1 == 0 &&
        ((iVar1 = FUN_d900b248(local_80,0), iVar1 == 0 &&
         (iVar1 = FUN_d900b248(local_c8,0), iVar1 == 0)))))) {
      iVar1 = FUN_d900b248(local_e0,1);
      while (iVar1 == 0) {
        while ((*local_40 & 1) != 0) {
          while ((*local_a0 & 1) == 0) {
            iVar1 = FUN_d900b6b0(local_b0,1);
            if ((iVar1 != 0) ||
               (((((*local_b8 & 1) != 0 || ((*local_d0 & 1) != 0)) &&
                 ((iVar1 = FUN_d900bcd0(local_c8,local_c8,local_98), iVar1 != 0 ||
                  (iVar1 = FUN_d900bd58(local_e0,local_e0,local_38), iVar1 != 0)))) ||
                ((iVar1 = FUN_d900b6b0(local_c8,1), iVar1 != 0 ||
                 (iVar1 = FUN_d900b6b0(local_e0,1), iVar1 != 0)))))) goto LAB_d900d444;
          }
          iVar1 = FUN_d900b974(local_50,local_b0);
          if (iVar1 < 0) {
            iVar1 = FUN_d900bd58(local_b0,local_b0,local_50);
            if ((iVar1 != 0) || (iVar1 = FUN_d900bd58(local_c8,local_c8,local_68), iVar1 != 0))
            goto LAB_d900d444;
            puVar3 = local_e0;
            puVar4 = local_80;
          }
          else {
            iVar1 = FUN_d900bd58(local_50,local_50,local_b0);
            if ((iVar1 != 0) || (iVar1 = FUN_d900bd58(local_68,local_68,local_c8), iVar1 != 0))
            goto LAB_d900d444;
            puVar3 = local_80;
            puVar4 = local_e0;
          }
          iVar1 = FUN_d900bd58(puVar3,puVar3,puVar4);
          if (iVar1 != 0) goto LAB_d900d444;
          iVar1 = FUN_d900ba68(local_50,0);
          if (iVar1 == 0) goto LAB_d900d3f8;
        }
        iVar1 = FUN_d900b6b0(local_50,1);
        if (((iVar1 != 0) ||
            ((((*local_58 & 1) != 0 || ((*local_70 & 1) != 0)) &&
             ((iVar1 = FUN_d900bcd0(local_68,local_68,local_98), iVar1 != 0 ||
              (iVar1 = FUN_d900bd58(local_80,local_80,local_38), iVar1 != 0)))))) ||
           (iVar1 = FUN_d900b6b0(local_68,1), iVar1 != 0)) break;
        iVar1 = FUN_d900b6b0(local_80,1);
      }
    }
  }
  goto LAB_d900d444;
  while (iVar1 = FUN_d900bcd0(local_c8,local_c8,param_3), iVar1 == 0) {
LAB_d900d3f8:
    iVar1 = FUN_d900ba68(local_c8,0);
    if (-1 < iVar1) goto LAB_d900d424;
  }
  goto LAB_d900d444;
  while (iVar1 = FUN_d900bd58(local_c8,local_c8,param_3), iVar1 == 0) {
LAB_d900d424:
    iVar1 = FUN_d900b974(local_c8,param_3);
    if (iVar1 < 0) {
      iVar1 = FUN_d900b170(param_1,local_c8);
      break;
    }
  }
LAB_d900d444:
  FUN_d900b074(local_38);
  FUN_d900b074(local_50);
  FUN_d900b074(local_68);
  FUN_d900b074(local_80);
  FUN_d900b074(local_20);
  FUN_d900b074(local_98);
  FUN_d900b074(local_b0);
  FUN_d900b074(local_c8);
  FUN_d900b074(local_e0);
  return iVar1;
}



undefined8 FUN_d900d4b4(uint param_1,undefined4 param_2)

{
  undefined8 uVar1;
  
  if (param_1 < 0x41) {
    *(undefined4 *)(&DAT_d9013e00 + param_1) = param_2;
    uVar1 = 0;
  }
  else {
    uVar1 = 0xffffffff;
  }
  return uVar1;
}



undefined8 FUN_d900d4e8(uint param_1,undefined8 param_2,int param_3)

{
  undefined8 uVar1;
  
  if ((param_1 < 0xc1) && (param_1 + param_3 < 0xc1)) {
    FUN_d9004b9c(param_2,&DAT_d9013e40,param_3);
    uVar1 = 0;
  }
  else {
    uVar1 = 0xffffffff;
  }
  return uVar1;
}



long FUN_d900d540(ulong param_1)

{
  long lVar1;
  ulong *puVar2;
  ulong uVar3;
  ulong uVar4;
  ulong uVar5;
  ulong uVar6;
  
                    /* What is this?! */
  puVar2 = (ulong *)(param_1 & 0xfffffffffffffff0);
  uVar5 = param_1 & 0xf;
  if (uVar5 == 0) goto LAB_d900d550;
  uVar6 = 0xffffffffffffffff >> (uVar5 * -8 & 0x3f);
  uVar3 = *puVar2 | uVar6;
  uVar4 = puVar2[1];
  if (8 < uVar5) {
    uVar3 = 0xffffffffffffffff;
    uVar4 = puVar2[1] | uVar6;
  }
  while( true ) {
    puVar2 = puVar2 + 2;
    uVar3 = uVar3 + 0xfefefefefefefeff & ((uVar3 | 0x7f7f7f7f7f7f7f7f) ^ 0xffffffffffffffff);
    uVar5 = uVar4 + 0xfefefefefefefeff & ((uVar4 | 0x7f7f7f7f7f7f7f7f) ^ 0xffffffffffffffff);
    if (uVar5 != 0 || uVar3 != 0) break;
LAB_d900d550:
    uVar3 = *puVar2;
    uVar4 = puVar2[1];
  }
  lVar1 = (long)puVar2 - param_1;
  if (uVar3 != 0) {
    lVar1 = lVar1 + -8;
    uVar5 = uVar3;
  }
  uVar5 = (uVar5 & 0xff00ff00ff00ff00) >> 8 | (uVar5 & 0xff00ff00ff00ff) << 8;
  uVar5 = (uVar5 & 0xffff0000ffff0000) >> 0x10 | (uVar5 & 0xffff0000ffff) << 0x10;
  uVar5 = uVar5 >> 0x20 | uVar5 << 0x20;
  uVar5 = uVar5 | uVar5 >> 1;
  uVar5 = uVar5 | uVar5 >> 2;
  uVar5 = uVar5 | uVar5 >> 4;
  uVar5 = uVar5 | uVar5 >> 8;
  uVar5 = uVar5 | uVar5 >> 0x10;
  uVar5 = uVar5 | uVar5 >> 0x20;
  uVar5 = ((uVar5 & 0xaaaaaaaaaaaaaaaa) >> 1) + (uVar5 & 0x5555555555555555);
  uVar5 = ((uVar5 & 0xcccccccccccccccc) >> 2) + (uVar5 & 0x3333333333333333);
  uVar5 = ((uVar5 & 0xf0f0f0f0f0f0f0f0) >> 4) + (uVar5 & 0xf0f0f0f0f0f0f0f);
  uVar5 = ((uVar5 & 0xff00ff00ff00ff00) >> 8) + (uVar5 & 0xff00ff00ff00ff);
  uVar5 = ((uVar5 & 0xffff0000ffff0000) >> 0x10) + (uVar5 & 0xffff0000ffff);
  return lVar1 + -8 + (0x40 - ((uVar5 >> 0x20) + (uVar5 & 0xffffffff)) >> 3);
}


